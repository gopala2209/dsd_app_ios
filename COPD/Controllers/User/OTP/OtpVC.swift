//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class OtpVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var verifyOTPButton: UIButton!
    @IBOutlet weak var otpView: UIView!
    
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        otpTextField.delegate = self
        otpTextField.clearButtonMode = .whileEditing
        
        otpTextField.keyboardType = .asciiCapable
        
        ServiceModel().disableButton(button: verifyOTPButton)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    // MARK: - IBActions
    @IBAction func verifyOTPAction(_ sender: UIButton) {
        
        // after verifying OTP
        // navigate to forget password screen
        let forgetPasswordVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "forgetpasswordvc") as! ForgetPasswordVC
        self.navigationController?.pushViewController(forgetPasswordVC, animated: true)

    }
    
    @IBAction func viewTapAction(_ sender: UITapGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        
        otpTextField.resignFirstResponder()
    }
    
        
    @IBAction func backAction(_ sender: UIButton) {
        
        // dismiss screen
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resendAgainAction(_ sender: UIButton) {
        
        // alert popup
        ServiceModel().showAlert(title: "Success", message: "OTP has been sent to your phone!", parentView: self)
    }
    
}


extension OtpVC: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        var text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        text = text.trimmingCharacters(in: .whitespaces)
        
            if !text.isEmpty{
                ServiceModel().enableButton(button: verifyOTPButton)
            } else {
                ServiceModel().disableButton(button: verifyOTPButton)
            }
        
            return range.location < 50
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        otpView.layer.borderWidth = 2.0
        otpView.layer.borderColor = UIColor.borderBlueTheme.cgColor // your color
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        otpView.layer.borderWidth = 0 // default width
    }
    
}
