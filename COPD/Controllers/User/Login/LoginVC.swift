//
//  LoginVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class LoginVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var btnUserType: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        userNameTextField.delegate = self
        passwordTextField.delegate = self
        
        userNameTextField.keyboardType = .asciiCapable
        passwordTextField.keyboardType = .asciiCapable
        
        userNameTextField.clearButtonMode = .whileEditing
        passwordTextField.clearButtonMode = .whileEditing
                
        ServiceModel().disableButton(button: btnLogin)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ServiceModel().disableButton(button: btnLogin)
        passwordTextField.text = ""
    }
    
    
    func getEnteredEmailId() throws -> String {
        
        guard let phoneNumber = userNameTextField.text,
            phoneNumber != "" else {
                throw AppError.LoginScreenError.usernameFieldEmpty
        }

        
        return phoneNumber
    }
    
    func getEnteredPassword() throws -> String {
        
        guard let password = passwordTextField.text,
              password != "" else {
            throw AppError.LoginScreenError.passwordFieldEmpty
        }
        
        return password
    }
    
    func getSelectedUsertype() throws -> String {
        
        guard let usertype = btnUserType.title(for: .normal),
              usertype != "Select User Type" else {
            throw AppError.LoginScreenError.usertypeFieldEmpty
        }
        
        return usertype
    }
    
    
    // MARK: - IBActions
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                let username = try getEnteredEmailId()
                let password = try getEnteredPassword()
                
                let encryptedPassword = try NGUserDefaults().encryptMessage(message: password)
                
                let completeEncryptedPass = ("3A14BDEE9F4D47380C33A7B8B9BBB2C4" + "FCE950E055544D85D3005E874E3BB4EF" + encryptedPassword)
                                
                print("API Request -> Username : \(username), Encrypted Password : \(completeEncryptedPass)")
                
                self.postloginAPI(username:username, password:completeEncryptedPass, usertype:"")
                                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
        
    }
    
    func postloginAPI(username: String, password: String, usertype: String) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Signing in ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.login(username: username, password: password, usertype: usertype) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                                return
                            }
                                                        
                            let status = responseStr?.status
                            
                            if(status == 1){
                                                                
                                let jsonEncoder = JSONEncoder()
                                let jsonData = try! jsonEncoder.encode(responseStr)
                                let json = String(data: jsonData, encoding: String.Encoding.utf8)
                                
                                var roleUserType = ""
                               
                                if let roleType = responseStr?.roleType{
                                    
                                    switch roleType {
                                    case 6:
                                        roleUserType = "DCPU"
                                    case 7:
                                        roleUserType = "DCPU"
                                    case 10:
                                        roleUserType = "PO"
                                    case 14:
                                        roleUserType = "JJB"
                                    case 17:
                                        roleUserType = "CWC"
                                    default:
                                        roleUserType = "UNKNOWN"
                                    }
                                        
                                }
                                
                                // save into userdefaults
                                UserDefaults.standard.set(username, forKey: "Username")
                                UserDefaults.standard.set(responseStr?.userID, forKey: "Userid")
                                UserDefaults.standard.set(roleUserType, forKey: "Usertype")
                                UserDefaults.standard.set(responseStr?.accessToken, forKey: "Authentcation_Token")
                                
                                UserDefaults.standard.set(true, forKey: "isFirstLaunch")
                                
                                UserDefaults.standard.set(jsonData, forKey: "UserLoginResponseData")
                                UserDefaults.standard.synchronize()
                                        
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // navigate to home screen
                                self.navigateToHomeVC()
                                
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Login Failed", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                 
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
        
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        
        // navigate to forget password screen
        let forgetPasswordVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "forgetpasswordvc") as! ForgetPasswordVC
        self.navigationController?.pushViewController(forgetPasswordVC, animated: true)
                
    }
    
    @IBAction func selectUserTypeAction(_ sender: UIButton) {
        
        // navigate to select user type screen
        let selectUserTypeVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "selectusertypeviewcontroller") as! SelectUserTypeViewController
        selectUserTypeVC.modalPresentationStyle = .overCurrentContext
        selectUserTypeVC.delegate = self
        self.userNameTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        
        self.navigationController?.present(selectUserTypeVC, animated: false, completion: nil)
    }
    
    @IBAction func viewTapAction(_ sender: UITapGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        
        userNameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
}



extension LoginVC: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        var text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        text = text.trimmingCharacters(in: .whitespaces)
        
        if textField == userNameTextField{
            
            if text.isEmpty == false && passwordTextField.text?.isEmpty == false{
                ServiceModel().enableButton(button: btnLogin)
            }else{
                ServiceModel().disableButton(button: btnLogin)
            }
            
            return range.location < 50
        }
        else{
            if userNameTextField.text?.isEmpty == false && text.isEmpty == false{
                ServiceModel().enableButton(button: btnLogin)
            }else{
                ServiceModel().disableButton(button: btnLogin)
            }
            return range.location < 50
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == userNameTextField {
            passwordTextField.becomeFirstResponder()
        }else if textField == passwordTextField {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == userNameTextField {
            usernameView.layer.borderWidth = 2
            usernameView.layer.borderColor = UIColor.borderBlueTheme.cgColor // your color
        }
        else if textField == passwordTextField {
            passwordView.layer.borderWidth = 2
            passwordView.layer.borderColor = UIColor.borderBlueTheme.cgColor // your color
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == userNameTextField {
            usernameView.layer.borderWidth = 0 // default width
        }
        else if textField == passwordTextField {
            passwordView.layer.borderWidth = 0 // default width
        }
    }
    
}


extension LoginVC: UserTypeDelegate{
    func selectUser(type: String) {
        
        // set user type
        self.btnUserType.setTitle(type, for: .normal)
        
    }
}
