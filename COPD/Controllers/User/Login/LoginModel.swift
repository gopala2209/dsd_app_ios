//
//  LoginModel.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let lastName, timezone, mobile, message: String?
    let roleType, userID: Int?
    let newUserIdForForgetPassword: Int?
    let accessToken, firstName, refreshToken: String?
    let validUpto, loginTime: Int?
    let email: String?
    let status: Int?

    enum CodingKeys: String, CodingKey {
        case lastName, timezone, mobile, message, roleType
        case userID = "userId"
        case newUserIdForForgetPassword = "user_id"
        case accessToken = "access_token"
        case firstName
        case refreshToken = "refresh_token"
        case validUpto = "valid_upto"
        case loginTime, email, status
    }
}
