//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class ForgetPasswordVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var sendRequestButton: UIButton!
    @IBOutlet weak var passwordView: UIView!
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        usernameTextField.delegate = self
        usernameTextField.clearButtonMode = .whileEditing
        
        usernameTextField.keyboardType = .asciiCapable
        
        ServiceModel().disableButton(button: sendRequestButton)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func getEnteredUsername() throws -> String {
        
        guard let phoneNumber = usernameTextField.text,
            phoneNumber != "" else {
                throw AppError.LoginScreenError.usernameFieldEmpty
        }

        
        return phoneNumber
    }
    
    // MARK: - IBActions
    @IBAction func sendRequestAction(_ sender: UIButton) {
        
        usernameTextField.resignFirstResponder()
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                let username = try getEnteredUsername()
                   
                // api call
                self.postForgetPasswordAPI(username:username)
                                
            } catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
                
    }
    
    @IBAction func viewTapAction(_ sender: UITapGestureRecognizer) {
        
        sender.cancelsTouchesInView = false
        
        usernameTextField.resignFirstResponder()
    }
    
    
    // MARK: - IBActions
    
    @IBAction func backAction(_ sender: UIButton) {
        
        // dismiss screen
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension ForgetPasswordVC: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        var text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        text = text.trimmingCharacters(in: .whitespaces)
        
            if !text.isEmpty{
                ServiceModel().enableButton(button: sendRequestButton)
            } else {
                ServiceModel().disableButton(button: sendRequestButton)
            }
        
            return range.location < 50
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        passwordView.layer.borderWidth = 2.0
        passwordView.layer.borderColor = UIColor.borderBlueTheme.cgColor // your color
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        passwordView.layer.borderWidth = 0 // default width
    }
    
}


extension ForgetPasswordVC{
    
    func postForgetPasswordAPI(username: String) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Sending Request ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.forgetPassword(username: username) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                let alert = UIAlertController(title: "Success",
                                                              message: responseStr?.message,
                                                              preferredStyle: .alert)
                                
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                                                
                                    alert.dismiss(animated: true, completion: {
                                        
                                        // navigate to verify otp screen
                                        let verifyOTPVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "verifyotpvc") as! VerifyOTPVC
                                        verifyOTPVC.userId = responseStr?.newUserIdForForgetPassword ?? 0
                                        self.navigationController?.pushViewController(verifyOTPVC, animated: true)
                                    })
                                })
                                
                                alert.addAction(okAction)
                                
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Request Failed!", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
}
