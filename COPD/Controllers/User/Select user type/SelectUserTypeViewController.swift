//
//  SelectGenderViewController.swift
//  COPD
//
//  Created by Anand Prakash on 14/02/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

protocol UserTypeDelegate {
    func selectUser(type: String)
}

class SelectUserTypeViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var popupView: UIView!
    
    // MARK: - Properties
    var delegate: UserTypeDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
    }
    
    override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
        
        popupView.roundCorners(corners: [.topLeft, .topRight], radius: 20.0)
    }
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func selectUserBtnClicked(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectUser(type: "CWC")
            })
            break
        case 1:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectUser(type: "JJB")
            })
            break
        case 2:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectUser(type: "DCPU")
            })
            break
        case 3:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectUser(type: "PO")
            })
            break

        default:
            break
        }
    }
}
