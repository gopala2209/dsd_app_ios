//
//  SelectTransferReportVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/06/21.
//  Copyright © 2021 Anand Prakash. All rights reserved.
//

import UIKit

protocol TransferReportDelegate {
    func selectTransferReport(option: String, forSelectedReport: String)
}

class SelectTransferReportVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var newReport: UIButton!
    @IBOutlet weak var inProgress: UIButton!
    @IBOutlet weak var completed: UIButton!
    
    @IBOutlet weak var popupView: UIView!
    
    // MARK: - Properties
    var delegate: TransferReportDelegate?
    var forSelectedReport: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = "Select \(forSelectedReport ?? "") Report"
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
    }
    
    override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
        
        popupView.roundCorners(corners: [.topLeft, .topRight], radius: 20.0)
    }
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func selectBtnClicked(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectTransferReport(option: "New Report", forSelectedReport: self.forSelectedReport ?? "")
            })
            break
        case 1:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectTransferReport(option: "In-Progress", forSelectedReport: self.forSelectedReport ?? "")
            })
            break
        case 2:
            self.dismiss(animated: false, completion: {
                self.delegate?.selectTransferReport(option: "Completed", forSelectedReport: self.forSelectedReport ?? "")
            })
            break
        default:
            break
        }
    }
}
