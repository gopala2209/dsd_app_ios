//
//  SingleTextBoxPoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 26/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

protocol AnswerDataDelegate {
    func optionSelected(id: String, value: String)
}

class SingleTextBoxPoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var textfieldAnswer: UITextField!
        
    
    // MARK: - Properties
    var delegate: AnswerDataDelegate?
    var id = ""
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // initialization code
        textfieldAnswer.delegate = self
        textfieldAnswer.keyboardType = .asciiCapable
        textfieldAnswer.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
    }
    
}


extension SingleTextBoxPoTableViewCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        self.delegate?.optionSelected(id: self.id, value: textField.text ?? "")

        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        self.delegate?.optionSelected(id: self.id, value: textField.text ?? "")
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
                
        if self.lblQuestionTitle.text?.lowercased().contains("aadhar") == true{
            
            return range.location < 12 && allowedCharacters.isSuperset(of: characterSet)
        }
        
        if self.lblQuestionTitle.text?.lowercased().contains("pincode") == true{
            
            return range.location < 6 && allowedCharacters.isSuperset(of: characterSet)
        }
        
        if self.lblQuestionTitle.text?.lowercased() == "contact no" || self.lblQuestionTitle.text?.lowercased().contains("contact number") == true{
            
            return range.location < 10 && allowedCharacters.isSuperset(of: characterSet)
        }
        
        if self.lblQuestionTitle.text?.lowercased() == "income"{
            
            return range.location < 20 && allowedCharacters.isSuperset(of: characterSet)
        }
        
        if self.lblQuestionTitle.text?.lowercased() == "any other remarks"{
            return range.location < 250
        }
                
        
        return true
    }
    
}
