//
//  DatepickerPOTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 26/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class DatepickerPOTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var dateAnswerLbl: UILabel!
    @IBOutlet weak var touchView: UIView!
    
    @IBOutlet weak var dateAnswerLbl2: UILabel!
    @IBOutlet weak var touchView2: UIView!
    
    //MARK: - Constraints
    
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var height: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate: AnswerDataDelegate?
    var id = ""
    var fieldId1 = ""
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createTouchForView()
        
    }
    
    func createTouchForView(){
        let yearTap = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectYearTap(_:)))
        touchView.addGestureRecognizer(yearTap)
        
        let yearTap1 = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectYearTap2(_:)))
        touchView2.addGestureRecognizer(yearTap1)
    }
    
    func showSingleDateField(){
        dateAnswerLbl.text = "Select Date"
        top.constant = 0
        height.constant = 0
        dateAnswerLbl2.isHidden = true
    }
    
    func showDoubleDateField(){
        dateAnswerLbl.text = "Progress From Date"
        dateAnswerLbl2.text = "Progress To Date"
        top.constant = 10
        height.constant = 45
        dateAnswerLbl2.isHidden = false
    }
    

    @objc func displaySelectYearTap(_ sender: UITapGestureRecognizer? = nil) {
        self.delegate?.optionSelected(id: self.id, value: self.dateAnswerLbl.text ?? "")
    }
    
    @objc func displaySelectYearTap2(_ sender: UITapGestureRecognizer? = nil) {
        self.delegate?.optionSelected(id: self.fieldId1, value: self.dateAnswerLbl2.text ?? "")
    }

}
