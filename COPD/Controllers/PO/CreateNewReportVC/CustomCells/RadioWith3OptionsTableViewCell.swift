//
//  RadioWith3OptionsTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 26/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class RadioWith3OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestionTitle: UILabel!
    
    @IBOutlet weak var option1image: UIImageView!
    @IBOutlet weak var option1Lbl: UILabel!
    @IBOutlet weak var option1View: UIView!
    
    @IBOutlet weak var option2Image: UIImageView!
    @IBOutlet weak var option2Lbl: UILabel!
    @IBOutlet weak var option2View: UIView!
    
    @IBOutlet weak var option3image: UIImageView!
    @IBOutlet weak var option3Lbl: UILabel!
    @IBOutlet weak var option3View: UIView!
    
    // MARK: - Properties
    var delegate: AnswerDataDelegate?
    var id = ""
    var optionSelected = ""
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if optionSelected == option1Lbl.text || optionSelected == "1"{
            selectView(imageView: option1image)
            deselectView(imageView: option2Image)
            deselectView(imageView: option3image)
        }else if optionSelected == option2Lbl.text || optionSelected == "2"{
            deselectView(imageView: option1image)
            selectView(imageView: option2Image)
            deselectView(imageView: option3image)
        }else{
            deselectView(imageView: option1image)
            deselectView(imageView: option2Image)
            
            if optionSelected == option3Lbl.text || optionSelected == "3"{
                selectView(imageView: option3image)
            }
            else{
                deselectView(imageView: option3image)
            }
            
        }
        createTouchForView()
    }
    
    func createTouchForView(){
        let option1Tap = UITapGestureRecognizer(target: self, action: #selector(self.option1ViewTapped(_:)))
        option1View.addGestureRecognizer(option1Tap)
        
        let option2Tap = UITapGestureRecognizer(target: self, action: #selector(self.option2ViewTapped(_:)))
        option2View.addGestureRecognizer(option2Tap)
        
        let option3Tap = UITapGestureRecognizer(target: self, action: #selector(self.option3ViewTapped(_:)))
        option3View.addGestureRecognizer(option3Tap)
    }
    
    func selectView(imageView: UIImageView){
        imageView.image = UIImage(named: "radio_button_checked")
    }
    
    func deselectView(imageView: UIImageView){
        imageView.image = UIImage(named: "radio_button_unchecked")
    }
    
    @objc func option1ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        selectView(imageView: option1image)
        deselectView(imageView: option2Image)
        deselectView(imageView: option3image)
        self.delegate?.optionSelected(id: self.id, value: self.option1Lbl.text ?? "")
    }
    
    @objc func option2ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        deselectView(imageView: option1image)
        selectView(imageView: option2Image)
        deselectView(imageView: option3image)
        self.delegate?.optionSelected(id: self.id, value: self.option2Lbl.text ?? "")
    }
    
    @objc func option3ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        deselectView(imageView: option1image)
        deselectView(imageView: option2Image)
        selectView(imageView: option3image)
        self.delegate?.optionSelected(id: self.id, value: self.option3Lbl.text ?? "")
    }
    
}
