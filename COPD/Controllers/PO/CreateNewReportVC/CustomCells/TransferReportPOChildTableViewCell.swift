//
//  TransferReportPOChildTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 18/10/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit
import DropDown

class TransferReportPOChildTableViewCell: UITableViewCell {

    
    @IBOutlet weak var option1image: UIImageView!
    @IBOutlet weak var option1View: UIView!
   
    
    @IBOutlet weak var option2Image: UIImageView!
    @IBOutlet weak var option2View: UIView!
    
    @IBOutlet weak var option3Lbl: UILabel!
    @IBOutlet weak var option3View: UIView!
    
    var delegate: AnswerDataDelegate?
    var optionsValue: [String] = []
    var dropDown = DropDown()

    
    @IBOutlet weak var cwcTop: NSLayoutConstraint! //15
    @IBOutlet weak var cwcDistrictHeight: NSLayoutConstraint! //20
    @IBOutlet weak var option3Top: NSLayoutConstraint! // 15
    @IBOutlet weak var option3Height: NSLayoutConstraint! //45
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createTouchForView()
    }
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    func createTouchForView(){
        let option1Tap = UITapGestureRecognizer(target: self, action: #selector(self.option1ViewTapped(_:)))
        option1View.addGestureRecognizer(option1Tap)
        
        let option2Tap = UITapGestureRecognizer(target: self, action: #selector(self.option2ViewTapped(_:)))
        option2View.addGestureRecognizer(option2Tap)
        
        let option3Tap = UITapGestureRecognizer(target: self, action: #selector(self.option3ViewTapped(_:)))
        option3View.addGestureRecognizer(option3Tap)
        hideView()
    }
    
    func showView(){
        cwcTop.constant = 15
        cwcDistrictHeight.constant = 20
        option3Top.constant = 15
        option3Height.constant = 45
        option3View.isHidden = false
    }
    
    func hideView(){
        cwcTop.constant = 0
        cwcDistrictHeight.constant = 0
        option3Top.constant = 0
        option3Height.constant = 0
        option3View.isHidden = true
    }
    
    func selectView(imageView: UIImageView){
        imageView.image = UIImage(named: "radio_button_checked")
    }
    
    func deselectView(imageView: UIImageView){
        imageView.image = UIImage(named: "radio_button_unchecked")
    }
    
    @objc func option1ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        selectView(imageView: option1image)
        deselectView(imageView: option2Image)
        self.delegate?.optionSelected(id: "transfer", value: "1")
    }
    
    @objc func option2ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        deselectView(imageView: option1image)
        selectView(imageView: option2Image)
        self.delegate?.optionSelected(id: "transfer", value: "0")
    }
    
    @objc func option3ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        selectQuarterDropDown()
    }
        
    func selectQuarterDropDown(){
        DispatchQueue.main.async {
            
            
            self.dropDown.dataSource = self.optionsValue
            self.dropDown.anchorView = self.option3View
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.option3Lbl.text = item
                self.delegate?.optionSelected(id: "transfercwc", value: "\(index)")
            }
            
            self.dropDown.show()
            
        }
    }
    
}
