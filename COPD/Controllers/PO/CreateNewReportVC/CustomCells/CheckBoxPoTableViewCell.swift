//
//  CheckBoxPoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 26/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CheckBoxPoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestionTitle: UILabel!
    
    @IBOutlet weak var option1image: UIImageView!
    @IBOutlet weak var option1View: UIView!
    
    
    // MARK: - Properties
    var delegate: AnswerDataDelegate?
    var id = ""
    
    var isSelect = ""{
        didSet{
            if self.isSelect == "true"{
                self.selectView(imageView: option1image)
            }else{
                self.deselectView(imageView: option1image)
            }
        }
    }
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if isSelect == "true"{
            selectView(imageView: option1image)
        }else{
            deselectView(imageView: option1image)
        }
        createTouchForView()
    }
    
    func createTouchForView(){
        let option1Tap = UITapGestureRecognizer(target: self, action: #selector(self.option1ViewTapped(_:)))
        option1View.addGestureRecognizer(option1Tap)
        
    }
    
    func selectView(imageView: UIImageView){
        imageView.image = UIImage(named: "select")
    }
    
    func deselectView(imageView: UIImageView){
        imageView.image = UIImage(named: "unselect")?.withTintColor(.darkGray)
    }
    
    @objc func option1ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        if self.isSelect == "true"{
            self.delegate?.optionSelected(id: self.id, value: "false")
        }else{
            self.delegate?.optionSelected(id: self.id, value: "true")
        }
    }
    
}
