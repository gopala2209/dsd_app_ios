//
//  DropDownPoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 26/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit
import DropDown

class DropDownPoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var dateAnswerLbl: UILabel!
    @IBOutlet weak var touchView: UIView!
    
    var dropDownOptions: [String]? = []
    var dropDown = DropDown()

    // MARK: - Properties
    var delegate: AnswerDataDelegate?
    var id = ""
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createTouchForView()
    }
    
    func createTouchForView(){
        let yearTap = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectYearTap(_:)))
        touchView.addGestureRecognizer(yearTap)
    }
    
    

    @objc func displaySelectYearTap(_ sender: UITapGestureRecognizer? = nil) {
        selectQuarterDropDown()
    }

    func selectQuarterDropDown(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropDownOptions ?? []
            self.dropDown.anchorView = self.touchView
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.dateAnswerLbl.text = item
                self.delegate?.optionSelected(id: self.id, value: self.dateAnswerLbl.text ?? "")
            }
            
            self.dropDown.show()
            
        }
    }
    
}
