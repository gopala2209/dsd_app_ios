//
//  RadioWith2OptionsTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 26/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit
import DropDown

class RadioWith2OptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var selectionViewAnser: UILabel!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var lblAnswer1: UITextField!
    @IBOutlet weak var lblQuestion1: UILabel!
    
    @IBOutlet weak var lblAnswer2: UITextField!
    @IBOutlet weak var lblQuestion2: UILabel!
    
    //MARK:- Properties
    @IBOutlet weak var bottomQuestion: NSLayoutConstraint!
    @IBOutlet weak var heightAnswer: NSLayoutConstraint!
    @IBOutlet weak var view1Height: NSLayoutConstraint!
    @IBOutlet weak var topQuestion2: NSLayoutConstraint!
    @IBOutlet weak var bottomQuestion2: NSLayoutConstraint!
    @IBOutlet weak var heightAnswer2: NSLayoutConstraint!
    @IBOutlet weak var view2BottomHeight: NSLayoutConstraint!
    @IBOutlet weak var view2height: NSLayoutConstraint!
    
    @IBOutlet weak var lblQuestionTitle: UILabel!
    
    @IBOutlet weak var option1image: UIImageView!
    @IBOutlet weak var option1Lbl: UILabel!
    @IBOutlet weak var option1View: UIView!
   
    
    @IBOutlet weak var option2Image: UIImageView!
    @IBOutlet weak var option2Lbl: UILabel!
    @IBOutlet weak var option2View: UIView!
    
    @IBOutlet weak var viewDate: UIView!
    
    @IBOutlet weak var viewTop: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewLbl: UILabel!
    
    // MARK: - Properties
    var delegate: AnswerDataDelegate?
    var id = ""
    var fieldId1 = ""
    var fieldId2 = ""
    var optionSelected = ""
    
    var dropDownOptions: [String]? = []
    var dropDown = DropDown()
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblAnswer1.delegate = self
        
        lblAnswer1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        lblAnswer2.delegate = self
        
        lblAnswer2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidBegin)
        createTouchForView()
    }
    
    func createTouchForView(){
        let option1Tap = UITapGestureRecognizer(target: self, action: #selector(self.option1ViewTapped(_:)))
        option1View.addGestureRecognizer(option1Tap)
        
        let option2Tap = UITapGestureRecognizer(target: self, action: #selector(self.option2ViewTapped(_:)))
        option2View.addGestureRecognizer(option2Tap)
        
        let option3Tap = UITapGestureRecognizer(target: self, action: #selector(self.option3ViewTapped(_:)))
        viewDate.addGestureRecognizer(option3Tap)
        
        let yearTap = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectYearTap(_:)))
        selectionView.addGestureRecognizer(yearTap)
        
    }
    

    @objc func displaySelectYearTap(_ sender: UITapGestureRecognizer? = nil) {
        selectQuarterDropDown()
    }

    func selectQuarterDropDown(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropDownOptions ?? []
            self.dropDown.anchorView = self.selectionView
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.selectionViewAnser.text = item
                self.delegate?.optionSelected(id: self.fieldId1, value: self.selectionViewAnser.text ?? "")
            }
            
            self.dropDown.show()
            
        }
    }
    
    
    
    func hideTextField(){
        lblAnswer1.isHidden = true
        selectionView.isHidden = false
    }
    
    func showTextField(){
        lblAnswer1.isHidden = false
        selectionView.isHidden = true
    }
    
    func showOneTextField(){
        bottomQuestion.constant = 10
        heightAnswer.constant = 35
        view1Height.constant = 1
        topQuestion2.constant = 0
        bottomQuestion2.constant = 0
        heightAnswer2.constant = 0
        view2BottomHeight.constant = 15
        view2height.constant = 0
        lblQuestion2.text = ""
        viewHeight.constant = 0
        viewTop.constant = 0
        viewLbl.isHidden = true
    }
    
    func showTwoTextField(){
        bottomQuestion.constant = 10
        heightAnswer.constant = 35
        view1Height.constant = 1
        topQuestion2.constant = 15
        bottomQuestion2.constant = 10
        heightAnswer2.constant = 35
        view2BottomHeight.constant = 15
        view2height.constant = 1
        viewHeight.constant = 35
        viewTop.constant = 10
        viewLbl.isHidden = false
    }
    
    func hideAllTextField(){
        bottomQuestion.constant = 0
        heightAnswer.constant = 0
        view1Height.constant = 0
        topQuestion2.constant = 0
        bottomQuestion2.constant = 0
        heightAnswer2.constant = 0
        view2BottomHeight.constant = 0
        view2height.constant = 0
        lblQuestion2.text = ""
        lblQuestion1.text = ""
        lblAnswer1.text = ""
        lblAnswer2.text = ""
        viewHeight.constant = 0
        viewTop.constant = 0
        viewLbl.isHidden = true
    }
    
    func selectView(imageView: UIImageView){
        imageView.image = UIImage(named: "radio_button_checked")
    }
    
    func deselectView(imageView: UIImageView){
        imageView.image = UIImage(named: "radio_button_unchecked")
    }
    
    @objc func option1ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        selectView(imageView: option1image)
        deselectView(imageView: option2Image)
        self.delegate?.optionSelected(id: self.id, value: self.option1Lbl.text ?? "")
    }
    
    @objc func option2ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        deselectView(imageView: option1image)
        selectView(imageView: option2Image)
        self.delegate?.optionSelected(id: self.id, value: self.option2Lbl.text ?? "")
    }
    
    @objc func option3ViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        self.delegate?.optionSelected(id: self.fieldId2, value: viewLbl.text ?? "")
    }
        
}

extension RadioWith2OptionsTableViewCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField == lblAnswer1{
            self.delegate?.optionSelected(id: self.fieldId1, value: textField.text ?? "")
        }else{
            self.delegate?.optionSelected(id: self.fieldId2, value: textField.text ?? "")
        }

        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == lblAnswer1{
            self.delegate?.optionSelected(id: self.fieldId1, value: textField.text ?? "")
        }else{
            self.delegate?.optionSelected(id: self.fieldId2, value: textField.text ?? "")
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        return true
    }
    
}
