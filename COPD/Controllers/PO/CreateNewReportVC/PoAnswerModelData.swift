//
//  PoAnswerModelData.swift
//  COPD
//
//  Created by Anand Praksh on 27/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import Foundation


// MARK: - Intake Update Api
struct IntakeUpdateApiModelData: Decodable {
    var id: Int = 0
    var firstName: String = ""
    var lastName: String = ""
    var aadharNumber: String = ""
    var gender: Int = 0
    var dob: String = ""
    var religionID: Int = 0
    var communityID: Int = 0
    var isAddressVerified: Int = 0
    var addressLine1: String  = ""
    var addressLine2: String  = ""
    var city: String  = ""
    var landmark: String  = ""
    var zipcode: String  = ""
    var talukID: Int = 0
    var districtID: Int = 0
    var stateID: Int = 0
    var countryID: Int = 0
    var personalIdentificationMark: String = ""
    var languageByChild: String = ""
    var sibling: String = ""
    var isGuardianAvailable: Bool = false
    var guardianType: Int = 0
    var guardianFirstName: String = ""
    var guardianLastName: String = ""
    var guardianDob: String = ""
    var guardianMobile: String = ""
    var guardianDesignation: String = ""
    var guardianAddressLine1: String = ""
    var guardianAddressLine2: String = ""
    var guardianLandmark: String = ""
    var guardianCity: String = ""
    var guardianTalukID: Int = 0
    var guardianDistrictID: Int = 0
    var guardianStateID: Int = 0
    var guardianCountryID: Int = 0
    var guardianZipcode: String = ""
    var interestedPersonDetails: String = ""
    var habits: String = ""
    var summary: String = ""
    var secondaryGuardianFirstName = ""
    var secondaryGuardianLastName = ""
    var transferSuggestions = ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = ["id":self.id as Any,
                                  "first_name":self.firstName as Any,
                                  "last_name":self.lastName as Any,
                                  "aadhar_number":self.aadharNumber as Any,
                                  "religion_id":self.religionID as Any,
                                  "gender":self.gender as Any,
                                  "dob":self.dob as Any,
                                  "community_id":self.communityID as Any,
                                  "is_address_verified":self.isAddressVerified as Any,
                                  "address_line1":self.addressLine1 as Any,
                                  "address_line2":self.addressLine2 as Any,
                                  "city":self.city as Any,
                                  "landmark":self.landmark as Any,
                                  "zipcode":self.zipcode as Any,
                                  "taluk_id":self.talukID as Any,
                                  "district_id":self.districtID as Any,
                                  "state_id":self.stateID as Any,
                                  "country_id":self.countryID as Any,
                                  "personal_identification_mark":self.personalIdentificationMark as Any,
                                  "language_by_child":self.languageByChild as Any,
                                  "sibling":self.sibling as Any,
                                  "is_guardian_available":self.isGuardianAvailable as Any,
                                  "guardian_type":self.guardianType as Any,
                                  "guardian_first_name":self.guardianFirstName as Any,
                                  "guardian_last_name":self.guardianLastName as Any,
                                  "guardian_dob":self.guardianDob as Any,
                                  "guardian_mobile":self.guardianMobile as Any,
                                  "guardian_designation":self.guardianDesignation as Any,
                                  "guardian_address_line1":self.guardianAddressLine1 as Any,
                                  "guardian_address_line2":self.guardianAddressLine2 as Any,
                                  "guardian_landmark":self.guardianLandmark as Any,
                                  "guardian_city":self.guardianCity as Any,
                                  "guardian_taluk_id":self.guardianTalukID as Any,
                                  "guardian_district_id":self.guardianDistrictID as Any,
                                  "guardian_state_id":self.guardianStateID as Any,
                                  "guardian_country_id":self.guardianCountryID as Any,
                                  "guardian_zipcode":self.guardianZipcode as Any,
                                  "interested_person_details":self.interestedPersonDetails as Any,
                                  "habits":self.habits as Any,
                                  "summary":self.summary as Any,
                                  "secondary_guardian_first_name":self.secondaryGuardianFirstName as Any,
                                  "secondary_guardian_last_name":self.secondaryGuardianLastName as Any,
                                  "transfer_suggestions":self.transferSuggestions as Any
                                  
        ]
        return dic
    }
    
}




//REHABILITATION MODULE

// MARK: - Rehabilitation
struct ICPRehabilitationAnswerModelData{
    var id = 0
    var type = 0
    var calendar_year = 0
    var month = 0
    var child_expectation = ""
    var health_nutrition = ""
    var emotion_psychological_support = ""
    var education_training = ""
    var creativity = ""
    var interpersonal_relationships = ""
    var self_care = ""
    var independent_living_skills = ""
    var significant_experiences = ""
    var religious_beliefs = ""
    var outcome_child_expectation = ""
    var outcome_health_nutrition = ""
    var outcome_emotion_psychological_support = ""
    var outcome_education_training = ""
    var outcome_creativity = ""
    var outcome_interpersonal_relationships = ""
    var outcome_self_care = ""
    var outcome_independent_living_skills = ""
    var outcome_significant_experiences = ""
    var outcome_religious_beliefs = ""
    var commitee_or_board = 1
    var supervision_completed_at = ""
    
    var supervision_result = ""
    var address_parent_guardian = ""
    

    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "type": self.type as Any,
            "calendar_year": self.calendar_year as Any,
            "month": self.month as Any,
            "child_expectation": self.child_expectation as Any,
            "health_nutrition": self.health_nutrition as Any,
            "emotion_psychological_support": self.emotion_psychological_support as Any,
            "education_training": self.education_training as Any,
            "creativity": self.creativity as Any,
            "interpersonal_relationships": self.interpersonal_relationships as Any,
            "self_care": self.self_care as Any,
            "independent_living_skills": self.independent_living_skills as Any,
            "significant_experiences": self.significant_experiences as Any,
            "religious_beliefs": self.religious_beliefs as Any,
            "outcome_child_expectation": self.outcome_child_expectation as Any,
            "outcome_health_nutrition": self.outcome_health_nutrition as Any,
            "outcome_emotion_psychological_support": self.outcome_emotion_psychological_support as Any,
            "outcome_education_training": self.outcome_education_training as Any,
            "outcome_creativity": self.outcome_creativity as Any,
            "outcome_interpersonal_relationships": self.outcome_interpersonal_relationships as Any,
            "outcome_self_care": self.outcome_self_care as Any,
            "outcome_independent_living_skills": self.outcome_independent_living_skills as Any,
            "outcome_significant_experiences": self.outcome_significant_experiences as Any,
            "outcome_religious_beliefs": self.outcome_religious_beliefs as Any,
            "commitee_or_board": self.commitee_or_board as Any,
            "supervision_completed_at": self.supervision_completed_at as Any,
            
            "supervision_result": self.supervision_result as Any,
            "address_parent_guardian": self.address_parent_guardian as Any
        ]
        return dic
    }
    
}


//PRE RELEASE MODULE

// MARK: - PreRelase
struct ICPPreReleaseAnswerModelData{
    var id = 0
    var icp_id = 0
    var type = 0
    var transfer_release = ""
    var child_placement = ""
    var training_skills = ""
    var release_date = ""
    var is_escort_required = 0
    var proof_type = 0
    var rehabilitation_plan = ""
    var probation_officer = ""
    var sponsorship_individual = ""
    var medical_examination = ""
    var other_information = ""
    var child_expectation = ""
    var health_needs = ""
    var emotional_needs = ""
    var educational_needs = ""
    var leisure = ""
    var relationships = ""
    var religious_beliefs = ""
    var skill_training = ""
    var living_skills = ""
    var experience = ""
    var document: [String: Any] =  [:]
    var non_governmental: [String: Any] =  [:]
    var sponsoring_agency: [String: Any] =  [:]

    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "icp_id": self.icp_id as Any,
            "type": self.type as Any,
            "transfer_release": self.transfer_release as Any,
            "child_placement": self.child_placement as Any,
            "training_skills": self.training_skills as Any,
            "release_date": self.release_date as Any,
            "is_escort_required": self.is_escort_required as Any,
            "proof_type": self.proof_type as Any,
            "rehabilitation_plan": self.rehabilitation_plan as Any,
            "probation_officer": self.probation_officer as Any,
            "sponsorship_individual": self.sponsorship_individual as Any,
            "medical_examination": self.medical_examination as Any,
            "other_information": self.other_information as Any,
            "child_expectation": self.child_expectation as Any,
            "health_needs": self.health_needs as Any,
            "emotional_needs": self.emotional_needs as Any,
            "educational_needs": self.educational_needs as Any,
            "leisure": self.leisure as Any,
            "relationships": self.relationships as Any,
            "religious_beliefs": self.religious_beliefs as Any,
            "skill_training": self.skill_training as Any,
            "living_skills": self.living_skills as Any,
            "experience": self.experience as Any,
            "document": self.document as Any,
            "non_governmental": self.non_governmental as Any,
            "sponsoring_agency": self.sponsoring_agency as Any
        ]
        return dic
    }
    
}

//POST RELEASE MODULE
struct ICPPostReleaseAnswerModelData{
    var id = 0
    var icp_id = 0
    var icp_type = 0
    var bank_status = 0
    var child_belongings = 0
    var other_child_belongings = ""
    var rehabilitation_restoration_plan = ""
    var family_behaviour = ""
    var neighbours_attitude = ""
    var skills_acquired = ""
    var admitted_school_vaction = 0
    var admitted_date = ""
    var admitted_school = ""
    var child_admitted = ""
    var child_opinion = ""
    var identity_compensation = ""
    var birth_certificate = 0
    var school_certificate = 0
    var bpl_card = 0
    var disability_certificate = 0
    var immunisation_card = 0
    var ration_card = 0
    var aadhar_card = 0
    var goverment_compensation = 0
    var document: [String: Any] =  [:]
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "icp_id": self.icp_id as Any,
            "icp_type": self.icp_type as Any,
            "bank_status": self.bank_status as Any,
            "child_belongings": self.child_belongings as Any,
            "other_child_belongings": self.other_child_belongings as Any,
            "rehabilitation_restoration_plan": self.rehabilitation_restoration_plan as Any,
            "family_behaviour": self.family_behaviour as Any,
            "neighbours_attitude": self.neighbours_attitude as Any,
            "skills_acquired": self.skills_acquired as Any,
            "admitted_school_vaction": self.admitted_school_vaction as Any,
            "admitted_date": self.admitted_date as Any,
            "admitted_school": self.admitted_school as Any,
            "child_admitted": self.child_admitted as Any,
            "child_opinion": self.child_opinion as Any,
            "identity_compensation": self.identity_compensation as Any,
            "birth_certificate": self.birth_certificate as Any,
            "school_certificate": self.school_certificate as Any,
            "bpl_card": self.bpl_card as Any,
            "disability_certificate": self.disability_certificate as Any,
            "immunisation_card": self.immunisation_card as Any,
            "ration_card": self.ration_card as Any,
            "aadhar_card": self.aadhar_card as Any,
            "goverment_compensation": self.goverment_compensation as Any,
            "document": self.document as Any
        ]
        return dic
    }
         
}

struct DocumentAnswerModel{
    var id = 0
    var type = 0
    var location = ""
    var name = ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "location": self.location as Any,
            "type": self.type as Any,
            "name": self.name as Any
        ]
        return dic
    }
}

//icp answer Model

struct ICPAnswerModelData: Decodable{
    var is_child_in_institution = 1
    var date_of_admission = ""
    var admission_number = ""
    var type_of_stay = 1
    var is_savings_detail_available = 1
    var about_savings_detail = ""
    var is_earnings_detail_available = 1
    var about_earnings_detail = ""
    var is_awards_detail_available = 1
    var about_awards_detail = ""
    var child_expectation = ""
    var emotional_needs = ""
    var experience = ""
    var relationships = ""
    var religious_beliefs = ""
    var leisure = ""
    var skill_training = ""
    var living_skills = ""
    var health_needs = ""
    var educational_needs = ""
    
    //NEW ICP
    
    var board_address = "board_address"
    var place_of_interview = "place_of_interview"
    var date_of_interview = "date_of_interview"
    var progress_from_date = "progress_from_date"
    var progress_to_date = "progress_to_date"
    var proposed_interventions = "proposed_interventions"
    var intervention_leisure = "intervention_leisure"
    var intervention_emotional_needs = "intervention_emotional_needs"
    var intervention_religious_beliefs = "intervention_religious_beliefs"
    var intervention_experience = "intervention_experience"
    var intervention_skill_training = "intervention_skill_training"
    var intervention_health_needs = "intervention_health_needs"
    var intervention_relationships = "intervention_relationships"
    var intervention_living_skills = "intervention_living_skills"
    var intervention_child_expectation = "intervention_child_expectation"
    var intervention_educational_needs = "intervention_educational_needs"
    
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "is_child_in_institution": self.is_child_in_institution as Any,
            "date_of_admission": self.date_of_admission as Any,
            "admission_number": self.admission_number as Any,
            "type_of_stay": self.type_of_stay as Any,
            "is_savings_detail_available": self.is_savings_detail_available as Any,
            "about_savings_detail": self.about_savings_detail as Any,
            "is_earnings_detail_available": self.is_earnings_detail_available as Any,
            "about_earnings_detail": self.about_earnings_detail as Any,
            "is_awards_detail_available": self.is_awards_detail_available as Any,
            "about_awards_detail": self.about_awards_detail as Any,
            "child_expectation": self.child_expectation as Any,
            "emotional_needs": self.emotional_needs as Any,
            "experience": self.experience as Any,
            "relationships": self.relationships as Any,
            "religious_beliefs": self.religious_beliefs as Any,
            "leisure": self.leisure as Any,
            "skill_training": self.skill_training as Any,
            "living_skills": self.living_skills as Any,
            "health_needs": self.health_needs as Any,
            "educational_needs": self.educational_needs as Any,
            "board_address": self.board_address as Any,
            "place_of_interview": self.place_of_interview as Any,
            "date_of_interview": self.date_of_interview as Any,
            "progress_from_date": self.progress_from_date as Any,
            "progress_to_date": self.progress_to_date as Any,
            "proposed_interventions": self.proposed_interventions as Any,
            "intervention_leisure": self.intervention_leisure as Any,
            "intervention_emotional_needs": self.intervention_emotional_needs as Any,
            "intervention_religious_beliefs": self.intervention_religious_beliefs as Any,
            "intervention_experience": self.intervention_experience as Any,
            "intervention_skill_training": self.intervention_skill_training as Any,
            "intervention_health_needs": self.intervention_health_needs as Any,
            "intervention_relationships": self.intervention_relationships as Any,
            "intervention_living_skills": self.intervention_living_skills as Any,
            "intervention_child_expectation": self.intervention_child_expectation as Any,
            "intervention_educational_needs": self.intervention_educational_needs as Any
            
        ]
        return dic
    }
    
}


//SIR COndition

struct SirConditionAnswerModelData: Decodable{
    var id =  0
    var home_type =  "Apartment"
    var rooms_count =  "3 rooms"
    var drinking_water =  "1"
    var bathroom =  "1"
    var environment =  "Good to live"
    var marital_type =  "0"
    var marital_type_response = ""
    var other_relatives =  "No"
    var attitude_of_religion =  "Strong"
    var home_ethical_code =  ""
    var delinquency_family_record =  ""
    var family_offence_committed =  ""
    var father_mother_relation =  ""
    var parent_child_relation =  ""
    var sibling_child_relation =  ""
    var other_factors =  ""
    var economic_status = ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "home_type": self.home_type as Any,
            "rooms_count": self.rooms_count as Any,
            "drinking_water": self.drinking_water as Any,
            "bathroom": self.bathroom as Any,
            "environment": self.environment as Any,
            "marital_type": self.marital_type as Any,
            "marital_type_response": self.marital_type_response as Any,
            "other_relatives": self.other_relatives as Any,
            "attitude_of_religion": self.attitude_of_religion as Any,
            "home_ethical_code": self.home_ethical_code as Any,
            "delinquency_family_record": self.delinquency_family_record as Any,
            "family_offence_committed": self.family_offence_committed as Any,
            "father_mother_relation": self.father_mother_relation as Any,
            "parent_child_relation": self.parent_child_relation as Any,
            "sibling_child_relation": self.sibling_child_relation as Any,
            "other_factors": self.other_factors as Any,
            "economic_status": self.economic_status as Any
        ]
        return dic
    }
    
}


//Sir Child Vocation

struct SirVocationAnswerModelData: Decodable{
    var id =  0
    var vocational_detail =  ""
    var training_undergone =  ""
    var reason_for_leaving =  ""
    var employment_attitude =  ""
    var religious_beliefs =  ""
    var parental_care =  ""
    var neighbour_report =  ""
    var any_other = ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "vocational_detail": self.vocational_detail as Any,
            "training_undergone": self.training_undergone as Any,
            "reason_for_leaving": self.reason_for_leaving as Any,
            "employment_attitude": self.employment_attitude as Any,
            "religious_beliefs": self.religious_beliefs as Any,
            "parental_care": self.parental_care as Any,
            "neighbour_report": self.neighbour_report as Any,
            "other_remarks": self.any_other as Any
        ]
        return dic
    }
    
}

//Sir Inquiry

struct SirInquiryAnswerModelData: Decodable{
    var id =  0
    var behaviour_observed =  ""
    var physical_condition =  ""
    var intelligence =  ""
    var social_factors =  ""
    var religious_factors =  ""
    var suggested_causes =  ""
    var reasons_for_offence =  ""
    var experts_consulted =  ""
    var recommendation =  ""
    
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "behaviour_observed": self.behaviour_observed as Any,
            "physical_condition": self.physical_condition as Any,
            "intelligence": self.intelligence as Any,
            "social_factors": self.social_factors as Any,
            "religious_factors": self.religious_factors as Any,
            "suggested_causes": self.suggested_causes as Any,
            "reasons_for_offence": self.reasons_for_offence as Any,
            "experts_consulted": self.experts_consulted as Any,
            "recommendation": self.recommendation as Any
        ]
        return dic
    }
}

//Sir Family Member

struct SirFamilyMemberAnswerModelData: Decodable{
    var id =  0
    var first_name =  ""
    var last_name =  ""
    var relationship =  ""
    var gender: Int = 0
    var dob =  ""
    var occupation =  ""
    var education =  ""
    var health_status =  ""
    var is_disability: Int = 0
    var disability_note = ""
    var income_type: Int = 0
    var income =  ""
    var social_habits =  ""
    var contact_number =  ""
    var child_type: Int = 0
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "first_name": self.first_name as Any,
            "last_name": self.last_name as Any,
            "relationship": self.relationship as Any,
            "gender": self.gender as Any,
            "dob": self.dob as Any,
            "occupation": self.occupation as Any,
            "education": self.education as Any,
            "health_status": self.health_status as Any,
            "is_disability": self.is_disability as Any,
            "income_type": self.income_type as Any,
            "disability_note": self.disability_note as Any,
            "income": self.income as Any,
            "social_habits": self.social_habits as Any,
            "contact_number": self.contact_number as Any,
            "child_type": self.child_type as Any
        ]
        return dic
    }
    
}


//Sir child history

struct SirChildHistoryAnswerModelData: Decodable{
    var id =  0
    var past_mental_condition =  ""
    var present_mental_condition =  ""
    var past_physical_condition =  ""
    var present_physical_condition =  ""
    var habits_interest =  ""
    var personalitytraits =  ""
    var companions_influence =  ""
    var truancy_from_home =  ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "past_mental_condition": self.past_mental_condition as Any,
            "present_mental_condition": self.present_mental_condition as Any,
            "past_physical_condition": self.past_physical_condition as Any,
            "present_physical_condition": self.present_physical_condition as Any,
            "habits_interest": self.habits_interest as Any,
            "personalitytraits": self.personalitytraits as Any,
            "companions_influence": self.companions_influence as Any,
            "truancy_from_home": self.truancy_from_home as Any
        ]
        return dic
    }
    
}


//Sir Education


struct SirChildEducationAnswerModelData: Decodable{
    var id =  0
    var education_level =  ""
    var last_school =  ""
    var reason_for_leaving =  ""
    var extra_curricular =  ""
    var punishment_in_school =  ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "education_level": self.education_level as Any,
            "last_school": self.last_school as Any,
            "reason_for_leaving": self.reason_for_leaving as Any,
            "extra_curricular": self.extra_curricular as Any,
            "punishment_in_school": self.punishment_in_school as Any,
        ]
        return dic
    }
    
}

//SuperVision Preliminary

struct SupervisionPreliminaryAnswerModelData: Decodable{
    var id =  0
    var visit_date =  ""
    var parent_name =  ""
    var probation_officer_interacted_with =  ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "visit_date": self.visit_date as Any,
            "parent_name": self.parent_name as Any,
            "probation_officer_interacted_with": self.probation_officer_interacted_with as Any
        ]
        return dic
    }
    
}


//SupervisionVocation


struct SupervisionVocationAnswerModelData{
    var id =  0
    var action_point =  ""
    var unusual_behaviour =  ""
    var child_attitude_peers =  ""
    var child_attitude_work =  ""
    var peers_attitude_child =  ""
    var next_visit_date =  ""
    var time_spend_with_child =  ""
    var reason =  ""
    var child_progress_feedback =  ""
    var school_name =  ""
    var teacher_name =  ""
    var work_nature =  ""
    var work_hours = ""
    var violation_labour_laws = ""
    var care_plan_progress =  ""
    var care_plan_modification =  ""
    var document: [String: Any] =  [:]
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "action_point": self.action_point as Any,
            "unusual_behaviour": self.unusual_behaviour as Any,
            "child_attitude_peers": self.child_attitude_peers as Any,
            "child_attitude_work": self.child_attitude_work as Any,
            "peers_attitude_child": self.peers_attitude_child as Any,
            "next_visit_date": self.next_visit_date as Any,
            "time_spend_with_child": self.time_spend_with_child as Any,
            "reason": self.reason as Any,
            "child_progress_feedback": self.child_progress_feedback as Any,
            "school_name": self.school_name as Any,
            "teacher_name": self.teacher_name as Any,
            "work_nature": self.work_nature as Any,
            "work_hours": self.work_hours as Any,
            "violation_labour_laws": self.violation_labour_laws as Any,
            "care_plan_progress": self.care_plan_progress as Any,
            "care_plan_modification": self.care_plan_modification as Any,
            "document": self.document as Any
        ]
        return dic
    }
    
}


//supervision obervation

struct SupervisionObservartionAnswerModelData: Decodable{
    var id =  0
    var child_behaviour =  ""
    var physical_health_status =  ""
    var relationship_with_family =  ""
    var saftey_in_family =  ""
    var difficulties_faced_by_child =  ""
    var difficulties_faced_by_family =  ""
    var household_change =  ""
    var vacational_training =  ""
    var child_engagement =  ""
    var last_engagement =  ""
    
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = [
            "id": self.id as Any,
            "child_behaviour": self.child_behaviour as Any,
            "physical_health_status": self.physical_health_status as Any,
            "relationship_with_family": self.relationship_with_family as Any,
            "saftey_in_family": self.saftey_in_family as Any,
            
            "difficulties_faced_by_child": self.difficulties_faced_by_child as Any,
            "difficulties_faced_by_family": self.difficulties_faced_by_family as Any,
            "household_change": self.household_change as Any,
            "vacational_training": self.vacational_training as Any,
            "child_engagement": self.child_engagement as Any,
            "last_engagement": self.last_engagement as Any
            
        ]
        return dic
    }
    
}

//MARK:- Country Detail Api

struct CountryListModelData: Codable {
    let countries: [IntakePoCountry]?
    let states: [IntakeState]?
    let districts: [IntakeDistrict]?
    let taluks: [IntakeTaluk]?
    
    enum CodingKeys: String, CodingKey {
        case countries
        case states
        case districts
        case taluks
    }
}

struct IntakePoCountry: Codable {
    let fullName: String?
    let id: Int?
    let sortName, phoneCode: String?
    
    enum CodingKeys: String, CodingKey {
        case fullName = "full_name"
        case id
        case sortName = "sort_name"
        case phoneCode = "phone_code"
    }
}

struct IntakeState: Codable {
    let name: String?
    let id, countryID: Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
        case countryID = "country_id"
    }
}

struct IntakeDistrict: Codable {
    let name: String?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
    }
}

struct IntakeTaluk: Codable {
    let name: String?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
    }
}
