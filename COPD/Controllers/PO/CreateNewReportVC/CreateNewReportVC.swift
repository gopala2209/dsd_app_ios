//
//  ListViewController.swift
//  QuestionListApp
//

import UIKit
import MobileCoreServices
import UniformTypeIdentifiers


class CreateNewReportVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblQuestionList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnSubmitReport: UIButton!
    @IBOutlet weak var btnCompleteReportViewHeightContraints: UIButton!
    @IBOutlet weak var stackBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var stackBtnBottom: NSLayoutConstraint!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblHeaderName: UILabel!
    @IBOutlet weak var lblHeaderDOB: UILabel!
    @IBOutlet weak var lblHeaderAadhaar: UILabel!
    @IBOutlet weak var lblHeaderGender: UILabel!
    @IBOutlet weak var lblHeaderStatus: UILabel!
    
    @IBOutlet weak var completeView: ShadowBottomView!
    
    var documentsToBeUploaded: [String: PostReleaseUploadModelData] = [:]
    
    var uploadTypeIndex = -1
    var icpSelectedMenu = 0
    var isTransfer = 0
    var cwcDistrictId = 0
    var transferDistrictData: [IntakeDistrict] = []{
        didSet{
            self.transferOptionValue.removeAll()
            for i in self.transferDistrictData{
                self.transferOptionValue.append(i.name ?? "")
            }
            self.tblQuestionList.reloadData()
        }
    }
    var transferOptionValue: [String] = []
    
    var reportType = "intake"
    var statusText = ""
    var supervisionIndex: Int = -1
    var aliasId = ""
    var selectedIndex: Int = -1
    var intakeId = 0
    var isComplete = false
    var isAddFamily = false
    
    var fieldId2Selected = false
    var fieldId1Selected = false
    
    var childDataPo_ICP: ChildDataPo?
    var sirCondition: SirCondition?
    var sirHistory: SirHistory?
    var sirEducation: SirEducation?
    var sirVocation: SirVocation?
    var sirInquiry: SirInquiry?
    var sirFamily: SirFamily?
    
    var vocationalReport: VocationalReport?
    var preliminaryReport: PreliminaryReport?
    var observationReport: ObservationReport?
    
    var countryData: [IntakePoCountry] = []
    var stateData: [IntakeState] = []
    var districtData: [IntakeDistrict] = []
    var talukData: [IntakeTaluk] = []
    
    var countryDataGuardian: [IntakePoCountry] = []
    var stateDataGuardian: [IntakeState] = []
    var districtDataGuardian: [IntakeDistrict] = []
    var talukDataGuardian: [IntakeTaluk] = []
    
    // MARK: - Properties
    var inspectionQuestionListArray: [IntakeQuestion]? = []
    var tempInspectionQuestionListArray: [IntakeQuestion]? = []
    
    var selectedInspectionForm : InspectionForm?
    var passingReportId = ""
    
    var isCompletedReport = false
    
    var passingFormCategory = ""
    
    
    var isGuardianSelected = false
    
    var postReleaseModelData: PostReleaseModelData?
    
    var preReleaseModelData: PreReleaseModelData?
    
    var rehabilitationModelData: RehabilitationModelData?
    
    var uploadModels: [String: UploadPdfResponse]? = [:]
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isComplete{
            stackBtnHeight.constant = 0
            stackBtnBottom.constant = 0
        }
        
        var pathToJson = ""
        if reportType != "sir" && reportType != "supervision"{
            btnCompleteReportViewHeightContraints.isHidden = false
            completeView.isHidden = false
            if reportType == "intake" || reportType == "transfer"{
                pathToJson = "IntakeQuestionList"
            }else{
                if icpSelectedMenu == 0{
                    pathToJson = "ICPBasicQuestionList"
                }
                else if icpSelectedMenu == 1{
                    pathToJson = "ICPRehabilitationQuestionList"
                }
                else if icpSelectedMenu == 2{
                    pathToJson = "ICPPreReleaseQuestionList"
                }else{
                    pathToJson = "ICPPostReleaseQuestionList"
                }
            }
            if let path = Bundle.main.path(forResource: pathToJson, ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let model  = try JSONDecoder().decode(QuestionsAnswerModelData.self, from: data)
                    self.inspectionQuestionListArray = model.intakeQuestions
                    self.tblQuestionList.reloadData()
                } catch {
                    // handle error
                }
            }
        }else{
            btnCompleteReportViewHeightContraints.isHidden = true
            completeView.isHidden = true
        }
        
        setInitialUI()
        
        lblTitle.text = passingFormCategory
        
        // set back button with alert
        self.setCustomBackBarButtonWithAlertOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        if reportType != "sir" && reportType != "supervision"{
            if reportType.contains("icp"){
                completeView.isHidden = true
                btnSubmitReport.setTitle("Update & Submit", for: .normal)
            }
            
            if icpSelectedMenu == 0{
                getIcpBasicDetailsData()
            }
            else if icpSelectedMenu == 1{
                getIcpRehabilitationData()
            }
            else if icpSelectedMenu == 2{
                getIcpPreReleaseData()
            }else{
                getIcpPostReleaseData()
            }
        }
        
        if reportType == "intake" || reportType == "transfer"{
            self.getCountryRelatedDetail(postUrl: "master/country")
        }
        
        if reportType == "transfer"{
            self.getCountryRelatedDetail(postUrl: "master/district?state_id=35")
        }
        
        if reportType == "intake" || reportType == "transfer"{
            self.btnSubmitReport.setTitle("UPDATE", for: .normal)
            self.btnSubmitReport.backgroundColor = UIColor.orangeTheme
        }
        else if reportType.contains("icp"){
            self.btnSubmitReport.setTitle("Update & Submit", for: .normal)
            self.btnSubmitReport.backgroundColor = UIColor.cciStatusColorTheme
        }
        else if reportType == "supervision"{
            self.btnSubmitReport.setTitle("Save Changes", for: .normal)
            self.btnSubmitReport.backgroundColor = UIColor.cciStatusColorTheme
        }
        else{
            self.btnSubmitReport.setTitle("SUBMIT", for: .normal)
            self.btnSubmitReport.backgroundColor = UIColor.cciStatusColorTheme
        }
        
        if isAddFamily == true{
            
            lblTitle.text = "Add Family Details"
            self.btnSubmitReport.setTitle("Add Family", for: .normal)
            self.btnSubmitReport.backgroundColor = UIColor.cciStatusColorTheme
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
        if self.reportType == "icp"{
            
            self.headerView.isHidden = false
            
            let firstName = childDataPo_ICP?.firstName ?? ""
            let lastName = childDataPo_ICP?.lastName ?? ""
            let fullName = "Name : " + firstName + " " + lastName
            self.lblHeaderName.text = fullName
            
            self.lblHeaderDOB.text = "DOB : " + (childDataPo_ICP?.dob ?? "")
            let gender = "    |    Sex : " + (childDataPo_ICP?.genderName ?? "")
            let age = "Age : " + (childDataPo_ICP?.age ?? "")
            self.lblHeaderGender.text =  age + gender
            self.lblHeaderStatus.text = statusText
            if statusText.lowercased() == "completed"{
                self.lblHeaderStatus.textColor = UIColor.greenTheme
            }else{
                self.lblHeaderStatus.textColor = UIColor.redErrorTheme
            }
            
            self.lblHeaderName.isHidden = false
            self.lblHeaderDOB.isHidden = false
            self.lblHeaderAadhaar.isHidden = false
            self.lblHeaderGender.isHidden = false
            self.lblHeaderStatus.isHidden = false
            
            self.headerViewHeight.constant = 175
        }
        else{
            self.headerView.isHidden = true
            
            self.lblHeaderName.isHidden = true
            self.lblHeaderDOB.isHidden = true
            self.lblHeaderAadhaar.isHidden = true
            self.lblHeaderGender.isHidden = true
            self.lblHeaderStatus.isHidden = true
            
            self.headerViewHeight.constant = 0
        }
        
    }
    
    func setCustomBackBarButtonWithAlertOnNavigationBar()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "back_icon")?.imageWithColor(color: UIColor.blueTheme), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(backButtonPressedWithAlert), for: UIControl.Event.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backButtonPressedWithAlert()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // method to register cell
    func registerCell(tableView: UITableView) {
        tableView.allowsSelection = false
        tableView.register(SingleTextAreaPoTableViewCell.nib, forCellReuseIdentifier: SingleTextAreaPoTableViewCell.reuseIdentifier)
        tableView.register(SingleTextBoxPoTableViewCell.nib, forCellReuseIdentifier: SingleTextBoxPoTableViewCell.reuseIdentifier)
        tableView.register(DatepickerPOTableViewCell.nib, forCellReuseIdentifier: DatepickerPOTableViewCell.reuseIdentifier)
        tableView.register(RadioWith3OptionsTableViewCell.nib, forCellReuseIdentifier: RadioWith3OptionsTableViewCell.reuseIdentifier)
        tableView.register(RadioWith4OptionsTableViewCell.nib, forCellReuseIdentifier: RadioWith4OptionsTableViewCell.reuseIdentifier)
        tableView.register(RadioWith5OptionsTableViewCell.nib, forCellReuseIdentifier: RadioWith5OptionsTableViewCell.reuseIdentifier)
        tableView.register(DropDownPoTableViewCell.nib, forCellReuseIdentifier: DropDownPoTableViewCell.reuseIdentifier)
        tableView.register(RadioWith2OptionsTableViewCell.nib, forCellReuseIdentifier: RadioWith2OptionsTableViewCell.reuseIdentifier)
        tableView.register(CheckBoxPoTableViewCell.nib, forCellReuseIdentifier: CheckBoxPoTableViewCell.reuseIdentifier)
        tableView.register(OnlyLblPoTableViewCell.nib, forCellReuseIdentifier: OnlyLblPoTableViewCell.reuseIdentifier)
        tableView.register(TransferReportPOChildTableViewCell.nib, forCellReuseIdentifier: TransferReportPOChildTableViewCell.reuseIdentifier)
        tableView.register(UploadPDFPOTableViewCell.nib, forCellReuseIdentifier: UploadPDFPOTableViewCell.reuseIdentifier)
        
        
    }
    
    func getIntakeTypePostData() -> [String: Any]{
        
        var intakeResult: IntakeUpdateApiModelData! = IntakeUpdateApiModelData()
        intakeResult.id = self.intakeId
        
        if inspectionQuestionListArray?.last?.type == "checkBox"{
            for i in tempInspectionQuestionListArray!{
                inspectionQuestionListArray?.append(i)
            }
        }
        
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                
                if answerList.id?.lowercased() == "guardiantype"{
                    
                    if answerList.optionValue == "Parents(Father and Mother)"{
                        answer = "\(4)"
                    }
                    else if answerList.optionValue == "Father only"{
                        answer = "\(1)"
                    }
                    else if answerList.optionValue == "Mother only"{
                        answer = "\(2)"
                    }
                    else if answerList.optionValue == "Uncle(Guardian)"{
                        answer = "\(5)"
                    }
                    else if answerList.optionValue == "Aunty(Guardian)"{
                        answer = "\(6)"
                    }
                    else if answerList.optionValue == "Grandpa(Guardian)"{
                        answer = "\(7)"
                    }
                    else if answerList.optionValue == "Grandma(Guardian)"{
                        answer = "\(8)"
                    }
                }
                else{
                    for i in 0..<answerList.options!.count{
                        if answerList.optionValue == answerList.options![i]{
                            answer = "\(i)"
                        }
                    }
                }
                
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            if questionID == "firstName"{
                intakeResult.firstName =  answer
            }
            if questionID == "lastName"{
                intakeResult.lastName = answer
            }
            if questionID == "aadharNumber"{
                intakeResult.aadharNumber =  answer
            }
            
            if questionID == "genderName"{
                intakeResult.gender =  Int(answer) ?? 0
            }
            
            if questionID == "dob"{
                intakeResult.dob = answer
            }
            if questionID == "religionName"{
                intakeResult.religionID = Int(answer) ?? 0
            }
            if questionID == "communityName"{
                intakeResult.communityID = Int(answer) ?? 0
            }
            if questionID == "isAddressVerified"{
                intakeResult.isAddressVerified = Int(answer) ?? 0
            }
            if questionID == "addressLine1"{
                intakeResult.addressLine1 = answer
            }
            
            if questionID == "addressLine2"{
                intakeResult.addressLine2 =  answer
            }
            
            if questionID == "city"{
                intakeResult.city =  answer
            }
            
            if questionID == "landmark"{
                intakeResult.landmark =  answer
            }
            
            if questionID == "zipcode"{
                intakeResult.zipcode =  answer
            }
            
            if questionID == "talukName"{
                if Int(answer) ?? 0 < self.talukData.count{
                    intakeResult.talukID =  self.talukData[Int(answer) ?? 0].id!
                }else{
                    intakeResult.talukID =  0
                }
            }
            if questionID == "districtName"{
                if Int(answer) ?? 0 < self.districtData.count{
                    intakeResult.districtID =  self.districtData[Int(answer) ?? 0].id!
                }else{
                    intakeResult.districtID = 0
                }
            }
            if questionID == "stateName"{
                if Int(answer) ?? 0 < self.stateData.count{
                    intakeResult.stateID =  self.stateData[Int(answer) ?? 0].id!
                }else{
                    intakeResult.stateID =  0
                }
            }
            if questionID == "countryName"{
                if Int(answer) ?? 0 < self.countryData.count{
                    intakeResult.countryID = self.countryData[Int(answer) ?? 0].id!
                }else{
                    intakeResult.countryID = self.countryData[Int(answer) ?? 0].id!
                }
            }
            if questionID == "personalIdentificationMark"{
                intakeResult.personalIdentificationMark =  answer
            }
            if questionID == "languageByChild"{
                intakeResult.languageByChild =  answer
            }
            if questionID == "sibling"{
                intakeResult.sibling = answer
            }
            if questionID == "isGuardianAvailable"{
                if answer == "-1"{
                    intakeResult.isGuardianAvailable = false
                }else{
                    intakeResult.isGuardianAvailable = true
                }
            }
            if questionID == "guardianType"{
                intakeResult.guardianType =  Int(answer) ?? 0
            }
            if questionID == "guardianFirstName"{
                intakeResult.guardianFirstName =  answer
            }
            if questionID == "guardianLastName"{
                intakeResult.guardianLastName =  answer
            }
            if questionID == "guardianDob"{
                intakeResult.guardianDob =  answer
            }
            if questionID == "guardianMobile"{
                intakeResult.guardianMobile =  answer
            }
            if questionID == "guardianDesignation"{
                intakeResult.guardianDesignation =  answer
            }
            if questionID == "guardianAddressLine1"{
                intakeResult.guardianAddressLine1 =  answer
            }
            if questionID == "guardianAddressLine2"{
                intakeResult.guardianAddressLine2 =  answer
            }
            if questionID == "guardianLandmark"{
                intakeResult.guardianLandmark =  answer
            }
            if questionID == "guardianCity"{
                intakeResult.guardianCity =  answer
            }
            if questionID == "guardianTalukId"{
                if Int(answer) ?? 0 < self.talukDataGuardian.count{
                    intakeResult.guardianTalukID =  self.talukDataGuardian[Int(answer) ?? 0].id!
                }else{
                    intakeResult.guardianTalukID =  0
                }
            }
            if questionID == "guardianDistrictId"{
                if Int(answer) ?? 0 < self.districtDataGuardian.count{
                    intakeResult.guardianDistrictID =  self.districtDataGuardian[Int(answer) ?? 0].id!
                }else{
                    intakeResult.guardianDistrictID = 0
                }
            }
            if questionID == "guardianStateId"{
                if Int(answer) ?? 0 < self.stateDataGuardian.count{
                    intakeResult.guardianStateID =  self.stateDataGuardian[Int(answer) ?? 0].id!
                }else{
                    intakeResult.guardianStateID = 0
                }
            }
            if questionID == "guardianCountryId"{
                if Int(answer) ?? 0 < self.countryDataGuardian.count{
                    intakeResult.guardianCountryID = self.countryDataGuardian[Int(answer) ?? 0].id!
                }else{
                    intakeResult.guardianCountryID = self.countryDataGuardian[Int(answer) ?? 0].id!
                }
            }
            if questionID == "guardianZipcode"{
                intakeResult.guardianZipcode =  answer
            }
            if questionID == "interestedPersonDetails"{
                intakeResult.interestedPersonDetails =  answer
            }
            if questionID == "habits"{
                intakeResult.habits =  answer
            }
            if questionID == "summary"{
                
                intakeResult.summary =  answer
            }
        }
        
        var addedData = intakeResult.map { $0.convertToDictionary() }!
        if reportType == "transfer"{
            addedData["is_transfered"] = self.isTransfer
            addedData["cwc_district_id"] = self.cwcDistrictId
        }
        print(addedData)
        return addedData
    }
    
    //Basic Details MODULE
    func getIcpTypePostData() -> [String: Any]{
        
        var intakeResult: ICPAnswerModelData! = ICPAnswerModelData()
        
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "ischildininstitution"{
                intakeResult.is_child_in_institution = Int(answer) ?? 0
                if answerList.fieldId2?.lowercased() == "dateofadmission"{
                    intakeResult.date_of_admission = answerList.fieldValue2 ?? ""
                }
                if answerList.fieldId1?.lowercased() == "admissionnumber"{
                    intakeResult.admission_number = answerList.fieldValue1 ?? ""
                }
            }
            
            if questionID == "typeofstay"{
                intakeResult.type_of_stay = Int(answer) ?? 0
            }
            if questionID == "issavingsdetailavailable"{
                intakeResult.is_savings_detail_available = Int(answer) ?? 0
                if answerList.fieldId1?.lowercased() == "aboutsavingsdetail"{
                    intakeResult.about_savings_detail = answerList.fieldValue1 ?? ""
                }
            }
            
            if questionID == "isearningsdetailavailable"{
                intakeResult.is_earnings_detail_available = Int(answer) ?? 0
                if answerList.fieldId1?.lowercased() == "aboutearningsdetail"{
                    intakeResult.about_earnings_detail = answerList.fieldValue1 ?? ""
                }
            }
            
            if questionID == "isawardsdetailavailable"{
                intakeResult.is_awards_detail_available = Int(answer) ?? 0
                if answerList.fieldId1?.lowercased() == "about_awards_detail".replacingOccurrences(of: "_", with: ""){
                    intakeResult.about_awards_detail = answerList.fieldValue1 ?? ""
                }
            }
            
            
            if questionID == "child_expectation".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_expectation = answer
            }
            if questionID == "emotional_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.emotional_needs = answer
            }
            if questionID == "emotional_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.emotional_needs = answer
            }
            if questionID == "experience".replacingOccurrences(of: "_", with: ""){
                intakeResult.experience = answer
            }
            if questionID == "relationships".replacingOccurrences(of: "_", with: ""){
                intakeResult.relationships = answer
            }
            if questionID == "religious_beliefs".replacingOccurrences(of: "_", with: ""){
                intakeResult.religious_beliefs = answer
            }
            if questionID == "leisure".replacingOccurrences(of: "_", with: ""){
                intakeResult.leisure = answer
            }
            if questionID == "skill_training".replacingOccurrences(of: "_", with: ""){
                intakeResult.skill_training = answer
            }
            if questionID == "living_skills".replacingOccurrences(of: "_", with: ""){
                intakeResult.living_skills = answer
            }
            if questionID == "health_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.health_needs = answer
            }
            if questionID == "educational_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.educational_needs = answer
            }
            
            
            //NEW ICP
            if questionID == "board_address".replacingOccurrences(of: "_", with: ""){
                intakeResult.board_address = answer
            }
            if questionID == "place_of_interview".replacingOccurrences(of: "_", with: ""){
                intakeResult.place_of_interview = answer
            }
            if questionID == "date_of_interview".replacingOccurrences(of: "_", with: ""){
                intakeResult.date_of_interview = answer
            }
            
            if questionID == "progress_from_date".replacingOccurrences(of: "_", with: ""){
                intakeResult.progress_from_date = answer
                intakeResult.progress_to_date = answerList.fieldValue1 ?? ""
            }
            if questionID == "proposed_interventions".replacingOccurrences(of: "_", with: ""){
                intakeResult.proposed_interventions = answer
            }
            if questionID == "intervention_leisure".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_leisure = answer
            }
            if questionID == "intervention_child_expectation".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_child_expectation = answer
            }
            if questionID == "intervention_health_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_health_needs = answer
            }
            if questionID == "intervention_emotional_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_emotional_needs = answer
            }
            if questionID == "intervention_educational_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_educational_needs = answer
            }
            if questionID == "intervention_relationships".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_relationships = answer
            }
            if questionID == "intervention_religious_beliefs".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_religious_beliefs = answer
            }
            if questionID == "intervention_living_skills".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_living_skills = answer
            }
            if questionID == "intervention_experience".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_experience = answer
            }
            if questionID == "intervention_skill_training".replacingOccurrences(of: "_", with: ""){
                intakeResult.intervention_skill_training = answer
            }
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    
    //REHABILITATION MODULE
    func getIcpRehabilitationTypePostData() -> [String: Any]{
        
        var intakeResult: ICPRehabilitationAnswerModelData! = ICPRehabilitationAnswerModelData()
        
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "0"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "0"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            
            if questionID == "calendar_year".replacingOccurrences(of: "_", with: ""){
                
                intakeResult.calendar_year = Int(answerList.optionValue ?? "0") ?? 0
            }
            
            if questionID!.contains("month".replacingOccurrences(of: "_", with: "")){
                intakeResult.month = Int(answer) ?? 0
            }
            if questionID == "child_expectation".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_expectation = answer
            }
            if questionID == "health_nutrition".replacingOccurrences(of: "_", with: ""){
                intakeResult.health_nutrition = answer
            }
            if questionID == "emotion_psychological_support".replacingOccurrences(of: "_", with: ""){
                intakeResult.emotion_psychological_support = answer
            }
            if questionID == "education_training".replacingOccurrences(of: "_", with: ""){
                intakeResult.education_training = answer
            }
            if questionID == "creativity".replacingOccurrences(of: "_", with: ""){
                intakeResult.creativity = answer
            }
            if questionID == "interpersonal_relationships".replacingOccurrences(of: "_", with: ""){
                intakeResult.interpersonal_relationships = answer
            }
            if questionID == "religious_beliefs".replacingOccurrences(of: "_", with: ""){
                intakeResult.religious_beliefs = answer
            }
            if questionID == "self_care".replacingOccurrences(of: "_", with: ""){
                intakeResult.self_care = answer
            }
            if questionID == "independent_living_skills".replacingOccurrences(of: "_", with: ""){
                intakeResult.independent_living_skills = answer
            }
            if questionID == "significant_experiences".replacingOccurrences(of: "_", with: ""){
                intakeResult.significant_experiences = answer
            }
            if questionID == "outcome_child_expectation".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_child_expectation = answer
            }
            if questionID == "outcome_health_nutrition".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_health_nutrition = answer
            }
            if questionID == "outcome_emotion_psychological_support".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_emotion_psychological_support = answer
            }
            if questionID == "outcome_education_training".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_education_training = answer
            }
            if questionID == "outcome_creativity".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_creativity = answer
            }
            if questionID == "outcome_interpersonal_relationships".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_interpersonal_relationships = answer
            }
            if questionID == "outcome_religious_beliefs".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_religious_beliefs = answer
            }
            if questionID == "outcome_self_care".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_self_care = answer
            }
            if questionID == "outcome_independent_living_skills".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_independent_living_skills = answer
            }
            if questionID == "outcome_significant_experiences".replacingOccurrences(of: "_", with: ""){
                intakeResult.outcome_significant_experiences = answer
            }
            if questionID == "commitee_or_board".replacingOccurrences(of: "_", with: ""){
                
                intakeResult.commitee_or_board = Int(answer) ?? 0

            }
            if questionID == "supervision_completed_at".replacingOccurrences(of: "_", with: ""){
                intakeResult.supervision_completed_at = answer
            }
            
        }
        
        
        intakeResult.id = rehabilitationModelData?.significantExperiences?.first?.id ?? 0
        
        //icp type is missing from backend api
        intakeResult.type = rehabilitationModelData?.significantExperiences?.first?.childID ?? 0
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
        
    }
    
    //PRE RELEASE MODULE
    func getIcpPreReleaseTypePostData() -> [String: Any]{
        
        var intakeResult: ICPPreReleaseAnswerModelData! = ICPPreReleaseAnswerModelData()
        
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "0"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "0"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "transfer_release".replacingOccurrences(of: "_", with: ""){
                intakeResult.transfer_release = answer
            }
            
            if questionID == "child_placement".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_placement = answer
            }
            
            if questionID == "training_skills".replacingOccurrences(of: "_", with: ""){
                intakeResult.training_skills = answer
            }
            
            //upload
            
            if questionID == "release_date".replacingOccurrences(of: "_", with: ""){
                intakeResult.release_date = answer
            }
            
            if questionID == "is_escort_required".replacingOccurrences(of: "_", with: ""){
                intakeResult.is_escort_required = Int(answer) ?? 0
                if answerList.fieldId1!.lowercased().contains("proof_type".replacingOccurrences(of: "_", with: "")) {
                    var selectedAnswer = 0
                    for i in 0..<answerList.fieldOption1!.count{
                        if answerList.fieldOption1![i] == answerList.fieldValue1{
                            selectedAnswer = i
                        }
                    }
                    intakeResult.proof_type = selectedAnswer
                }
            }
            
            if questionID == "rehabilitation_plan".replacingOccurrences(of: "_", with: ""){
                intakeResult.rehabilitation_plan = answer
            }
            
            if questionID == "probation_officer".replacingOccurrences(of: "_", with: ""){
                intakeResult.probation_officer = answer
            }
            
            if questionID == "sponsorship_individual".replacingOccurrences(of: "_", with: ""){
                intakeResult.sponsorship_individual = answer
            }
            
            if questionID == "medical_examination".replacingOccurrences(of: "_", with: ""){
                intakeResult.medical_examination = answer
            }
            
            if questionID == "other_information".replacingOccurrences(of: "_", with: ""){
                intakeResult.other_information = answer
            }
            
            if questionID == "child_expectation".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_expectation = answer
            }
            
            if questionID == "health_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.health_needs = answer
            }
            
            if questionID == "emotional_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.emotional_needs = answer
            }
            
            if questionID == "educational_needs".replacingOccurrences(of: "_", with: ""){
                intakeResult.educational_needs = answer
            }
            
            if questionID == "leisure".replacingOccurrences(of: "_", with: ""){
                intakeResult.leisure = answer
            }
            
            if questionID == "relationships".replacingOccurrences(of: "_", with: ""){
                intakeResult.relationships = answer
            }
            
            if questionID == "religious_beliefs".replacingOccurrences(of: "_", with: ""){
                intakeResult.religious_beliefs = answer
            }
            
            if questionID == "skill_training".replacingOccurrences(of: "_", with: ""){
                intakeResult.skill_training = answer
            }
            
            if questionID == "living_skills".replacingOccurrences(of: "_", with: ""){
                intakeResult.living_skills = answer
            }
            
            if questionID == "experience".replacingOccurrences(of: "_", with: ""){
                intakeResult.experience = answer
            }
            
        }
        
        
        intakeResult.id = preReleaseModelData?.id ?? 0
        intakeResult.type = preReleaseModelData?.icpType ?? 0
        
//        var documentToUpload = DocumentAnswerModel()
//        documentToUpload.id = preReleaseModelData?.document?.id ?? 0
//        documentToUpload.name = preReleaseModelData?.document?.name ?? ""
//        documentToUpload.location = preReleaseModelData?.document?.location ?? ""
//        documentToUpload.type = preReleaseModelData?.document?.type ?? 0
//        intakeResult.document = documentToUpload.convertToDictionary()
        
        
        //document upload question
        if preReleaseModelData?.document == nil{
            var documentToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["lastProgressDocument"]
            documentToUpload.id =  0
            documentToUpload.name = documentData?.name ?? ""
            documentToUpload.location = documentData?.location ?? ""
            documentToUpload.type = 24
            intakeResult.document = documentToUpload.convertToDictionary()
        }else{
            var documentToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["lastProgressDocument"]
            documentToUpload.id = preReleaseModelData?.document?.id ?? 0
            if(documentData != nil){
                documentToUpload.name = documentData?.name ?? ""
                documentToUpload.location = documentData?.location ?? ""
            }else{
                documentToUpload.name = preReleaseModelData?.document?.name ?? ""
                documentToUpload.location = preReleaseModelData?.document?.location ?? ""
            }
            documentToUpload.type = 24
            intakeResult.document = documentToUpload.convertToDictionary()
        }
        
        //nonGovernmental upload question
        if preReleaseModelData?.nonGovernmental == nil{
            var nonGovernmentalToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["nonGovernmental"]
            nonGovernmentalToUpload.id =  0
            nonGovernmentalToUpload.name = documentData?.name ?? ""
            nonGovernmentalToUpload.location = documentData?.location ?? ""
            nonGovernmentalToUpload.type = 38
            intakeResult.non_governmental = nonGovernmentalToUpload.convertToDictionary()
        }else{
            var nonGovernmentalToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["nonGovernmental"]
            nonGovernmentalToUpload.id = preReleaseModelData?.nonGovernmental?.id ?? 0
            if(documentData != nil){
                nonGovernmentalToUpload.name = documentData?.name ?? ""
                nonGovernmentalToUpload.location = documentData?.location ?? ""
            }else{
                nonGovernmentalToUpload.name = preReleaseModelData?.nonGovernmental?.name ?? ""
                nonGovernmentalToUpload.location = preReleaseModelData?.nonGovernmental?.location ?? ""
            }
            nonGovernmentalToUpload.type = 38
            intakeResult.non_governmental = nonGovernmentalToUpload.convertToDictionary()
        }
        
        
        //sponsoringAgency upload question
        if preReleaseModelData?.sponsoringAgency == nil{
            var sponsoringAgencyToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["sponsoringAgency"]
            sponsoringAgencyToUpload.id =  0
            sponsoringAgencyToUpload.name = documentData?.name ?? ""
            sponsoringAgencyToUpload.location = documentData?.location ?? ""
            sponsoringAgencyToUpload.type = 39
            intakeResult.sponsoring_agency = sponsoringAgencyToUpload.convertToDictionary()
        }else{
            var sponsoringAgencyToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["sponsoringAgency"]
            sponsoringAgencyToUpload.id = preReleaseModelData?.sponsoringAgency?.id ?? 0
            if(documentData != nil){
                sponsoringAgencyToUpload.name = documentData?.name ?? ""
                sponsoringAgencyToUpload.location = documentData?.location ?? ""
            }else{
                sponsoringAgencyToUpload.name = preReleaseModelData?.sponsoringAgency?.name ?? ""
                sponsoringAgencyToUpload.location = preReleaseModelData?.sponsoringAgency?.location ?? ""
            }
            sponsoringAgencyToUpload.type = 39
            intakeResult.sponsoring_agency = sponsoringAgencyToUpload.convertToDictionary()
        }
        
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
        
    }
    
    
    //POST RELEASE MODULE
    func getIcpPostReleaseTypePostData() -> [String: Any]{
        
        var intakeResult: ICPPostReleaseAnswerModelData! = ICPPostReleaseAnswerModelData()
        
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            
            if answerList.type == "upload"{
                print(answerList)
                answer = answerList.optionValue ?? ""
            }
            
            else if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "0"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "0"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "bank_status".replacingOccurrences(of: "_", with: ""){
                
                if answerList.optionValue?.lowercased() == "transferred"{
                    intakeResult.bank_status = 0
                }else{
                    intakeResult.bank_status = 1
                }
            }
            
            if questionID == "child_belongings".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_belongings = Int(answer) ?? 0
                if answerList.fieldId1?.lowercased() == "other_child_belongings".replacingOccurrences(of: "_", with: ""){
                    intakeResult.other_child_belongings = answerList.fieldValue1 ?? ""
                }
            }
            if questionID == "rehabilitation_restoration_plan".replacingOccurrences(of: "_", with: ""){
                intakeResult.rehabilitation_restoration_plan = answer
            }
            if questionID == "family_behaviour".replacingOccurrences(of: "_", with: ""){
                intakeResult.family_behaviour = answer
            }
            if questionID == "neighbours_attitude".replacingOccurrences(of: "_", with: ""){
                intakeResult.neighbours_attitude = answer
            }
            if questionID == "skills_acquired".replacingOccurrences(of: "_", with: ""){
                intakeResult.skills_acquired = answer
            }
            if questionID == "admitted_school_vaction".replacingOccurrences(of: "_", with: ""){
                intakeResult.admitted_school_vaction = Int(answer) ?? 0
                if answerList.fieldId2?.lowercased() == "admitted_date".replacingOccurrences(of: "_", with: ""){
                    intakeResult.admitted_date = answerList.fieldValue2 ?? ""
                }
                if answerList.fieldId1?.lowercased() == "admitted_school".replacingOccurrences(of: "_", with: ""){
                    intakeResult.admitted_school = answerList.fieldValue1 ?? ""
                }
            }
            if questionID == "child_admitted".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_admitted = answer
            }
            if questionID == "child_opinion".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_opinion = answer
            }
            if questionID == "identity_compensation".replacingOccurrences(of: "_", with: ""){
                intakeResult.identity_compensation = answer
            }
            if questionID == "birth_certificate".replacingOccurrences(of: "_", with: ""){
                intakeResult.birth_certificate = Int(answer) ?? 0
            }
            if questionID == "school_certificate".replacingOccurrences(of: "_", with: ""){
                intakeResult.school_certificate = Int(answer) ?? 0
            }
            if questionID == "bpl_card".replacingOccurrences(of: "_", with: ""){
                intakeResult.bpl_card = Int(answer) ?? 0
            }
            if questionID == "disability_certificate".replacingOccurrences(of: "_", with: ""){
                intakeResult.disability_certificate = Int(answer) ?? 0
            }
            if questionID == "immunisation_card".replacingOccurrences(of: "_", with: ""){
                intakeResult.immunisation_card = Int(answer) ?? 0
            }
            if questionID == "ration_card".replacingOccurrences(of: "_", with: ""){
                intakeResult.ration_card = Int(answer) ?? 0
            }
            if questionID == "aadhar_card".replacingOccurrences(of: "_", with: ""){
                intakeResult.aadhar_card = Int(answer) ?? 0
            }
            if questionID == "goverment_compensation".replacingOccurrences(of: "_", with: ""){
                intakeResult.goverment_compensation = Int(answer) ?? 0
            }
            
        }
        
        intakeResult.id = postReleaseModelData?.id ?? 0
        intakeResult.icp_type = postReleaseModelData?.icpType ?? 0
        
//        var documentToUpload = DocumentAnswerModel()
//        documentToUpload.id = postReleaseModelData?.document?.id ?? 0
//        documentToUpload.name = postReleaseModelData?.document?.name ?? ""
//        documentToUpload.location = postReleaseModelData?.document?.location ?? ""
//        documentToUpload.type = postReleaseModelData?.document?.type ?? 0
//        intakeResult.document = documentToUpload.convertToDictionary()
        
        //document upload question
        if postReleaseModelData?.document == nil{
            var nonGovernmentalToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["document"]
            nonGovernmentalToUpload.id =  0
            nonGovernmentalToUpload.name = documentData?.name ?? ""
            nonGovernmentalToUpload.location = documentData?.location ?? ""
            nonGovernmentalToUpload.type = 23
            intakeResult.document = nonGovernmentalToUpload.convertToDictionary()
        }else{
            var nonGovernmentalToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["document"]
            nonGovernmentalToUpload.id = postReleaseModelData?.document?.id ?? 0
            if(documentData != nil){
                nonGovernmentalToUpload.name = documentData?.name ?? ""
                nonGovernmentalToUpload.location = documentData?.location ?? ""
            }else{
                nonGovernmentalToUpload.name = postReleaseModelData?.document?.name ?? ""
                nonGovernmentalToUpload.location = postReleaseModelData?.document?.location ?? ""
            }
            nonGovernmentalToUpload.type = 23
            intakeResult.document = nonGovernmentalToUpload.convertToDictionary()
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    
    func getSirVocationData() -> [String: Any]{
        
        var intakeResult: SirVocationAnswerModelData! = SirVocationAnswerModelData()
        intakeResult.id = self.sirVocation?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            
            if questionID == "vocational_detail".replacingOccurrences(of: "_", with: ""){
                intakeResult.vocational_detail = answer
            }
            if questionID == "training_undergone".replacingOccurrences(of: "_", with: ""){
                intakeResult.training_undergone = answer
            }
            if questionID == "reason_for_leaving".replacingOccurrences(of: "_", with: ""){
                intakeResult.reason_for_leaving = answer
            }
            if questionID == "religious_beliefs".replacingOccurrences(of: "_", with: ""){
                intakeResult.religious_beliefs = answer
            }
            if questionID == "employment_attitude".replacingOccurrences(of: "_", with: ""){
                intakeResult.employment_attitude = answer
            }
            if questionID == "parental_care".replacingOccurrences(of: "_", with: ""){
                intakeResult.parental_care = answer
            }
            if questionID == "neighbour_report".replacingOccurrences(of: "_", with: ""){
                intakeResult.neighbour_report = answer
            }
            if questionID == "any_other".replacingOccurrences(of: "_", with: ""){
                intakeResult.any_other = answer
            }
            
            
            
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    func getSirEducationData() -> [String: Any]{
        
        var intakeResult: SirChildEducationAnswerModelData! = SirChildEducationAnswerModelData()
        intakeResult.id = self.sirEducation?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "education_level".replacingOccurrences(of: "_", with: ""){
                intakeResult.education_level = answer
            }
            if questionID == "last_school".replacingOccurrences(of: "_", with: ""){
                intakeResult.last_school = answer
            }
            if questionID == "reason_for_leaving".replacingOccurrences(of: "_", with: ""){
                intakeResult.reason_for_leaving = answer
            }
            if questionID == "extra_curricular".replacingOccurrences(of: "_", with: ""){
                intakeResult.extra_curricular = answer
            }
            if questionID == "punishment_in_school".replacingOccurrences(of: "_", with: ""){
                intakeResult.punishment_in_school = answer
            }
            
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    
    func getSirHistoryData() -> [String: Any]{
        
        var intakeResult: SirChildHistoryAnswerModelData! = SirChildHistoryAnswerModelData()
        intakeResult.id = self.sirHistory?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "past_mental_condition".replacingOccurrences(of: "_", with: ""){
                intakeResult.past_mental_condition = answer
            }
            if questionID == "present_mental_condition".replacingOccurrences(of: "_", with: ""){
                intakeResult.present_mental_condition = answer
            }
            if questionID == "past_physical_condition".replacingOccurrences(of: "_", with: ""){
                intakeResult.past_physical_condition = answer
            }
            if questionID == "present_physical_condition".replacingOccurrences(of: "_", with: ""){
                intakeResult.present_physical_condition = answer
            }
            if questionID == "habits_interest".replacingOccurrences(of: "_", with: ""){
                intakeResult.habits_interest = answer
            }
            if questionID == "personalitytraits".replacingOccurrences(of: "_", with: ""){
                intakeResult.personalitytraits = answer
            }
            if questionID == "companions_influence".replacingOccurrences(of: "_", with: ""){
                intakeResult.companions_influence = answer
            }
            if questionID == "truancy_from_home".replacingOccurrences(of: "_", with: ""){
                intakeResult.truancy_from_home = answer
            }
            
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    func getSirInquiryData() -> [String: Any]{
        
        var intakeResult: SirInquiryAnswerModelData! = SirInquiryAnswerModelData()
        intakeResult.id = self.sirInquiry?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            
            if questionID == "behaviour_observed".replacingOccurrences(of: "_", with: ""){
                intakeResult.behaviour_observed = answer
            }
            if questionID == "physical_condition".replacingOccurrences(of: "_", with: ""){
                intakeResult.physical_condition = answer
            }
            if questionID == "intelligence".replacingOccurrences(of: "_", with: ""){
                intakeResult.intelligence = answer
            }
            if questionID == "social_factors".replacingOccurrences(of: "_", with: ""){
                intakeResult.social_factors = answer
            }
            if questionID == "religious_factors".replacingOccurrences(of: "_", with: ""){
                intakeResult.religious_factors = answer
            }
            if questionID == "suggested_causes".replacingOccurrences(of: "_", with: ""){
                intakeResult.suggested_causes = answer
            }
            if questionID == "reasons_for_offence".replacingOccurrences(of: "_", with: ""){
                intakeResult.reasons_for_offence = answer
            }
            if questionID == "experts_consulted".replacingOccurrences(of: "_", with: ""){
                intakeResult.experts_consulted = answer
            }
            if questionID == "recommendation".replacingOccurrences(of: "_", with: ""){
                intakeResult.recommendation = answer
            }
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    
    func getSIRFamilyMemberData() -> [String: Any]{
        
        var intakeResult: SirFamilyMemberAnswerModelData! = SirFamilyMemberAnswerModelData()
        intakeResult.id = self.sirFamily?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "familyfirstname"{
                intakeResult.first_name =  answer
            }
            if questionID == "familylastname"{
                intakeResult.last_name = answer
            }
            if questionID == "familyrelationship"{
                intakeResult.relationship =  answer
            }
            if questionID == "familygendername"{
                intakeResult.gender =  Int(answer) ?? 0
            }
            if questionID == "familydob"{
                intakeResult.dob = answer
            }
            if questionID == "familyoccupation"{
                intakeResult.occupation =  answer
            }
            if questionID == "familyeducation"{
                intakeResult.education =  answer
            }
            if questionID == "familyincometype"{
                intakeResult.income_type = Int(answer) ?? 0
                
                if answerList.fieldId1?.lowercased() == "income"{
                    
                    if intakeResult.income_type == 4{
                        intakeResult.income = ""
                    }else{
                        intakeResult.income = answerList.fieldValue1 ?? ""
                    }
                }
            }
            
            if questionID == "familyhealthstatus"{
                intakeResult.health_status = answer
            }
            if questionID == "familydisability"{
                
                intakeResult.is_disability = Int(answer) ?? 0
                if answerList.fieldId1?.lowercased() == "familydisablitydetail"{
                    intakeResult.disability_note = answerList.fieldValue1 ?? ""
                }
            }
            if questionID == "familysocialhabits"{
                intakeResult.social_habits =  answer
            }
            if questionID == "familycontactnumber"{
                intakeResult.contact_number =  answer
            }
            if questionID == "familychildtype"{
                intakeResult.child_type = Int(answer) ?? 0
            }
            
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    func getSirConditionData() -> [String: Any]{
        
        var intakeResult: SirConditionAnswerModelData! = SirConditionAnswerModelData()
        intakeResult.id = self.sirCondition?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "home_type".replacingOccurrences(of: "_", with: ""){
                intakeResult.home_type = answer
            }
            if questionID == "rooms_count".replacingOccurrences(of: "_", with: ""){
                intakeResult.rooms_count = answer
            }
            if questionID == "drinking_water".replacingOccurrences(of: "_", with: ""){
                intakeResult.drinking_water = answer
            }
            if questionID == "bathroom".replacingOccurrences(of: "_", with: ""){
                intakeResult.bathroom = answer
            }
            if questionID == "environment".replacingOccurrences(of: "_", with: ""){
                intakeResult.environment = answer
            }
            if questionID == "other_relatives".replacingOccurrences(of: "_", with: ""){
                intakeResult.other_relatives = answer
            }
            if questionID == "attitude_of_religion".replacingOccurrences(of: "_", with: ""){
                intakeResult.attitude_of_religion = answer
            }

            if questionID == "marital_type".replacingOccurrences(of: "_", with: ""){
                
                intakeResult.marital_type = answer
                
                if answerList.fieldId1?.lowercased() == "maritaltyperesponse"{
                    intakeResult.marital_type_response = answerList.fieldValue1 ?? ""
                }
            }
            
            if questionID == "home_ethical_code".replacingOccurrences(of: "_", with: ""){
                intakeResult.home_ethical_code = answer
            }
            if questionID == "delinquency_family_record".replacingOccurrences(of: "_", with: ""){
                intakeResult.delinquency_family_record = answer
            }
            if questionID == "family_offence_committed".replacingOccurrences(of: "_", with: ""){
                intakeResult.family_offence_committed = answer
            }
            if questionID == "father_mother_relation".replacingOccurrences(of: "_", with: ""){
                intakeResult.father_mother_relation = answer
            }
            if questionID == "parent_child_relation".replacingOccurrences(of: "_", with: ""){
                intakeResult.parent_child_relation = answer
            }
            if questionID == "sibling_child_relation".replacingOccurrences(of: "_", with: ""){
                intakeResult.sibling_child_relation = answer
            }
            if questionID == "other_factors".replacingOccurrences(of: "_", with: ""){
                intakeResult.other_factors = answer
            }
            if questionID == "economic_status".replacingOccurrences(of: "_", with: ""){
                intakeResult.economic_status = answer
            }
            
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    func getSupervisionVocationData() -> [String: Any]{
        
        var intakeResult: SupervisionVocationAnswerModelData! = SupervisionVocationAnswerModelData()
        intakeResult.id = self.vocationalReport?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "action_point".replacingOccurrences(of: "_", with: ""){
                intakeResult.action_point = answer
            }
            if questionID == "unusual_behaviour".replacingOccurrences(of: "_", with: ""){
                intakeResult.unusual_behaviour = answer
            }
            if questionID == "child_attitude_peers".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_attitude_peers = answer
            }
            if questionID == "child_attitude_work".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_attitude_work = answer
            }
            if questionID == "peers_attitude_child".replacingOccurrences(of: "_", with: ""){
                intakeResult.peers_attitude_child = answer
            }
            if questionID == "next_visit_date".replacingOccurrences(of: "_", with: ""){
                intakeResult.next_visit_date = answer
            }
            if questionID == "time_spend_with_child".replacingOccurrences(of: "_", with: ""){
                intakeResult.time_spend_with_child = answer
            }
            if questionID == "reason".replacingOccurrences(of: "_", with: ""){
                intakeResult.reason = answer
            }
            if questionID == "child_progress_feedback".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_progress_feedback = answer
            }
            if questionID == "school_name".replacingOccurrences(of: "_", with: ""){
                intakeResult.school_name = answer
            }
            if questionID == "teacher_name".replacingOccurrences(of: "_", with: ""){
                intakeResult.teacher_name = answer
            }
            if questionID == "work_nature".replacingOccurrences(of: "_", with: ""){
                intakeResult.work_nature = answer
            }
            if questionID == "work_hours".replacingOccurrences(of: "_", with: ""){
                intakeResult.work_hours = answer
            }
            if questionID == "violation_labour_laws".replacingOccurrences(of: "_", with: ""){
                intakeResult.violation_labour_laws = answer
            }
            if questionID == "care_plan_progress".replacingOccurrences(of: "_", with: ""){
                intakeResult.care_plan_progress = answer
            }
            if questionID == "care_plan_modification".replacingOccurrences(of: "_", with: ""){
                intakeResult.care_plan_modification = answer
            }
        }
        
        
        intakeResult.id = vocationalReport?.id ?? 0
        
//        var documentToUpload = DocumentAnswerModel()
//        documentToUpload.id = vocationalReport?.document?.id ?? 0
//        documentToUpload.name = vocationalReport?.document?.name ?? ""
//        documentToUpload.location = vocationalReport?.document?.location ?? ""
//        documentToUpload.type = vocationalReport?.document?.type ?? 0
//
//        intakeResult.document = documentToUpload.convertToDictionary()
        
        //document upload question
        if vocationalReport?.document == nil{
            var uploadPretermReleaseToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["uploadPretermRelease"]
            uploadPretermReleaseToUpload.id =  0
            uploadPretermReleaseToUpload.name = documentData?.name ?? ""
            uploadPretermReleaseToUpload.location = documentData?.location ?? ""
            uploadPretermReleaseToUpload.type = 23
            intakeResult.document = uploadPretermReleaseToUpload.convertToDictionary()
        }else{
            var uploadPretermReleaseToUpload = DocumentAnswerModel()
            let documentData = uploadModels!["uploadPretermRelease"]
            uploadPretermReleaseToUpload.id = vocationalReport?.document?.id ?? 0
            if(documentData != nil){
                uploadPretermReleaseToUpload.name = documentData?.name ?? ""
                uploadPretermReleaseToUpload.location = documentData?.location ?? ""
            }else{
                uploadPretermReleaseToUpload.name = vocationalReport?.document?.name ?? ""
                uploadPretermReleaseToUpload.location = vocationalReport?.document?.location ?? ""
            }
            uploadPretermReleaseToUpload.type = 23
            intakeResult.document = uploadPretermReleaseToUpload.convertToDictionary()
        }
        
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    func getSupervisionObervationData() -> [String: Any]{
        
        var intakeResult: SupervisionObservartionAnswerModelData! = SupervisionObservartionAnswerModelData()
        intakeResult.id = self.observationReport?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "child_behaviour".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_behaviour = answer
            }
            if questionID == "physical_health_status".replacingOccurrences(of: "_", with: ""){
                intakeResult.physical_health_status = answer
            }
            if questionID == "relationship_with_family".replacingOccurrences(of: "_", with: ""){
                intakeResult.relationship_with_family = answer
            }
            if questionID == "saftey_in_family".replacingOccurrences(of: "_", with: ""){
                intakeResult.saftey_in_family = answer
            }
            if questionID == "difficulties_faced_by_child".replacingOccurrences(of: "_", with: ""){
                intakeResult.difficulties_faced_by_child = answer
            }
            if questionID == "difficulties_faced_by_family".replacingOccurrences(of: "_", with: ""){
                intakeResult.difficulties_faced_by_family = answer
            }
            if questionID == "household_change".replacingOccurrences(of: "_", with: ""){
                intakeResult.household_change = answer
            }
            if questionID == "vacational_training".replacingOccurrences(of: "_", with: ""){
                intakeResult.vacational_training = answer
            }
            if questionID == "child_engagement".replacingOccurrences(of: "_", with: ""){
                intakeResult.child_engagement = answer
            }
            if questionID == "last_engagement".replacingOccurrences(of: "_", with: ""){
                intakeResult.last_engagement = answer
            }
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    
    func getSupervisionPreliminaryData() -> [String: Any]{
        
        var intakeResult: SupervisionPreliminaryAnswerModelData! = SupervisionPreliminaryAnswerModelData()
        intakeResult.id = self.preliminaryReport?.id ?? 0
        for answerList in inspectionQuestionListArray!{
            let questionID = answerList.id?.lowercased()
            var answer = ""
            if answerList.type == "singleTxtBox" || answerList.type == "singleTxtArea"{
                answer = answerList.fieldValue1 ?? ""
            }else if answerList.type == "radio"{
                
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        if answerList.options!.count == 2{
                            if i == 0{
                                answer = "1"
                            }else{
                                answer = "-1"
                            }
                        }else{
                            answer = "\(i + 1)"//"\(i)"
                        }
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "dropDown"{
                for i in 0..<answerList.options!.count{
                    if answerList.optionValue == answerList.options![i]{
                        answer = "\(i)"
                    }
                }
                if answer == ""{
                    answer = answerList.optionValue ?? ""
                }
            }else if answerList.type == "checkBox"{
                if answerList.optionValue == "false" || answerList.optionValue == "-1"{
                    answer = "-1"
                }else{
                    answer = "1"
                }
            }else{
                answer = answerList.optionValue ?? ""
            }
            
            if questionID == "visit_date".replacingOccurrences(of: "_", with: ""){
                intakeResult.visit_date = answer
            }
            if questionID == "parent_name".replacingOccurrences(of: "_", with: ""){
                intakeResult.parent_name = answer
            }
            if questionID == "probation_officer_interacted_with".replacingOccurrences(of: "_", with: ""){
                intakeResult.probation_officer_interacted_with = answer
            }
            
        }
        
        let addedData = intakeResult.map { $0.convertToDictionary() }!
        print(addedData)
        return addedData
    }
    
    @IBAction func updateReportClicked(_ sender: Any) {
        
        var postUrl = ""
        var isGuardianTextBoxSelected = false
        var postData: [String: Any] = [:]
        
        var icpHttpMethodType = "POST"
        
        if reportType == "intake" || reportType == "transfer"{
            
            icpHttpMethodType = "PUT"
            if reportType == "transfer"{
                postUrl = "report/transfer/\(self.aliasId)"
            }else{
                postUrl = "report/intake/\(self.aliasId)"
            }
            
            for i in self.inspectionQuestionListArray!{
                
                if i.id == "firstName" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child First Name", parentView: self)
                    return
                }
                
                if i.id == "dob" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Date Of Birth", parentView: self)
                    return
                }
                
                if i.id == "aadharNumber"{
                    
                    if i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Aadhar number", parentView: self)
                        return
                    }
                    else if i.fieldValue1?.count != 12{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter valid Aadhar number", parentView: self)
                        return
                    }
                    
                }
                
                if i.id == "genderName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child Gender", parentView: self)
                    return
                }
                
                if i.id == "countryName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child Nationality", parentView: self)
                    return
                }
                
                if i.id == "religionName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child Religion", parentView: self)
                    return
                }
                
                if i.id == "communityName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child Community", parentView: self)
                    return
                }
                
                if i.id == "addressLine1" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Present Residential Address Line 1", parentView: self)
                    return
                }
                
                if i.id == "addressLine2" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Present Residential Address Line 2", parentView: self)
                    return
                }
                
//                if i.id == "landmark" && i.fieldValue1 == ""{
//                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Landmark", parentView: self)
//                    return
//                }
                
                if i.id == "city" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child City", parentView: self)
                    return
                }
                
                if i.id == "zipcode" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child PinCode", parentView: self)
                    return
                }
                
                if i.id == "stateName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child State", parentView: self)
                    return
                }
                
                if i.id == "districtName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child District", parentView: self)
                    return
                }
                
                if i.id == "talukName" && i.optionValue == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please select Child Taluk", parentView: self)
                    return
                }
                
                
                if i.id == "summary" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Summary", parentView: self)
                    return
                }
                
                if i.id == "isGuardianAvailable" && i.optionValue == "true"{
                    
                    isGuardianTextBoxSelected = true
                }
                
                if isGuardianTextBoxSelected{
                    
                    // GUARDIAN SELECTED (validates their fields)
                    if i.id == "guardianType" && (i.optionValue == "0" || i.optionValue == "Please Select a relation"){
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian Relationship", parentView: self)
                        return
                    }else if i.id == "guardianFirstName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian First name", parentView: self)
                        return
                    }
                    else if i.id == "guardianDob" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian Date Of Birth", parentView: self)
                        return
                    }
                    else if i.id == "guardianMobile" && i.fieldValue1 != ""{
                        
                        if !Utility.isValidPhoneNumber(phoneNumber: i.fieldValue1 ?? "") {
                            ServiceModel().showAlert(title: "Alert", message: "Please enter a valid Guardian phone number.", parentView: self)
                            return
                        }
                    }
                    else if i.id == "guardianAddressLine1" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian Address Line 1", parentView: self)
                        return
                    }
                    else if i.id == "guardianAddressLine2" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian Address Line 2", parentView: self)
                        return
                    }
//                    else if i.id == "guardianLandmark" && i.fieldValue1 == ""{
//                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian Landmark", parentView: self)
//                        return
//                    }
                    else if i.id == "guardianCity" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian City", parentView: self)
                        return
                    }
                    else if i.id == "guardianZipcode" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian Pincode", parentView: self)
                        return
                    }
                    else if i.id == "guardianCountryId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian Country", parentView: self)
                        return
                    }
                    
                    else if i.id == "guardianStateId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian State", parentView: self)
                        return
                    }
                    else if i.id == "guardianDistrictId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian District", parentView: self)
                        return
                    }
                    else if i.id == "guardianTalukId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian Taluk", parentView: self)
                        return
                    }
                }
            }
            
            postData = getIntakeTypePostData()
            
        }else if reportType.contains("icp"){
            
            if icpSelectedMenu == 0{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "isChildInInstitution"{
                        
                        if i.optionValue?.lowercased() == "yes" && (i.fieldValue1 == "" ||  i.fieldValue2 == ""){
                            
                            if i.fieldValue1 == ""{
                                ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                                return
                            }else{
                                ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion2 ?? "")", parentView: self)
                                return
                            }
                        }
                    }
                    
                    
                    if i.id == "typeOfStay" && (i.optionValue == "" || i.optionValue == "0"){
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "isSavingsdetailAvailable"{
                        
                        if i.optionValue?.lowercased() == "yes" && i.fieldValue1 == ""{
                            
                            ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                            return
                        }
                    }
                    
                    if i.id == "isEarningsDetailAvailable"{
                        if i.optionValue?.lowercased() == "yes" && i.fieldValue1 == ""{
                            
                            ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                            return
                        }
                    }
                    
                    if i.id == "isAwardsDetailAvailable"{
                        if i.optionValue?.lowercased() == "yes" && i.fieldValue1 == ""{
                            
                            ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                            return
                        }
                    }
                    
                    if i.id == "placeOfInterview" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "dateOfInterview" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "progressFromDate"{
                        
                        if i.optionValue == "" {
                            ServiceModel().showAlert(title: "Alert", message: "Please enter Progress From Date", parentView: self)
                            return
                        }
                        
                        if i.fieldValue1 == ""  {
                            ServiceModel().showAlert(title: "Alert", message: "Please enter Progress To Date", parentView: self)
                            return
                        }
                    }
                    
                    if i.id == "proposedInterventions" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                }
                
                postUrl = "report/icp/\(self.aliasId)"
                postData = getIcpTypePostData()
                
                icpHttpMethodType = "PUT"
                
            }
            else if icpSelectedMenu == 1{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "calendarYear" && (i.optionValue == "" || i.optionValue == "0"){
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "monthFullName" && i.optionValue == "" {
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "supervisionCompletedAt" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                }
                
                postUrl = "report/icp/rehabiliation/\(self.aliasId)"
                postData = getIcpRehabilitationTypePostData()
            }
            else if icpSelectedMenu == 2{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "transferRelease" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "childPlacement" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "trainingSkills" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "document" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please upload PDF for \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "sponsorshipIndividual" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                }
                
                postUrl = "report/icp/prerelease/\(self.aliasId)"
                postData = getIcpPreReleaseTypePostData()
            }else{
                
                for i in self.inspectionQuestionListArray!{
                    
                    
                    if i.id == "bankStatus" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "childBelongings" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "otherChildBelongings"{
                        if i.optionValue?.lowercased() == "yes" && i.fieldValue1 == ""{
                            
                            ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                            return
                        }
                    }
                    
                    if i.id == "document" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please upload PDF for \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "admittedSchoolVaction" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "admittedSchoolVaction"{
                        
                        if i.optionValue?.lowercased() == "yes" && (i.fieldValue1 == "" ||  i.fieldValue2 == ""){
                            
                            if i.fieldValue1 == ""{
                                ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                                return
                            }else{
                                ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion2 ?? "")", parentView: self)
                                return
                            }
                        }
                    }
                    
                    if i.id == "childAdmitted" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "childOpinion" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "identityCompensation" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "birthCertificate" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "schoolCertificate" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "bplCard" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "disabilityCertificate" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "immunisationCard" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "rationCard" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "aadharCard" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "govermentCompensation" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    
                    
  
                }
                
                postUrl = "report/icp/postrelease/\(self.aliasId)"
                postData = getIcpPostReleaseTypePostData()
            }
        }else if reportType == "sir"{
            if supervisionIndex == 0{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "homeType" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "roomsCount" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "drinkingWater" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please Select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "bathroom" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please Select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "environment" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
//                    if i.id == "maritalType" && i.optionValue == ""{
//                        ServiceModel().showAlert(title: "Alert", message: "Please Select \(i.questionName ?? "")", parentView: self)
//                        return
//                    }
                    
                    if i.id == "maritalType"{
                        
                        if i.optionValue == ""{
                            ServiceModel().showAlert(title: "Alert", message: "Please Select \(i.questionName ?? "")", parentView: self)
                            return
                        }
                        
                        if i.optionValue?.lowercased() == "yes" && i.fieldValue1 == ""{
                            
                            ServiceModel().showAlert(title: "Alert", message: "Please enter your comment for \(i.questionName ?? "")", parentView: self)
                            return
                        }
                    }
                    
                    if i.id == "otherRelatives" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "attitudeOfReligion" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "homeEthicalCode" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "delinquencyFamilyRecord" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyOffenceCommitted" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "fatherMotherRelation" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "parentChildRelation" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "siblingChildRelation" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "otherFactors" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "economicStatus" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                }
                postUrl = "report/sir/\(self.aliasId)/condition"
                postData = getSirConditionData()
            }else if supervisionIndex == 1{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "pastMentalCondition" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "presentMentalCondition" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "pastPhysicalCondition" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "presentPhysicalCondition" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "habitsInterest" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "personalitytraits" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "companionsInfluence" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "truancyFromHome" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                }
                
                postUrl = "report/sir/\(self.aliasId)/history"
                postData = getSirHistoryData()
                
            }else if supervisionIndex == 2{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "educationLevel" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "lastSchool" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "reasonForLeaving" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "extraCurricular" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "punishmentInSchool" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                }
                
                postUrl = "report/sir/\(self.aliasId)/education"
                postData = getSirEducationData()
                
            }else if supervisionIndex == 3{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "vocationalDetail" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "trainingUndergone" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "reasonForLeaving" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "employmentAttitude" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "religiousBeliefs" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "parentalCare" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Parental care towards the child", parentView: self)
                        return
                    }
                    
                    if i.id == "neighbourReport" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                }
                
                
                postUrl = "report/sir/\(self.aliasId)/vocation"
                postData = getSirVocationData()
                
            }else if supervisionIndex == 4{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "behaviourObserved" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "physicalCondition" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "intelligence" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "socialFactors" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "religiousFactors" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "suggestedCauses" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "reasonsForOffence" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "expertsConsulted" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "recommendation" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                }
                
                postUrl = "report/sir/\(self.aliasId)/inquiry"
                postData = getSirInquiryData()
            }
            else{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "familyFirstName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyLastName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyRelationship" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyGenderName" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyHealthStatus" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyDisability" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.fieldId1?.lowercased() == "familydisablitydetail"  && i.fieldValue1 == "" && i.optionValue == "Yes"{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.fieldQuestion1 ?? "")", parentView: self)
                    }
                    
                    if i.id == "familySocialHabits" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyContactNumber" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "familyContactNumber" && i.fieldValue1 != ""{
                        if !Utility.isValidPhoneNumber(phoneNumber: i.fieldValue1 ?? "") {
                            ServiceModel().showAlert(title: "Alert", message: "Please enter a valid Phone number.", parentView: self)
                            return
                        }
                    }
                    
                }
                
                postUrl = "report/sir/\(self.aliasId)/family"
                postData = getSIRFamilyMemberData()
            }
        }else{
            if supervisionIndex == 0{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "schoolName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "teacherName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                }
                
                postUrl = "report/supervision/\(self.aliasId)/vocational"
                postData = getSupervisionVocationData()
                
            }else if supervisionIndex == 1{
                
                for i in self.inspectionQuestionListArray!{
                    
                    if i.id == "visitDate" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "parentName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter \(i.questionName ?? "")", parentView: self)
                        return
                    }
                    
                    if i.id == "probationOfficerInteractedWith" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Probation Officer Interacted Details", parentView: self)
                        return
                    }
                }
                
                postUrl = "report/supervision/\(self.aliasId)/preliminary"
                postData = getSupervisionPreliminaryData()
            }else{
                postUrl = "report/supervision/\(self.aliasId)/observation"
                postData = getSupervisionObervationData()
            }
        }
        
        
        
        /*var isAllAnswerFilled = true
         
         let answerArray = Array(postData.values)
         
         for i in 0..<(answerArray.count){
         
         let answerValue = answerArray[i] as? String
         
         if answerValue == ""{
         
         isAllAnswerFilled = false
         break
         }
         }
         
         if isAllAnswerFilled == false{
         
         ErrorToast.show(message: "Please fill out all fields.", controller: self)
         return
         }*/
        
        
        
        if isAddFamily == true{
            
            self.submitPoReport(httpMethod: "POST", postUrl: postUrl, formAnswerModel: postData, isBackButtonPressed: false, isAddFamily: true)
        }else{
            
            // Submit Report
            let alert = UIAlertController(title: "Alert",
                                          message: "Do you want to submit your report?",
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                
                if self.reportType == "sir" || self.reportType == "supervision"{
                    self.submitPoReport(httpMethod: icpHttpMethodType, postUrl: postUrl, formAnswerModel: postData, isBackButtonPressed: false, isAddFamily: false)
                }else{
                    self.submitPoReport(httpMethod: icpHttpMethodType, postUrl: postUrl, formAnswerModel: postData, isBackButtonPressed: false, isAddFamily: false) //PUT for Basic details only, rest we need to use POST Anand Change
                }
                alert.dismiss(animated: true, completion: nil)
            })
            
            let cancelAction = UIAlertAction(title: "No",
                                             style: .cancel,
                                             handler: { (action) in
                
                // no action
                
                alert.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
    }
    
    
    
    @IBAction func submitReportClicked(_ sender: Any) {
        
        var postUrl = ""
        var isGuardianTextBoxSelected = false
        var postData: [String: Any] = [:]
        
        if reportType == "intake" || reportType == "transfer"{
            if reportType == "transfer"{
                postUrl = "report/transfer/\(self.aliasId)"
            }else{
                postUrl = "report/intake/\(self.aliasId)"
            }
            
            
            for i in self.inspectionQuestionListArray!{
                
//                if i.id == "aadharNumber" && i.fieldValue1 == ""{
//                    ServiceModel().showAlert(title: "Alert", message: "Please enter Aadhar number", parentView: self)
//                    return
//                }
                
                if i.id == "aadharNumber"{
                    
                    if i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter Child Aadhar number", parentView: self)
                        return
                    }
                    else if i.fieldValue1?.count != 12{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter valid Aadhar number", parentView: self)
                        return
                    }
                    
                }
                
                if i.id == "summary" && i.fieldValue1 == ""{
                    ServiceModel().showAlert(title: "Alert", message: "Please enter child summary", parentView: self)
                    return
                }
                
                
                if i.id == "isGuardianAvailable" && i.optionValue == "true"{
                    
                    isGuardianTextBoxSelected = true
                }
                
                if isGuardianTextBoxSelected{
                    
                    // GUARDIAN SELECTED (validates their fields)
                    if i.id == "guardianFirstName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian first name", parentView: self)
                        return
                    }
                    else if i.id == "guardianLastName" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian last name", parentView: self)
                        return
                    }
                    else if i.id == "guardianType" && (i.optionValue == "0" || i.optionValue == "Please Select a relation"){
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian relationship", parentView: self)
                        return
                    }
                    else if i.id == "guardianDob" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian date of birth", parentView: self)
                        return
                    }
//                    else if i.id == "guardianMobile"{
//
//                        if i.fieldValue1 == ""{
//                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian phone number", parentView: self)
//                            return
//                        }
//                        else if !Utility.isValidPhoneNumber(phoneNumber: i.fieldValue1 ?? "") {
//                            ServiceModel().showAlert(title: "Alert", message: "Please enter a valid Guardian phone number.", parentView: self)
//                            return
//                        }
//
//                    }
                    
                    if i.id == "guardianMobile" && i.fieldValue1 != ""{
                        if !Utility.isValidPhoneNumber(phoneNumber: i.fieldValue1 ?? "") {
                            ServiceModel().showAlert(title: "Alert", message: "Please enter a valid Guardian phone number.", parentView: self)
                            return
                        }
                    }
                    else if i.id == "guardianDesignation" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian designation", parentView: self)
                        return
                    }
                    else if i.id == "guardianAddressLine1" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian address line 1", parentView: self)
                        return
                    }
                    else if i.id == "guardianAddressLine2" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian address line 2", parentView: self)
                        return
                    }
//                    else if i.id == "guardianLandmark" && i.fieldValue1 == ""{
//                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian landmark", parentView: self)
//                        return
//                    }
                    else if i.id == "guardianCity" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian city", parentView: self)
                        return
                    }
                    else if i.id == "guardianZipcode" && i.fieldValue1 == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please enter Guardian pincode", parentView: self)
                        return
                    }
                    else if i.id == "guardianCountryId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian country", parentView: self)
                        return
                    }
                    
                    else if i.id == "guardianStateId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian state", parentView: self)
                        return
                    }
                    else if i.id == "guardianDistrictId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian district", parentView: self)
                        return
                    }
                    else if i.id == "guardianTalukId" && i.optionValue == ""{
                        ServiceModel().showAlert(title: "Alert", message: "Please select Guardian taluk", parentView: self)
                        return
                    }
                }
            }
            
            postData = getIntakeTypePostData()
        }else{
            if icpSelectedMenu == 0{
                postUrl = "report/icp/\(self.aliasId)"
                postData = getIcpTypePostData()
            }
            else if icpSelectedMenu == 1{
                postUrl = "report/icp/rehabiliation/\(self.aliasId)"
                postData = getIcpRehabilitationTypePostData()
            }
            else if icpSelectedMenu == 2{
                postUrl = "report/icp/prerelease/\(self.aliasId)"
                postData = getIcpPreReleaseTypePostData()
            }else{
                postUrl = "report/icp/postrelease/\(self.aliasId)"
                postData = getIcpPostReleaseTypePostData()
            }
            
        }
        
        // Submit Report
        let alert = UIAlertController(title: "Alert",
                                      message: "Do you want to submit your report?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            
            self.submitPoReport(httpMethod: "POST", postUrl: postUrl, formAnswerModel: postData, isBackButtonPressed: false, isAddFamily: false)
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .cancel,
                                         handler: { (action) in
            
            // no action
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showReportSubmittedSuccessfullyPopup(){
        
        // Submit Report
        let alert = UIAlertController(title: "Report Submitted Successfully!",
                                      message: "Do you want to transfer The Child?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            
            // navigate to transfer child screen
            let transferChildVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "transferchildvc") as! TransferChildVC
            
            self.navigationController?.pushViewController(transferChildVC, animated: true)
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .cancel,
                                         handler: { (action) in
            
            // no action
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    func showErrorAlertForIncompleteFormSubmission(missedQuestionCount: Int){
        
        var questionText = ""
        
        if missedQuestionCount == 1{
            questionText = "question"
        }else{
            questionText = "questions"
        }
        
        // error alert
        let alert = UIAlertController(title: "Alert",
                                      message: "You have missed \(missedQuestionCount) \(questionText). Do you want to submit this form?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "YES", style: .default, handler: { (action) in
            
            
            // call api to save inspection
            //self.submitReport(formId: self.selectedInspectionForm?.formID ?? 0, formAnswerModel: postData, isBackButtonPressed: false)
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "NO",
                                         style: .cancel,
                                         handler: { (action) in
            
            // no action
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}


// MARK: - Private Methods
extension CreateNewReportVC {
    
    ///configuring initial view
    private func setInitialUI() {
        
        // registering cell
        self.registerCell(tableView: tblQuestionList)
        
        // reload table view
        tblQuestionList.reloadData()
    }
}

// MARK: - Table Delegate and Data Source
extension CreateNewReportVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reportType == "transfer"{
            return (self.inspectionQuestionListArray?.count ?? 0) + 1
        }else{
            return self.inspectionQuestionListArray?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if reportType == "transfer" && indexPath.row == (self.inspectionQuestionListArray?.count ?? 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: TransferReportPOChildTableViewCell.reuseIdentifier) as! TransferReportPOChildTableViewCell
            
            cell.delegate = self
            if self.isTransfer == 1{
                cell.showView()
                cell.selectView(imageView: cell.option1image)
                cell.deselectView(imageView: cell.option2Image)
            }else{
                cell.deselectView(imageView: cell.option1image)
                cell.selectView(imageView: cell.option2Image)
                cell.hideView()
            }
            cell.optionsValue = self.transferOptionValue
            for i in self.transferDistrictData{
                if i.id == self.cwcDistrictId{
                    cell.option3Lbl.text = i.name
                }
            }
            if isComplete{
                cell.contentView.isUserInteractionEnabled = false
            }
            else{
                cell.contentView.isUserInteractionEnabled = true
            }
            
            return cell
        }else{
            
            let data = inspectionQuestionListArray![indexPath.row]
            
            switch data.type?.lowercased() {
            case "label":
                let cell = tableView.dequeueReusableCell(withIdentifier: OnlyLblPoTableViewCell.reuseIdentifier) as! OnlyLblPoTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                return cell
            case "singletxtbox":
                let cell = tableView.dequeueReusableCell(withIdentifier: SingleTextBoxPoTableViewCell.reuseIdentifier) as! SingleTextBoxPoTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                cell.textfieldAnswer.text = data.fieldValue1
                cell.delegate = self
                cell.id = data.id!
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
                
                
            case "singletxtarea":
                let cell = tableView.dequeueReusableCell(withIdentifier: SingleTextAreaPoTableViewCell.reuseIdentifier) as! SingleTextAreaPoTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                if data.fieldValue1 == ""{
                    cell.textfieldAnswer.text = "Enter your details"
                    cell.textfieldAnswer.textColor = UIColor.lightGray
                }else{
                    cell.textfieldAnswer.text = data.fieldValue1
                    cell.textfieldAnswer.textColor = UIColor.label
                }
                
                cell.delegate = self
                cell.id = data.id!
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
                
            case "upload":
                let cell = tableView.dequeueReusableCell(withIdentifier: UploadPDFPOTableViewCell.reuseIdentifier) as! UploadPDFPOTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                if data.optionValue == ""{
                    cell.lblAnswer.text = "Only PDF is allowed for upload"
                }else{
                    cell.lblAnswer.text = data.optionValue
                }
                cell.delegate = self
                cell.id = data.id!
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
                
            case "radio":
                if data.options!.count == 2{
                    let cell = tableView.dequeueReusableCell(withIdentifier: RadioWith2OptionsTableViewCell.reuseIdentifier) as! RadioWith2OptionsTableViewCell
                    
                    if data.isMandate == true{
                        let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                        let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                        questionAttriburedString.append(asterix)
                        
                        cell.lblQuestionTitle.attributedText = questionAttriburedString
                    }else{
                        cell.lblQuestionTitle.text = data.questionName
                    }
                    
                    cell.option1Lbl.text = data.options?.first
                    cell.option2Lbl.text = data.options?.last
                    cell.optionSelected = data.optionValue ?? ""
                    cell.dropDownOptions = data.fieldOption1
                    cell.fieldId1 = data.fieldId1 ?? ""
                    cell.fieldId2 = data.fieldId2 ?? ""
                    if cell.optionSelected == ""{
                        cell.deselectView(imageView:  cell.option2Image)
                        cell.deselectView(imageView:  cell.option1image)
                    }else{
                        if  cell.optionSelected ==  cell.option1Lbl.text ||  cell.optionSelected == "Yes" || cell.optionSelected == "1"{
                            cell.selectView(imageView:  cell.option1image)
                            cell.deselectView(imageView:  cell.option2Image)
                        }else{
                            cell.deselectView(imageView:  cell.option1image)
                            cell.selectView(imageView:  cell.option2Image)
                        }
                    }
                    
                    if cell.optionSelected == "1" || cell.optionSelected == "Yes"{
                        if data.fieldId1 != nil && data.fieldId2 != nil{
                            cell.showTwoTextField()
                            cell.lblQuestion1.text = data.fieldQuestion1 ?? ""
                            cell.lblQuestion2.text = data.fieldQuestion2 ?? ""
                            if data.fieldOption1?.count ?? 0 > 0{
                                if data.fieldValue1 == ""{
                                    cell.selectionViewAnser.text = data.fieldOption1?.first
                                }else{
                                    cell.selectionViewAnser.text = data.fieldValue1 ?? ""
                                }
                            }else{
                                cell.lblAnswer1.text = data.fieldValue1 ?? ""
                            }
                            cell.lblAnswer2.text = data.fieldValue2 ?? ""
                            cell.viewLbl.text = data.fieldValue2 ?? ""
                        }else{
                            if data.fieldId1 != nil{
                                cell.showOneTextField()
                                cell.lblQuestion1.text = data.fieldQuestion1 ?? ""
                                cell.lblQuestion2.text = ""
                                if data.fieldOption1?.count ?? 0 > 0{
                                    if data.fieldValue1 == ""{
                                        cell.selectionViewAnser.text = data.fieldOption1?.first
                                    }else{
                                        cell.selectionViewAnser.text = data.fieldValue1 ?? ""
                                    }
                                }else{
                                    cell.lblAnswer1.text = data.fieldValue1 ?? ""
                                }
                                cell.lblAnswer2.text = ""
                                cell.viewLbl.text = ""
                                
                            }else{
                                cell.hideAllTextField()
                                cell.lblQuestion1.text = ""
                                cell.lblQuestion2.text = ""
                                cell.lblAnswer1.text = ""
                                cell.lblAnswer2.text = ""
                                cell.viewLbl.text = ""
                                cell.selectionViewAnser.text = ""
                            }
                        }
                        
                        if data.fieldOption1?.count ?? 0 > 0{
                            cell.hideTextField()
                        }else{
                            cell.showTextField()
                        }
                        
                    }else{
                        cell.hideAllTextField()
                    }
                    cell.delegate = self
                    cell.id = data.id!
                    
                    if isComplete{
                        cell.contentView.isUserInteractionEnabled = false
                    }
                    else{
                        cell.contentView.isUserInteractionEnabled = true
                    }
                    
                    return cell
                }else if data.options!.count == 3{
                    let cell = tableView.dequeueReusableCell(withIdentifier: RadioWith3OptionsTableViewCell.reuseIdentifier) as! RadioWith3OptionsTableViewCell
                    
                    if data.isMandate == true{
                        let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                        let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                        questionAttriburedString.append(asterix)
                        
                        cell.lblQuestionTitle.attributedText = questionAttriburedString
                    }else{
                        cell.lblQuestionTitle.text = data.questionName
                    }
                    
                    cell.option1Lbl.text = data.options?[0] ?? ""
                    cell.option2Lbl.text = data.options?[1] ?? ""
                    cell.option3Lbl.text = data.options?[2] ?? ""
                    cell.optionSelected = data.optionValue ?? ""
                    
                    if cell.optionSelected == ""{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView:  cell.option2Image)
                        cell.deselectView(imageView:  cell.option3image)
                    }else{
                        
                    if cell.optionSelected == cell.option1Lbl.text || cell.optionSelected == "1"{
                        cell.selectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                    }else if cell.optionSelected == cell.option2Lbl.text || cell.optionSelected == "2"{
                        cell.deselectView(imageView: cell.option1image)
                        cell.selectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                    }else{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        
                        if cell.optionSelected == cell.option3Lbl.text || cell.optionSelected == "3"{
                            cell.selectView(imageView: cell.option3image)
                        }
                        else{
                            cell.deselectView(imageView: cell.option3image)
                        }
                    }
                    }
                    cell.delegate = self
                    
                    cell.id = data.id!
                    
                    if isComplete{
                        cell.contentView.isUserInteractionEnabled = false
                    }
                    else{
                        cell.contentView.isUserInteractionEnabled = true
                    }
                    
                    return cell
                }else if data.options!.count == 4{
                    let cell = tableView.dequeueReusableCell(withIdentifier: RadioWith4OptionsTableViewCell.reuseIdentifier) as! RadioWith4OptionsTableViewCell
                    
                    if data.isMandate == true{
                        let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                        let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                        questionAttriburedString.append(asterix)
                        
                        cell.lblQuestionTitle.attributedText = questionAttriburedString
                    }else{
                        cell.lblQuestionTitle.text = data.questionName
                    }
                    
                    cell.option1Lbl.text = data.options?[0] ?? ""
                    cell.option2Lbl.text = data.options?[1] ?? ""
                    cell.option3Lbl.text = data.options?[2] ?? ""
                    cell.option4Lbl.text = data.options?[3] ?? ""
                    
                    cell.optionSelected = data.optionValue ?? ""
                    
                    cell.fieldId1 = data.fieldId1 ?? ""
                    
                    if cell.optionSelected == ""{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView:  cell.option2Image)
                        cell.deselectView(imageView:  cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                    }else{
                    
                    if cell.optionSelected == cell.option1Lbl.text || cell.optionSelected == "1"{
                        cell.selectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                    }else if cell.optionSelected == cell.option2Lbl.text || cell.optionSelected == "2"{
                        cell.deselectView(imageView: cell.option1image)
                        cell.selectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                    }else if cell.optionSelected == cell.option3Lbl.text || cell.optionSelected == "3"{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.selectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                    }else{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        
                        if cell.optionSelected == cell.option4Lbl.text || cell.optionSelected == "4"{
                            cell.selectView(imageView: cell.option4image)
                        }
                        else{
                            cell.deselectView(imageView: cell.option4image)
                        }
                        
                    }
                    }
                    
                    if cell.optionSelected == "1" || cell.optionSelected == "Daily" || cell.optionSelected == "2" || cell.optionSelected == "Weekly" || cell.optionSelected == "3" || cell.optionSelected == "Month"{
                        if data.fieldId1 != nil{
                            cell.showOneTextField()
                            cell.lblQuestion1.text = data.fieldQuestion1 ?? ""
                            cell.lblAnswer1.text = data.fieldValue1 ?? ""
                            
                        }else{
                            cell.hideTextField()
                            cell.lblQuestion1.text = ""
                            cell.lblAnswer1.text = ""
                        }
                    }else{
                        cell.hideTextField()
                    }
                    
                    cell.delegate = self
                    cell.id = data.id!
                    
                    if isComplete{
                        cell.contentView.isUserInteractionEnabled = false
                    }
                    else{
                        cell.contentView.isUserInteractionEnabled = true
                    }
                    
                    return cell
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: RadioWith5OptionsTableViewCell.reuseIdentifier) as! RadioWith5OptionsTableViewCell
                    
                    if data.isMandate == true{
                        let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                        let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                        questionAttriburedString.append(asterix)
                        
                        cell.lblQuestionTitle.attributedText = questionAttriburedString
                    }else{
                        cell.lblQuestionTitle.text = data.questionName
                    }
                    
                    cell.option1Lbl.text = data.options?[0] ?? ""
                    cell.option2Lbl.text = data.options?[1] ?? ""
                    cell.option3Lbl.text = data.options?[2] ?? ""
                    cell.option4Lbl.text = data.options?[3] ?? ""
                    cell.option5Lbl.text = data.options?[4] ?? ""
                    
                    cell.optionSelected = data.optionValue ?? ""
                    
                    if cell.optionSelected == ""{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                        cell.deselectView(imageView: cell.option5image)
                    }else{
                    
                    if cell.optionSelected == cell.option1Lbl.text || cell.optionSelected == "1"{
                        cell.selectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                        cell.deselectView(imageView: cell.option5image)
                    }else if cell.optionSelected == cell.option2Lbl.text || cell.optionSelected == "2"{
                        cell.deselectView(imageView: cell.option1image)
                        cell.selectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                        cell.deselectView(imageView: cell.option5image)
                    }else if cell.optionSelected == cell.option3Lbl.text || cell.optionSelected == "3"{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.selectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                        cell.deselectView(imageView: cell.option5image)
                    }else if cell.optionSelected == cell.option4Lbl.text || cell.optionSelected == "4"{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.selectView(imageView: cell.option4image)
                        cell.deselectView(imageView: cell.option5image)
                    }else{
                        cell.deselectView(imageView: cell.option1image)
                        cell.deselectView(imageView: cell.option2Image)
                        cell.deselectView(imageView: cell.option3image)
                        cell.deselectView(imageView: cell.option4image)
                        cell.selectView(imageView: cell.option5image)
                        
                        if cell.optionSelected == cell.option5Lbl.text || cell.optionSelected == "5"{
                            cell.selectView(imageView: cell.option5image)
                        }
                        else{
                            cell.deselectView(imageView: cell.option5image)
                        }
                    }
                    }
                    cell.delegate = self
                    cell.id = data.id!
                    
                    if isComplete{
                        cell.contentView.isUserInteractionEnabled = false
                    }
                    else{
                        cell.contentView.isUserInteractionEnabled = true
                    }
                    
                    return cell
                }
                
            case "dropdown":
                let cell = tableView.dequeueReusableCell(withIdentifier: DropDownPoTableViewCell.reuseIdentifier) as! DropDownPoTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                if Int(data.optionValue ?? "") != nil && Int(data.optionValue ?? "") ?? 0 < (data.options?.count ?? 0){
                    
                    if data.id == "guardianType"{
                        
                        if data.optionValue == "0"{
                            cell.dateAnswerLbl.text = "Please Select a relation"
                        }
                        else if data.optionValue == "1"{
                            cell.dateAnswerLbl.text = "Father only"
                        }
                        else if data.optionValue == "2"{
                            cell.dateAnswerLbl.text = "Mother only"
                        }
                        else if data.optionValue == "4"{
                            cell.dateAnswerLbl.text = "Parents(Father and Mother)"
                        }
                        else if data.optionValue == "5"{
                            cell.dateAnswerLbl.text = "Uncle(Guardian)"
                        }
                        else if data.optionValue == "6"{
                            cell.dateAnswerLbl.text = "Aunty(Guardian)"
                        }
                        else if data.optionValue == "7"{
                            cell.dateAnswerLbl.text = "Grandpa(Guardian)"
                        }
                        else if data.optionValue == "8"{
                            cell.dateAnswerLbl.text = "Grandma(Guardian)"
                        }
                    }
                    else{
                 
                        cell.dateAnswerLbl.text = data.options?[Int(data.optionValue ?? "")!]
                    }
                }else{
                    cell.dateAnswerLbl.text = data.optionValue
                }
                
                if data.id == "guardianCountryId"{
                    print(data.options)
                }
                
                if data.id == "guardianStateId"{
                    print(data.options)
                }
                cell.dropDownOptions = data.options
                cell.delegate = self
                cell.id = data.id!
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
            case "checkbox":
                let cell = tableView.dequeueReusableCell(withIdentifier: CheckBoxPoTableViewCell.reuseIdentifier) as! CheckBoxPoTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                cell.isSelect = data.optionValue ?? "false"
                cell.delegate = self
                cell.id = data.id!
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
            case "datepicker":
                let cell = tableView.dequeueReusableCell(withIdentifier: DatepickerPOTableViewCell.reuseIdentifier) as! DatepickerPOTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                if data.fieldId1 != "" && data.fieldId1 != nil{
                    cell.showDoubleDateField()
                }else{
                    cell.showSingleDateField()
                }
                
                if data.optionValue != nil && data.optionValue != ""{
                    cell.dateAnswerLbl.text = data.optionValue
                }
                cell.delegate = self
                cell.id = data.id!
                cell.fieldId1 = data.fieldId1 ?? ""
                if data.fieldValue1 != nil && data.fieldValue1 != ""{
                    cell.dateAnswerLbl2.text = data.fieldValue1
                }
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                
                return cell
            case "bluelbl":
                let cell = tableView.dequeueReusableCell(withIdentifier: OnlyLblPoTableViewCell.reuseIdentifier) as! OnlyLblPoTableViewCell
                
                if data.isMandate == true{
                    let questionAttriburedString = NSMutableAttributedString(string: data.questionName ?? "")
                    let asterix = NSAttributedString(string: " *", attributes: [.foregroundColor: UIColor.red])
                    questionAttriburedString.append(asterix)
                    
                    cell.lblQuestionTitle.attributedText = questionAttriburedString
                }else{
                    cell.lblQuestionTitle.text = data.questionName
                }
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: SingleTextBoxPoTableViewCell.reuseIdentifier) as! SingleTextBoxPoTableViewCell
                
                if isComplete{
                    cell.contentView.isUserInteractionEnabled = false
                }
                else{
                    cell.contentView.isUserInteractionEnabled = true
                }
                
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
}

extension CreateNewReportVC: AnswerDataDelegate{
    
    func optionSelected(id: String, value: String) {
        
        if id == "transfer"{
            isTransfer = Int(value)!
        }
        if id == "transfercwc"{
            self.cwcDistrictId = self.transferDistrictData[Int(value)!].id!
        }
        selectedIndex = -1
        var fieldIdSelected = 0
        for i in 0..<self.inspectionQuestionListArray!.count{
            let questionId = self.inspectionQuestionListArray![i].id
            let fieldId1: String = self.inspectionQuestionListArray![i].fieldId1 ?? ""
            let fieldId2: String = self.inspectionQuestionListArray![i].fieldId2 ?? ""
            
            if id == fieldId1{
                selectedIndex = i
                fieldIdSelected = 1
            }
            
            if id == fieldId2 {
                selectedIndex = i
                fieldIdSelected = 2
            }
            
            if id == questionId{
                selectedIndex = i
            }
        }
        
        if selectedIndex != -1{
            
            let questionType = self.inspectionQuestionListArray![selectedIndex].type
            
            if questionType == "singleTxtBox" || questionType == "singleTxtArea"{
                self.inspectionQuestionListArray![selectedIndex].fieldValue1 = "\(value)"
            }else if questionType == "upload"{
                uploadTypeIndex = selectedIndex
                let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)
                documentPicker.delegate = self
                documentPicker.allowsMultipleSelection = true
                documentPicker.modalPresentationStyle = .overCurrentContext
                present(documentPicker, animated: true, completion: nil)
            }else if questionType == "datePicker"{
                let fieldId1 = self.inspectionQuestionListArray![selectedIndex].fieldId1
                if id == fieldId1{
                    self.fieldId1Selected = true
                }
                let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
                nextVC.modalPresentationStyle = .overCurrentContext
                nextVC.delegate = self
                nextVC.passingTitle = "SELECT DATE"
                nextVC.passingVisitDate = value
                nextVC.isPicker = "dob_date"
                self.present(nextVC, animated: false, completion: nil)
            }else if questionType == "checkBox"{
                if value == "true"{
                    for i in tempInspectionQuestionListArray!{
                        inspectionQuestionListArray?.append(i)
                    }
                }else{
                    for i in stride(from: self.inspectionQuestionListArray!.count - 1, to: selectedIndex, by: -1){
                        self.inspectionQuestionListArray?.remove(at: i)
                    }
                }
                self.inspectionQuestionListArray![selectedIndex].optionValue = "\(value)"
            }else if questionType == "radio"{
                if fieldIdSelected == 0{
                    self.inspectionQuestionListArray![selectedIndex].optionValue = "\(value)"
                }else if fieldIdSelected == 1{
                    self.inspectionQuestionListArray![selectedIndex].fieldValue1 = "\(value)"
                }else{
                    self.fieldId2Selected = true
                    let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
                    nextVC.modalPresentationStyle = .overCurrentContext
                    nextVC.delegate = self
                    nextVC.passingTitle = "SELECT VISIT DATE"
                    nextVC.passingVisitDate = value
                    nextVC.isPicker = "visit_date"
                    self.present(nextVC, animated: false, completion: nil)
                }
            }else if questionType == "dropDown"{
                self.inspectionQuestionListArray![selectedIndex].optionValue = "\(value)"
                
                if reportType == "intake" || reportType == "transfer"{
                    if self.inspectionQuestionListArray![selectedIndex].id == "countryName" || self.inspectionQuestionListArray![selectedIndex].id == "guardianCountryId"{
                        
                        if self.inspectionQuestionListArray![selectedIndex].id == "countryName"{
                            
                            self.isGuardianSelected = false
                            
                            var cid = 0
                            for i in countryData{
                                if i.fullName == "\(value)"{
                                    cid = i.id!
                                }
                            }
                            
                            for i in 0..<self.inspectionQuestionListArray!.count{
                                if self.inspectionQuestionListArray![i].id == "stateName" || self.inspectionQuestionListArray![i].id == "districtName" || self.inspectionQuestionListArray![i].id == "talukName"{
                                    self.inspectionQuestionListArray![i].options = []
                                    self.inspectionQuestionListArray![i].optionValue = ""
                                }
                            }
                            
                            self.getCountryRelatedDetail(postUrl: "master/state?country_id=\(cid)")
                        }else{
                            
                            self.isGuardianSelected = true
                            
                            var cid = 0
                            for i in countryDataGuardian{
                                if i.fullName == "\(value)"{
                                    cid = i.id!
                                }
                            }
                            
                            for i in 0..<self.inspectionQuestionListArray!.count{
                                if  self.inspectionQuestionListArray![i].id == "guardianStateId" || self.inspectionQuestionListArray![i].id == "guardianDistrictId" || self.inspectionQuestionListArray![i].id == "guardianTalukId"{
                                    self.inspectionQuestionListArray![i].options = []
                                    self.inspectionQuestionListArray![i].optionValue = ""
                                }
                            }
                            
                            self.getCountryRelatedDetail(postUrl: "master/state?country_id=\(cid)")
                        }
                        
                    }else if self.inspectionQuestionListArray![selectedIndex].id == "stateName" || self.inspectionQuestionListArray![selectedIndex].id == "guardianStateId"{
                        
                        if self.inspectionQuestionListArray![selectedIndex].id == "stateName"{
                            
                            self.isGuardianSelected = false
                            
                            var cid = 0
                            for i in stateData{
                                if i.name == "\(value)"{
                                    cid = i.id!
                                }
                            }
                            
                            
                            for i in 0..<self.inspectionQuestionListArray!.count{
                                if self.inspectionQuestionListArray![i].id == "districtName" || self.inspectionQuestionListArray![i].id == "talukName"{
                                    self.inspectionQuestionListArray![i].options = []
                                    self.inspectionQuestionListArray![i].optionValue = ""
                                }
                            }
                            
                            self.getCountryRelatedDetail(postUrl: "master/district?state_id=\(cid)")
                            
                        }
                        else{
                            
                            self.isGuardianSelected = true
                            
                            var cid = 0
                            for i in stateDataGuardian{
                                if i.name == "\(value)"{
                                    cid = i.id!
                                }
                            }
                            for i in 0..<self.inspectionQuestionListArray!.count{
                                if  self.inspectionQuestionListArray![i].id == "guardianDistrictId" || self.inspectionQuestionListArray![i].id == "guardianTalukId"{
                                    self.inspectionQuestionListArray![i].options = []
                                    self.inspectionQuestionListArray![i].optionValue = ""
                                }
                            }
                            
                            self.getCountryRelatedDetail(postUrl: "master/district?state_id=\(cid)")
                            
                        }
                        
                    }else if self.inspectionQuestionListArray![selectedIndex].id == "districtName" || self.inspectionQuestionListArray![selectedIndex].id == "guardianDistrictId"{
                        
                        if self.inspectionQuestionListArray![selectedIndex].id == "districtName"{
                            
                            self.isGuardianSelected = false
                            
                            var cid = 0
                            for i in districtData{
                                if i.name == "\(value)"{
                                    cid = i.id!
                                }
                            }
                            for i in 0..<self.inspectionQuestionListArray!.count{
                                if self.inspectionQuestionListArray![i].id == "talukName"{
                                    self.inspectionQuestionListArray![i].options = []
                                    self.inspectionQuestionListArray![i].optionValue = ""
                                }
                            }
                            
                            
                            self.getCountryRelatedDetail(postUrl: "master/taluk?district_id=\(cid)")
                        }
                        else{
                            
                            self.isGuardianSelected = true
                            
                            var cid = 0
                            for i in districtDataGuardian{
                                if i.name == "\(value)"{
                                    cid = i.id!
                                }
                            }
                            for i in 0..<self.inspectionQuestionListArray!.count{
                                if self.inspectionQuestionListArray![i].id == "guardianTalukId"{
                                    self.inspectionQuestionListArray![i].options = []
                                    self.inspectionQuestionListArray![i].optionValue = ""
                                }
                            }
                            
                            self.getCountryRelatedDetail(postUrl: "master/taluk?district_id=\(cid)")
                        }
                        
                    }
                    
                }
            }
            else{
                self.inspectionQuestionListArray![selectedIndex].optionValue = "\(value)"
            }
            
            if questionType != "upload"{
                self.tblQuestionList.reloadData()
            }
        }
        
        if id == "transfer"{
        
            self.tblQuestionList.reloadData()
        }        
        
    }
}

extension CreateNewReportVC: SelectDateDelegate{
    func selectVisit(date: String, isPicker: String) {
        if fieldId2Selected{
            self.inspectionQuestionListArray![selectedIndex].fieldValue2 = date
            fieldId2Selected = false
        }
        else{
            if selectedIndex != -1{
                if fieldId1Selected{
                    self.inspectionQuestionListArray![selectedIndex].fieldValue1 = date
                    fieldId1Selected = false
                }else{
                    self.inspectionQuestionListArray![selectedIndex].optionValue = date
                }
            }
        }
        
        DispatchQueue.main.async {
            self.tblQuestionList.reloadData()
        }
    }
}

extension CreateNewReportVC{
    
    func submitPoReport(httpMethod: String, postUrl: String, formAnswerModel: [String: Any], isBackButtonPressed: Bool, isAddFamily: Bool) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.submitPoQuestionsToServer(httpMethod: httpMethod, postUrl: postUrl, formAnswerModel: formAnswerModel) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                if isAddFamily == false{
                                    Toast.show(message: "Your report has been saved successfully!", controller: self)
                                }
                                else{
                                    Toast.show(message: "Family has been saved successfully!", controller: self)
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    
                                    // dismiss
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Failed to submit report!", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
    
    func scrollTableToFirstRow() {
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.tblQuestionList.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    
    
}


extension CreateNewReportVC{
    
    // Basic Details
    func getIcpBasicDetailsData(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getIntakeReportDetail(reportType: self.reportType, aliasId: self.aliasId) { responseStr in
                        
                        DispatchQueue.main.async { [self] in
                            
                            //                            self.intakeReportList?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let _ = responseStr {
                                self.intakeId = responseStr?.child?.id ?? 0
                                self.isTransfer = responseStr?.child?.isTransfered ?? 0
                                self.cwcDistrictId = responseStr?.child?.cwcDistrictID ?? 0
                                var selectedIndex = -1
                                
                                self.lblHeaderAadhaar.text = "Aadhar No : " + (responseStr?.child?.aadharNumber ?? "")
                                
                                for i in 0..<self.inspectionQuestionListArray!.count{
                                    let id = self.inspectionQuestionListArray![i].id
                                    
                                    let type = self.inspectionQuestionListArray![i].type
                                    
                                    let data = responseStr?.valueByPropertyName(name: id!)
                                    
                                    if type == "singleTxtBox" || type == "singleTxtArea"{
                                        
                                        self.inspectionQuestionListArray![i].fieldValue1 = "\(data!)"
                                    }
                                    else if type == "checkBox"{
                                        selectedIndex = i
                                        var check = ""
                                        if "\(data ?? "")" == ""{
                                            check = ""
                                        }else{
                                            
                                        if "\(data!)" == "-1" || "\(data!)" == "false"{
                                            check = "false"
                                        }else{
                                            check = "true"
                                        }
                                        }
                                        self.inspectionQuestionListArray![i].optionValue = check
                                    }
                                    else if type == "radio"{
                                        if self.inspectionQuestionListArray![i].options!.count == 3{
                                            self.inspectionQuestionListArray![i].optionValue = "\(data!)"
                                        }else{
                                            if self.inspectionQuestionListArray![i].fieldId1 != nil{
                                                let data1 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                                                self.inspectionQuestionListArray![i].fieldValue1 = "\(data1!)"
                                            }
                                            if self.inspectionQuestionListArray![i].fieldId2 != nil{
                                                let data2 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId2!)
                                                self.inspectionQuestionListArray![i].fieldValue2 = "\(data2!)"
                                            }
                                            var check = ""
                                            if "\(data ?? "")" == ""{
                                                check = ""
                                            }else{
                                            if "\(data!)" == "-1" || "\(data!)" == "false" || "\(data!)" == "0"{
                                                check = "No"
                                            }else{
                                                check = "Yes"
                                            }
                                            }
                                            self.inspectionQuestionListArray![i].optionValue = check
                                        }
                                        
                                    }else{
                                        if id == "progressFromDate"{
                                            let toData = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                                            self.inspectionQuestionListArray![i].fieldValue1 = "\(toData!)"
                                        }
                                        self.inspectionQuestionListArray![i].optionValue = "\(data!)"
                                    }
                                }
                                if selectedIndex != -1{
                                    self.tempInspectionQuestionListArray?.removeAll()
                                    for i in (selectedIndex + 1)..<self.inspectionQuestionListArray!.count{
                                        self.tempInspectionQuestionListArray?.append(self.inspectionQuestionListArray![i])
                                    }
                                    if self.inspectionQuestionListArray![selectedIndex].optionValue == "false" {
                                        for i in stride(from: self.inspectionQuestionListArray!.count - 1, to: selectedIndex, by: -1){
                                            self.inspectionQuestionListArray?.remove(at: i)
                                        }
                                    }
                                }
                                
                                self.tblQuestionList.reloadData()
                                
                            }
                            
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Intake Detail Data.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
    
    // Rehabilitation
    func getIcpRehabilitationData(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getICPRehabilitationReportDetail(reportType: self.reportType, aliasId: self.aliasId) { responseStr in
                        
                        DispatchQueue.main.async { [self] in
                            
                            //                            self.intakeReportList?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let _ = responseStr {
                                
                                self.rehabilitationModelData = responseStr
                                
                                for i in 0..<self.inspectionQuestionListArray!.count{
                                    let id = self.inspectionQuestionListArray![i].id
                                    
                                    let type = self.inspectionQuestionListArray![i].type
                                    
                                    let data = responseStr?.valueByPropertyName(name: id!)
                                    
                                    if type == "singleTxtBox" || type == "singleTxtArea"{
                                        
                                        self.inspectionQuestionListArray![i].fieldValue1 = "\(data!)"
                                    }
                                    else if type == "checkBox"{
                                        selectedIndex = i
                                        var check = ""
                                        if "\(data ?? "")" == ""{
                                            check = ""
                                        }else{
                                        if "\(data!)" == "-1" || "\(data!)" == "false"{
                                            check = "false"
                                        }else{
                                            check = "true"
                                        }
                                        }
                                        self.inspectionQuestionListArray![i].optionValue = check
                                    }
                                    else if type == "radio"{
                                        if self.inspectionQuestionListArray![i].options!.count == 3{
                                            self.inspectionQuestionListArray![i].optionValue = "\(data!)"
                                        }else{
                                            if self.inspectionQuestionListArray![i].fieldId1 != nil{
                                                let data1 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                                                self.inspectionQuestionListArray![i].fieldValue1 = "\(data1!)"
                                            }
                                            if self.inspectionQuestionListArray![i].fieldId2 != nil{
                                                let data2 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId2!)
                                                self.inspectionQuestionListArray![i].fieldValue2 = "\(data2!)"
                                            }
                                            var check = ""
                                            if "\(data ?? "")" == ""{
                                                check = ""
                                            }else{
                                            if "\(data!)" == "-1" || "\(data!)" == "false" || "\(data!)" == "0"{
                                                check = "No"
                                            }else{
                                                check = "Yes"
                                            }
                                            }
                                            self.inspectionQuestionListArray![i].optionValue = check
                                        }
                                        
                                    }else{
                                        self.inspectionQuestionListArray![i].optionValue = "\(data!)"
                                    }
                                }
                                if selectedIndex != -1{
                                    self.tempInspectionQuestionListArray?.removeAll()
                                    for i in (selectedIndex + 1)..<self.inspectionQuestionListArray!.count{
                                        self.tempInspectionQuestionListArray?.append(self.inspectionQuestionListArray![i])
                                    }
                                    if self.inspectionQuestionListArray![selectedIndex].optionValue == "false" {
                                        for i in stride(from: self.inspectionQuestionListArray!.count - 1, to: selectedIndex, by: -1){
                                            self.inspectionQuestionListArray?.remove(at: i)
                                        }
                                    }
                                }
                                
                                self.tblQuestionList.reloadData()
                                
                            }
                            
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Intake Detail Data.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
    
    
    
    
    // Pre-release
    func getIcpPreReleaseData(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getICPPreReleaseReportDetail(reportType: self.reportType, aliasId: self.aliasId) { responseStr in
                        
                        DispatchQueue.main.async { [self] in
                            
                            //                            self.intakeReportList?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let _ = responseStr {
                                
                                self.preReleaseModelData = responseStr
                                
                                for i in 0..<self.inspectionQuestionListArray!.count{
                                    let id = self.inspectionQuestionListArray![i].id
                                    
                                    let type = self.inspectionQuestionListArray![i].type
                                    
                                    print("Question Id = \(id ?? ""), and type = \(type ?? "")")
                                    
                                    let data = responseStr?.valueByPropertyName(name: id!)
                                    
                                    if type == "singleTxtBox" || type == "singleTxtArea"{
                                        
                                        self.inspectionQuestionListArray![i].fieldValue1 = "\(data!)"
                                    }
                                    else if type == "checkBox"{
                                        selectedIndex = i
                                        var check = ""
                                        if "\(data ?? "")" == ""{
                                            check = ""
                                        }else{
                                        if "\(data!)" == "-1" || "\(data!)" == "false"{
                                            check = "false"
                                        }else{
                                            check = "true"
                                        }
                                        }
                                        self.inspectionQuestionListArray![i].optionValue = check
                                    }
                                    else if type == "radio"{

                                        if self.inspectionQuestionListArray![i].options!.count == 3{
                                            self.inspectionQuestionListArray![i].optionValue = "\(data!)"
                                        }else{
                                            if self.inspectionQuestionListArray![i].fieldId1 != nil{
                                                let data1 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                                                self.inspectionQuestionListArray![i].fieldValue1 = "\(data1!)"
                                            }
                                            if self.inspectionQuestionListArray![i].fieldId2 != nil{
                                                let data2 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId2!)
                                                self.inspectionQuestionListArray![i].fieldValue2 = "\(data2!)"
                                            }
                                            var check = ""
                                            if "\(data ?? "")" == ""{
                                                check = ""
                                            }else{
                                            if "\(data!)" == "-1" || "\(data!)" == "false" || "\(data!)" == "0"{
                                                check = "No"
                                            }else{
                                                check = "Yes"
                                            }
                                            }
                                            self.inspectionQuestionListArray![i].optionValue = check
                                        }
                                        
                                    }else{
                                        let newData = data as? PostReleaseUploadModelData
                                        if newData != nil{
                                            documentsToBeUploaded[id!] = newData
                                            self.inspectionQuestionListArray![i].optionValue = "\(newData!.name ?? "")"
                                        }else{
                                            if data == nil {
                                                self.inspectionQuestionListArray![i].optionValue = ""
                                            }else{
                                                self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                                            }
                                        }
                                        
                                    }
                                }
                                if selectedIndex != -1{
                                    self.tempInspectionQuestionListArray?.removeAll()
                                    for i in (selectedIndex + 1)..<self.inspectionQuestionListArray!.count{
                                        self.tempInspectionQuestionListArray?.append(self.inspectionQuestionListArray![i])
                                    }
                                    if self.inspectionQuestionListArray![selectedIndex].optionValue == "false" {
                                        for i in stride(from: self.inspectionQuestionListArray!.count - 1, to: selectedIndex, by: -1){
                                            self.inspectionQuestionListArray?.remove(at: i)
                                        }
                                    }
                                }
                                
                                self.tblQuestionList.reloadData()
                                
                            }
                            
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Intake Detail Data.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
    
    // Post-release
    func getIcpPostReleaseData(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getICPPostReleaseReportDetail(reportType: self.reportType, aliasId: self.aliasId) { responseStr in
                        
                        DispatchQueue.main.async { [self] in
                            
                            //                            self.intakeReportList?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let _ = responseStr {
                                
                                self.postReleaseModelData = responseStr
                                
                                for i in 0..<self.inspectionQuestionListArray!.count{
                                    let id = self.inspectionQuestionListArray![i].id
                                    
                                    let type = self.inspectionQuestionListArray![i].type
                                    
                                    let data = responseStr?.valueByPropertyName(name: id!)
                                    
                                    if type == "singleTxtBox" || type == "singleTxtArea"{
                                        
                                        self.inspectionQuestionListArray![i].fieldValue1 = "\(data!)"
                                    }
                                    else if type == "checkBox"{
                                        selectedIndex = i
                                        var check = ""
                                        if "\(data ?? "")" == ""{
                                            check = ""
                                        }else{
                                        if "\(data!)" == "-1" || "\(data!)" == "false"{
                                            check = "false"
                                        }else{
                                            check = "true"
                                        }
                                        }
                                        self.inspectionQuestionListArray![i].optionValue = check
                                    }
                                    else if type == "radio"{
                                        if self.inspectionQuestionListArray![i].options!.count == 3{
                                            self.inspectionQuestionListArray![i].optionValue = "\(data!)"
                                        }else{
                                            if self.inspectionQuestionListArray![i].fieldId1 != nil{
                                                let data1 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                                                self.inspectionQuestionListArray![i].fieldValue1 = "\(data1!)"
                                            }
                                            if self.inspectionQuestionListArray![i].fieldId2 != nil{
                                                let data2 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId2!)
                                                self.inspectionQuestionListArray![i].fieldValue2 = "\(data2!)"
                                            }
                                            var check = ""
                                            if "\(data ?? "")" == ""{
                                                check = ""
                                            }else{
                                            if "\(data!)" == "-1" || "\(data!)" == "false" || "\(data!)" == "0"{
                                                check = "No"
                                            }else{
                                                check = "Yes"
                                            }
                                            }
                                            self.inspectionQuestionListArray![i].optionValue = check
                                        }
                                        
                                    }else{
                                        let newData = data as? PostReleaseUploadModelData
                                        if newData != nil{
                                            documentsToBeUploaded[id!] = newData
                                            self.inspectionQuestionListArray![i].optionValue = "\(newData!.name ?? "")"
                                        }else{
                                            if data == nil {
                                                self.inspectionQuestionListArray![i].optionValue = ""
                                            }else{
                                                self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                                            }
                                        }
                                    }
                                }
                                if selectedIndex != -1{
                                    self.tempInspectionQuestionListArray?.removeAll()
                                    for i in (selectedIndex + 1)..<self.inspectionQuestionListArray!.count{
                                        self.tempInspectionQuestionListArray?.append(self.inspectionQuestionListArray![i])
                                    }
                                    if self.inspectionQuestionListArray![selectedIndex].optionValue == "false" {
                                        for i in stride(from: self.inspectionQuestionListArray!.count - 1, to: selectedIndex, by: -1){
                                            self.inspectionQuestionListArray?.remove(at: i)
                                        }
                                    }
                                }
                                
                                self.tblQuestionList.reloadData()
                                
                            }
                            
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Intake Detail Data.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
}


extension CreateNewReportVC{
    
    func getCountryRelatedDetail(postUrl: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getCountryRelatedDetail(postUrl: postUrl) { responseStr in
                        
                        DispatchQueue.main.async { [self] in
                            
                            //                            self.poCasesData?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                                
                            if let data = responseStr {
                                                                
                                //for guardian checkbox
                                for i in 0..<self.inspectionQuestionListArray!.count{
                                    
                                    if self.inspectionQuestionListArray![i].id == "isGuardianAvailable"{
                                        
                                        let isGuardianSelectedString = self.inspectionQuestionListArray![i].optionValue
                                        
                                        if isGuardianSelectedString == "true"{
                                            self.isGuardianSelected = true
                                        }else{
                                            self.isGuardianSelected = false
                                        }
                                    }
                                    
                                }
                                
                                
                                if postUrl.lowercased().contains("/country"){
                                    if data.countries?.count ?? 0 == 0{
                                        ErrorToast.show(message: "No country data available", controller: self)
                                        return
                                    }else{
                                        
                                        for i in 0..<self.inspectionQuestionListArray!.count{
                                            
                                            if self.inspectionQuestionListArray![i].id == "countryName"{
                                                self.countryData = data.countries!
                                                
                                                let nameOfCountry = data.countries!.compactMap{$0.fullName}
                                                self.inspectionQuestionListArray![i].options = nameOfCountry
                                                
                                                var cid = 0
                                                let optionValue = self.inspectionQuestionListArray![i].optionValue
                                                for i in countryData{
                                                    if i.fullName == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/state?country_id=\(cid)")
                                                    }
                                                }
                                                
                                            }else if self.inspectionQuestionListArray![i].id == "guardianCountryId"{
                                                self.countryDataGuardian = data.countries!
                                                
                                                let nameOfCountry = data.countries!.compactMap{$0.fullName}
                                                self.inspectionQuestionListArray![i].options = nameOfCountry
                                                
                                                var cid = 0
                                                let optionValue = self.inspectionQuestionListArray![i].optionValue
                                                for i in countryDataGuardian{
                                                    if i.fullName == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/state?country_id=\(cid)")
                                                    }
                                                }
                                            }
                                        }
                                        
                                        for i in 0..<self.tempInspectionQuestionListArray!.count{
                                            if self.tempInspectionQuestionListArray![i].id == "countryName"{
                                                self.countryData = data.countries!
                                                
                                                let nameOfCountry = data.countries!.compactMap{$0.fullName}
                                                self.tempInspectionQuestionListArray![i].options = nameOfCountry
                                                
                                            }else if self.tempInspectionQuestionListArray![i].id == "guardianCountryId"{
                                                self.countryDataGuardian = data.countries!
                                                
                                                let nameOfCountry = data.countries!.compactMap{$0.fullName}
                                                self.tempInspectionQuestionListArray![i].options = nameOfCountry
                                                
                                                var cid = 0
                                                let optionValue = self.tempInspectionQuestionListArray![i].optionValue
                                                for i in countryDataGuardian{
                                                    if i.fullName == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/state?country_id=\(cid)")
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else if postUrl.lowercased().contains("/state"){
                                    if data.states?.count ?? 0 == 0{
                                        ErrorToast.show(message: "No state data available", controller: self)
                                        return
                                    }else{
                                        
                                        for i in 0..<self.inspectionQuestionListArray!.count{
                                            
                                            if self.inspectionQuestionListArray![i].id == "stateName"  && self.isGuardianSelected == false{
                                                self.stateData = data.states!
                                                
                                                let nameOfStates = data.states!.compactMap{$0.name}
                                                self.inspectionQuestionListArray![i].options = nameOfStates
                                                
                                                var cid = 0
                                                let optionValue = self.inspectionQuestionListArray![i].optionValue
                                                for i in stateData{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/district?state_id=\(cid)")
                                                    }
                                                }
                                                
                                            }
                                            else if self.inspectionQuestionListArray![i].id == "guardianStateId"  && self.isGuardianSelected == true{
                                            //else if self.inspectionQuestionListArray![i].id == "guardianStateId"{
                                                self.stateDataGuardian = data.states!
                                                
                                                let nameOfStates = data.states!.compactMap{$0.name}
                                                self.inspectionQuestionListArray![i].options = nameOfStates
                                                
                                                var cid = 0
                                                let optionValue = self.inspectionQuestionListArray![i].optionValue
                                                for i in stateDataGuardian{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/district?state_id=\(cid)")
                                                    }
                                                }
                                            }
                                            
                                        }
                                        
                                        for i in 0..<self.tempInspectionQuestionListArray!.count{
                                            
                                            if self.tempInspectionQuestionListArray![i].id == "stateName" && self.isGuardianSelected == false{
                                                self.stateData = data.states!
                                                
                                                let nameOfStates = data.states!.compactMap{$0.name}
                                                self.tempInspectionQuestionListArray![i].options = nameOfStates
                                                
                                                var cid = 0
                                                let optionValue = self.tempInspectionQuestionListArray![i].optionValue
                                                for i in stateData{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/district?state_id=\(cid)")
                                                    }
                                                }
                                                
                                            }
                                            else if self.tempInspectionQuestionListArray![i].id == "guardianStateId" && self.isGuardianSelected == true{
                                                
                                            //else if self.tempInspectionQuestionListArray![i].id == "guardianStateId"{
                                                self.stateDataGuardian = data.states!
                                                
                                                let nameOfStates = data.states!.compactMap{$0.name}
                                                self.tempInspectionQuestionListArray![i].options = nameOfStates
                                                
                                                var cid = 0
                                                let optionValue = self.tempInspectionQuestionListArray![i].optionValue
                                                for i in stateDataGuardian{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/district?state_id=\(cid)")
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else if postUrl.lowercased().contains("/district"){
                                    if data.districts?.count ?? 0 == 0{
                                        ErrorToast.show(message: "No district data available", controller: self)
                                        return
                                    }else{
                                        if reportType == "transfer"{
                                            self.transferDistrictData = data.districts!
                                            self.tblQuestionList.reloadData()
                                        }
                                        for i in 0..<self.inspectionQuestionListArray!.count{
                                            
                                            if self.inspectionQuestionListArray![i].id == "districtName"  && self.isGuardianSelected == false{
                                                self.districtData = data.districts!
                                                
                                                let nameOfDistrict = data.districts!.compactMap{$0.name}
                                                self.inspectionQuestionListArray![i].options = nameOfDistrict
                                                
                                                var cid = 0
                                                let optionValue = self.inspectionQuestionListArray![i].optionValue
                                                for i in districtData{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/taluk?district_id=\(cid)")
                                                    }
                                                }
                                                
                                            }
                                            else if self.inspectionQuestionListArray![i].id == "guardianDistrictId" && self.isGuardianSelected == true{
                                            
                                            //else if self.inspectionQuestionListArray![i].id == "guardianDistrictId"{
                                                
                                                self.districtDataGuardian = data.districts!
                                                
                                                let nameOfDistrict = data.districts!.compactMap{$0.name}
                                                self.inspectionQuestionListArray![i].options = nameOfDistrict
                                                
                                                var cid = 0
                                                let optionValue = self.inspectionQuestionListArray![i].optionValue
                                                for i in districtDataGuardian{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/taluk?district_id=\(cid)")
                                                    }
                                                }
                                            }
                                        }
                                        for i in 0..<self.tempInspectionQuestionListArray!.count{
                                            
                                            
                                            if self.tempInspectionQuestionListArray![i].id == "districtName"  && self.isGuardianSelected == false{
                                                self.districtData = data.districts!
                                                
                                                let nameOfDistrict = data.districts!.compactMap{$0.name}
                                                self.tempInspectionQuestionListArray![i].options = nameOfDistrict
                                                
                                                var cid = 0
                                                let optionValue = self.tempInspectionQuestionListArray![i].optionValue
                                                for i in districtData{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/taluk?district_id=\(cid)")
                                                    }
                                                }
                                                
                                            }
                                            else if self.tempInspectionQuestionListArray![i].id == "guardianDistrictId"  && self.isGuardianSelected == true{
                                                
                                            //else if self.tempInspectionQuestionListArray![i].id == "guardianDistrictId"{
                                                self.districtDataGuardian = data.districts!
                                                
                                                let nameOfDistrict = data.districts!.compactMap{$0.name}
                                                self.tempInspectionQuestionListArray![i].options = nameOfDistrict
                                                
                                                var cid = 0
                                                let optionValue = self.tempInspectionQuestionListArray![i].optionValue
                                                for i in districtDataGuardian{
                                                    if i.name == "\(optionValue ?? "")"{
                                                        cid = i.id!
                                                        
                                                        self.getCountryRelatedDetail(postUrl: "master/taluk?district_id=\(cid)")
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    if data.taluks?.count ?? 0 == 0{
                                        ErrorToast.show(message: "No taluk data available", controller: self)
                                        return
                                    }else{
                                        
                                        for i in 0..<self.inspectionQuestionListArray!.count{
                                            
                                            if self.inspectionQuestionListArray![i].id == "talukName"  && self.isGuardianSelected == false{
                                                self.talukData = data.taluks!
                                                
                                                let nameOfTaluk = data.taluks!.compactMap{$0.name}
                                                self.inspectionQuestionListArray![i].options = nameOfTaluk
                                                
                                            }
                                            //else if self.inspectionQuestionListArray![i].id == "guardianTalukId"  && self.isGuardianSelected == true{
                                            
                                            else if self.inspectionQuestionListArray![i].id == "guardianTalukId"{
                                                
                                                self.talukDataGuardian = data.taluks!
                                                
                                                let nameOfTaluk = data.taluks!.compactMap{$0.name}
                                                self.inspectionQuestionListArray![i].options = nameOfTaluk
                                            }
                                        }
                                        for i in 0..<self.tempInspectionQuestionListArray!.count{
                                            
                                            if self.tempInspectionQuestionListArray![i].id == "talukName"  && self.isGuardianSelected == false{
                                                self.talukData = data.taluks!
                                                
                                                let nameOfTaluk = data.taluks!.compactMap{$0.name}
                                                self.tempInspectionQuestionListArray![i].options = nameOfTaluk
                                                
                                            }
                                            else if self.tempInspectionQuestionListArray![i].id == "guardianTalukId"  && self.isGuardianSelected == true{
                                            
                                            //else if self.tempInspectionQuestionListArray![i].id == "guardianTalukId"{
                                                self.talukDataGuardian = data.taluks!
                                                
                                                let nameOfTaluk = data.taluks!.compactMap{$0.name}
                                                self.tempInspectionQuestionListArray![i].options = nameOfTaluk
                                            }
                                        }
                                    }
                                }
                                
                                
                                self.tblQuestionList.reloadData()
                                
                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Case Detail.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
}



extension CreateNewReportVC: UIDocumentPickerDelegate{
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        
        var uploadID = 0
        var uploadType = 0 //postReleaseUploadData.type ?? 0
        
        let dataOfFile = FileManager.default.contents(atPath: myURL.path)
        
        let base64Data = dataOfFile?.base64EncodedString() ?? ""
        let id = self.inspectionQuestionListArray![self.uploadTypeIndex].id!
        
        if documentsToBeUploaded[id] != nil{
            let postReleaseUploadData: PostReleaseUploadModelData = documentsToBeUploaded[id]!
            uploadID = postReleaseUploadData.id ?? 0
        }
        
        if self.inspectionQuestionListArray![self.uploadTypeIndex].questionName == "First interaction report of the Probation Officer/Child Welfare Officer/Case Worker / Social Worker/Non Governmental Organisation identified for follow-up with the child post-release"{
            uploadType = 23
        }
        if self.inspectionQuestionListArray![self.uploadTypeIndex].questionName == "Last progress report of the child"{
            uploadType = 24
        }
        if self.inspectionQuestionListArray![self.uploadTypeIndex].questionName == "Memorandum of Understanding with non-governmental organisation identified for post release follow up"{
            uploadType = 38
        }
        if self.inspectionQuestionListArray![self.uploadTypeIndex].questionName == "Memorandum of Understanding between the sponsoring agency and individual sponsor (Attach a copy)"{
            uploadType = 39
        }
        
        if self.inspectionQuestionListArray![self.uploadTypeIndex].questionName == "Upload Preterm release report & Release report (Upload the documents if Preterm release order or Release order has been issued)"{
            uploadType = 56
        }
        
        self.postUploadDocumentAPI(id: uploadID, file_string: base64Data, type: uploadType)
        
    }
    
    
    func postUploadDocumentAPI(id: Int, file_string: String, type: Int) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Uploading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.uploadPdfDocument(id: id, file_string: file_string, type: type) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == true){
                                let id = self.inspectionQuestionListArray![self.uploadTypeIndex].id
                                self.uploadModels![id!] = responseStr
                                self.inspectionQuestionListArray![self.uploadTypeIndex].optionValue = responseStr?.uploadName ?? ""
                                self.tblQuestionList.reloadData()
                                HUDIndicatorView.shared.dismissHUD()
                            }
                            else{
                                self.uploadTypeIndex = -1
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Failed to upload", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                uploadTypeIndex = -1
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            uploadTypeIndex = -1
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
        uploadTypeIndex = -1
    }
    
    
}



