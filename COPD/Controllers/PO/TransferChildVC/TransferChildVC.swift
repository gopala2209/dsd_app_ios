//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class TransferChildVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSelectionTitle: UILabel!
    
    @IBOutlet weak var btnSelectOption: UIButton!
    
    @IBOutlet weak var lblOption1: UILabel!
    @IBOutlet weak var lblOption2: UILabel!
    @IBOutlet weak var lblOption3: UILabel!
    
    @IBOutlet weak var imgSelectionButton1: UIImageView!
    @IBOutlet weak var imgSelectionButton2: UIImageView!
    @IBOutlet weak var imgSelectionButton3: UIImageView!
    
    // MARK: - Properties
    var selectedOption = ""
    
    var dropdownDistrictArray = ["Chennai", "Madurai"]
    var dropdownStateArray = ["Andhra Pradesh", "Karnataka", "Kerala", "Madhya Pradesh", "Tamil Nadu"]
    var dropdownCountryArray = ["Chine", "India", "Japan", "Singapore", "United State of America"]
    
    var dropDown = DropDown()
        
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        lblTitle.text = "Transfer Child"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // Unselected state initially
        imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_checked").withTintColor(#colorLiteral(red: 0.4156862745, green: 0.8745098039, blue: 0.7960784314, alpha: 1))
        imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
        imgSelectionButton3.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
        
        lblSelectionTitle.text = "Select District"
        
        btnSelectOption.setTitle("Select District", for: .normal)
    
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
        
        self.navigationController?.navigationBar.isHidden = false

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)


    }
    
    override func viewDidLayoutSubviews(){

    }
    

    // MARK: - IBActions
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
        
    }
    
    @IBAction func optionSelectedAction(_ sender: UIButton) {
        
        if sender.tag == 1{
            
            // selected option
            print("Selected Option: \(lblOption1.text ?? "")")
                        
            imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_checked").withTintColor(#colorLiteral(red: 0.4156862745, green: 0.8745098039, blue: 0.7960784314, alpha: 1))
            imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
            imgSelectionButton3.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
            
            lblSelectionTitle.text = "Select District"
            
            btnSelectOption.setTitle("Select District", for: .normal)
        }
        else if sender.tag == 2{
            
            // selected option
            print("Selected Option: \(lblOption2.text ?? "")")
            
            imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
            imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_checked").withTintColor(#colorLiteral(red: 0.4156862745, green: 0.8745098039, blue: 0.7960784314, alpha: 1))
            imgSelectionButton3.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
            
            lblSelectionTitle.text = "Select State"

            btnSelectOption.setTitle("Select State", for: .normal)
        }
        else{
            
            // selected option
            print("Selected Option: \(lblOption3.text ?? "")")
                        
            imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
            imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(#colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.3294117647, alpha: 1))
            imgSelectionButton3.image = #imageLiteral(resourceName: "radio_button_checked").withTintColor(#colorLiteral(red: 0.4156862745, green: 0.8745098039, blue: 0.7960784314, alpha: 1))
            
            lblSelectionTitle.text = "Select Country"
            
            btnSelectOption.setTitle("Select Country", for: .normal)

        }
        
    }
    
    @IBAction func selectOptionAction(_ sender: UIButton) {
        
        if lblSelectionTitle.text == "Select District"{
            
            // display dropdown for district selection
            self.displayOptionSelectionPopup(dropdownArray: dropdownDistrictArray)
        }
        else if lblSelectionTitle.text == "Select State"{
           
            // display dropdown for state selection
            self.displayOptionSelectionPopup(dropdownArray: dropdownStateArray)
        }
        else{
            
            // display dropdown for country selection
            self.displayOptionSelectionPopup(dropdownArray: dropdownCountryArray)
        }

    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        // submit
        Toast.show(message: "Submitted Successfully!", controller: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            // navigate to home screen
            self.navigationController?.popToViewController(ofClass: NewReportVC.self)

        }
        
    }
    
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        
        // cancel
        self.navigationController?.popToViewController(ofClass: NewReportVC.self)
    }
    
    
    func displayOptionSelectionPopup(dropdownArray: [String]){
        
        DispatchQueue.main.async {
                        
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = dropdownArray
            self.dropDown.anchorView = self.btnSelectOption
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.btnSelectOption.setTitle(item, for: .normal)
                    
            }
            
            self.dropDown.show()
            
        }
    }

    
}
