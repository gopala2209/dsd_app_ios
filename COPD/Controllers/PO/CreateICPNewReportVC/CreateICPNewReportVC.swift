//
//  ListViewController.swift
//  QuestionListApp
//

import UIKit

class CreateICPNewReportVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblForm: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnCompleteInspection: UIButton!
    @IBOutlet weak var btnCompleteInspectionHeightContraints: NSLayoutConstraint!
    
    //Basic details
    @IBOutlet weak var lblNameOfChild: UILabel!
    @IBOutlet weak var lblCrimeNumber: UILabel!
    @IBOutlet weak var lblRequestedOn: UILabel!
    @IBOutlet weak var lblDueOn: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPoliceStation: UILabel!
    
    var selectedReportId = 0
    var reportType = ""
    var aliasId = ""
    var inspectionQuestionListArray: [IntakeQuestion]? = []
    var responseOfApi: SIRDetailModelPoData?
    var responseOfSupervision: SupervisionDetainPoModelData?
    
    var documentsToBeUploaded: [String: PostReleaseUploadModelData] = [:]

    var supervisionIndex = -1
    
    
    // MARK: - Properties
    var passingInspectionFormListArray: [String] = []
    
    var passingReportId = ""
    var isCompletedReport = false
    var passingReportCategory = ""
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectedReportId == 2{
            reportType = "sir"
            lblTitle.text = "\(passingReportCategory) Details"
        }else{
            reportType = "supervision"
            lblTitle.text = passingReportCategory
        }
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white

        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()
        
        // basic report detail information
        
        if selectedReportId == 2{
            passingInspectionFormListArray = ["Description of Home & Living Conditions","Child History","Education Details","Vocational Training Details, If Any","Results Of Inquiry", "Family Member"]
        }else{
            passingInspectionFormListArray = ["Visit to school/ vocational training centre","Preliminary Details", "Observations"]
        }
        self.tblForm.reloadData()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // hide/show Completed button
        if isCompletedReport == true{
            btnCompleteInspection.isHidden = true
            btnCompleteInspectionHeightContraints.constant = 0
        }else{
            btnCompleteInspection.isHidden = false
            btnCompleteInspectionHeightContraints.constant = 50
        }
        
        
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
        if reportType != "sir"{
            getSuperVisionDetail()
        }else{
            getSIRDetailForChild()
        }
    }
    
    
    // method to register cell
    func registerNib(){
        
        let customNib = UINib(nibName: "ReportFormTitleCell", bundle: Bundle.main)
        tblForm.register(customNib, forCellReuseIdentifier: "ReportFormTitleCell")
        
        tblForm.separatorStyle = .none
    }
    
    
    func populateBasicInformation(name: String, crimeNo: String, requestedOn: String, dueOn: String, status: String, policeSt: String){
        
        lblNameOfChild.text = "Name of Child : \(name)"
            
        lblCrimeNumber.text = "Crime Number : \(crimeNo)"
        lblRequestedOn.text = "Requested On : \(requestedOn)"
        lblDueOn.text = "Due On : \(dueOn)"
        lblStatus.text = "Status : \(status)"
        lblPoliceStation.text = "Police Station : \(policeSt)"
       
    }
    
    
    @IBAction func completeInspectionClicked(_ sender: Any) {
        
        var posturl = ""
        if reportType == "sir"{
            posturl = "report/sir/\(self.aliasId)"
        }else{
            posturl = "report/supervision/\(self.aliasId)"

        }
        
        let alert = UIAlertController(title: "Alert",
                                      message: "Are you sure you want to submit \(passingReportCategory) form?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "YES", style: .default, handler: { (action) in
            
            // call api to complete inspection
            self.submitPoReport(httpMethod: "POST", postUrl: posturl, formAnswerModel: [:], isBackButtonPressed: true)
            
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "NO",
                                         style: .cancel,
                                         handler: { (action) in
                                            
                                            alert.dismiss(animated: true, completion: nil)
                                         })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
        
}


// MARK: - Table Delegate and Data Source
extension CreateICPNewReportVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passingInspectionFormListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportFormTitleCell", for: indexPath) as! ReportFormTitleCell
        
        cell.listTextLabel.text = passingInspectionFormListArray[indexPath.row]
        
        if (indexPath.row % 2 != 0){
            // odd
            cell.listBackgroundView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.listTextLabel.textColor = #colorLiteral(red: 0, green: 0.0431372549, blue: 0.2509803922, alpha: 1)
            
        } else {
            // even
            cell.listBackgroundView.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.9215686275, blue: 0.9803921569, alpha: 1)
            cell.listTextLabel.textColor = .label
        }
        
        cell.selectionStyle = .none
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        supervisionIndex = indexPath.row
        var pathToJson = ""
        if reportType == "sir"{
            if self.supervisionIndex != -1{
                if self.supervisionIndex == 0{
                    pathToJson = "SirDescriptionQuestionList"
                }else if self.supervisionIndex == 1{
                    pathToJson = "SirChildHistoryQuestionList"
                }else if self.supervisionIndex == 2{
                    pathToJson = "SirEducationDetailsQuestionList"
                }else if self.supervisionIndex == 3{
                    pathToJson = "SirVocationalQuestionList"
                }else if self.supervisionIndex == 4{
                    pathToJson = "SirInquiryQuestionList"
                }else if self.supervisionIndex == 5{
                    pathToJson = "SirFamilyMemberQuestionList"
                }
            }
            
        }else{
            if self.supervisionIndex == 0{
                pathToJson = "SupervisionVisitToSchool"
            }else if self.supervisionIndex == 1{
                pathToJson = "SupervisionPreliminaryQuestionList"
            }else {
                pathToJson = "SupervisionObservationsQuestionList"
            }
        }
        if let path = Bundle.main.path(forResource: pathToJson, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let model  = try JSONDecoder().decode(QuestionsAnswerModelData.self, from: data)
                self.inspectionQuestionListArray = model.intakeQuestions
              } catch {
                   // handle error
              }
        }
        
        if reportType == "sir"{
            if responseOfApi != nil{
                getQuestionList()
            }
        }else{
            if responseOfSupervision != nil{
                getQuestionList()
            }
        }
        
        // navigate to report form question details screen
        let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "CreateNewReportVC") as! CreateNewReportVC
        caseListVC.supervisionIndex = indexPath.row
        caseListVC.inspectionQuestionListArray = self.inspectionQuestionListArray
        if reportType == "sir"{
            if self.supervisionIndex != -1{
                if self.supervisionIndex == 0{
                    caseListVC.sirCondition = responseOfApi?.sirReport.sirCondition
                }else if self.supervisionIndex == 1{
                    caseListVC.sirHistory = responseOfApi?.sirReport.sirHistory
                }else if self.supervisionIndex == 2{
                    caseListVC.sirEducation = responseOfApi?.sirReport.sirEducation
                }else if self.supervisionIndex == 3{
                    caseListVC.sirVocation = responseOfApi?.sirReport.sirVocation
                }else if self.supervisionIndex == 4{
                    caseListVC.sirInquiry = responseOfApi?.sirReport.sirInquiry
                }else{
                    
                    //navigate to the family members List VC
                    
                    let familyMembersListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "FamilyMembersListVC") as! FamilyMembersListVC
                    familyMembersListVC.inspectionQuestionListArray = self.inspectionQuestionListArray
                    familyMembersListVC.supervisionIndex = indexPath.row
                    familyMembersListVC.reportType = "sir"
                    familyMembersListVC.aliasId = self.aliasId
                    familyMembersListVC.isComplete = isCompletedReport
                    familyMembersListVC.passingFormCategory = passingInspectionFormListArray[indexPath.row]
                    
                    familyMembersListVC.passingFamilyMemberArray = responseOfApi?.sirReport.sirFamily
                    
                    self.navigationController?.pushViewController(familyMembersListVC, animated: true)
                    
                    return
                    
                    //caseListVC.sirFamily = responseOfApi?.sirReport.sirFamilyIndividual
                }
            }
                        
        }else{
            if self.supervisionIndex == 0{
                caseListVC.vocationalReport = responseOfSupervision?.vocationalReport
            }else if self.supervisionIndex == 1{
                caseListVC.preliminaryReport = responseOfSupervision?.preliminaryReport
            }else {
                caseListVC.observationReport = responseOfSupervision?.observationReport
            }
        }
        if(selectedReportId == 2){
            caseListVC.reportType = "sir"
            caseListVC.aliasId = self.aliasId
        }else{
            caseListVC.reportType = "supervision"
            caseListVC.aliasId = self.aliasId
        }
        caseListVC.isComplete = isCompletedReport
        caseListVC.passingFormCategory = passingInspectionFormListArray[indexPath.row]
        self.navigationController?.pushViewController(caseListVC, animated: true)
    }
}


extension CreateICPNewReportVC{
    
    func getQuestionList(){
        var responseStr: SirCondition?
        var responseStr1: SirHistory?
        var responseStr2: SirEducation?
        var responseStr3: SirVocation?
        var responseStr4: SirInquiry?
        var responseStr5: VocationalReport?
        var responseStr6: PreliminaryReport?
        var responseStr7: ObservationReport?
        var responseStr8: SirFamily?

        if reportType == "sir"{
            if self.supervisionIndex != -1{
                if self.supervisionIndex == 0{
                    responseStr = responseOfApi?.sirReport.sirCondition
                }else if self.supervisionIndex == 1{
                    responseStr1 = responseOfApi?.sirReport.sirHistory
                }else if self.supervisionIndex == 2{
                    responseStr2 = responseOfApi?.sirReport.sirEducation
                }else if self.supervisionIndex == 3{
                    responseStr3 = responseOfApi?.sirReport.sirVocation
                }else if self.supervisionIndex == 4{
                    responseStr4 = responseOfApi?.sirReport.sirInquiry
                }else{
                    if (responseOfApi?.sirReport.sirFamily?.count ?? 0) > 0{
                        responseStr8 = responseOfApi?.sirReport.sirFamily?[0]
                    }
                }
            }
            
        }else{
            if self.supervisionIndex == 0{
                responseStr5 = responseOfSupervision?.vocationalReport
            }else if self.supervisionIndex == 1{
                responseStr6 = responseOfSupervision?.preliminaryReport
            }else {
                responseStr7 = responseOfSupervision?.observationReport
            }
        }
        for i in 0..<self.inspectionQuestionListArray!.count{
            let id = self.inspectionQuestionListArray![i].id
            let type = self.inspectionQuestionListArray![i].type
            var data: Any?
            if reportType == "sir"{
                if self.supervisionIndex != -1{
                    if self.supervisionIndex == 0{
                        data = responseStr?.valueByPropertyName(name: id!)
                    }else if self.supervisionIndex == 1{
                        data = responseStr1?.valueByPropertyName(name: id!)
                    }else if self.supervisionIndex == 2{
                        data = responseStr2?.valueByPropertyName(name: id!)
                    }else if self.supervisionIndex == 3{
                        data = responseStr3?.valueByPropertyName(name: id!)
                    }else if self.supervisionIndex == 4{
                        data = responseStr4?.valueByPropertyName(name: id!)
                    }else{
                        data = responseStr8?.valueByPropertyName(name: id!)
                    }
                }
                
            }else{
                if self.supervisionIndex == 0{
                    data = responseStr5?.valueByPropertyName(name: id!)
                }else if self.supervisionIndex == 1{
                    data = responseStr6?.valueByPropertyName(name: id!)
                }else {
                    data = responseStr7?.valueByPropertyName(name: id!)
                }
            }
            
            if type == "singleTxtBox"{
                
                self.inspectionQuestionListArray![i].fieldValue1 = "\(data ?? "")"
            }
            else if type == "checkBox"{
                var check = ""
                
                if "\(data ?? "")" == ""{
                    check = ""
                }else{
                    
                if "\(data ?? "")" == "-1" || "\(data ?? "")" == "false"{
                    check = "false"
                }else{
                    check = "true"
                }
                }
                self.inspectionQuestionListArray![i].optionValue = check
            }
            else if type == "radio"{
                
                /*if self.inspectionQuestionListArray![i].options!.count == 4{
                    self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                }
                else{
                    self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                }*/
                
                
                if self.inspectionQuestionListArray![i].options!.count == 5{
                    
                    if self.inspectionQuestionListArray![i].id == "economicStatus"{
                        
                        var check = ""
                        
                        if "\(data ?? "")" == ""{
                            check = ""
                        }else{
                            
                        if "\(data ?? "")" == "1"{
                            check = "Bpl"
                        }else if "\(data ?? "")" == "2"{
                            check = "Poor"
                        }else if "\(data ?? "")" == "3"{
                            check = "Lower Middle Class"
                        }else if "\(data ?? "")" == "3"{
                            check = "Upper Middle Class"
                        }else{
                            check = "Well-To-Do"
                        }
                        }
                        
                        self.inspectionQuestionListArray![i].optionValue = check
                        
                    }
                    
                }
                
                if self.inspectionQuestionListArray![i].options!.count == 4{
                    
                    if self.inspectionQuestionListArray![i].id == "familyIncomeType"{
                        
                        if self.inspectionQuestionListArray![i].fieldId1 != nil{
                            data = responseStr8?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                            self.inspectionQuestionListArray![i].fieldValue1 = "\(data ?? "")"
                        }
                        if self.inspectionQuestionListArray![i].fieldId2 != nil{
                            data = responseStr8?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId2!)
                            self.inspectionQuestionListArray![i].fieldValue2 = "\(data ?? "")"
                        }
                        
                        var check = ""
                        
                        if "\(data ?? "")" == ""{
                            check = ""
                        }else{
                            
                        if "\(data ?? "")" == "1"{
                            check = "Daily"
                        }else if "\(data ?? "")" == "2"{
                            check = "Weekly"
                        }else if "\(data ?? "")" == "3"{
                            check = "Month"
                        }else{
                            check = "None"
                        }
                        }
                        self.inspectionQuestionListArray![i].optionValue = check
                    }
                    else{
                    self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                    }
                }
                else if self.inspectionQuestionListArray![i].options!.count == 3{
                    self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                }else if self.inspectionQuestionListArray![i].options!.count == 2{
                    if self.inspectionQuestionListArray![i].fieldId1 != nil{
                        
                        if self.inspectionQuestionListArray![i].id == "maritalType"{
                            let data2 = responseStr?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                            self.inspectionQuestionListArray![i].fieldValue1 = "\(data2 ?? "")"
                        }
                        else{
                        let data2 = responseStr8?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId1!)
                        self.inspectionQuestionListArray![i].fieldValue1 = "\(data2 ?? "")"
                        }
                    }
                    if self.inspectionQuestionListArray![i].fieldId2 != nil{
                        data = responseStr8?.valueByPropertyName(name: self.inspectionQuestionListArray![i].fieldId2!)
                        self.inspectionQuestionListArray![i].fieldValue2 = "\(data ?? "")"
                    }
                    var check = ""
                    if "\(data ?? "")" == ""{
                        check = ""
                    }else{
                        if "\(data ?? "")" == "-1" || "\(data ?? "")" == "false" || "\(data ?? "")" == "0"{
                            check = "No"
                        }else{
                            check = "Yes"
                        }
                    }
                    self.inspectionQuestionListArray![i].optionValue = check
                }
            }
            else{
                //self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                
                    let newData = data as? PostReleaseUploadModelData
                    if newData != nil{
                        documentsToBeUploaded[id!] = newData
                        self.inspectionQuestionListArray![i].optionValue = "\(newData!.name ?? "")"
                    }else{
                        if data == nil {
                            self.inspectionQuestionListArray![i].optionValue = ""
                        }else{
                            self.inspectionQuestionListArray![i].optionValue = "\(data ?? "")"
                    }
                }
            }
        }
    }
    
    func getDataOfData(string: String) -> String{
        if let index = string.firstIndex(of: "\""),
            case let start = string.index(after: index),
            let end = string[start...].firstIndex(of: "\"") {
            let substring = string[start..<end]
            return String(substring)
        }
        
        if let index = string.firstIndex(of: "("),
            case let start = string.index(after: index),
            let end = string[start...].firstIndex(of: ")") {
            let substring = string[start..<end]
            return String(substring)
        }
        
        return ""
    }
    
    func getSIRDetailForChild(){

        let servicemodel = ServiceModel()

        if (NetworkConnection.isConnectedToNetwork()){

            do {

                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")

                DispatchQueue.global(qos: .userInitiated).async {

                    servicemodel.getSIRReportDetail(reportType: self.reportType, aliasId: self.aliasId) { responseStr in

                        DispatchQueue.main.async { [self] in

//                            self.intakeReportList?.removeAll()

                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                                return
                            }

                            HUDIndicatorView.shared.dismissHUD()
                            
                            
                                if let _ = responseStr {
                                    responseOfApi = responseStr
                                    let caseNo = responseStr?.sirReport.sirCase?.crimeNumber ?? ""
                                    let requestedDate = getDataOfData(string: "\(responseStr?.sirReport.completedAt)")
                                    let dueDate = getDataOfData(string: "\(responseStr?.sirReport.dueAt)")
                                    let psName = (responseStr?.sirReport.sirCase?.policeStation?.number) ?? ""
                                    let psCity = (responseStr?.sirReport.sirCase?.districtName) ?? ""
                                    
                                    var policeSt = ""
                                    
                                    if psName.isEmpty == false && psName != ""{
                                        
                                        policeSt = policeSt + "\(psName)" + ", "
                                    }
                                    if psCity.isEmpty == false && psCity != ""{
                                        
                                        policeSt = policeSt + "\(psCity)"
                                    }
                                    
                                    if policeSt.isEmpty == true  && policeSt == ""{
                                        
                                        policeSt = "--"
                                    }
                                    
                                    let name = (responseStr?.sirReport.child?.firstName ?? "") + " " + (responseStr?.sirReport.child?.lastName ?? "")
                                    let statusData = responseStr?.sirReport.statusText
                                    self.populateBasicInformation(name: name, crimeNo: caseNo, requestedOn: requestedDate, dueOn: dueDate, status: statusData ?? "", policeSt: policeSt)
                                    
                                }

                                else{

                                    HUDIndicatorView.shared.dismissHUD()

                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: "Failed to load SIR Detail Data.", message: "", parentView: self)
                                }
                            
                        }
                    }

                }
            }

        }
    }
    
    
    func getSuperVisionDetail(){

        let servicemodel = ServiceModel()

        if (NetworkConnection.isConnectedToNetwork()){

            do {

                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")

                DispatchQueue.global(qos: .userInitiated).async {

                    servicemodel.getSupervisionPODetail(reportType: self.reportType, aliasId: self.aliasId) { responseStr in

                        DispatchQueue.main.async { [self] in

//                            self.intakeReportList?.removeAll()

                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                                return
                            }

                            HUDIndicatorView.shared.dismissHUD()
                            
                            
                                if let _ = responseStr {
                                    responseOfSupervision = responseStr
                                    let caseNo = responseStr?.welcomeCase?.crimeNumber ?? ""
                                    let requestedDate = getDataOfData(string: "\(responseStr?.requestedAt)")
                                    let dueDate = getDataOfData(string: "\(responseStr?.completedAt)")
                                    let psName = (responseStr?.welcomeCase?.policeStation?.number) ?? ""
                                    let psCity = (responseStr?.welcomeCase?.districtName) ?? ""
                                   
                                    var policeSt = ""
                                    
                                    if psName.isEmpty == false && psName != ""{
                                        
                                        policeSt = policeSt + "\(psName)" + ", "
                                    }
                                    if psCity.isEmpty == false && psCity != ""{
                                        
                                        policeSt = policeSt + "\(psCity)"
                                    }
                                    
                                    if policeSt.isEmpty == true  && policeSt == ""{
                                        
                                        policeSt = "--"
                                    }
                                    
                                    let name = (responseStr?.child?.firstName ?? "") + " " + (responseStr?.child?.lastName ?? "")
                                    let statusData = responseStr?.statusText
                                    self.populateBasicInformation(name: name, crimeNo: caseNo, requestedOn: requestedDate, dueOn: dueDate, status: statusData ?? "", policeSt: policeSt)
                                    
                                }

                                else{

                                    HUDIndicatorView.shared.dismissHUD()

                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: "Failed to load Intake Detail Data.", message: "", parentView: self)
                                }
                            
                        }
                    }

                }
            }

        }
    }
}

extension CreateICPNewReportVC{
    
    func submitPoReport(httpMethod: String, postUrl: String, formAnswerModel: [String: Any], isBackButtonPressed: Bool) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.submitPoQuestionsToServer(httpMethod: httpMethod, postUrl: postUrl, formAnswerModel: formAnswerModel) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                Toast.show(message: "Your report has been completed successfully!", controller: self)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    
                                    // dismiss
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Failed to complete report", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
    
    
}


// MARK: - SupervisionReportDetail
struct SupervisionDetainPoModelData: Codable {
    let completedAt: DueDate?
    let vocationalReport: VocationalReport?
    let observationReport: ObservationReport?
    let alias, statusText: String?
    let supervisionID: Int?
    let preliminaryReport: PreliminaryReport?
    let welcomeCase: SirCasePo?
    let status: Int?
    let requestedAt: DueDate?
    let child: IntakeDetailChild?
    
    
    enum CodingKeys: String, CodingKey {
        case completedAt = "completed_at"
        case vocationalReport = "vocational_report"
        case observationReport = "observation_report"
        case alias
        case statusText = "status_text"
        case supervisionID = "supervision_id"
        case preliminaryReport = "preliminary_report"
        case welcomeCase = "case"
        case status
        case requestedAt = "requested_at"
        case child
    }
}
