//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/06/21.
//  Copyright © 2021 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class HomeVC_PO: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblHome: UITableView!
    @IBOutlet weak var tblHeaderView: UIView!
    
    @IBOutlet weak var btnSelectCategory: UIButton!
    
    // MARK: - Properties
    var dropdownCategoryArray = ["CNCP", "CICL"]
    var dropDown = DropDown()
    
    let cncp_Titles = ["CICL Case List", "CNCP Case List"]
    
    
    let paddingRight = 20
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = .white
                
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()
        
        // set button category title to CNCP
        self.btnSelectCategory.setTitle(dropdownCategoryArray[0], for: .normal)
        
        let isFirstLaunch = UserDefaults.standard.bool(forKey: "isFirstLaunch")

            if isFirstLaunch == true{
                //It's the initial launch of application.
                Toast.show(message: "You have successfully logged in.", controller: self)
                
                UserDefaults.standard.set(false, forKey: "isFirstLaunch")
                UserDefaults.standard.synchronize()
            }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    
    func registerNib(){
        
        let customNib = UINib(nibName: "HomeTableViewCell", bundle: Bundle.main)
        tblHome.register(customNib, forCellReuseIdentifier: "HomeTableViewCell")
        tblHome.separatorStyle = .none
        
        tblHome.automaticallyAdjustsScrollIndicatorInsets = true
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
//        tblHeaderView.translatesAutoresizingMaskIntoConstraints = true
//        tblHeaderView.bounds = CGRect(x: tblHome.frame.origin.x - 10, y: 0, width: tblHome.bounds.width, height: 100)
//
//        tblHome.tableHeaderView = tblHeaderView
        
    }
    
    
    func displayCategorySelectionPopup(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropdownCategoryArray
            self.dropDown.anchorView = self.btnSelectCategory
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.btnSelectCategory.setTitle(item, for: .normal)
                
                // reload table
                self.tblHome.reloadData()
            }
            
            self.dropDown.show()
            
        }
    }
    
    
    // MARK: - IBActions
    @IBAction func selectCategoryAction(_ sender: UIButton) {
        
        // display popup
//        self.displayCategorySelectionPopup()
    }
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
    }
    
}


extension HomeVC_PO: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cncp_Titles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.titleOption.text = self.cncp_Titles[indexPath.row]
        
        cell.labelIncompleted.isHidden = true
        cell.widthOfImageView.constant = 0
        cell.rightConstraint.constant = 0
        
        switch indexPath.row {
        case 0:
            self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.8117647059, green: 0.8509803922, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.8862745098, green: 0.9215686275, blue: 0.9411764706, alpha: 1)], tableCellView: cell.titleBGView, width: tblHome.bounds.width + CGFloat(paddingRight))

            
            break
            
        case 1:
            self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.9960784314, green: 0.6784313725, blue: 0.6509803922, alpha: 1), #colorLiteral(red: 0.9607843137, green: 0.937254902, blue: 0.937254902, alpha: 1)], tableCellView: cell.titleBGView, width: tblHome.bounds.width + CGFloat(paddingRight))//cell.contentView.bounds.width

            break
            
        default:
            break
        }
        
        
        cell.selectionStyle = .none
        
        cell.imageArrow.highlightedImage = UIImage(named: "right_arrow")?.imageWithColor(color: UIColor.whiteTheme)
        
        cell.titleOption.highlightedTextColor = UIColor.whiteTheme
        cell.labelIncompleted.highlightedTextColor = UIColor.whiteTheme
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        var caseName = ""
        if indexPath.row == 0{
            caseName = "cicl"
        }else{
            caseName = "cncp"
        }
        let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "CaseListPOViewController") as! CaseListPOViewController
        caseListVC.caseName = caseName
        self.navigationController?.pushViewController(caseListVC, animated: true)
        
    }
    
    
    func selectTransferReport(forSelectedReport: String){
        
        let nextVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "SelectTransferReportVC") as! SelectTransferReportVC
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.forSelectedReport = forSelectedReport
        
        self.navigationController?.present(nextVC, animated: false, completion: nil)
    }
    
    
    func applyGradientColorToTableCell(colours: [UIColor], tableCellView: UIView, width: CGFloat) {
        
        // Add gradient to Puff Summary view
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0,y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0,y: 0.5)
        gradient.frame = CGRect(x: 0, y: 0, width: width - 40, height: 110)
        gradient.cornerRadius = 10
        
        tableCellView.layer.insertSublayer(gradient, at: 0)
        
    }
    
}


extension HomeVC_PO: TransferReportDelegate{
    func selectTransferReport(option: String, forSelectedReport: String){
        
        print("Selected option \(option) for report title = \(forSelectedReport)")
        
        if option == "New Report"{
            
            // navigate to new report screen
            
        }
        
        else{
            
            // in-progress and completed report
            let inProgressCompletedReportVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "inprogresscompletedreportvc") as! InProgressCompletedReportVC
            
            if option == "Completed"{
                inProgressCompletedReportVC.isCompletedReport = true
            }
            else{
                inProgressCompletedReportVC.isCompletedReport = false
            }
            
            inProgressCompletedReportVC.passingReportCategory = forSelectedReport
            self.navigationController?.pushViewController(inProgressCompletedReportVC, animated: true)
            
        }
        
    }
}
