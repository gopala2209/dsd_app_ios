//
//  ReportlistPoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 23/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class ReportlistPoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var orderTop: NSLayoutConstraint!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var orderHeight: NSLayoutConstraint!
    
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var order: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var gender: UILabel!

    @IBOutlet weak var statusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func showOrder(){
        self.orderTop.constant = 10
        self.orderHeight.constant = 30
        self.orderView.isHidden = false
    }
    
    func hideOrder(){
        self.orderTop.constant = 0
        self.orderHeight.constant = 0
        self.orderView.isHidden = true
    }

}
