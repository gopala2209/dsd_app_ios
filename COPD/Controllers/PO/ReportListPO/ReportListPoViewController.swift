//
//  ReportListPoViewController.swift
//  COPD
//
//  Created by Anand Praksh on 23/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit
import DropDown

class ReportListPoViewController: UIViewController {
    
    @IBOutlet weak var selectionViewTop: NSLayoutConstraint!
    @IBOutlet weak var selectionview: UIView!
    @IBOutlet weak var reportlistTitle: UILabel!
    @IBOutlet weak var reportListTVC: UITableView!
    @IBOutlet weak var totalViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var inspectionBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var inspectionBtn: UIButton!
    
    @IBOutlet weak var selectChildLbl: UILabel!
    @IBOutlet weak var pleaseSelectChild: UIView!
    
    
    var caseId = ""
    var id = ""
    var selectedReportId = 0
    var selectedTypeId = 1
    var reportTitle = ""
    var reportType = ""
    var caseType = ""
    var dropDown = DropDown()
    var dropDownMenuData: [String]? = []
    var idForSelection: [String: Int] = [:]
    var childId: [String: Int] = [:]
    
    var supervisionReportDropDown_CICL = ["JJB Order", "Commissioner/Director Order", "Children Court Order"]
    var supervisionReportDropDown_CNCP = ["CWC Order", "Commissioner/Director Order", "Children Court Order"]

    var intakeReportList: [IntakeReportPo]? = []
    var icpReportList: [ICPReportPO]? = []
    var sirReportList: [SirReport]? = []
    var supervisionReportList: [SupervisionReport]? = []
    var transferreportsList: [TransferReportPO]? = []

    override func viewDidLoad() {
        super.viewDidLoad()
        if selectedReportId == 1{
            if caseType == "cncp"{
                self.reportType = "transfer"
            }else{
                reportType = "intake"
            }
        }else if selectedReportId == 2{
            reportType = "sir"
        }else if selectedReportId == 3{
            reportType = "icp"
        }else{
            reportType = "supervision"
        }
        setUpInitialUi()
        createTouchForView()
        registerNib()
        
        navigationController?.navigationBar.barTintColor = .white
        
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
                
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
    }
    
    
    
    func createTouchForView(){
        if self.selectedReportId == 4{
            
            if caseType == "cncp"{
            
                self.selectChildLbl.text = self.supervisionReportDropDown_CNCP.first
            }
            else{//cicl
                self.selectChildLbl.text = self.supervisionReportDropDown_CICL.first
            }
        }
        
        let selectionviewTap = UITapGestureRecognizer(target: self, action: #selector(self.selectionViewTapped(_:)))
        selectionview.addGestureRecognizer(selectionviewTap)
    }
    
    

    @objc func selectionViewTapped(_ sender: UITapGestureRecognizer? = nil) {
        selectViewData()
    }
    
    func selectViewData(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            if self.selectedReportId == 4{
                
                if self.caseType == "cncp"{
                
                    self.dropDown.dataSource = self.supervisionReportDropDown_CNCP
                }
                else{//cicl
                    self.dropDown.dataSource = self.supervisionReportDropDown_CICL
                }
            }else{
                self.dropDown.dataSource = self.dropDownMenuData ?? ["Please Select Child"]
            }
            self.dropDown.anchorView = self.pleaseSelectChild
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.selectChildLbl.text = item
                
                if self.selectedReportId == 4{
                    self.reportListTVC.reloadData()
                    
                    if self.selectChildLbl.text == "JJB Order"{
                        
                        self.selectedTypeId = 1
                    }else if self.selectChildLbl.text == "Children Court Order"{
                        
                        self.selectedTypeId = 2
                    }else{
                        
                        self.selectedTypeId = 3
                    }
                    
                    self.getAllIntakeReportPO(typeId: self.selectedTypeId)
                    
                }
            }
            
            self.dropDown.show()
            
        }
    }
    
    
    func setUpInitialUi(){
        reportlistTitle.text = reportTitle + " List"
        
        if selectedReportId == 1{
            
            if reportTitle == "Intake Report"{
                reportlistTitle.text = reportTitle + " List"
            }else{
            reportlistTitle.text = "Transfer Report List"
            inspectionBtn.setTitle("Start Intake Report", for: .normal)
            }
        }
        
        if selectedReportId == 2{
            selectionview.isHidden = true
            selectionViewTop.constant = 0
            totalViewHeight.constant = 0
            inspectionBtnHeight.constant = 0
        }
        
        if selectedReportId == 3{
            inspectionBtn.setTitle("Start ICP Report", for: .normal)
        }
        
        if selectedReportId == 4{
            totalViewHeight.constant = 50
            inspectionBtnHeight.constant = 0
            inspectionBtn.isHidden = true
        }
        dropDownMenuData?.removeAll()
        dropDownMenuData?.append("Please Select Child")
        for (key,_) in childId{
            if !(dropDownMenuData?.contains(key))!{
                dropDownMenuData?.append(key)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getAllIntakeReportPO(typeId: selectedTypeId)
                    
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
                
    }
        
    
    func registerNib(){
        
        let customNib = UINib(nibName: "ReportlistPoTableViewCell", bundle: Bundle.main)
        reportListTVC.register(customNib, forCellReuseIdentifier: "ReportlistPoTableViewCell")
        
        reportListTVC.separatorStyle = .none
        
    }
    
    @IBAction func startIntakeReport(_ sender: Any) {
        if selectChildLbl.text == "Please Select Child"{
            ServiceModel().showAlert(title: "Alert", message: "Please select the child", parentView: self)
            return
        }else{
            self.startIntakeInspection(caseId: self.id, childId: "\(self.childId[self.selectChildLbl.text!] ?? 0)")
        }
    }
    
}


extension ReportListPoViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedReportId == 1{
            if self.caseType == "cncp"{
                return transferreportsList?.count ?? 0
            }else{
                return intakeReportList?.count ?? 0
            }
        }else if selectedReportId == 2{
            return sirReportList?.count ?? 0
        }else if selectedReportId == 3{
            return icpReportList?.count ?? 0
        }else{
            return supervisionReportList?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportlistPoTableViewCell", for: indexPath) as! ReportlistPoTableViewCell
                
        cell.selectionStyle = .none
        
        if selectedReportId == 1{
            if caseType == "cncp"{
                cell.hideOrder()
                let intakeData = transferreportsList![indexPath.row]
                let firstName = intakeData.child?.firstName ?? ""
                let lastName = intakeData.child?.lastName ?? ""
                let fullName = "Name : " + firstName + " " + lastName
                cell.name.text = fullName
                cell.number.text = intakeData.child?.alias
                cell.age.text = "DOB : " + (intakeData.child?.dob ?? "")
                let gender = "    |    Sex : " + (intakeData.child?.genderName ?? "")
                let age = "Age : " + (intakeData.child?.age ?? "")
                cell.gender.text =  age + gender
                cell.status.text = intakeData.statusText
                if intakeData.statusText?.lowercased() == "completed"{
                    cell.statusView.backgroundColor = UIColor.greenTheme
                }else{
                    cell.statusView.backgroundColor = UIColor.orangeTheme
                }
            }else{
                cell.hideOrder()
                let intakeData = intakeReportList![indexPath.row]
                let firstName = intakeData.child?.firstName ?? ""
                let lastName = intakeData.child?.lastName ?? ""
                let fullName = "Name : " + firstName + " " + lastName
                cell.name.text = fullName
                cell.number.text = intakeData.child?.alias
                cell.age.text = "DOB : " + (intakeData.child?.dob ?? "")
                let gender = "    |    Sex : " + (intakeData.child?.genderName ?? "")
                let age = "Age : " + (intakeData.child?.age ?? "")
                cell.gender.text =  age + gender
                cell.status.text = intakeData.status_text
                if intakeData.status_text?.lowercased() == "completed"{
                    cell.statusView.backgroundColor = UIColor.greenTheme
                }else{
                    cell.statusView.backgroundColor = UIColor.orangeTheme
                }
            }
            
        }else if selectedReportId == 2{
            cell.hideOrder()
            let intakeData = sirReportList![indexPath.row]
            let firstName = intakeData.child?.firstName ?? ""
            let lastName = intakeData.child?.lastName ?? ""
            let fullName = "Name : " + firstName + " " + lastName
            cell.name.text = fullName
            cell.number.text = intakeData.child?.alias
            cell.age.text = "DOB : " + (intakeData.child?.dob ?? "")
            let gender = "    |    Sex : " + (intakeData.child?.genderName ?? "")
            let age = "Age : " + (intakeData.child?.age ?? "")
            cell.gender.text =  age + gender
            cell.status.text = intakeData.statusText
            if intakeData.statusText?.lowercased() == "completed"{
                cell.statusView.backgroundColor = UIColor.greenTheme
            }else{
                cell.statusView.backgroundColor = UIColor.orangeTheme
            }
        }else if selectedReportId == 3{
            cell.hideOrder()
            let intakeData = icpReportList![indexPath.row]
            let firstName = intakeData.child?.firstName ?? ""
            let lastName = intakeData.child?.lastName ?? ""
            let fullName = "Name : " + firstName + " " + lastName
            cell.name.text = fullName
            cell.number.text = intakeData.child?.alias
            cell.age.text = "DOB : " + (intakeData.child?.dob ?? "")
            let gender = "    |    Sex : " + (intakeData.child?.genderName ?? "")
            let age = "Age : " + (intakeData.child?.age ?? "")
            cell.gender.text =  age + gender
            cell.status.text = intakeData.status_text
            if intakeData.status_text?.lowercased() == "completed"{
                cell.statusView.backgroundColor = UIColor.greenTheme
            }else{
                cell.statusView.backgroundColor = UIColor.orangeTheme
            }
            
        }else{
            cell.showOrder()
            
            if self.caseType == "cncp"{
            
                cell.order.text = self.supervisionReportDropDown_CNCP.first
            }
            else{
                cell.order.text = self.supervisionReportDropDown_CICL.first
            }
            
            if self.selectChildLbl.text == "Commissioner/Director Order"{
                cell.order.text = "Child discharged"
            }else if self.selectChildLbl.text == "Children Court Order"{
                cell.order.text = "JJB Order - Children Court"
            }else{
                cell.order.text = self.selectChildLbl.text
            }
            let intakeData = supervisionReportList![indexPath.row]
            let firstName = intakeData.child?.firstName ?? ""
            let lastName = intakeData.child?.lastName ?? ""
            let fullName = "Name : " + firstName + " " + lastName
            cell.name.text = fullName
            cell.number.text = intakeData.child?.alias
            cell.age.text = "DOB : " + (intakeData.child?.dob ?? "")
            let gender = "    |    Sex : " + (intakeData.child?.genderName ?? "")
            let age = "Age : " + (intakeData.child?.age ?? "")
            cell.gender.text =  age + gender
            cell.status.text = intakeData.statusText
            if intakeData.statusText?.lowercased() == "completed"{
                cell.statusView.backgroundColor = UIColor.greenTheme
            }else{
                cell.statusView.backgroundColor = UIColor.orangeTheme
            }
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if selectedReportId == 1 || selectedReportId == 3{
            
            if(selectedReportId == 1){
                let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "CreateNewReportVC") as! CreateNewReportVC

                if caseType == "cncp"{
                    caseListVC.reportType = "transfer"
                    caseListVC.passingFormCategory = "Transfer Report Details"
                    
                    if transferreportsList?.count ?? 0 > 0{
                    caseListVC.aliasId = transferreportsList![indexPath.row].alias!
                    if transferreportsList![indexPath.row].statusText?.lowercased() == "completed"{
                        caseListVC.isComplete = true
                    }
                    }
                }else{
                    caseListVC.reportType = "intake"
                    caseListVC.passingFormCategory = "Intake Report Details"
                    
                    if intakeReportList?.count ?? 0 > 0{
                    caseListVC.aliasId = intakeReportList![indexPath.row].alias!
                    if intakeReportList![indexPath.row].status_text?.lowercased() == "completed"{
                        caseListVC.isComplete = true
                    }
                    }
                }
                self.navigationController?.pushViewController(caseListVC, animated: true)
            }else{
                let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "ICPDetailsMenuVC") as! ICPDetailsMenuVC

                caseListVC.reportType = "icp"
                caseListVC.passingFormCategory = "ICP Report Details"
                
                if icpReportList?.count ?? 0 > 0{
                if icpReportList![indexPath.row].status_text?.lowercased() == "completed"{
                    caseListVC.isComplete = true
                }
                caseListVC.aliasId = icpReportList![indexPath.row].alias!
                caseListVC.childDataPo_ICP = icpReportList![indexPath.row].child
                caseListVC.statusText = icpReportList![indexPath.row].status_text ?? ""
                }
                self.navigationController?.pushViewController(caseListVC, animated: true)
            }
        }else{
            let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "CreateICPNewReportVC") as! CreateICPNewReportVC
            caseListVC.selectedReportId = self.selectedReportId
            
            if selectedReportId == 2{
                
                if sirReportList?.count ?? 0 > 0{
                    
                    if sirReportList![indexPath.row].statusText?.lowercased() == "completed"{
                        caseListVC.isCompletedReport = true
                    }
                    caseListVC.aliasId = sirReportList![indexPath.row].alias!
                }

                caseListVC.passingReportCategory = "SIR Report"
            }else{
                
                if supervisionReportList?.count ?? 0 > 0{
                    
                    if supervisionReportList![indexPath.row].statusText?.lowercased() == "completed"{
                        caseListVC.isCompletedReport = true
                    }
                    caseListVC.aliasId = supervisionReportList![indexPath.row].alias!
                }
                
                caseListVC.passingReportCategory = "Supervision Report"
            }
            self.navigationController?.pushViewController(caseListVC, animated: true)
        }
        
    }
    
    
    
}


extension ReportListPoViewController{
    
    func getAllIntakeReportPO(typeId: Int){

        let servicemodel = ServiceModel()

        if (NetworkConnection.isConnectedToNetwork()){

            do {

                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")

                DispatchQueue.global(qos: .userInitiated).async {

                    servicemodel.getAllIntakeReportPO(reportType: self.reportType, caseID: self.caseId, typeId: typeId) { responseStr in

                        DispatchQueue.main.async {

                            self.intakeReportList?.removeAll()

                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                                return
                            }

                            HUDIndicatorView.shared.dismissHUD()

                            if self.selectedReportId == 1{
                                if self.caseType == "cncp"{
                                    if let count = responseStr?.transferReports?.count {

                                        if(count > 0){

                                            self.transferreportsList = responseStr?.transferReports
                                        }
                                        else{
                                            ErrorToast.show(message: "No data found!", controller: self)
                                        }

                                        self.reportListTVC.reloadData()

                                    }else{
                                        
                                        HUDIndicatorView.shared.dismissHUD()

                                        // handle Error or show error message to the user
                                        servicemodel.showAlert(title: "Failed to load Transfer Report list.", message: "", parentView: self)
                                    }
                                }else{
                                    if let count = responseStr?.intakeReports?.count {

                                        if(count > 0){

                                            self.intakeReportList = responseStr?.intakeReports
                                        }
                                        else{
                                            ErrorToast.show(message: "No data found!", controller: self)
                                        }

                                        self.reportListTVC.reloadData()

                                    }else{
                                        
                                        HUDIndicatorView.shared.dismissHUD()

                                        // handle Error or show error message to the user
                                        servicemodel.showAlert(title: "Failed to load Intake Report list.", message: "", parentView: self)
                                    }
                                }
                                
                            }else if self.selectedReportId == 2{
                                if let count = responseStr?.sirReports?.count {

                                    if(count > 0){

                                        self.sirReportList = responseStr?.sirReports

                                    }
                                    else{
                                        ErrorToast.show(message: "No data found!", controller: self)
                                    }

                                    self.reportListTVC.reloadData()

                                }
                                
                                else{

                                    HUDIndicatorView.shared.dismissHUD()

                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: "Failed to load Sir Report list.", message: "", parentView: self)
                                }
                            }
                            else if self.selectedReportId == 3{
                                if let count = responseStr?.icpReports?.count {

                                    if(count > 0){

                                        self.icpReportList = responseStr?.icpReports

                                    }
                                    else{
                                        ErrorToast.show(message: "No data found!", controller: self)
                                    }

                                    self.reportListTVC.reloadData()

                                }
                                
                                else{

                                    HUDIndicatorView.shared.dismissHUD()

                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: "Failed to load Icp Report list.", message: "", parentView: self)
                                }

                            }else{
                                if let count = responseStr?.supervisionReports?.count {

                                    if(count > 0){

                                        self.supervisionReportList = responseStr?.supervisionReports

                                    }
                                    else{
                                        self.supervisionReportList?.removeAll()
                                        
                                        ErrorToast.show(message: "No data found!", controller: self)
                                    }

                                    self.reportListTVC.reloadData()

                                }
                                
                                else{

                                    HUDIndicatorView.shared.dismissHUD()

                                    // handle Error or show error message to the user
                                    servicemodel.showAlert(title: "Failed to load Supervision Report list.", message: "", parentView: self)
                                }
                            }
                            
                        }
                    }

                }
            }

        }
    }
}




//MARK:- Intake Report Model

struct IntakeReportListPoModelData: Codable {
    let intakeReports: [IntakeReportPo]?
    let icpReports: [ICPReportPO]?
    let sirReports: [SirReport]?
    let supervisionReports: [SupervisionReport]?
    let transferReports: [TransferReportPO]?
    
    enum CodingKeys: String, CodingKey {
        case intakeReports = "intake_reports"
        case icpReports = "icp_reports"
        case sirReports = "sir_reports"
        case supervisionReports = "supervision_reports"
        case transferReports = "transfer_reports"
    }
}

struct IntakeReportPo: Codable {
    let alias: String?
    let child: ChildDataPo?
    let status_text: String?
    let intakeCase: IntakeCase?

    enum CodingKeys: String, CodingKey {
        case alias
        case  child
        case status_text
        case intakeCase = "case"
    }
}

struct IntakeCase: Codable{
    let id: Int?
}

struct ChildDataPo: Codable {
    
    let firstName: String?
    let lastName: String?
    let genderName : String?
    let dob: String?
    let alias: String?
    let age: String?

    enum CodingKeys: String, CodingKey {
       
        case alias
        case firstName = "first_name"
        case lastName = "last_name"
        case genderName = "gender_name"
        case dob, age
    }
}

// MARK: - SupervisionReport
struct SupervisionReport: Codable {
    let completedAt, alias, statusText: String?
    let supervisionReportCase: IntakeCase?
    let status: Int?
    let requestedAt: String?
    let child: ChildDataPo?

    enum CodingKeys: String, CodingKey {
        case completedAt = "completed_at"
        case alias
        case statusText = "status_text"
        case supervisionReportCase = "case"
        case status
        case requestedAt = "requested_at"
        case child
    }
}

// MARK: - SirReport
struct SirReport: Codable {
    let completedAt, alias, dueAt, statusText: String?
    let status: Int?
    let requestedAt: String?
    let child: ChildDataPo?

    enum CodingKeys: String, CodingKey {
        case completedAt = "completed_at"
        case alias
        case dueAt = "due_at"
        case statusText = "status_text"
        case status
        case requestedAt = "requested_at"
        case child
    }
}


// MARK: - ICPReport
struct ICPReportPO: Codable {
    let alias, id : String?
    let child: ChildDataPo?
    let status_text: String?

    enum CodingKeys: String, CodingKey {
        case alias, id, child, status_text
    }
}


struct TransferReportPO: Codable {
    let submittedAt, alias, startedAt, statusText: String?
    let transferReportCase: IntakeCase?
    let status: Int?
    let child: ChildDataPo?

    enum CodingKeys: String, CodingKey {
        case submittedAt = "submitted_at"
        case alias
        case startedAt = "started_at"
        case statusText = "status_text"
        case transferReportCase = "case"
        case status, child
    }
}


extension ReportListPoViewController{
func startIntakeInspection(caseId: String, childId: String) {
    
    let servicemodel = ServiceModel()
    
    if (NetworkConnection.isConnectedToNetwork()){
        
        do {
            
            HUDIndicatorView.shared.showHUD(view: self.view,
                                            title: "Starting Inspection ..")
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                servicemodel.startNewIntakeReport(reportType: self.reportType, childId: childId, caseId: caseId) { responseStr in
                    
                    DispatchQueue.main.async {
                        
                        if responseStr == nil {
                            HUDIndicatorView.shared.dismissHUD()
                            servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                            return
                        }
                                                    
                        let status = responseStr?.status
                        
                        if(status == 1){
                                                                                                
                            Toast.show(message: responseStr?.message ?? "Your submission has been sent successfully!", controller: self)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                
                                self.getAllIntakeReportPO(typeId: self.selectedTypeId)
                            }

                            
                        }
                        else{
                            let message = responseStr?.message
                            servicemodel.showAlert(title: "Failed to submit!", message: message ?? "", parentView: self)
                            
                            HUDIndicatorView.shared.dismissHUD()
                        }
                    }
                }
            }
             
        }catch {
            
            servicemodel.handleError(error: error, parentView: self)
        }
    }else{
        servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
    }
}

}
