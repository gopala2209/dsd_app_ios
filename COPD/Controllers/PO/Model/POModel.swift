//
//  ProfileModel.swift
//  COPD
//
//  Created by Anand Prakash on 1/3/21.
//

import UIKit

// MARK: - SIRReportModel
struct SIRReportModel: Codable {
    let sirReports: [SIRReport]?

    enum CodingKeys: String, CodingKey {
        case sirReports = "sir_reports"
    }
}

// MARK: - SirReport
struct SIRReport: Codable {
    let completedAt, alias, dueAt, statusText: String?
    let sirReportCase: Case?
    let status: Int?
    let requestedAt: String?
    let child: Child?

    enum CodingKeys: String, CodingKey {
        case completedAt = "completed_at"
        case alias
        case dueAt = "due_at"
        case statusText = "status_text"
        case sirReportCase = "case"
        case status
        case requestedAt = "requested_at"
        case child
    }
}

// MARK: - Child
struct Child: Codable {
    let religionID, gender: Int?
    let city: String?
    let isAddressVerified: Int?
    let alias, firstName, lastName, aadharNumber: String?
    let genderName, dob, age: String?

    enum CodingKeys: String, CodingKey {
        case religionID = "religion_id"
        case gender, city
        case isAddressVerified = "is_address_verified"
        case alias
        case firstName = "first_name"
        case lastName = "last_name"
        case aadharNumber = "aadhar_number"
        case genderName = "gender_name"
        case dob, age
    }
}

// MARK: - Case
struct Case: Codable {
    let districtName, alias, crimeNumber: String?
    let policeStation: PoliceStation?
    let id: Int?

    enum CodingKeys: String, CodingKey {
        case districtName = "district_name"
        case alias
        case crimeNumber = "crime_number"
        case policeStation = "police_station"
        case id
    }
}

// MARK: - PoliceStation
struct PoliceStation: Codable {
    let number, name: String?
    let id: Int?
}


// MARK: - PoCaseListModelData
struct PoCaseListModelData: Codable {
    let cases: [PoCasesData]?
}

struct PoCasesData: Codable {
    let officerDesignation, poIntimatedAt: String?
    let dueInDays, caseType: Int?
    let policeStation: PoCasePoliceStation?
    let startAt: DueDate?
    let officerName, officerMobile, addedAt: String?
    let districtName: String?
    let isPoIntimated: Int?
    let alias, crimeNumber: String?
    let dueAt: DueDate?
    let id, calendarYear, status: Int?

    enum CodingKeys: String, CodingKey {
        case officerDesignation = "officer_designation"
        case poIntimatedAt = "po_intimated_at"
        case dueInDays = "due_in_days"
        case caseType = "case_type"
        case policeStation = "police_station"
        case startAt = "start_at"
        case officerName = "officer_name"
        case officerMobile = "officer_mobile"
        case addedAt = "added_at"
        case districtName = "district_name"
        case isPoIntimated = "is_po_intimated"
        case alias
        case crimeNumber = "crime_number"
        case dueAt = "due_at"
        case id
        case calendarYear = "calendar_year"
        case status
    }
}

// MARK: - PoCasePoliceStation
struct PoCasePoliceStation: Codable {
    let zipcode, number: String?
    let sjpuID: Int?
    let addressLine2, addressLine1, city, name: String?
    let id, districtID: Int?
    let landmark: String?

    enum CodingKeys: String, CodingKey {
        case zipcode, number
        case sjpuID = "sjpu_id"
        case addressLine2 = "address_line2"
        case addressLine1 = "address_line1"
        case city, name, id
        case districtID = "district_id"
        case landmark
    }
}

enum DueDate: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(DueDate.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for At"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}


// MARK: - Rehabilitation
struct RehabilitationModelData: Codable {
    let significantExperiences: [RehabilitationChildExpectation]?
    let basicDetails: [BasicDetail]?
    let supervisionCompletedAt: String?
    let interpersonalRelationships, independentLivingSkills, healthNutrition, selfCare: [RehabilitationChildExpectation]?
    let emotionPsychologicalSupport, religiousBeliefs, creativity: [RehabilitationChildExpectation]?
 
    let childExpectation, educationTraining: [RehabilitationChildExpectation]?
    
    let supervisionResult: String?
    let addressParentGuardian: String?

    enum CodingKeys: String, CodingKey {
        case significantExperiences = "significant_experiences"
        case basicDetails = "basic_details"
        case supervisionCompletedAt = "supervision_completed_at"
        case interpersonalRelationships = "interpersonal_relationships"
        case independentLivingSkills = "independent_living_skills"
        case healthNutrition = "health_nutrition"
        case selfCare = "self_care"
        case emotionPsychologicalSupport = "emotion_psychological_support"
        case religiousBeliefs = "religious_beliefs"
        case creativity
       
        case childExpectation = "child_expectation"
        case educationTraining = "education_training"
        
        case supervisionResult = "supervision_result"
        case addressParentGuardian = "address_parent_guardian"
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
            
        case "monthFullName" : return significantExperiences?.first?.monthFullName ?? ""
        case "calendarYear": return significantExperiences?.first?.calendarYear ?? 0
            
        case "commiteeOrBoard" : return basicDetails?.first?.commiteeOrBoard ?? 0

        case "childExpectation"  : return childExpectation?.first?.childExpectation ?? ""
        case "outcomeChildExpectation" : return childExpectation?.first?.outcomeChildExpectation ?? ""
        case "outcomeCreativity" : return creativity?.first?.outcomeCreativity ?? ""
        case "creativity" : return creativity?.first?.creativity ?? ""
        case "educationTraining" : return educationTraining?.first?.educationTraining ?? ""
        case "outcomeEducationTraining" : return educationTraining?.first?.outcomeEducationTraining ?? ""
        case "outcomeEmotionPsychologicalSupport" : return emotionPsychologicalSupport?.first?.outcomeEmotionPsychologicalSupport ?? ""
        case "emotionPsychologicalSupport" : return emotionPsychologicalSupport?.first?.emotionPsychologicalSupport ?? ""
        case "healthNutrition" : return healthNutrition?.first?.healthNutrition ?? ""
        case "outcomeHealthNutrition" : return healthNutrition?.first?.outcomeHealthNutrition ?? ""
        case "independentLivingSkills" : return independentLivingSkills?.first?.independentLivingSkills ?? ""
        case "outcomeIndependentLivingSkills" : return independentLivingSkills?.first?.outcomeIndependentLivingSkills ?? ""
        case "interpersonalRelationships" : return interpersonalRelationships?.first?.interpersonalRelationships ?? ""
        case "outcomeInterpersonalRelationships" : return interpersonalRelationships?.first?.outcomeInterpersonalRelationships ?? ""
        case "outcomeReligiousBeliefs" : return religiousBeliefs?.first?.outcomeReligiousBeliefs ?? ""
        case "religiousBeliefs" : return religiousBeliefs?.first?.religiousBeliefs ?? ""
        case "selfCare" : return selfCare?.first?.selfCare ?? ""
        case "outcomeSelfCare" : return selfCare?.first?.outcomeSelfCare ?? ""
        case "significantExperiences" : return significantExperiences?.first?.significantExperiences ?? ""
        case "outcomeSignificantExperiences" : return significantExperiences?.first?.outcomeSignificantExperiences ?? ""
        case "supervisionCompletedAt" : return supervisionCompletedAt ?? ""
            
        case "supervisionResult" : return supervisionResult ?? ""
        case "addressParentGuardian" : return addressParentGuardian ?? ""

        default: return ""
        }
    }
    
    
    // MARK: - BasicDetail
    struct BasicDetail: Codable {
        let commiteeOrBoard: Int?

        enum CodingKeys: String, CodingKey {
            case commiteeOrBoard = "commitee_or_board"
        }
    }
}

// MARK: - RehabilitationChildExpectation
struct RehabilitationChildExpectation: Codable {
    let childID, month: Int?
    let monthFullName: String?
    let monthShortName: String?
    let id, calendarYear: Int?
    let childExpectation, outcomeChildExpectation, outcomeCreativity, creativity: String?
    let educationTraining, outcomeEducationTraining, outcomeEmotionPsychologicalSupport, emotionPsychologicalSupport: String?
    let healthNutrition, outcomeHealthNutrition, independentLivingSkills, outcomeIndependentLivingSkills: String?
    let interpersonalRelationships, outcomeInterpersonalRelationships, outcomeReligiousBeliefs, religiousBeliefs: String?
    let selfCare, outcomeSelfCare, significantExperiences, outcomeSignificantExperiences: String?
    
    let commiteeOrBoard: Int?

    enum CodingKeys: String, CodingKey {
        case childID = "child_id"
        case month
        case monthFullName = "month_full_name" //Anand
        case monthShortName = "month_short_name"
        case id
        case calendarYear = "calendar_year"
        case childExpectation = "child_expectation"
        case outcomeChildExpectation = "outcome_child_expectation"
        case outcomeCreativity = "outcome_creativity"
        case creativity
        case educationTraining = "education_training"
        case outcomeEducationTraining = "outcome_education_training"
        case outcomeEmotionPsychologicalSupport = "outcome_emotion_psychological_support"
        case emotionPsychologicalSupport = "emotion_psychological_support"
        case healthNutrition = "health_nutrition"
        case outcomeHealthNutrition = "outcome_health_nutrition"
        case independentLivingSkills = "independent_living_skills"
        case outcomeIndependentLivingSkills = "outcome_independent_living_skills"
        case interpersonalRelationships = "interpersonal_relationships"
        case outcomeInterpersonalRelationships = "outcome_interpersonal_relationships"
        case outcomeReligiousBeliefs = "outcome_religious_beliefs"
        case religiousBeliefs = "religious_beliefs"
        case selfCare = "self_care"
        case outcomeSelfCare = "outcome_self_care"
        case significantExperiences = "significant_experiences"
        case outcomeSignificantExperiences = "outcome_significant_experiences"
        case commiteeOrBoard = "commitee_or_board"
    }
}


// MARK: - PreReleaseModule

struct PreReleaseModelData: Codable {
    let otherInformation: String?
    let proofType: Int?
    let medicalExamination: String?
    let document: PostReleaseUploadModelData?
    let emotionalNeeds, experience, childExpectation, relationships: String?
    let addedBy: Int?
    let childPlacement: String?
    let id: Int?
    let religiousBeliefs: String?
    let modifiedAt: Int?
    let leisure, trainingSkills, sponsorshipIndividual, skillTraining: String?
    let sponsoringAgency: PostReleaseUploadModelData?
    let transferRelease, livingSkills, rehabilitationPlan: String?
    let icpID, addedAt, childReportDocumentID, isEscortRequired: Int?
    let releaseDate, probationOfficer: String?
    let icpType: Int?
    let healthNeeds: String?
    let modifiedBy: Int?
    let proofTypeText, educationalNeeds: String?
    let nonGovernmental: PostReleaseUploadModelData?

    enum CodingKeys: String, CodingKey {
        case otherInformation = "other_information"
        case proofType = "proof_type"
        case medicalExamination = "medical_examination"
        case document
        case emotionalNeeds = "emotional_needs"
        case experience
        case childExpectation = "child_expectation"
        case relationships
        case addedBy = "added_by"
        case childPlacement = "child_placement"
        case id
        case religiousBeliefs = "religious_beliefs"
        case modifiedAt = "modified_at"
        case leisure
        case trainingSkills = "training_skills"
        case sponsorshipIndividual = "sponsorship_individual"
        case skillTraining = "skill_training"
        case sponsoringAgency = "sponsoring_agency"
        case transferRelease = "transfer_release"
        case livingSkills = "living_skills"
        case rehabilitationPlan = "rehabilitation_plan"
        case icpID = "icp_id"
        case addedAt = "added_at"
        case childReportDocumentID = "child_report_document_id"
        case isEscortRequired = "is_escort_required"
        case releaseDate = "release_date"
        case probationOfficer = "probation_officer"
        case icpType = "icp_type"
        case healthNeeds = "health_needs"
        case modifiedBy = "modified_by"
        case proofTypeText = "proof_type_text"
        case educationalNeeds = "educational_needs"
        case nonGovernmental = "non_governmental"
    }
    
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
            
        case "otherInformation" : return otherInformation ?? ""
        case "proofType" : return proofType ?? 0
        case "medicalExamination" : return medicalExamination ?? ""
        case "lastProgressDocument" : return document ?? ""
        case "emotionalNeeds" : return emotionalNeeds ?? ""
        case "experience" : return experience ?? ""
        case "childExpectation" : return childExpectation ?? ""
        case "relationships" : return relationships ?? ""
        case "addedBy" : return addedBy ?? 0
        case "childPlacement" : return childPlacement ?? ""
        case "id" : return id ?? 0
        case "religiousBeliefs" : return religiousBeliefs ?? ""
        case "modifiedAt" : return modifiedAt ?? 0
        case "leisure" : return leisure ?? ""
        case "trainingSkills" : return trainingSkills ?? ""
        case "sponsorshipIndividual" : return sponsorshipIndividual ?? ""
        case "skillTraining" : return skillTraining ?? ""
        case "sponsoringAgency" : return sponsoringAgency ?? ""
        case "transferRelease" : return transferRelease ?? ""
        case "livingSkills" : return livingSkills ?? ""
        case "rehabilitationPlan" : return rehabilitationPlan ?? ""
        case "icpID" : return icpID ?? 0
        case "addedAt" : return addedAt ?? 0
        case "childReportDocumentID" : return childReportDocumentID ?? 0
        case "isEscortRequired" : return isEscortRequired ?? ""
        case "releaseDate" : return releaseDate ?? ""
        case "probationOfficer" : return probationOfficer ?? ""
        case "icpType" : return icpType ?? 0
        case "healthNeeds" : return healthNeeds ?? ""
        case "modifiedBy" : return modifiedBy ?? 0
        case "proofTypeText" : return proofTypeText ?? ""
        case "educationalNeeds" : return educationalNeeds ?? ""
        case "nonGovernmental" : return nonGovernmental ?? ""
        
        default: return ""
        }
    }
}


// MARK: - PostReleaseModule

struct PostReleaseModelData: Codable {
    let birthCertificate, interactionReport: Int?
    let childOpinion: String?
    let document: PostReleaseUploadModelData?
    let skillsAcquired, childAdmitted, bankStatusText: String?
    let govermentCompensation: Int?
    let identityCompensation: String?
    let addedBy, schoolCertificate, admittedSchoolVaction: Int?
    let admittedDate: String?
    let id, modifiedAt, childBelongings: Int?
    let familyBehaviour, rehabilitationRestorationPlan: String?
    let aadharCard: Int?
    let neighboursAttitude, admittedSchool: String?
    let icpID, bplCard, addedAt: Int?
    let otherChildBelongings: String?
    let disabilityCertificate, bankStatus, icpType, rationCard: Int?
    let modifiedBy, immunisationCard: Int?

    enum CodingKeys: String, CodingKey {
        case birthCertificate = "birth_certificate"
        case interactionReport = "interaction_report"
        case childOpinion = "child_opinion"
        case document
        case skillsAcquired = "skills_acquired"
        case childAdmitted = "child_admitted"
        case bankStatusText = "bank_status_text"
        case govermentCompensation = "goverment_compensation"
        case identityCompensation = "identity_compensation"
        case addedBy = "added_by"
        case schoolCertificate = "school_certificate"
        case admittedSchoolVaction = "admitted_school_vaction"
        case admittedDate = "admitted_date"
        case id
        case modifiedAt = "modified_at"
        case childBelongings = "child_belongings"
        case familyBehaviour = "family_behaviour"
        case rehabilitationRestorationPlan = "rehabilitation_restoration_plan"
        case aadharCard = "aadhar_card"
        case neighboursAttitude = "neighbours_attitude"
        case admittedSchool = "admitted_school"
        case icpID = "icp_id"
        case bplCard = "bpl_card"
        case addedAt = "added_at"
        case otherChildBelongings = "other_child_belongings"
        case disabilityCertificate = "disability_certificate"
        case bankStatus = "bank_status"
        case icpType = "icp_type"
        case rationCard = "ration_card"
        case modifiedBy = "modified_by"
        case immunisationCard = "immunisation_card"
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "birthCertificate": return birthCertificate ?? ""
        case "interactionReport": return interactionReport ?? ""
        case "childOpinion": return childOpinion ?? ""
        case "skillsAcquired": return skillsAcquired ?? ""
        case "childAdmitted": return childAdmitted ?? ""
        case "bankStatusText": return bankStatusText ?? ""
        case "govermentCompensation": return govermentCompensation ?? ""
        case "identityCompensation": return identityCompensation ?? ""
        case "addedBy": return addedBy ?? 0
        case "schoolCertificate": return schoolCertificate ?? ""
        case "admittedSchoolVaction": return admittedSchoolVaction ?? ""
        case "admittedDate": return admittedDate ?? ""
        case "id": return id ?? 0
        case "modifiedAt": return modifiedAt ?? 0
        case "childBelongings": return childBelongings ?? ""
        case "familyBehaviour": return familyBehaviour ?? ""
        case "rehabilitationRestorationPlan": return rehabilitationRestorationPlan ?? ""
        case "aadharCard": return aadharCard ?? ""
        case "neighboursAttitude": return neighboursAttitude ?? ""
        case "admittedSchool": return admittedSchool ?? ""
        case "icpID": return icpID ?? 0
        case "bplCard": return bplCard ?? ""
        case "addedAt": return addedAt ?? 0
        case "otherChildBelongings": return otherChildBelongings ?? ""
        case "disabilityCertificate": return disabilityCertificate ?? ""
        case "bankStatus": return bankStatus ?? ""
        case "icpType": return icpType ?? 0
        case "rationCard": return rationCard ?? ""
        case "modifiedBy": return modifiedBy ?? 0
        case "immunisationCard": return immunisationCard ?? ""
        case "document": return document ?? ""
        default: return ""
        }
    }

}

// MARK: - Document
struct PostReleaseUploadModelData: Codable {
    let uploadedBy, uploadedAt: Int?
    let name, location: String?
    let id, type: Int?

    enum CodingKeys: String, CodingKey {
        case uploadedBy = "uploaded_by"
        case uploadedAt = "uploaded_at"
        case name, location, id, type
    }
}



//MARK:- RehabilitationModule

struct ICPRehabilitationChildModelData: Codable {
    let significantExperiences: [RehabilitationChildExpectationModelData]
    let supervisionCompletedAt: String?
    let interpersonalRelationships, independentLivingSkills, healthNutrition, selfCare: [RehabilitationChildExpectationModelData]?
    let emotionPsychologicalSupport, religiousBeliefs, creativity: [RehabilitationChildExpectationModelData]?
   
    let childExpectation, educationTraining: [RehabilitationChildExpectationModelData]?
    
    let supervisionResult: String?
    let addressParentGuardian: String?

    enum CodingKeys: String, CodingKey {
        case significantExperiences = "significant_experiences"
        case supervisionCompletedAt = "supervision_completed_at"
        case interpersonalRelationships = "interpersonal_relationships"
        case independentLivingSkills = "independent_living_skills"
        case healthNutrition = "health_nutrition"
        case selfCare = "self_care"
        case emotionPsychologicalSupport = "emotion_psychological_support"
        case religiousBeliefs = "religious_beliefs"
        case creativity
       
        case childExpectation = "child_expectation"
        case educationTraining = "education_training"
        
        case supervisionResult = "supervision_result"
        case addressParentGuardian = "address_parent_guardian"
    }
}

// MARK: - ChildExpectation
struct RehabilitationChildExpectationModelData: Codable {
    let childID, month: Int?
    let monthFullName, monthShortName: String?
    let id, calendarYear, commiteeOrBoard: Int?
    let childExpectation, outcomeChildExpectation, creativity, outcomeCreativity: String?
    let educationTraining, outcomeEducationTraining, emotionPsychologicalSupport, outcomeEmotionPsychologicalSupport: String?
    let healthNutrition, outcomeHealthNutrition, independentLivingSkills, outcomeIndependentLivingSkills: String?
    let interpersonalRelationships, outcomeInterpersonalRelationships, religiousBeliefs, outcomeReligiousBeliefs: String?
    let selfCare, outcomeSelfCare, significantExperiences, outcomeSignificantExperiences: String?

    enum CodingKeys: String, CodingKey {
        case childID = "child_id"
        case month
        case monthFullName = "month_full_name" //Anand
        case monthShortName = "month_short_name"
        case id
        case calendarYear = "calendar_year"
        case childExpectation = "child_expectation"
        case outcomeChildExpectation = "outcome_child_expectation"
        case creativity
        case outcomeCreativity = "outcome_creativity"
        case educationTraining = "education_training"
        case outcomeEducationTraining = "outcome_education_training"
        case emotionPsychologicalSupport = "emotion_psychological_support"
        case outcomeEmotionPsychologicalSupport = "outcome_emotion_psychological_support"
        case healthNutrition = "health_nutrition"
        case outcomeHealthNutrition = "outcome_health_nutrition"
        case independentLivingSkills = "independent_living_skills"
        case outcomeIndependentLivingSkills = "outcome_independent_living_skills"
        case interpersonalRelationships = "interpersonal_relationships"
        case outcomeInterpersonalRelationships = "outcome_interpersonal_relationships"
        case religiousBeliefs = "religious_beliefs"
        case outcomeReligiousBeliefs = "outcome_religious_beliefs"
        case selfCare = "self_care"
        case outcomeSelfCare = "outcome_self_care"
        case significantExperiences = "significant_experiences"
        case outcomeSignificantExperiences = "outcome_significant_experiences"
        case commiteeOrBoard = "commitee_or_board"
    }
    
    
}


///MARK:- Intake Detail po OLD

struct IntakeDetailModelPoData: Codable {
    let submittedAt, alias, startedAt, statusText: String?
    let intakeCase: SirCasePo?
    let status: Int?
    let child: IntakeDetailChild?
    
    //ICP
    let aboutEarningsDetail, aboutSavingsDetail, emotionalNeeds: String?
    let experience, aboutAwardsDetail, childExpectation, relationships: String?
    let id, religiousBeliefs, leisure: String?
    let dateOfAdmission, skillTraining: String?
    let typeOfStay: Int?
    let livingSkills: String?
    let isChildInInstitution, isEarningsDetailAvailable, isSavingsDetailAvailable: Int?
    let healthNeeds, dateOfICP, admissionNumber: String?
    let educationalNeeds: String?
    let isAwardsDetailAvailable: Int?
    //NEW ICP
    let boardAddress: String?
    let placeOfInterview: String?
    let dateOfInterview: String?
    let progressFromDate: String?
    let progressToDate: String?
    let proposedInterventions: String?
    
    let interventionLeisure: String?
    let interventionEmotionalNeeds: String?
    let interventionReligiousBeliefs: String?
    let interventionExperience : String?
    let interventionSkillTraining : String?
    let interventionHealthNeeds : String?
    let interventionRelationships : String?
    let interventionLivingSkills : String?
    let interventionChildExpectation : String?
    let interventionEducationalNeeds : String?
    
    //SIR
    let sirVocation: SirVocation?
    let sirCondition: SirCondition?
    let completedAt: String?
    let sirInquiry: SirInquiry?
    let sirHistory: SirHistory?
    let sirEducation: SirEducation?
    let dueAt: String?
    let sirFamily: [SirFamily]?
    let sirCase: SirCasePo?
    let requestedAt: String?
    
    
    enum CodingKeys: String, CodingKey {
        case submittedAt = "submitted_at"
        case alias
        case startedAt = "started_at"
        case statusText = "status_text"
        case intakeCase, sirCase = "case"
        case status, child
        //ICP
        case aboutEarningsDetail = "about_earnings_detail"
        case aboutSavingsDetail = "about_savings_detail"
        case emotionalNeeds = "emotional_needs"
        case experience
        case aboutAwardsDetail = "about_awards_detail"
        case childExpectation = "child_expectation"
        case relationships, id
        case religiousBeliefs = "religious_beliefs"
        case leisure
        case dateOfAdmission = "date_of_admission"
        case skillTraining = "skill_training"
        case typeOfStay = "type_of_stay"
        case livingSkills = "living_skills"
        case isChildInInstitution = "is_child_in_institution"
        case isEarningsDetailAvailable = "is_earnings_detail_available"
        case isSavingsDetailAvailable = "is_savings_detail_available"
        case healthNeeds = "health_needs"
        case dateOfICP = "date_of_icp"
        case admissionNumber = "admission_number"
        case educationalNeeds = "educational_needs"
        case isAwardsDetailAvailable = "is_awards_detail_available"
        //NEW ICP
        case boardAddress = "board_address"
        case placeOfInterview = "place_of_interview"
        case dateOfInterview = "date_of_interview"
        case progressFromDate = "progress_from_date"
        case progressToDate = "progress_to_date"
        case proposedInterventions = "proposed_interventions"
        case interventionLeisure = "intervention_leisure"
        case interventionEmotionalNeeds = "intervention_emotional_needs"
        case interventionReligiousBeliefs = "intervention_religious_beliefs"
        case interventionExperience = "intervention_experience"
        case interventionSkillTraining = "intervention_skill_training"
        case interventionHealthNeeds = "intervention_health_needs"
        case interventionRelationships = "intervention_relationships"
        case interventionLivingSkills = "intervention_living_skills"
        case interventionChildExpectation = "intervention_child_expectation"
        case interventionEducationalNeeds = "intervention_educational_needs"
        
        //SIR
        case sirVocation = "sir_vocation"
        case sirCondition = "sir_condition"
        case completedAt = "completed_at"
        case sirInquiry = "sir_inquiry"
        case sirHistory = "sir_history"
        case sirEducation = "sir_education"
        case dueAt = "due_at"
        case sirFamily = "sir_family"
        case requestedAt = "requested_at"
        
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "submittedAt": return submittedAt ?? ""
        case "alias": return alias ?? ""
        case "startedAt": return startedAt ?? ""
        case "statusText": return statusText ?? ""
        case "habits": return child?.habits ?? ""
        case "guardianDistrictId": return child?.guardianDistrictName ?? ""
        case "communityName": return child?.communityName ?? ""
        case "guardianLandmark": return child?.guardianLandmark ?? ""
        case "stateName": return child?.stateName ?? ""
        case "guardianType": return child?.guardianType ?? ""
        case "talukID": return child?.talukID ?? 0
        case "isAddressVerified": return child?.isAddressVerified ?? ""
        case "id": return child?.id ?? 0
        case "stateID": return child?.stateID ?? 0
        case "landmark": return child?.landmark ?? ""
        case "isGuardianAvailable": return child?.isGuardianAvailable ?? false
        case "guardianAge": return child?.guardianAge ?? ""
        case "zipcode": return child?.zipcode ?? ""
        case "dob": return child?.dob ?? ""
        case "guardianDesignation": return child?.guardianDesignation ?? ""
        case "personalIdentificationMark": return child?.personalIdentificationMark ?? ""
        case "religionName": return child?.religionName ?? ""
        case "countryID": return child?.countryID ?? 0
        case "guardianCountryId": return child?.guardianCountryName ?? ""
        case "religionID": return child?.religionID ?? 0
        case "gender": return child?.gender ?? ""
        case "city": return child?.city ?? ""
        case "guardianTalukId": return child?.guardianTalukName ?? ""
        case "districtName": return child?.districtName ?? ""
        case "guardianFirstName": return child?.guardianFirstName ?? ""
        case "communityID": return child?.communityID ?? 0
        case "addressLine2": return child?.addressLine2 ?? ""
        case "addressLine1": return child?.addressLine1 ?? ""
        case "guardianMobile": return child?.guardianMobile ?? ""
            
        case "guardianStateId": return child?.guardianStateName ?? ""
            
        case "guardianAddressLine1": return child?.guardianAddressLine1 ?? ""
        case "guardianAddressLine2": return child?.guardianAddressLine2 ?? ""
        case "countryName": return child?.countryName ?? ""
        case "childAlias": return child?.alias ?? ""
        case "firstName": return child?.firstName ?? ""
        case "summary": return child?.summary ?? ""
        case "guardianLastName": return child?.guardianLastName ?? ""
        case "talukName": return child?.talukName ?? ""
        case "lastName": return child?.lastName ?? ""
        case "guardianCity": return child?.guardianCity ?? ""
        case "aadharNumber": return child?.aadharNumber ?? ""
        case "genderName": return child?.genderName ?? ""
        case "guardianZipcode": return child?.guardianZipcode ?? ""
        case "interestedPersonDetails": return child?.interestedPersonDetails ?? ""
        case "languageByChild": return child?.languageByChild ?? ""
        case "sibling": return child?.sibling ?? ""

        case "guardianDob": return child?.guardianDob ?? ""
        case "districtID": return child?.districtID ?? 0
        case "age": return child?.age ?? ""
            
            
        case "aboutEarningsDetail" : return aboutEarningsDetail ?? ""
        case "aboutSavingsDetail" : return aboutSavingsDetail ?? ""
        case "emotionalNeeds" : return emotionalNeeds ?? ""
        case "experience" : return experience ?? ""
        case "aboutAwardsDetail" : return aboutAwardsDetail ?? ""
        case "childExpectation" : return childExpectation ?? ""
        case "relationships" : return relationships ?? ""
        case "religiousBeliefs" : return religiousBeliefs ?? ""
        case "leisure" : return leisure ?? ""
        case "dateOfAdmission" : return dateOfAdmission ?? ""
        case "skillTraining": return skillTraining ?? ""
        case "typeOfStay" : return typeOfStay ?? 0
        case "livingSkills" : return livingSkills ?? ""
        case "isChildInInstitution" : return isChildInInstitution ?? ""
        case "isEarningsDetailAvailable" : return isEarningsDetailAvailable ?? ""
        case "isSavingsDetailAvailable" : return isSavingsDetailAvailable ?? ""
        case "healthNeeds" : return healthNeeds ?? ""
        case "dateOfICP" : return dateOfICP ?? ""
        case "admissionNumber" : return admissionNumber ?? ""
        case "educationalNeeds" : return educationalNeeds ?? ""
        case "isAwardsDetailAvailable" : return isAwardsDetailAvailable ?? ""
            
        //NEWICP
            
        case "boardAddress": return boardAddress ?? ""
        case "placeOfInterview": return placeOfInterview ?? ""
        case "dateOfInterview": return dateOfInterview ?? ""
        case "progressFromDate": return progressFromDate ?? ""
        case "progressToDate": return progressToDate ?? ""
        case "proposedInterventions": return proposedInterventions ?? ""
            
        case "interventionLeisure": return interventionLeisure ?? ""
        case "interventionEmotionalNeeds": return interventionEmotionalNeeds ?? ""
        case "interventionReligiousBeliefs": return interventionReligiousBeliefs ?? ""
        case "interventionExperience" : return interventionExperience ?? ""
        case "interventionSkillTraining" : return interventionSkillTraining ?? ""
        case "interventionHealthNeeds" : return interventionHealthNeeds ?? ""
        case "interventionRelationships" : return interventionRelationships ?? ""
        case "interventionLivingSkills" : return interventionLivingSkills ?? ""
        case "interventionChildExpectation" : return interventionChildExpectation ?? ""
        case "interventionEducationalNeeds" : return interventionEducationalNeeds ?? ""
            
        default: return ""
        }
    }
}




//MARK:- Intake Detail PO NEW


// MARK: - IntakeDetailModelPoData
struct SIRDetailModelPoData: Codable {
    let sirReport: SirReportPO

    enum CodingKeys: String, CodingKey {
        case sirReport = "sir_report"
    }
}

struct SirReportPO: Codable {
    let submittedAt, alias, startedAt, statusText: String?
    let intakeCase: SirCasePo?
    let status: Int?
    let child: IntakeDetailChild?

    //ICP
    let aboutEarningsDetail, aboutSavingsDetail, emotionalNeeds: String?
    let experience, aboutAwardsDetail, childExpectation, relationships: String?
    let id, religiousBeliefs, leisure: String?
    let dateOfAdmission, skillTraining: String?
    let typeOfStay: Int?
    let livingSkills: String?
    let isChildInInstitution, isEarningsDetailAvailable, isSavingsDetailAvailable: Int?
    let healthNeeds, dateOfICP, admissionNumber: String?
    let educationalNeeds: String?
    let isAwardsDetailAvailable: Int?

    //SIR
    let sirVocation: SirVocation?
    let sirCondition: SirCondition?
    let completedAt: String?
    let sirInquiry: SirInquiry?
    let sirHistory: SirHistory?
    let sirEducation: SirEducation?
    let dueAt: String?
    let sirFamily: [SirFamily]?
    //let sirFamilyIndividual: SirFamily?
    let sirCase: SirCasePo?
    let requestedAt: String?


    enum CodingKeys: String, CodingKey {
        case submittedAt = "submitted_at"
        case alias
        case startedAt = "started_at"
        case statusText = "status_text"
        case intakeCase, sirCase = "case"
        case status, child
        //ICP
        case aboutEarningsDetail = "about_earnings_detail"
        case aboutSavingsDetail = "about_savings_detail"
        case emotionalNeeds = "emotional_needs"
        case experience
        case aboutAwardsDetail = "about_awards_detail"
        case childExpectation = "child_expectation"
        case relationships, id
        case religiousBeliefs = "religious_beliefs"
        case leisure
        case dateOfAdmission = "date_of_admission"
        case skillTraining = "skill_training"
        case typeOfStay = "type_of_stay"
        case livingSkills = "living_skills"
        case isChildInInstitution = "is_child_in_institution"
        case isEarningsDetailAvailable = "is_earnings_detail_available"
        case isSavingsDetailAvailable = "is_savings_detail_available"
        case healthNeeds = "health_needs"
        case dateOfICP = "date_of_icp"
        case admissionNumber = "admission_number"
        case educationalNeeds = "educational_needs"
        case isAwardsDetailAvailable = "is_awards_detail_available"

        //SIR
        case sirVocation = "sir_vocation"
        case sirCondition = "sir_condition"
        case completedAt = "completed_at"
        case sirInquiry = "sir_inquiry"
        case sirHistory = "sir_history"
        case sirEducation = "sir_education"
        case dueAt = "due_at"
        //case sirFamilyIndividual = "sir_family"
        case sirFamily = "sir_family"
        case requestedAt = "requested_at"

    }

    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "submittedAt": return submittedAt ?? ""
        case "alias": return alias ?? ""
        case "startedAt": return startedAt ?? ""
        case "statusText": return statusText ?? ""
        case "habits": return child?.habits ?? ""
        case "guardianDistrictId": return child?.guardianDistrictName ?? ""
        case "communityName": return child?.communityName ?? ""
        case "guardianLandmark": return child?.guardianLandmark ?? ""
        case "stateName": return child?.stateName ?? ""
        case "guardianType": return child?.guardianType ?? 0
        case "talukID": return child?.talukID ?? 0
        case "isAddressVerified": return child?.isAddressVerified ?? 0
        case "id": return child?.id ?? 0
        case "stateID": return child?.stateID ?? 0
        case "landmark": return child?.landmark ?? ""
        case "isGuardianAvailable": return child?.isGuardianAvailable ?? false
        case "guardianAge": return child?.guardianAge ?? ""
        case "zipcode": return child?.zipcode ?? ""
        case "dob": return child?.dob ?? ""
        case "guardianDesignation": return child?.guardianDesignation ?? ""
        case "personalIdentificationMark": return child?.personalIdentificationMark ?? ""
        case "religionName": return child?.religionName ?? ""
        case "countryID": return child?.countryID ?? 0
        case "guardianCountryId": return child?.guardianCountryName ?? ""
        case "religionID": return child?.religionID ?? 0
        case "gender": return child?.gender ?? 0
        case "city": return child?.city ?? ""
        case "guardianTalukId": return child?.guardianTalukName ?? ""
        case "districtName": return child?.districtName ?? ""
        case "guardianFirstName": return child?.guardianFirstName ?? ""
        case "communityID": return child?.communityID ?? 0
        case "addressLine2": return child?.addressLine2 ?? ""
        case "addressLine1": return child?.addressLine1 ?? ""
        case "guardianMobile": return child?.guardianMobile ?? ""

        case "guardianStateId": return child?.guardianStateName ?? ""

        case "guardianAddressLine1": return child?.guardianAddressLine1 ?? ""
        case "guardianAddressLine2": return child?.guardianAddressLine2 ?? ""
        case "countryName": return child?.countryName ?? ""
        case "childAlias": return child?.alias ?? ""
        case "firstName": return child?.firstName ?? ""
        case "summary": return child?.summary ?? ""
        case "guardianLastName": return child?.guardianLastName ?? ""
        case "talukName": return child?.talukName ?? ""
        case "lastName": return child?.lastName ?? ""
        case "guardianCity": return child?.guardianCity ?? ""
        case "aadharNumber": return child?.aadharNumber ?? ""
        case "genderName": return child?.genderName ?? ""
        case "guardianZipcode": return child?.guardianZipcode ?? ""
        case "interestedPersonDetails": return child?.interestedPersonDetails ?? ""
        case "languageByChild": return child?.languageByChild ?? ""
        case "sibling": return child?.sibling ?? ""
        case "guardianDob": return child?.guardianDob ?? ""
        case "districtID": return child?.districtID ?? 0
        case "age": return child?.age ?? ""


        case "aboutEarningsDetail" : return aboutEarningsDetail ?? ""
        case "aboutSavingsDetail" : return aboutSavingsDetail ?? ""
        case "emotionalNeeds" : return emotionalNeeds ?? ""
        case "experience" : return experience ?? ""
        case "aboutAwardsDetail" : return aboutAwardsDetail ?? ""
        case "childExpectation" : return childExpectation ?? ""
        case "relationships" : return relationships ?? ""
        case "religiousBeliefs" : return religiousBeliefs ?? ""
        case "leisure" : return leisure ?? ""
        case "dateOfAdmission" : return dateOfAdmission ?? ""
        case "skillTraining": return skillTraining ?? ""
        case "typeOfStay" : return typeOfStay ?? 0
        case "livingSkills" : return livingSkills ?? ""
        case "isChildInInstitution" : return isChildInInstitution ?? 0
        case "isEarningsDetailAvailable" : return isEarningsDetailAvailable ?? 0
        case "isSavingsdetailAvailable" : return isSavingsDetailAvailable ?? 0
        case "healthNeeds" : return healthNeeds ?? ""
        case "dateOfICP" : return dateOfICP ?? ""
        case "admissionNumber" : return admissionNumber ?? ""
        case "educationalNeeds" : return educationalNeeds ?? ""
        case "isAwardsDetailAvailable" : return isAwardsDetailAvailable ?? 0

        default: return ""
        }
    }
}



struct IntakeDetailChild: Codable {
    let isTransfered: Int?
    let cwcDistrictID: Int?
    let habits: String?
    let guardianDistrictID: Int?
    let communityName, guardianLandmark, stateName: String?
    let guardianType, talukID, isAddressVerified, id: Int?
    let stateID: Int?
    let landmark: String?
    let isGuardianAvailable: Bool?
    let guardianAge, zipcode, dob, guardianDesignation: String?
    let personalIdentificationMark, religionName: String?
    let countryID, guardianCountryID, religionID, gender: Int?
    let city: String?
    let guardianTalukID: Int?
    let districtName, guardianFirstName: String?
    let communityID: Int?
    let addressLine2, addressLine1, guardianMobile: String?
    let guardianStateID: Int?
    let guardianAddressLine1, guardianAddressLine2, countryName, alias: String?
    let firstName, summary, guardianLastName, talukName: String?
    let lastName, guardianCity, aadharNumber, genderName: String?
    let guardianZipcode, interestedPersonDetails, languageByChild, sibling, guardianDob: String?
    let districtID: Int?
    let age: String?
    
    let guardianTalukName, guardianCountryName, guardianStateName, guardianDistrictName: String?
    
    let secondaryGuardianFirstName, secondaryGuardianLastName, transferSuggestions: String?
    
    enum CodingKeys: String, CodingKey {
        case isTransfered = "is_transfered"
        case cwcDistrictID = "cwc_district_id"
        case habits
        case guardianDistrictID = "guardian_district_id"
        case communityName = "community_name"
        case guardianLandmark = "guardian_landmark"
        case stateName = "state_name"
        case guardianType = "guardian_type"
        case talukID = "taluk_id"
        case isAddressVerified = "is_address_verified"
        case id
        case stateID = "state_id"
        case landmark
        case isGuardianAvailable = "is_guardian_available"
        case guardianAge = "guardian_age"
        case zipcode, dob
        case guardianDesignation = "guardian_designation"
        case personalIdentificationMark = "personal_identification_mark"
        case religionName = "religion_name"
        case countryID = "country_id"
        case guardianCountryID = "guardian_country_id"
        case religionID = "religion_id"
        case gender, city
        case guardianTalukID = "guardian_taluk_id"
        case districtName = "district_name"
        case guardianFirstName = "guardian_first_name"
        case communityID = "community_id"
        case addressLine2 = "address_line2"
        case addressLine1 = "address_line1"
        case guardianMobile = "guardian_mobile"
        case guardianStateID = "guardian_state_id"
        case guardianAddressLine1 = "guardian_address_line1"
        case guardianAddressLine2 = "guardian_address_line2"
        case countryName = "country_name"
        case alias
        case firstName = "first_name"
        case summary
        case guardianLastName = "guardian_last_name"
        case talukName = "taluk_name"
        case lastName = "last_name"
        case guardianCity = "guardian_city"
        
        case aadharNumber = "aadhar_number"
        case genderName = "gender_name"
        case guardianZipcode = "guardian_zipcode"
        case interestedPersonDetails = "interested_person_details"
        case languageByChild = "language_by_child"
        case sibling = "sibling"
        case guardianDob = "guardian_dob"
        case districtID = "district_id"
        case age
        
        case guardianTalukName = "guardian_taluk_name"
        case guardianCountryName = "guardian_country_name"
        case guardianStateName = "guardian_state_name"
        case guardianDistrictName = "guardian_district_name"
        
        case secondaryGuardianFirstName = "secondary_guardian_first_name"
        case secondaryGuardianLastName = "secondary_guardian_last_name"
        case transferSuggestions = "transfer_suggestions"
    }
}

struct IntakeDetailCase: Codable {
    let districtName, alias, crimeNumber: String?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case districtName = "district_name"
        case alias
        case crimeNumber = "crime_number"
        case id
    }
}


// MARK: - SirCondition
struct SirCondition: Codable {
    let roomsCount, delinquencyFamilyRecord, familyOffenceCommitted, siblingChildRelation: String?
    let otherRelatives, otherFactors, environment, fatherMotherRelation: String?
    let economicStatus: Int?
    let maritalType, drinkingWater: Int?
    let attitudeOfReligion, parentChildRelation: String?
    let id: Int?
    let homeType: String?
    let bathroom: Int?
    let homeEthicalCode: String?
    let maritalTypeResponse: String?
    
    enum CodingKeys: String, CodingKey {
        case roomsCount = "rooms_count"
        case delinquencyFamilyRecord = "delinquency_family_record"
        case familyOffenceCommitted = "family_offence_committed"
        case siblingChildRelation = "sibling_child_relation"
        case otherRelatives = "other_relatives"
        case otherFactors = "other_factors"
        case economicStatus = "economic_status"
        case environment
        case fatherMotherRelation = "father_mother_relation"
        case maritalType = "marital_type"
        case drinkingWater = "drinking_water"
        case attitudeOfReligion = "attitude_of_religion"
        case parentChildRelation = "parent_child_relation"
        case id
        case homeType = "home_type"
        case bathroom
        case homeEthicalCode = "home_ethical_code"
        case maritalTypeResponse = "marital_type_response"
    }
    
    func valueByPropertyName(name:String) -> Any {
        
        if name.contains("maritalType") == true{
            
        }
        
        switch name {
        case "roomsCount" : return roomsCount ?? ""
        case "delinquencyFamilyRecord" : return delinquencyFamilyRecord ?? ""
        case "familyOffenceCommitted" : return familyOffenceCommitted ?? ""
        case "siblingChildRelation" : return siblingChildRelation ?? ""
        case "otherRelatives" : return otherRelatives ?? ""
        case "otherFactors" : return otherFactors ?? ""
        case "economicStatus" : return economicStatus ?? ""
        case "environment" : return environment ?? ""
        case "fatherMotherRelation" : return fatherMotherRelation ?? ""
        case "maritalType" : return maritalType ?? ""
        case "drinkingWater" : return drinkingWater ?? ""
        case "attitudeOfReligion" : return attitudeOfReligion ?? ""
        case "parentChildRelation" : return parentChildRelation ?? ""
        case "homeType" : return homeType ?? ""
        case "bathroom" : return bathroom ?? ""
        case "homeEthicalCode" : return homeEthicalCode ?? ""
        case "maritalTypeResponse" : return maritalTypeResponse ?? ""
        default: return ""
        }
    }
}

// MARK: - SirEducation
struct SirEducation: Codable {
    let educationLevel: String?
    let id: Int?
    let lastSchool, reasonForLeaving, extraCurricular, punishmentInSchool: String?
    
    enum CodingKeys: String, CodingKey {
        case educationLevel = "education_level"
        case id
        case lastSchool = "last_school"
        case reasonForLeaving = "reason_for_leaving"
        case extraCurricular = "extra_curricular"
        case punishmentInSchool = "punishment_in_school"
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "educationLevel" : return educationLevel ?? ""
        case "lastSchool" : return lastSchool ?? ""
        case "reasonForLeaving" : return reasonForLeaving ?? ""
        case "extraCurricular" : return extraCurricular ?? ""
        case "punishmentInSchool" : return punishmentInSchool ?? ""
        default: return ""
        }
    }
}

// MARK: - SirFamily
struct SirFamily: Codable {

    let familyFirstName, familyLastName, familyRelationship: String?
    let gender: Int
    let familyDob: String?
    let familyOccupation, familyEducation: String?
    let id: Int?
    let familyIncomeType: Int?
    let income: String?
    let familyDisability: Int?
    let familyDisablityDetail: String?
    let familyHealthStatus: String?
    let familySocialHabits: String?
    let familyChildType: Int?
    let familyContactNumber: String?
    
    enum CodingKeys: String, CodingKey {
        case id, gender
        case familyFirstName = "first_name"
        case familyLastName = "last_name"
        case familyRelationship = "relationship"
        case familyDob = "dob"
        case familyOccupation = "occupation"
        case familyEducation = "education"
        case familyIncomeType = "income_type"
        case income
        case familyDisability = "is_disability"
        case familyDisablityDetail = "disability_note"
        case familyHealthStatus = "health_status"
        case familySocialHabits = "social_habits"
        case familyChildType = "child_type"
        case familyContactNumber = "contact_number"

    }
    
    func valueByPropertyName(name:String) -> Any {
                
        switch name {
        case "familyFirstName" : return familyFirstName ?? ""
        case "familyLastName" : return familyLastName ?? ""
        case "familyRelationship" : return familyRelationship ?? ""
        case "familyGenderName" : return gender
        case "familyDob" : return familyDob ?? ""
        case "familyOccupation" : return familyOccupation ?? ""
        case "familyEducation" : return familyEducation ?? ""
        case "familyIncomeType" : return familyIncomeType ?? 0
        case "income" : return income ?? ""
        case "familyDisability" : return familyDisability ?? 0
        case "familyDisablityDetail" : return familyDisablityDetail ?? ""
        case "familyHealthStatus" : return familyHealthStatus ?? ""
        case "familySocialHabits" : return familySocialHabits ?? ""
        case "familyContactNumber" : return familyContactNumber ?? ""
        case "familyChildType" : return familyChildType ?? 0
            
        default: return ""
        }
    }
}

// MARK: - SirHistory
struct SirHistory: Codable {
    let presentPhysicalCondition, pastPhysicalCondition, presentMentalCondition, companionsInfluence: String?
    let pastMentalCondition: String?
    let id: Int?
    let personalitytraits, truancyFromHome, habitsInterest: String?
    
    enum CodingKeys: String, CodingKey {
        case presentPhysicalCondition = "present_physical_condition"
        case pastPhysicalCondition = "past_physical_condition"
        case presentMentalCondition = "present_mental_condition"
        case companionsInfluence = "companions_influence"
        case pastMentalCondition = "past_mental_condition"
        case id, personalitytraits
        case truancyFromHome = "truancy_from_home"
        case habitsInterest = "habits_interest"
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "presentPhysicalCondition" : return presentPhysicalCondition ?? ""
        case "pastPhysicalCondition" : return pastPhysicalCondition ?? ""
        case "presentMentalCondition" : return presentMentalCondition ?? ""
        case "companionsInfluence" : return companionsInfluence ?? ""
        case "pastMentalCondition" : return pastMentalCondition ?? ""
        case "personalitytraits" : return personalitytraits ?? ""
        case "truancyFromHome" : return truancyFromHome ?? ""
        case "habitsInterest" : return habitsInterest ?? ""
        default: return ""
        }
    }
}

// MARK: - SirInquiry
struct SirInquiry: Codable {
    let religiousFactors, behaviourObserved, physicalCondition, expertsConsulted: String?
    let suggestedCauses, recommendation: String?
    let id: Int?
    let reasonsForOffence, socialFactors, intelligence: String?
    
    enum CodingKeys: String, CodingKey {
        case religiousFactors = "religious_factors"
        case behaviourObserved = "behaviour_observed"
        case physicalCondition = "physical_condition"
        case expertsConsulted = "experts_consulted"
        case suggestedCauses = "suggested_causes"
        case recommendation, id
        case reasonsForOffence = "reasons_for_offence"
        case socialFactors = "social_factors"
        case intelligence
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "religiousFactors" : return religiousFactors ?? ""
        case "behaviourObserved" : return behaviourObserved ?? ""
        case "physicalCondition" : return physicalCondition ?? ""
        case "expertsConsulted" : return expertsConsulted ?? ""
        case "suggestedCauses" : return suggestedCauses ?? ""
        case "recommendation" : return recommendation ?? ""
        case "reasonsForOffence" : return reasonsForOffence ?? ""
        case "socialFactors" : return socialFactors ?? ""
        case "intelligence" : return intelligence ?? ""
        default: return ""
        }
    }
}

// MARK: - SirVocation
struct SirVocation: Codable {
    let neighbourReport, vocationalDetail, employmentAttitude, anyOther: String?
    let id: Int?
    let religiousBeliefs, trainingUndergone, reasonForLeaving, parentalCare: String?
    
    enum CodingKeys: String, CodingKey {
        case neighbourReport = "neighbour_report"
        case vocationalDetail = "vocational_detail"
        case employmentAttitude = "employment_attitude"
        case id
        case religiousBeliefs = "religious_beliefs"
        case trainingUndergone = "training_undergone"
        case reasonForLeaving = "reason_for_leaving"
        case parentalCare = "parental_care"
        case anyOther = "other_remarks"
    }
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "neighbourReport" : return neighbourReport ?? ""
        case "vocationalDetail" : return vocationalDetail ?? ""
        case "employmentAttitude" : return employmentAttitude ?? ""
        case "religiousBeliefs" : return religiousBeliefs ?? ""
        case "trainingUndergone" : return trainingUndergone ?? ""
        case "reasonForLeaving" : return reasonForLeaving ?? ""
        case "parentalCare" : return parentalCare ?? ""
        case "anyOther" : return anyOther ?? ""
            
        default: return ""
        }
    }
}

//Supervision
struct ObservationReport: Codable {
    let householdChange, difficultiesFacedByChild, vacationalTraining, lastEngagement: String?
    let relationshipWithFamily: String?
    let id: Int?
    let childBehaviour, physicalHealthStatus, childEngagement, safteyInFamily: String?
    let difficultiesFacedByFamily: String?
    
    enum CodingKeys: String, CodingKey {
        case householdChange = "household_change"
        case difficultiesFacedByChild = "difficulties_faced_by_child"
        case vacationalTraining = "vacational_training"
        case lastEngagement = "last_engagement"
        case relationshipWithFamily = "relationship_with_family"
        case id
        case childBehaviour = "child_behaviour"
        case physicalHealthStatus = "physical_health_status"
        case childEngagement = "child_engagement"
        case safteyInFamily = "saftey_in_family"
        case difficultiesFacedByFamily = "difficulties_faced_by_family"
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "householdChange" : return householdChange ?? ""
        case "difficultiesFacedByChild" : return difficultiesFacedByChild ?? ""
        case "vacationalTraining" : return vacationalTraining ?? ""
        case "lastEngagement" : return lastEngagement ?? ""
        case "relationshipWithFamily" : return relationshipWithFamily ?? ""
        case "childBehaviour" : return childBehaviour ?? ""
        case "physicalHealthStatus" : return physicalHealthStatus ?? ""
        case "childEngagement" : return childEngagement ?? ""
        case "safteyInFamily" : return safteyInFamily ?? ""
        case "difficultiesFacedByFamily" : return difficultiesFacedByFamily ?? ""
            
        default: return ""
        }
    }
}

// MARK: - PreliminaryReport
struct PreliminaryReport: Codable {
    let parentName, probationOfficerInteractedWith: String?
    let id: Int?
    let visitDate: String?
    
    enum CodingKeys: String, CodingKey {
        case parentName = "parent_name"
        case probationOfficerInteractedWith = "probation_officer_interacted_with"
        case id
        case visitDate = "visit_date"
    }
    
    func valueByPropertyName(name:String) -> Any {
        switch name {
        case "parentName" : return parentName ?? ""
        case "probationOfficerInteractedWith" : return probationOfficerInteractedWith ?? ""
        case "visitDate" : return visitDate ?? ""
            
        default: return ""
        }
    }
}

// MARK: - VocationalReport
struct VocationalReport: Codable {
    let reason, teacherName, actionPoint, nextVisitDate: String?
    let workNature, workHours, carePlanProgress, childAttitudePeers, unusualBehaviour: String?
    let schoolName, childProgressFeedback, childAttitudeWork, peersAttitudeChild: String?
    let carePlanModification, violationLabourLaws: String?
    let timeSpendWithChild, id: Int?
    
    let document: PostReleaseUploadModelData?
    
    enum CodingKeys: String, CodingKey {
        case reason
        case teacherName = "teacher_name"
        case actionPoint = "action_point"
        case nextVisitDate = "next_visit_date"
        case workNature = "work_nature"
        case workHours = "work_hours"
        case violationLabourLaws = "violation_labour_laws"
        case carePlanProgress = "care_plan_progress"
        case childAttitudePeers = "child_attitude_peers"
        case unusualBehaviour = "unusual_behaviour"
        case schoolName = "school_name"
        case childProgressFeedback = "child_progress_feedback"
        case childAttitudeWork = "child_attitude_work"
        case peersAttitudeChild = "peers_attitude_child"
        case carePlanModification = "care_plan_modification"
        case timeSpendWithChild = "time_spend_with_child"
        case id
        case document = "document"
    }
    
    func valueByPropertyName(name:String) -> Any {

        switch name {
            
        case "reason" : return reason ?? ""
        case "teacherName" : return teacherName ?? ""
        case "actionPoint" : return actionPoint ?? ""
        case "nextVisitDate" : return nextVisitDate ?? ""
        case "workNature" : return workNature ?? ""
        case "workHours" : return workHours ?? ""
        case "violationLabourLaws" : return violationLabourLaws ?? ""
        case "carePlanProgress" : return carePlanProgress ?? ""
        case "childAttitudePeers" : return childAttitudePeers ?? ""
        case "unusualBehaviour" : return unusualBehaviour ?? ""
        case "schoolName" : return schoolName ?? ""
        case "childProgressFeedback" : return childProgressFeedback ?? ""
        case "childAttitudeWork" : return childAttitudeWork ?? ""
        case "peersAttitudeChild" : return peersAttitudeChild ?? ""
        case "carePlanModification" : return carePlanModification ?? ""
        case "timeSpendWithChild" : return timeSpendWithChild ?? ""
        case "uploadPretermRelease" : return document ?? ""
            
        default: return ""
        }
    }
}



// MARK: - Case
struct SirCasePo: Codable {
    
    let policeStation: SirPoliceStation?
    let crimeNumber: String?
    let districtName: String?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case policeStation = "police_station"
        case crimeNumber = "crime_number"
        case districtName = "district_name"
        case id
    }
}



// MARK: - PoliceStation
struct SirPoliceStation: Codable {
    let number, name: String?
    let id: Int?
}



//MARK: --- QUESTION ANSWER

struct QuestionsAnswerModelData: Codable {
    var intakeQuestions: [IntakeQuestion]?
    
    enum CodingKeys: String, CodingKey {
        case intakeQuestions = "IntakeQuestions"
    }
}

struct IntakeQuestion: Codable {
    var questionName, type, fieldValue1, fieldValue2: String?
    var optionValue: String?
    var fieldQuestion1: String?
    var fieldQuestion2: String?
    var options: [String]?
    let id: String?
    let fieldId1: String?
    let fieldId2: String?
    let isMandate: Bool?
    let fieldOption1: [String]?
    
    enum CodingKeys: String, CodingKey {
        case questionName = "QuestionName"
        case type, fieldValue1, fieldValue2, optionValue, options, id, fieldQuestion1, fieldQuestion2
        case fieldId1
        case fieldId2
        case isMandate
        case fieldOption1
    }
}

