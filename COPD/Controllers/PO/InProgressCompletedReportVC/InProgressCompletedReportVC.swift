//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import CalendarDateRangePickerViewController

class InProgressCompletedReportVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblCompletedReport: UITableView!
    
    @IBOutlet weak var tfSearchReport: UITextField!
    
    @IBOutlet weak var btnGetAll: UIButton!
    
    @IBOutlet weak var lblNoReportFound: UILabel!
       
    // MARK: - Properties
    var reportArray : [SIRReport]? = []{
        
        didSet{
            self.lblNoReportFound.isHidden = !(self.reportArray?.count == 0)
        }
    }
    
    var isCompletedReport: Bool? = true
    
    var dropdownOptionsArray = ["GET ALL", "TODAY", "YESTERDAY", "THIS WEEK", "CUSTOM"]
    var dropDown = DropDown()
        
    var passingReportCategory = ""
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        lblTitle.text = "\(passingReportCategory) Report List"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        tfSearchReport.delegate = self
        tfSearchReport.clearButtonMode = .whileEditing
        
        // register custom table cell
        registerNib()
                
        self.btnGetAll.setTitle("GET ALL", for: .normal)
        
        // get report list
        self.fetchReportListFilterBy(searchText: "", startDate: "", endDate: "")

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.tblCompletedReport.endEditing(true)
        
        //Set shadow
        tblCompletedReport.layer.shadowColor = UIColor.grayShadowTheme.cgColor
        tblCompletedReport.layer.shadowOpacity = 0.4
        tblCompletedReport.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        tblCompletedReport.layer.shadowRadius = 3.0
        tblCompletedReport.layer.shouldRasterize = true
        tblCompletedReport.layer.rasterizationScale = true ? UIScreen.main.scale : 1
        
        tblCompletedReport.keyboardDismissMode = .interactive
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)


    }
    
    override func viewDidLayoutSubviews(){

    }
    
    func registerNib(){
        
        let customNib = UINib(nibName: "NewReportTableViewCell", bundle: Bundle.main)
        tblCompletedReport.register(customNib, forCellReuseIdentifier: "NewReportTableViewCell")
        tblCompletedReport.separatorStyle = .none
    }
    

    // MARK: - IBActions
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
        
    }
    
    
    @IBAction func searchButtonAction(_ sender: UIButton) {
        
        // search
        
    }
    
    
    @IBAction func getAllAction(_ sender: UIButton) {
        
        // display popup
        self.displayOptionSelectionPopup()
    }
    
    func displayOptionSelectionPopup(){
        DispatchQueue.main.async {
                        
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropdownOptionsArray
            self.dropDown.anchorView = self.btnGetAll
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .center
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.btnGetAll.setTitle(item, for: .normal)
                
                if item == "GET ALL"{
                    
                    // make api call
                    self.fetchReportListFilterBy(searchText: "", startDate: "", endDate: "")

                }
                else if item == "TODAY"{
                    
                    // make api call
                    self.fetchReportListFilterBy(searchText: "", startDate: Date().currentDate, endDate: Date().currentDate)
                }
                else if item == "YESTERDAY"{
                    // make api call
                    self.fetchReportListFilterBy(searchText: "", startDate: Date().yesterdayDate, endDate: Date().yesterdayDate)
                }
                else if item == "THIS WEEK"{
                    // make api call
                    self.fetchReportListFilterBy(searchText: "", startDate: Date().startOfWeek!, endDate: Date().endOfWeek!)
                    
                }
                else if item == "CUSTOM"{
                    
                    //open calendar to choose start date and end date
                    
                    let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
                    dateRangePickerViewController.delegate = self

                    dateRangePickerViewController.selectedStartDate = Date()
                    dateRangePickerViewController.modalPresentationStyle = .overCurrentContext
                    
                    let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
                    self.navigationController?.present(navigationController, animated: true, completion: nil)
                }
                    
            }
            
            self.dropDown.show()
            
        }
    }
    
}


extension InProgressCompletedReportVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reportArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewReportTableViewCell", for: indexPath) as! NewReportTableViewCell
        
        let myReport = reportArray?[indexPath.row]
        
        var fullName = ""
        
        fullName = fullName + "\(myReport?.child?.firstName ?? "Unknown")" + " "
        fullName = fullName + "\(myReport?.child?.lastName ?? "")"
        
        cell.lblNameOfChildren.text = "Name : \(fullName)"
        
        cell.lblChildID.text = myReport?.child?.alias
        cell.lblAge.text = "Age : \(myReport?.child?.age ?? "0")"
        cell.lblGender.text = "Sex : \(myReport?.child?.genderName ?? "")"
        
        var policeStationName = ""
        
        if myReport?.sirReportCase?.policeStation?.number?.isEmpty == false && myReport?.sirReportCase?.policeStation?.number != ""{
            
            policeStationName = policeStationName + "\(myReport?.sirReportCase?.policeStation?.number ?? "Unknown")" + ", "
        }
        if myReport?.sirReportCase?.districtName?.isEmpty == false && myReport?.sirReportCase?.districtName != ""{
            
            policeStationName = policeStationName + "\(myReport?.sirReportCase?.districtName ?? "")"
        }
        
        cell.lblPoliceStation.text = "Police Station : \(policeStationName)"
        
        if isCompletedReport == true{
      
            cell.lblStartNewReport.text = "Completed"
            cell.lblStartNewReport.textColor = UIColor.greenTheme
            
        }
        else{
            cell.lblStartNewReport.text = "In-Progress"
            cell.lblStartNewReport.textColor = UIColor.redErrorTheme
        }
        
        cell.selectionStyle = .none
                
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        // navigate to report form question details screen
        let createNewReportVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "createnewreportvc") as! CreateNewReportVC
        
        createNewReportVC.passingFormCategory = passingReportCategory
        createNewReportVC.passingReportId = ""
        createNewReportVC.isCompletedReport = isCompletedReport ?? true
        
        self.navigationController?.pushViewController(createNewReportVC, animated: true)
    }
    
}




extension InProgressCompletedReportVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        var searchText = textField.text
        // search text
        searchText = searchText?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        // search from api
        //self.fetchCCIInstitutionDataFilterBy(searchText: searchText ?? "", homeTypeId: self.passingHomeTypeId, startDate: "", endDate: "") // search
        
        return true
    }
    
}


extension InProgressCompletedReportVC: CalendarDateRangePickerViewControllerDelegate{
    func didTapCancel() {
        
        // no action
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        
        self.dismiss(animated: true, completion: {
          
            let startDateNew = self.convertDateFormater("\(startDate!)") //05/10/2022
            let endDateNew = self.convertDateFormater("\(endDate!)") //05/10/2022
            
            // make API call
            //self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: startDateNew, endDate: endDateNew)
            
        })
        
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        if date.contains("-"){
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        }else{
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss z"
        }
        
        let date = dateFormatter.date(from: date)
        
        if date == nil{
            return "--"
        }
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return  dateFormatter.string(from: date!)

    }
    
}


extension InProgressCompletedReportVC{
    
    func fetchReportListFilterBy(searchText: String, startDate: String, endDate: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading Report List ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getReportListFilterBy(searchText: "", selectedYear: "", selectedQuarter: "", reportType: "") {
                        responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            print(responseStr!)
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.sirReports?.count {
                                
                                if(count > 0){
                                    
                                    self.reportArray = responseStr?.sirReports
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tblCompletedReport.reloadData()

                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load \(self.passingReportCategory) report list.", message: "", parentView: self)
                            }

                        }
                    }
                    
                }
            }
            
        }
    }
}
