//
//  HomeTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/06/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class NewReportTableViewCell: UITableViewCell {

    @IBOutlet weak var titleBGView: UIView!
    
    @IBOutlet weak var lblNameOfChildren: UILabel!
    @IBOutlet weak var lblChildID: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblPoliceStation: UILabel!
    
    @IBOutlet weak var lblStartNewReport: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
