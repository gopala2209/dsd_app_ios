//
//  ListTableViewCell.swift
//  QuestionListApp
//

import UIKit

class ReportFormTitleCell: UITableViewCell {
    
    // MARK: - Outlet
    @IBOutlet weak var listTextLabel: UILabel!
    @IBOutlet weak var listBackgroundView: UIView!
        
    ///configuring cell
    func configureCell(formTitle: String) {
        listTextLabel.text =  formTitle
                
    }
    
}
