//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class NewReportVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblNewReport: UITableView!
    
    @IBOutlet weak var tfSearchReport: UITextField!
    
    @IBOutlet weak var btnSelectYear: UIButton!
    @IBOutlet weak var btnSelectQuarter: UIButton!
    
    @IBOutlet weak var lblNoReportFound: UILabel!
       
    // MARK: - Properties
    var reportArray : [SIRReport]? = []{
        
        didSet{
            self.lblNoReportFound.isHidden = !(self.reportArray?.count == 0)
        }
    }
    
    var selectedQuarter = ""
    var selectedYear = Date().currentYear
    
    var passingReportCategory = ""
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        lblTitle.text = "\(passingReportCategory) Report List"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        tfSearchReport.delegate = self
        tfSearchReport.clearButtonMode = .whileEditing
        
        // register custom table cell
        registerNib()
        
        //set btnSelectYear title
        btnSelectYear.setTitle(selectedYear, for: .normal)

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.tblNewReport.endEditing(true)
        
        //Set shadow
        tblNewReport.layer.shadowColor = UIColor.grayShadowTheme.cgColor
        tblNewReport.layer.shadowOpacity = 0.4
        tblNewReport.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        tblNewReport.layer.shadowRadius = 3.0
        tblNewReport.layer.shouldRasterize = true
        tblNewReport.layer.rasterizationScale = true ? UIScreen.main.scale : 1
        
        tblNewReport.keyboardDismissMode = .interactive
        
        // get report list
        self.fetchReportListFilterBy(searchText: "", selectedYear: selectedYear, selectedQuarter: selectedQuarter, reportType: passingReportCategory)

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)


    }
    
    override func viewDidLayoutSubviews(){

    }
    
    func registerNib(){
        
        let customNib = UINib(nibName: "NewReportTableViewCell", bundle: Bundle.main)
        tblNewReport.register(customNib, forCellReuseIdentifier: "NewReportTableViewCell")
        tblNewReport.separatorStyle = .none
    }
    

    // MARK: - IBActions
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
        
    }
    
    
    @IBAction func selectYearAction(_ sender: UIButton) {
        
        // open menu
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT YEAR"
        
        nextVC.isSelectionType = "yearselection"
        
        self.present(nextVC, animated: false, completion: nil)
        
    }
    
    
    @IBAction func selectQuarterAction(_ sender: UIButton) {
        
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT QUARTER"
        nextVC.isSelectionType = "quarter"
        
        self.present(nextVC, animated: false, completion: nil)
    }
    
    
    @IBAction func searchButtonAction(_ sender: UIButton) {
        
        // search
        
    }
    
}


extension NewReportVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reportArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewReportTableViewCell", for: indexPath) as! NewReportTableViewCell
        
        let myReport = reportArray?[indexPath.row]
        
        var fullName = ""
        
        fullName = fullName + "\(myReport?.child?.firstName ?? "Unknown")" + " "
        fullName = fullName + "\(myReport?.child?.lastName ?? "")"
        
        cell.lblNameOfChildren.text = "Name : \(fullName)"
        
        cell.lblChildID.text = myReport?.child?.alias
        cell.lblAge.text = "Age : \(myReport?.child?.age ?? "0")"
        cell.lblGender.text = "Sex : \(myReport?.child?.genderName ?? "")"
        
        var policeStationName = ""
        
        if myReport?.sirReportCase?.policeStation?.number?.isEmpty == false && myReport?.sirReportCase?.policeStation?.number != ""{
            
            policeStationName = policeStationName + "\(myReport?.sirReportCase?.policeStation?.number ?? "Unknown")" + ", "
        }
        if myReport?.sirReportCase?.districtName?.isEmpty == false && myReport?.sirReportCase?.districtName != ""{
            
            policeStationName = policeStationName + "\(myReport?.sirReportCase?.districtName ?? "Unknown")"
        }
        
        cell.lblPoliceStation.text = "Police Station : \(policeStationName)"
        
        
        cell.lblStartNewReport.text = "New Report"
        cell.lblStartNewReport.textColor = UIColor.orangeTheme
        
        cell.selectionStyle = .none
                
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        print("New report pressed")
                
        if passingReportCategory == "Transfer" || passingReportCategory == "Intake" || passingReportCategory == "SIR"{
            
            // navigate to report form question details screen
            let createNewReportVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "createnewreportvc") as! CreateNewReportVC
            
            createNewReportVC.passingFormCategory = passingReportCategory
            createNewReportVC.passingReportId = ""
            createNewReportVC.isCompletedReport = false
            
            self.navigationController?.pushViewController(createNewReportVC, animated: true)
        }
        
        else if passingReportCategory == "ICP"{
            
            // navigate to report form question details screen
            let createICPNewReportVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "createicpnewreportvc") as! CreateICPNewReportVC
            
            createICPNewReportVC.passingReportCategory = passingReportCategory
            createICPNewReportVC.passingReportId = ""
            createICPNewReportVC.isCompletedReport = false
            
            self.navigationController?.pushViewController(createICPNewReportVC, animated: true)
        }
        
    }
    
}


extension NewReportVC: OptionSelectedDelegate{
    
    func selectMenu(option: String, isSelectType: String, selectedHomeType: HomeTypeModel){
        
        if isSelectType == "quarter"{
            
            selectedQuarter = option
            btnSelectQuarter.setTitle(option, for: .normal)
        }
        else if isSelectType == "yearselection"{
            
            selectedYear = option
            btnSelectYear.setTitle(option, for: .normal)
        }
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewReport.reloadData()
        }
    }
}


extension NewReportVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        var searchText = textField.text
        // search text
        searchText = searchText?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        // search from api
        
        
        return true
    }
    
}

extension NewReportVC{
    
    func fetchReportListFilterBy(searchText: String, selectedYear: String, selectedQuarter: String, reportType: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading Report List ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getReportListFilterBy(searchText: "", selectedYear: "", selectedQuarter: "", reportType: "") { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            print(responseStr!)
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.sirReports?.count {
                                
                                if(count > 0){
                                    
                                    self.reportArray = responseStr?.sirReports
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tblNewReport.reloadData()

                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load \(reportType) report list.", message: "", parentView: self)
                            }

                        }
                    }
                    
                }
            }
            
        }
    }
}
