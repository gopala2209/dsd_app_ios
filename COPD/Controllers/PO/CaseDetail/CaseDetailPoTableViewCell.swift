//
//  CaseDetailPoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 23/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CaseDetailPoTableViewCell: UITableViewCell {

    @IBOutlet weak var crimeNo: UILabel!
    @IBOutlet weak var totalChild: UILabel!
    @IBOutlet weak var offenceType: UILabel!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var policeStTitle: UILabel!
    @IBOutlet weak var policeSt: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
