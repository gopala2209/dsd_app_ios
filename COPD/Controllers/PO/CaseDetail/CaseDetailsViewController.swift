//
//  CaseDetailsViewController.swift
//  COPD
//
//  Created by Anand Praksh on 23/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CaseDetailsViewController: UIViewController {

    @IBOutlet weak var caseDetailTableView: UITableView!
    let paddingRight = 20

    var caseDetailId = ""
    var caseDetail: CaseDetailPOModelData?
    var caseName = ""
    
    var reportName: [String] = ["Intake Report","SIR Report","ICP Report","Supervision Report"]
    var reportImage = [#imageLiteral(resourceName: "inspection_history"),#imageLiteral(resourceName: "current_inspection_nw"),#imageLiteral(resourceName: "icp_report"),#imageLiteral(resourceName: "supervision")]
    
    var cncpReportName: [String] = ["Transfer Report","SIR Report","ICP Report","Supervision Report"]
    var cncpReportImage = [#imageLiteral(resourceName: "inspection_history"),#imageLiteral(resourceName: "current_inspection_nw"),#imageLiteral(resourceName: "icp_report"),#imageLiteral(resourceName: "supervision")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
                
        navigationController?.navigationBar.barTintColor = .white

        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
                
        // set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // fetch Case details
        getPoCaseDetails(alias: self.caseDetailId)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
                    
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.orangeTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false

    }
        
    
    func registerNib(){
        
        let customNib = UINib(nibName: "MiniHomeTableViewCell", bundle: Bundle.main)
        caseDetailTableView.register(customNib, forCellReuseIdentifier: "MiniHomeTableViewCell")
        
        let nib = UINib(nibName: "CaseDetailPoTableViewCell", bundle: Bundle.main)
        caseDetailTableView.register(nib, forCellReuseIdentifier: "CaseDetailPoTableViewCell")
        
        caseDetailTableView.separatorStyle = .none
        
    }
    
    
}


extension CaseDetailsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func getDataOfData(string: String) -> String{
        if let index = string.firstIndex(of: "\""),
            case let start = string.index(after: index),
            let end = string[start...].firstIndex(of: "\"") {
            let substring = string[start..<end]
            return String(substring)
        }
        
        if let index = string.firstIndex(of: "("),
            case let start = string.index(after: index),
            let end = string[start...].firstIndex(of: ")") {
            let substring = string[start..<end]
            return String(substring)
        }
        
        return ""
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CaseDetailPoTableViewCell", for: indexPath) as! CaseDetailPoTableViewCell
            
            cell.selectionStyle = .none
            
            if caseDetail != nil{
                
                cell.crimeNo.text = caseDetail!.crimeNumber?.uppercased()

                if getDataOfData(string: "\(caseDetail!.startAt!)") == "0"{
                    cell.startDate.text = "--"
                }else{
                    cell.startDate.text = getDataOfData(string: "\(caseDetail!.startAt!)")
                }
                
                if getDataOfData(string: "\(caseDetail!.dueAt!)") == "0"{
                    cell.dueDate.text = "--"
                }else{
                    cell.dueDate.text = getDataOfData(string: "\(caseDetail!.dueAt!)")
                }
                               
                let psn = (caseDetail!.policeStation?.number) ?? ""
                let psName = (caseDetail!.policeStation?.name) ?? ""
                let psCity = (caseDetail!.policeStation?.city) ?? ""
                                
                var policeSt = ""
                
                if psn.isEmpty == false && psn != ""{
                    
                    policeSt = policeSt + "\(psn)" + ", "
                }
                if psName.isEmpty == false && psName != ""{
                    
                    policeSt = policeSt + "\(psName)" + ", "
                }
                if psCity.isEmpty == false && psCity != ""{
                    
                    policeSt = policeSt + "\(psCity)"
                }
                
                if policeSt.isEmpty == true  && policeSt == ""{
                    
                    policeSt = "--"
                }
                
                if caseName == "cicl"{
                    cell.policeStTitle.text = "Police Station :"
                    cell.policeSt.text = policeSt
                }
                else{
                    
                    let name = "\(caseDetail?.officerName ?? "")"
                    let designation = "\(caseDetail?.officerDesignation ?? "")"
                    let mobile = "\(caseDetail?.officerMobile ?? "")"
                    
                    var policeSt = ""
                    
                    if name.isEmpty == false && name != ""{
                        
                        policeSt = policeSt + "\(name)" + ", "
                    }
                    if designation.isEmpty == false && designation != ""{
                        
                        policeSt = policeSt + "\(designation)" + ", "
                    }
                    if mobile.isEmpty == false && mobile != ""{
                        
                        policeSt = policeSt + "\(mobile)"
                    }
                    
                    if policeSt.isEmpty == true  && policeSt == ""{
                        
                        policeSt = "--"
                    }
                    
                    cell.policeStTitle.text = "Officer Details :"
                    cell.policeSt.text = policeSt
                }
                
                if caseDetail!.offenceTypeText == ""{
                    cell.offenceType.text = "--"
                }else{
                    cell.offenceType.text = caseDetail!.offenceTypeText
                }
                
                cell.totalChild.text = "\(caseDetail!.childs?.count ?? 0)"
            }

            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MiniHomeTableViewCell", for: indexPath) as! MiniHomeTableViewCell
            if caseName == "cncp"{
                cell.titleOption.text = self.cncpReportName[indexPath.row - 1]
                cell.imageOption.image = self.cncpReportImage[indexPath.row - 1]
            }else{
                cell.titleOption.text = self.reportName[indexPath.row - 1]
                cell.imageOption.image = self.reportImage[indexPath.row - 1]
            }
            
            switch (indexPath.row){
            case 1:
                self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.9960784314, green: 0.6784313725, blue: 0.6509803922, alpha: 1), #colorLiteral(red: 0.9607843137, green: 0.937254902, blue: 0.937254902, alpha: 1)], tableCellView: cell.titleBGView, width: caseDetailTableView.bounds.width + CGFloat(paddingRight))
                break
                
            case 2:
                self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.8117647059, green: 0.8509803922, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.8862745098, green: 0.9215686275, blue: 0.9411764706, alpha: 1)], tableCellView: cell.titleBGView, width: caseDetailTableView.bounds.width + CGFloat(paddingRight))
                break
                
            case 3:
                self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.7529411765, green: 0.7921568627, blue: 0.2, alpha: 0.9746314847), #colorLiteral(red: 0.5098039216, green: 0.6941176471, blue: 1, alpha: 0.2327985699)], tableCellView: cell.titleBGView, width: caseDetailTableView.bounds.width + CGFloat(paddingRight))
                break
                
            case 4:
                self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.8117647059, green: 0.8509803922, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.8862745098, green: 0.9215686275, blue: 0.9411764706, alpha: 1)], tableCellView: cell.titleBGView, width: caseDetailTableView.bounds.width + CGFloat(paddingRight))
                break
            default:
                break
            }
                    
            cell.selectionStyle = .none
            
            cell.imageArrow.highlightedImage = UIImage(named: "right_arrow")?.imageWithColor(color: UIColor.whiteTheme)

            cell.titleOption.highlightedTextColor = UIColor.whiteTheme

            return cell
        }
        

        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row != 0{
            if caseDetail != nil{
                var childId: [String: Int] = [:]
                for i in (caseDetail!.childs!){
                    let firstName = i.firstName ?? ""
                    let lastName = i.lastName ?? ""
                    let fullName = firstName + " " + lastName
                    let alias = i.alias ?? ""
                    childId[fullName + " (" + alias + ")"] = i.id
                    
                }
                let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "ReportListPoViewController") as! ReportListPoViewController
                if caseName == "cncp"{
                    caseListVC.selectedReportId = indexPath.row
                    caseListVC.reportTitle = cncpReportName[indexPath.row - 1]
                }else{
                    caseListVC.selectedReportId = indexPath.row
                    caseListVC.reportTitle = reportName[indexPath.row - 1]
                }
                caseListVC.caseType = caseName
                caseListVC.caseId = (caseDetail?.alias)!
                caseListVC.childId = childId
                caseListVC.id = "\(caseDetail?.id ?? 0)"
                self.navigationController?.pushViewController(caseListVC, animated: true)
            }
            
        }
        
    }
    
    
    func applyGradientColorToTableCell(colours: [UIColor], tableCellView: UIView, width: CGFloat) {
        
        // Add gradient to Puff Summary view
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0,y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0,y: 0.5)
        gradient.frame = CGRect(x: 0, y: 0, width: width - 40, height: 64)
        gradient.cornerRadius = 10
       
        tableCellView.layer.insertSublayer(gradient, at: 0)
        
    }
    
}


extension CaseDetailsViewController{
    
    func getPoCaseDetails(alias: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getPoCaseDetails(alias: alias) { responseStr in
                        
                        DispatchQueue.main.async {
                            
//                            self.poCasesData?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let data = responseStr {
                                self.caseDetail = data
                                
                                let indexPathRow:Int = 0
                                let indexPosition = IndexPath(row: indexPathRow, section: 0)
                                
                                self.caseDetailTableView.reloadRows(at: [indexPosition], with: .none)

                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Case Detail.", message: "", parentView: self)
                            }

                        }
                    }
                    
                }
            }
            
        }
    }
}





// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)


struct CaseDetailPOModelData: Codable {
    let startAt: DueDate?
    let childs: [ChildElement]?
    let districtName: String?
    let alias, crimeNumber: String?
    let dueAt: DueDate?
    let id: Int?
    let policeStation: PoCasePoliceStation?
    let offenceTypeText: String?
    let officerName, officerMobile, officerDesignation: String?

    enum CodingKeys: String, CodingKey {
        case startAt = "start_at"
        case childs
        case districtName = "district_name"
        case alias
        case crimeNumber = "crime_number"
        case officerName = "officer_name"
        case officerMobile = "officer_mobile"
        case officerDesignation = "officer_designation"
        case dueAt = "due_at"
        case id
        case policeStation = "police_station"
        case offenceTypeText = "offence_type_text"
    }
}



struct ChildElement: Codable {
    let firstName: String?
    let lastName: String?
    let genderName : String?
    let dob: String?
    let alias: String?
    let age: String?
    let id: Int?

    enum CodingKeys: String, CodingKey {
       
        case alias
        case firstName = "first_name"
        case lastName = "last_name"
        case genderName = "gender_name"
        case dob, age
        case id
    }
}

