//
//  CaseListPOViewController.swift
//  COPD
//
//  Created by Anand Praksh on 22/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit
import DropDown

class CaseListPOViewController: UIViewController {

    @IBOutlet weak var selectYearView: UIView!
    @IBOutlet weak var selectQuarterView: UIView!
    @IBOutlet weak var searchTxtFld: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectYearLbl: UILabel!
    @IBOutlet weak var selectQuarterLbl: UILabel!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var noReportFound: UILabel!
    
    var selectYearData: [String] = []
    
    var selectedQuarter = "1"
    
    var selectQuarterData: [String] = ["Quarter 1","Quarter 2","Quarter 3","Quarter 4"]

    var dropDown = DropDown()
    
    var poCasesData: [PoCasesData]? = []
    
    var caseName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageTitle.text = caseName.uppercased() + " Case List"
            
        createTouchForView()
        let currentDate = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: currentDate)
        
        let monthName = calendar.component(.month, from: currentDate)
        
        selectYearData = (1971...currentYear).map { String($0) }
        selectYearData.reverse()
        
        selectYearLbl.text = String(currentYear)
        
        if monthName == 1 || monthName == 2 || monthName == 3{
            selectedQuarter = "1"
        }else if monthName == 4 || monthName == 5 || monthName == 6{
            selectedQuarter = "2"
        }else if monthName == 7 || monthName == 8 || monthName == 9{
            selectedQuarter = "3"
        }else{
            selectedQuarter = "4"
        }
        
        let myIndex = Int(selectedQuarter)! - 1
        
        selectQuarterLbl.text = selectQuarterData[myIndex]
        
        //self.getPoListData(searchText: searchTxtFld.text ?? "", startDate: "01/01/\(selectYearData.first!)", endDate: "31/03/\(selectYearData.first!)")
        
        self.getPoListData(searchText: searchTxtFld.text ?? "", startDate: "", endDate: "", year: selectYearLbl.text ?? "", quarter: selectedQuarter)
        
        navigationController?.navigationBar.barTintColor = .white
        
        searchTxtFld.delegate = self
        searchTxtFld.keyboardType = .asciiCapable
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
                
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        registerNib()
        
    }
    
    func createTouchForView(){
        let yearTap = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectYearTap(_:)))
        selectYearView.addGestureRecognizer(yearTap)
        
        let quarterTap = UITapGestureRecognizer(target: self, action: #selector(self.selectQuarterTap(_:)))
        selectQuarterView.addGestureRecognizer(quarterTap)
    }
    
    

    @objc func displaySelectYearTap(_ sender: UITapGestureRecognizer? = nil) {
        displaySelectYearDropDown()
    }
    
    @objc func selectQuarterTap(_ sender: UITapGestureRecognizer? = nil) {
        selectQuarterDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    
    func registerNib(){
        
        let customNib = UINib(nibName: "CaseListPoTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "CaseListPoTableViewCell")
        tableView.separatorStyle = .none
        
        tableView.automaticallyAdjustsScrollIndicatorInsets = true
        
    }
    
    func selectQuarterDropDown(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.selectQuarterData
            self.dropDown.anchorView = self.selectQuarterView
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.selectQuarterLbl.text = item
                
                self.selectedQuarter = String(Int(index + 1))
                
                self.getPoListData(searchText: self.searchTxtFld.text ?? "", startDate: "", endDate: "", year: self.selectYearLbl.text ?? "", quarter: self.selectedQuarter)
               
            }
            
            self.dropDown.show()
            
        }
    }
    
    func displaySelectYearDropDown(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.selectYearData
            self.dropDown.anchorView = self.selectYearView
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.selectYearLbl.text = item

                self.getPoListData(searchText: self.searchTxtFld.text ?? "", startDate: "", endDate: "", year: self.selectYearLbl.text ?? "", quarter: self.selectedQuarter)
                
            }
            
            self.dropDown.show()
            
        }
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        
        searchTxtFld.resignFirstResponder()
        
        self.getPoListData(searchText: self.searchTxtFld.text ?? "", startDate: "", endDate: "", year: self.selectYearLbl.text ?? "", quarter: self.selectedQuarter)
    }
    

}


extension CaseListPOViewController: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        var text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        text = text.trimmingCharacters(in: .whitespaces)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        var searchText = textField.text
        
        // search text
        searchText = searchText?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        self.getPoListData(searchText: searchText ?? "", startDate: "", endDate: "", year: self.selectYearLbl.text ?? "", quarter: self.selectedQuarter)
                
        return true
    }
    
}


extension CaseListPOViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.poCasesData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CaseListPoTableViewCell", for: indexPath) as! CaseListPoTableViewCell
        let caseData = self.poCasesData![indexPath.row]
        cell.title.text = caseData.crimeNumber
        
        if getDataOfData(string: "\(caseData.startAt!)") == "0"{
            cell.addedOn.text = "Start Date : --"
        }else{
            cell.addedOn.text = "Start Date : " + getDataOfData(string: "\(caseData.startAt!)")
        }
        
        if getDataOfData(string: "\(caseData.dueAt!)") == "0"{
            cell.dueDate.text = "Due Date : --"
        }else{
            cell.dueDate.text = "Due Date : " + getDataOfData(string: "\(caseData.dueAt!)")
        }
             
        if caseName.lowercased() == "cicl"{
        
            let psn = (caseData.policeStation?.number) ?? ""
            let psName = (caseData.policeStation?.name) ?? ""
            let psCity = (caseData.policeStation?.city) ?? ""
            
            var policeSt = ""
            
            if psn.isEmpty == false && psn != ""{
                
                policeSt = policeSt + "\(psn)" + ", "
            }
            if psName.isEmpty == false && psName != ""{
                
                policeSt = policeSt + "\(psName)" + ", "
            }
            if psCity.isEmpty == false && psCity != ""{
                
                policeSt = policeSt + "\(psCity)"
            }
            
            if policeSt.isEmpty == true  && policeSt == ""{
                
                policeSt = "--"
            }
            
            cell.policeStation.text = "Police Station : " + policeSt
        }
        else{
            
            var officerName = caseData.officerName
            if officerName == ""{
                officerName = "--"
            }
            
            var officerDesignation = caseData.officerDesignation
            if officerDesignation == ""{
                officerDesignation = "--"
            }
            
            var officerMobile = caseData.officerMobile
            if officerMobile == ""{
                officerMobile = "--"
            }
            
            let name = "\(officerName ?? "--") \n "
            let designation = "                               Designation : " + "\(officerDesignation ?? "--") \n "
            let mobile = "                               Mobile : " + "\(officerMobile ?? "--")"
            
            cell.policeStation.text = "Official Details : " + name + designation + mobile
        }
        return cell
        
    }
    
    func getDataOfData(string: String) -> String{
        if let index = string.firstIndex(of: "\""),
            case let start = string.index(after: index),
            let end = string[start...].firstIndex(of: "\"") {
            let substring = string[start..<end]
            return String(substring)
        }
        
        if let index = string.firstIndex(of: "("),
            case let start = string.index(after: index),
            let end = string[start...].firstIndex(of: ")") {
            let substring = string[start..<end]
            return String(substring)
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let caseData = self.poCasesData![indexPath.row]

        let caseListVC = Constant.POStoryBoard.instantiateViewController(withIdentifier: "CaseDetailsViewController") as! CaseDetailsViewController
        caseListVC.caseDetailId = caseData.alias ?? ""
        caseListVC.caseName = self.caseName
        self.navigationController?.pushViewController(caseListVC, animated: true)
        
    }
    
    
    
}

extension CaseListPOViewController{
    
    func getPoListData(searchText: String, startDate: String, endDate: String, year: String, quarter: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getPoListData(searchText: searchText, startDate: startDate, endDate: endDate, year: year, quarter: quarter, admissionHomeTypeId: self.caseName) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            self.poCasesData?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()

                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.cases?.count {
                                
                                if(count > 0){
                                    
                                    self.poCasesData = responseStr?.cases
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tableView.reloadData()

                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load \(self.caseName.uppercased()) list.", message: "", parentView: self)
                            }

                        }
                    }
                    
                }
            }
            
        }
    }
}
