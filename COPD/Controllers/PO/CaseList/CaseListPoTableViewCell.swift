//
//  CaseListPoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 22/08/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CaseListPoTableViewCell: UITableViewCell {

    @IBOutlet weak var policeStation: UILabel!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var addedOn: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
