//
//  PaymentLinkViewController.swift
//  COPD
//
//  Created by Anand Praksh on 05/12/17.
//  Copyright © 2017 Anand Praksh. All rights reserved.
//

import UIKit
import WebKit

class PrivacyTermsVC: UIViewController {
    
    var webUrlLink: String = ""
    var titleText: String = ""
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var webView: WKWebView! {
        didSet {
            self.webView.backgroundColor = UIColor.white
            self.webView.navigationDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        HUDIndicatorView.shared.showHUD(view: self.view)
        
        lblTitle.text = self.titleText
        loadUrlInWebView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Set the Status bar bg color for iOS 13
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }

    
    @IBAction func backAction(_ sender: Any) {
        
        //dismiss
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
              let delegate = windowScene.delegate as? SceneDelegate, let window = delegate.window else { return  }
        
        let navigation = Constant.HomeStoryBoard.instantiateViewController(identifier: "\(UINavigationController.self)") as! UINavigationController
        
        // PO
        if AppManager.shared.getUsertype().lowercased() == "po"{
            
            let homeVC_PO = Constant.POStoryBoard.instantiateViewController(withIdentifier: "homevcpo") as! HomeVC_PO
            
            navigation.setViewControllers([homeVC_PO], animated: false)
        }
        else{// CWC, JJB & DCPU
            
            let homeVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "homevc") as! HomeVC
            
            navigation.setViewControllers([homeVC], animated: false)
        }
        
        window.rootViewController = navigation
        
        window.makeKeyAndVisible()
    }
    
    func loadUrlInWebView() {
        if self.webUrlLink != "" {
            let urlRequest = URLRequest(url: URL(string: webUrlLink)!)
            self.webView.load(urlRequest)
            
        }
    }
    
}


extension PrivacyTermsVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUDIndicatorView.shared.dismissHUD()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        HUDIndicatorView.shared.showHUD(view: self.view)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
        HUDIndicatorView.shared.dismissHUD()
        ServiceModel().showAlert(title: "Alert", message: "Failed to open!", parentView: self)
    }
    
}
