//
//  MenuViewController+TableView.swift
//  COPD
//
//  Created by Anand Prakash on 1/4/21.
//

import UIKit

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.profile.rawValue:
            return 1
        case Sections.options.rawValue:
            return AppUtilitiesManager.Menus.allCases.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.profile.rawValue:
            let profileCell = tableView.dequeueReusableCell(withIdentifier: "\(MenuProfileTableViewCell.self)") as! MenuProfileTableViewCell
            
            profileCell.buttonCross.addTarget(self, action: #selector(closeMenuAction), for: .touchUpInside)
            
            profileCell.configure(profile)
            
            return profileCell
            
        case Sections.options.rawValue:
            let optionCell = tableView.dequeueReusableCell(withIdentifier: "\(MenuOptionTableViewCell.self)") as! MenuOptionTableViewCell
            
            optionCell.configure(images[indexPath.row], titles[indexPath.row])
            
            return optionCell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case Sections.profile.rawValue:
            debugPrint("Do Nothing...")
            //closeMenuAction()
            
        case Sections.options.rawValue:
            let type = AppUtilitiesManager.Menus.allCases[indexPath.row].rawValue
            
            DispatchQueue.main.async {

                        switch type {
                        case AppUtilitiesManager.Menus.shareus.rawValue:
                           
                            // share us
                            self.shareApp()
                            break
                            
                        case AppUtilitiesManager.Menus.rateus.rawValue:
                            
                            // rate us
                            self.openAppStore()
                            break
                            
                        case AppUtilitiesManager.Menus.privacypolicy.rawValue:
                            
                            // privacy policy
                            self.navigateToPrivacyPolicyTermsScreen(screenTitle: "Privacy Policy", webUrl :"https://in22labs.com/")
                            break
                            
                        case AppUtilitiesManager.Menus.termsconditions.rawValue:
                            
                            // terms and conditions
                            self.navigateToPrivacyPolicyTermsScreen(screenTitle: "Terms & Conditions", webUrl :"https://in22labs.com/about.html")
                            break
                            
                        default:
                            break
                        }

            }
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == Sections.profile.rawValue ? UITableView.automaticDimension : 50
    }
    
    // MARK: - IBActions
    
    @objc func closeMenuAction(){
        
        self.dismiss(animated: true)
    }
    
    
    func shareApp() {
        
        let url = URL(string: Constant.APP_SHAREUS_URL)
        let activityViewController = UIActivityViewController(
            activityItems: ["", url as Any],
            applicationActivities: nil)
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func openAppStore() {
        
        if let url = URL(string: Constant.APP_SHAREUS_URL),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
    
    func navigateToPrivacyPolicyTermsScreen(screenTitle:String, webUrl: String){
        
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
              let delegate = windowScene.delegate as? SceneDelegate, let window = delegate.window else { return  }
                
        let navigation = Constant.UserStoryBoard.instantiateViewController(identifier: "UINavigationControllerForLogin") as! UINavigationController
                
        let controller = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "privacytermsvc") as! PrivacyTermsVC
        
        controller.webUrlLink = webUrl
        controller.titleText = screenTitle
        navigation.setViewControllers([controller], animated: false)
        
        window.rootViewController = navigation
        
        window.makeKeyAndVisible()
    }
}
