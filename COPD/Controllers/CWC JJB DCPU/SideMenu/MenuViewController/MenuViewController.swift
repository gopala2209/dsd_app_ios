//
//  MenuViewController.swift
//  COPD
//
//  Created by Anand Prakash on 1/2/21.
//

import UIKit

class MenuViewController: UIViewController {
    
    enum Sections: Int, CaseIterable {
        case profile
        case options
    }
    
    let images = [#imageLiteral(resourceName: "share_icon"), #imageLiteral(resourceName: "star_icon"), #imageLiteral(resourceName: "privacy_policy_icon"), #imageLiteral(resourceName: "terms_icon")]
    let titles = ["Share This App", "Rate This App", "Privacy Policy", "Terms And Conditions"]
    var profile: ProfileModel?
    
    @IBOutlet weak var tableViewMenu: UITableView! {
        didSet {
            ["\(MenuProfileTableViewCell.self)", "\(MenuOptionTableViewCell.self)"].forEach { (cell) in
                tableViewMenu.register(UINib(nibName: cell, bundle: nil), forCellReuseIdentifier: cell)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitials()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
    }
    
    private func setupInitials() {
        
    }
    
    // MARK: - IBActions
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        
        // logout
        logoutAlert()
        
    }
    
    
    func logoutAlert(){
        
        let alert = UIAlertController(title: "Logout",
                                      message: "Are you sure you want to logout?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "YES", style: .default, handler: { (action) in
            
            
            // navigate to login screen
            guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                  let delegate = windowScene.delegate as? SceneDelegate, let window = delegate.window else { return  }
            
            let navigation = Constant.UserStoryBoard.instantiateViewController(identifier: "UINavigationControllerForLogin") as! UINavigationController
            
            let loginVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "loginvc") as! LoginVC
            
            navigation.setViewControllers([loginVC], animated: false)
            
            window.rootViewController = navigation
            window.makeKeyAndVisible()
            
            AppManager.shared.logout()
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "NO",
                                         style: .cancel,
                                         handler: { (action) in
                                            
                                            alert.dismiss(animated: true, completion: nil)
                                         })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }

}
