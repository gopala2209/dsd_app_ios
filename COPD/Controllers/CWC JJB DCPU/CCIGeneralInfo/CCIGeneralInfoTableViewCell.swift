//
//  CCIGeneralInfoTableViewCell.swift
//  COPD
//
//  Created by Anand Praksh on 18/10/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CCIGeneralInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var question: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
