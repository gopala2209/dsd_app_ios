//
//  CCIGeneralInfoViewController.swift
//  COPD
//
//  Created by Anand Praksh on 18/10/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CCIGeneralInfoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var id: String = ""
    var inspectionQuestionListArray: [IntakeQuestion]? = []


    override func viewDidLoad() {
        super.viewDidLoad()
        if let path = Bundle.main.path(forResource: "GeneralInfoCCi", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let model  = try JSONDecoder().decode(QuestionsAnswerModelData.self, from: data)
                self.inspectionQuestionListArray = model.intakeQuestions
                self.tableView.reloadData()
            } catch {
                // handle error
            }
        }
        registerNib()
        getCciGeneralInfo()
        navigationController?.navigationBar.barTintColor = .white
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
    }
    
    func registerNib(){
        let customNib = UINib(nibName: "CCIGeneralInfoTableViewCell", bundle: Bundle.main)
        tableView.register(customNib, forCellReuseIdentifier: "CCIGeneralInfoTableViewCell")
        let nib = UINib(nibName: "ContactDetailLblTableViewCell", bundle: Bundle.main)
        tableView.register(nib, forCellReuseIdentifier: "ContactDetailLblTableViewCell")
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
    
    
    

}

extension CCIGeneralInfoViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return inspectionQuestionListArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if inspectionQuestionListArray![indexPath.row].id == "NA"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailLblTableViewCell", for: indexPath) as! ContactDetailLblTableViewCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CCIGeneralInfoTableViewCell", for: indexPath) as! CCIGeneralInfoTableViewCell
            cell.selectionStyle = .none
            cell.question.text = inspectionQuestionListArray![indexPath.row].questionName
            cell.answer.text = inspectionQuestionListArray![indexPath.row].optionValue
            return cell
        }
        
        
    }
    
}

extension CCIGeneralInfoViewController{
    
    func getCciGeneralInfo(){

        let servicemodel = ServiceModel()

        if (NetworkConnection.isConnectedToNetwork()){

            do {

                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading ..")

                DispatchQueue.global(qos: .userInitiated).async {

                    servicemodel.getCCiGeneralInfo(id: self.id) { responseStr in

                        DispatchQueue.main.async {


                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                                return
                            }

                            HUDIndicatorView.shared.dismissHUD()

                            for i in 0..<(self.inspectionQuestionListArray?.count ?? 0){
                                switch i {
                                case 0:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.orgName
                                    break
                                case 1:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.institutionName
                                    break
                                case 2:
                                    let add1 = responseStr?.addressLine1 ?? ""
                                    let add2 = responseStr?.addressLine2 ?? ""
                                    let taluk = responseStr?.talukName ?? ""
                                    let pincode = "Pincode - " + (responseStr?.zipcode ?? "")
                                    let distict = responseStr?.districtName ?? ""
                                    let state = responseStr?.stateName ?? ""
                                    let finalAddress = add1 + ", " + add2 + ", " + taluk + ", "  + pincode + ", " + distict + ", " + state
                                    self.inspectionQuestionListArray![i].optionValue = finalAddress
                                    break
                                case 3:
                                    if responseStr?.isReligiousBody == 1{
                                        self.inspectionQuestionListArray![i].optionValue = "Yes"
                                    }else{
                                        self.inspectionQuestionListArray![i].optionValue = "No"
                                    }
                                    break
                                case 4:
                                    if responseStr?.isTemporaryClosureDone == 1{
                                        self.inspectionQuestionListArray![i].optionValue = "Yes"
                                    }else{
                                        self.inspectionQuestionListArray![i].optionValue = "No"
                                    }
                                    break
                                case 5:
                                    if responseStr?.isBlacklisted == 1{
                                        self.inspectionQuestionListArray![i].optionValue = "Yes"
                                    }else{
                                        self.inspectionQuestionListArray![i].optionValue = "No"
                                    }
                                    break
                                case 6:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.registrationNumber
                                    break
                                case 7:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.registrationDate
                                    break
                                case 8:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.registrationPlace
                                    break
                                case 9:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.registrationActText
                                    break
                                case 10:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.mobile
                                    break
                                case 11:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.fax
                                    break
                                case 12:
                                    self.inspectionQuestionListArray![i].optionValue = responseStr?.email
                                    break
                                case 13:
                                    self.inspectionQuestionListArray![i].optionValue = "\(responseStr?.totalChild ?? 0)"
                                    break
                                case 14:
                                    self.inspectionQuestionListArray![i].optionValue = "\(responseStr?.totalStaff ?? 0)"
                                    break
                                case 15:
                                    self.inspectionQuestionListArray![i].optionValue = "\(responseStr?.campusSqft ?? 0)"
                                    break
                                case 16:
                                    
                                    break
                                case 17:
                                    self.inspectionQuestionListArray![i].optionValue = (responseStr?.contactFirstName ?? "") + " " + (responseStr?.contactLastName ?? "")
                                    break
                                case 18:
                                    let add1 = responseStr?.contactAddressLine1 ?? ""
                                    let add2 = responseStr?.contactAddressLine2 ?? ""
                                    let taluk = responseStr?.contactTalukName ?? ""
                                    let pincode = "Pincode - " + (responseStr?.contactZipcode ?? "")
                                    let distict = responseStr?.contactDistrictName ?? ""
                                    let state = responseStr?.contactStateName ?? ""
                                    let finalAddress = add1 + ", " + add2 + ", " + taluk + ", "  + pincode + ", " + distict + ", " + state
                                    self.inspectionQuestionListArray![i].optionValue = finalAddress
                                    break
                                default:
                                    break
                                }
                            }
                            self.tableView.reloadData()
                            }
                            
                        }
                    }

                }
        }
        
    }
}



// MARK: - CCIGeneralInfoModelData
struct CCIGeneralInfoModelData: Codable {
    let govHomeType, ngoHomeType: Int?
    let contactCity, ngoHomeTypeText: String?
    let type, grantAidType: Int?
    let nitiayogRegistrationNumber, stateName: String?
    let talukID: Int?
    let registrationActText: String?
    let ciclHomeType, id, stateID: Int?
    let landmark, fax, contactFirstName, natureOfHomeText: String?
    let natureOfHome, registrationAct, isReligiousBody, isBlacklisted: Int?
    let issueCertificate: Int?
    let closureStartDate: String?
    let isNitiayogRegistered: Int?
    let closureEndDate, zipcode: String?
    let totalStaff, contactTalukID: Int?
    let registrationDate, contactTalukName, childAdmissionTypeText: String?
    let countryID: Int?
    let contactLandmark, blacklistedDetail: String?
    let status, closureType: Int?
    let contactZipcode, city, registrationNumber, cncpHomeTypeText: String?
    let contactDistrictID: Int?
    let institutionName, contactDistrictName, registrationPlace: String?
    let totalChild: Int?
    let uid, districtName, contactStateName, addressLine2: String?
    let addressLine1, ngoGrantAidTypeText, countryName, registeredOfficeAddress: String?
    let contactAddressLine1, contactLastName, orgName, email: String?
    let contactAddressLine2: String?
    let childAdmissionType, campusSqft, cncpHomeType: Int?
    let talukName, mobile, religiousDetail: String?
    let isTemporaryClosureDone: Int?
    let homeTypes: [Int]?
    let districtID, contactCountryID: Int?
    let contactCountryName, govHomeTypeText: String?
    let contactStateID: Int?
    let ciclHomeTypeText: String?

    enum CodingKeys: String, CodingKey {
        case govHomeType = "gov_home_type"
        case ngoHomeType = "ngo_home_type"
        case contactCity = "contact_city"
        case ngoHomeTypeText = "ngo_home_type_text"
        case type
        case grantAidType = "grant_aid_type"
        case nitiayogRegistrationNumber = "nitiayog_registration_number"
        case stateName = "state_name"
        case talukID = "taluk_id"
        case registrationActText = "registration_act_text"
        case ciclHomeType = "cicl_home_type"
        case id
        case stateID = "state_id"
        case landmark, fax
        case contactFirstName = "contact_first_name"
        case natureOfHomeText = "nature_of_home_text"
        case natureOfHome = "nature_of_home"
        case registrationAct = "registration_act"
        case isReligiousBody = "is_religious_body"
        case isBlacklisted = "is_blacklisted"
        case issueCertificate = "issue_certificate"
        case closureStartDate = "closure_start_date"
        case isNitiayogRegistered = "is_nitiayog_registered"
        case closureEndDate = "closure_end_date"
        case zipcode
        case totalStaff = "total_staff"
        case contactTalukID = "contact_taluk_id"
        case registrationDate = "registration_date"
        case contactTalukName = "contact_taluk_name"
        case childAdmissionTypeText = "child_admission_type_text"
        case countryID = "country_id"
        case contactLandmark = "contact_landmark"
        case blacklistedDetail = "blacklisted_detail"
        case status
        case closureType = "closure_type"
        case contactZipcode = "contact_zipcode"
        case city
        case registrationNumber = "registration_number"
        case cncpHomeTypeText = "cncp_home_type_text"
        case contactDistrictID = "contact_district_id"
        case institutionName = "institution_name"
        case contactDistrictName = "contact_district_name"
        case registrationPlace = "registration_place"
        case totalChild = "total_child"
        case uid
        case districtName = "district_name"
        case contactStateName = "contact_state_name"
        case addressLine2 = "address_line2"
        case addressLine1 = "address_line1"
        case ngoGrantAidTypeText = "ngo_grant_aid_type_text"
        case countryName = "country_name"
        case registeredOfficeAddress = "registered_office_address"
        case contactAddressLine1 = "contact_address_line1"
        case contactLastName = "contact_last_name"
        case orgName = "org_name"
        case email
        case contactAddressLine2 = "contact_address_line2"
        case childAdmissionType = "child_admission_type"
        case campusSqft = "campus_sqft"
        case cncpHomeType = "cncp_home_type"
        case talukName = "taluk_name"
        case mobile
        case religiousDetail = "religious_detail"
        case isTemporaryClosureDone = "is_temporary_closure_done"
        case homeTypes = "home_types"
        case districtID = "district_id"
        case contactCountryID = "contact_country_id"
        case contactCountryName = "contact_country_name"
        case govHomeTypeText = "gov_home_type_text"
        case contactStateID = "contact_state_id"
        case ciclHomeTypeText = "cicl_home_type_text"
    }
    
}
