//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

protocol SelectEmployeesDelegate {
    func selectEmployee(employeeName: String, employeeDesignation: String, employeeId : Int)
}

class SelectEmployeeVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblEmployees: UITableView!
    
    @IBOutlet weak var lblNoEmployeesFound: UILabel!
    
    // MARK: - Properties
    var delegate: SelectEmployeesDelegate?
    
    var cwcEmployeeArray : [CwcEmployee]? = []{
        
        didSet{
            self.lblNoEmployeesFound.isHidden = !(self.cwcEmployeeArray?.count == 0)
        }
    }
    
    var jjbEmployeeArray : [Employee]? = []{
        
        didSet{
            self.lblNoEmployeesFound.isHidden = !(self.jjbEmployeeArray?.count == 0)
        }
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = .white
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // register custom table cell
        registerNib()
        
        if AppManager.shared.getUsertype().lowercased() == "cwc"{
            
            // get cwc employee list api call
            self.fetchCWCEmployee()
        }
        else{
            
            // get jjb employee list api call
            self.fetchJJBEmployee()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func registerNib(){
        
        let customNib = UINib(nibName: "EmployeeTableViewCell", bundle: Bundle.main)
        tblEmployees.register(customNib, forCellReuseIdentifier: "EmployeeTableViewCell")
        tblEmployees.separatorStyle = .none
        
    }
    
    
}


extension SelectEmployeeVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if AppManager.shared.getUsertype().lowercased() == "cwc"{
        return cwcEmployeeArray?.count ?? 0
        }
        else {
            return jjbEmployeeArray?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeTableViewCell", for: indexPath) as! EmployeeTableViewCell
        
        if AppManager.shared.getUsertype().lowercased() == "cwc"{
            
            let cwcEmployee = self.cwcEmployeeArray?[indexPath.row]
            
            cell.lblEmployeeName.text = cwcEmployee?.name
            cell.lblEmployeeDesignation.text = cwcEmployee?.designation
        }
        else{
            
            let jjbEmployee = self.jjbEmployeeArray?[indexPath.row]
            
            cell.lblEmployeeName.text = jjbEmployee?.name
            cell.lblEmployeeDesignation.text = jjbEmployee?.designation
        }
        
        cell.imageOption.image = #imageLiteral(resourceName: "user_placeholder").imageWithColor(color: #colorLiteral(red: 0.462745098, green: 0.462745098, blue: 0.462745098, alpha: 1))
        
        
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if AppManager.shared.getUsertype().lowercased() == "cwc"{
            
            let cwcEmployee = self.cwcEmployeeArray?[indexPath.row]

            // passing value using delegate

            delegate?.selectEmployee(employeeName: cwcEmployee?.name ?? "Unknown", employeeDesignation: cwcEmployee?.designation ?? "", employeeId: cwcEmployee?.id ?? 0)
        }
        else{
            
            let jjbEmployee = self.jjbEmployeeArray?[indexPath.row]
            
            // passing value using delegate
            delegate?.selectEmployee(employeeName: jjbEmployee?.name ?? "Unknown", employeeDesignation: jjbEmployee?.designation ?? "", employeeId: jjbEmployee?.id ?? 0)
        }
                
        self.navigationController?.popViewController(animated: true)
    }
    
}



extension SelectEmployeeVC{
    
    func fetchCWCEmployee(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading Employees ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getCWCEmployeesList() { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            self.cwcEmployeeArray?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.cwcEmployees?.count {
                                
                                if(count > 0){
                                    
                                    self.cwcEmployeeArray = responseStr?.cwcEmployees
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tblEmployees.reloadData()
                                
                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load employee list.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
    
    
    func fetchJJBEmployee(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading Employees ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getJJBEmployeesList() { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            self.jjbEmployeeArray?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.employees?.count {
                                
                                if(count > 0){
                                    
                                    self.jjbEmployeeArray = responseStr?.employees
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tblEmployees.reloadData()
                                
                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load employee list.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
}
