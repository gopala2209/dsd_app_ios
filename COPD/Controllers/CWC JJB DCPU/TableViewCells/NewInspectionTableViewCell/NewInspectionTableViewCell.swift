//
//  HomeTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/06/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class NewInspectionTableViewCell: UITableViewCell {

    @IBOutlet weak var tfForumTitle: UITextField!
    @IBOutlet weak var lblForumTitleSubTitle: UILabel!
    
    @IBOutlet weak var titleBGView: UIView!
    @IBOutlet weak var imageOption: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
