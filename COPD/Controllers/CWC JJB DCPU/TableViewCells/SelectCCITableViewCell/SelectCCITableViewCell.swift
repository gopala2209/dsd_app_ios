//
//  HomeTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/06/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class SelectCCITableViewCell: UITableViewCell {

    @IBOutlet weak var imageInstitution: UIImageView!
    @IBOutlet weak var titleOption: UILabel!
    @IBOutlet weak var titleBGView: UIView!
    @IBOutlet weak var subTitleOption: UILabel!
    @IBOutlet weak var superSubTitleOption: UILabel!
    @IBOutlet weak var imageArrow: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
