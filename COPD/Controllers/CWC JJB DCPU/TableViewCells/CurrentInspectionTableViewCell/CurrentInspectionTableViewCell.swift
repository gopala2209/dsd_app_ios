//
//  HomeTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/06/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class CurrentInspectionTableViewCell: UITableViewCell {

    @IBOutlet weak var titleOption: UILabel!
    @IBOutlet weak var titleBGView: UIView!
    @IBOutlet weak var subTitleOption: UILabel!
    @IBOutlet weak var lblDateOfVisit: UILabel!
    @IBOutlet weak var lblInspectionStatus: UILabel!
    @IBOutlet weak var lblEmpolyeeName: UILabel!
    @IBOutlet weak var lblEmpolyeeDesignation: UILabel!
    
    @IBOutlet weak var lblEmpolyeeNameHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var lblEmpolyeeNameTopContraints: NSLayoutConstraint!
    @IBOutlet weak var lblEmpolyeeDesignationHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var lblEmpolyeeDesignationBottomContraints: NSLayoutConstraint!
    
    @IBOutlet weak var imageTick: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
