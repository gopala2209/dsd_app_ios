//
//  MenuOptionTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 1/2/21.
//

import UIKit

class MenuOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewMenu: UIImageView!
    @IBOutlet weak var labelMenu: UILabel!
    @IBOutlet weak var viewSeperator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ image: UIImage, _ title: String) {
        imageViewMenu.image = image
        labelMenu.text = title
        
        labelMenu.textColor = .label
    }
    
}
