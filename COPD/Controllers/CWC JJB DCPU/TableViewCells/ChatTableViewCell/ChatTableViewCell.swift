//
//  HomeTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/06/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var imageInstitution: UIImageView!
    @IBOutlet weak var titleOption: UILabel!
    @IBOutlet weak var titleBGView: UIView!
    @IBOutlet weak var subTitleOption: UILabel!
    @IBOutlet weak var superSubTitleOption: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
