//
//  MenuProfileTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 1/2/21.
//

import UIKit

class MenuProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelUsertype: UILabel!
    @IBOutlet weak var buttonCross: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitials()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setupInitials() {
        
    }
    
    
    func configure(_ profile: ProfileModel?) {
        
        // Get Login model data
        if let data = UserDefaults.standard.data(forKey: "UserLoginResponseData") {
            do {
                // Create JSON Decoder
                let decoder = JSONDecoder()

                // Decode Note
                let loginModel = try decoder.decode(LoginModel.self, from: data)
                        
                /*let firstName = "\(loginModel.firstName ?? "")"
                let lastName = "\(loginModel.lastName ?? "")"
                
                if firstName.count == 0 && lastName.count == 0{
                    // no name found
                    labelUsername.text = "Unknown"
                }
                else{
                    
                    if firstName.count == 0{
                        labelUsername.text = "\(loginModel.lastName ?? "")"
                    }
                    else{
                        // show user full name
                        labelUsername.text = "\(loginModel.firstName ?? "") \(loginModel.lastName ?? "")"
                        
                        labelUsername.text = labelUsername.text?.trimmingCharacters(in: .whitespaces)
                    }
                }*/
                
                
                // show user name
                labelUsername.text = AppManager.shared.getUsername()
                
                // show user email
                labelUsertype.text = "\(loginModel.email ?? "")"

            } catch {
                print("Unable to Decode Login model (\(error))")
            }
        }
        
    }
    
}
