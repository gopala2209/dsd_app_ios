//
//  RecieverTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/02/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class RecieverTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var recieverAvatar: UIImageView!
    @IBOutlet weak var recieverTitleLbl: UILabel!
    @IBOutlet weak var recieverTitleText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 10.0
    }
    
    func updateUI(model: ConversationModel?){
        
        self.recieverTitleLbl.text = model?.message
        self.recieverTitleText.text = model?.addedAtText
    }
}
