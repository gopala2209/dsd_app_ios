//
//  ChatsTableViewCell.swift
//  COPD
//
//  Created by Anand Prakash on 23/02/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit
import SDWebImage


class ChatsTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var senderAvatar: UIImageView!
    @IBOutlet weak var senderTitleLbl: UILabel!
    @IBOutlet weak var senderTitleText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.layer.cornerRadius = 10.0
    }
    
    func updateUI(model: ConversationModel?){
        
        self.senderTitleLbl.text = model?.message
        self.senderTitleText.text = model?.addedAtText
    }
    
}


