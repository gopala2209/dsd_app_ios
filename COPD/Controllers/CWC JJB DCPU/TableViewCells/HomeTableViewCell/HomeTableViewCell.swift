//
//  HomeTableViewCell.swift
//  COPD
//
//  Created by  on 23/06/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var titleOption: UILabel!
    @IBOutlet weak var titleBGView: UIView!
    @IBOutlet weak var labelIncompleted: UILabel!
    @IBOutlet weak var imageOption: UIImageView!
    @IBOutlet weak var imageArrow: UIImageView!
    
    @IBOutlet weak var heightOfImageView: NSLayoutConstraint!
    @IBOutlet weak var widthOfImageView: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    @IBOutlet weak var overAllHeightOfView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
