//
//  SelectOptionViewController.swift
//  TestFoxFlowSwift
//
//  Created by Anand Prakash on 11/11/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

protocol OptionSelectedDelegate {
    
    func selectMenu(option: String, isSelectType: String, selectedHomeType: HomeTypeModel)
    
}

class SelectOptionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var delegate: OptionSelectedDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var optionPickerView: UIPickerView!
    @IBOutlet weak var popupView: UIView!
    
    var selectedPickerValue: String?
    var selectedCCIHomeType: HomeTypeModel?
    var passingTitle: String?
    var isSelectionType: String?
    
    // Input the data into the array
    var pickerSelectQuarterArray : [String]? = []
    
    var pickerSelectYearArray : [String]? = []
    
    var pickerLastFiftyYearArray : [String]? = []
    
    var pickerCCIHomeTypesArray : [HomeTypeModel]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = passingTitle
        
        if isSelectionType == "quarter"{
            //self.getQuarterList() // dynamic quarter values
            
            //static quarter values
            self.pickerSelectQuarterArray = ["Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"]
            
            self.optionPickerView.delegate = self
            self.optionPickerView.dataSource = self
        }
        else if isSelectionType == "calendar"{
            
            self.getCalendarList()
        }
        else if isSelectionType == "yearselection"{
            
            self.getLastFiftyYears()
        }
        else{
            self.getCCIHomeTypesList(userType: AppManager.shared.getUsertype().lowercased())
        }
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        popupView.roundCorners(corners: [.topLeft, .topRight], radius: 20.0)
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if isSelectionType == "quarter"{
            return pickerSelectQuarterArray?.count ?? 0
        }else if isSelectionType == "calendar"{
            return pickerSelectYearArray?.count ?? 0
        }else if isSelectionType == "yearselection"{
            return pickerLastFiftyYearArray?.count ?? 0
        }
        else{
            return pickerCCIHomeTypesArray?.count ?? 0
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if isSelectionType == "quarter"{
            return pickerSelectQuarterArray?[row]
        }else if isSelectionType == "calendar"{
            return pickerSelectYearArray?[row]
        }else if isSelectionType == "yearselection"{
            return pickerLastFiftyYearArray?[row]
        }
        else{
            return pickerCCIHomeTypesArray?[row].homeTypeName
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }
    
    // Capture the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if isSelectionType == "quarter"{
            self.selectedPickerValue = pickerSelectQuarterArray?[row]
        }else if isSelectionType == "calendar"{
            self.selectedPickerValue = pickerSelectYearArray?[row]
        }else if isSelectionType == "yearselection"{
            self.selectedPickerValue = pickerLastFiftyYearArray?[row]
        }else{
            self.selectedCCIHomeType = pickerCCIHomeTypesArray?[row]
        }
    }
    
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            
            if self.isSelectionType == "quarter"{
                
                if self.pickerSelectQuarterArray?.count ?? 0 > 0{
                    
                    self.delegate?.selectMenu(option: self.selectedPickerValue ?? self.pickerSelectQuarterArray![0], isSelectType: self.isSelectionType!, selectedHomeType: HomeTypeModel.init(admissionTypeId: 0, homeTypeName: "", homeTypeId: 0))
                }
            }else if self.isSelectionType == "calendar"{
                
                if self.pickerSelectYearArray?.count ?? 0 > 0{
                    
                    self.delegate?.selectMenu(option: self.selectedPickerValue ?? self.pickerSelectYearArray![0], isSelectType: self.isSelectionType!, selectedHomeType: HomeTypeModel.init(admissionTypeId: 0, homeTypeName: "", homeTypeId: 0))
                }
            }else if self.isSelectionType == "yearselection"{
                
                if self.pickerLastFiftyYearArray?.count ?? 0 > 0{
                    
                    self.delegate?.selectMenu(option: self.selectedPickerValue ?? self.pickerLastFiftyYearArray![0], isSelectType: self.isSelectionType!, selectedHomeType: HomeTypeModel.init(admissionTypeId: 0, homeTypeName: "", homeTypeId: 0))
                }
            }
            else{
                
                if self.pickerCCIHomeTypesArray?.count ?? 0 > 0{
                    
                    self.delegate?.selectMenu(option: "", isSelectType: self.isSelectionType!, selectedHomeType: HomeTypeModel.init(admissionTypeId: self.selectedCCIHomeType?.admissionTypeId ?? self.pickerCCIHomeTypesArray![0].admissionTypeId, homeTypeName: self.selectedCCIHomeType?.homeTypeName ?? self.pickerCCIHomeTypesArray![0].homeTypeName, homeTypeId: self.selectedCCIHomeType?.homeTypeId ?? self.pickerCCIHomeTypesArray![0].homeTypeId))
                }
            }
        })
    }
}


extension SelectOptionViewController{
    
    func getLastFiftyYears(){
        
        self.pickerLastFiftyYearArray = Date.getYears(forLastNYears: 50)
        
        self.optionPickerView.delegate = self
        self.optionPickerView.dataSource = self
        
    }
    
    func getCalendarList(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.popupView)
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getCalendarList() { responseStr in
                        
                        DispatchQueue.main.async {
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                // servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            self.pickerSelectYearArray?.removeAll()
                            
                            if let count = responseStr?.calenders?.count {
                                
                                if(count > 0){
                                    
                                    self.pickerSelectYearArray = responseStr?.calenders
                                    
                                    self.optionPickerView.delegate = self
                                    self.optionPickerView.dataSource = self
                                    
                                    HUDIndicatorView.shared.dismissHUD()
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                    HUDIndicatorView.shared.dismissHUD()
                                }
                            }
                            else{
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Calendar data.", message: "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
    
    
    func getQuarterList(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.popupView)
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getQuarterList() { responseStr in
                        
                        DispatchQueue.main.async {
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            self.pickerSelectQuarterArray?.removeAll()
                            
                            if let count = responseStr?.quarters?.count {
                                
                                if(count > 0){
                                    
                                    self.pickerSelectQuarterArray = responseStr?.quarters
                                    
                                    self.optionPickerView.delegate = self
                                    self.optionPickerView.dataSource = self
                                    
                                    HUDIndicatorView.shared.dismissHUD()
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                    HUDIndicatorView.shared.dismissHUD()
                                }
                            }
                            else{
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load Quarter data.", message: "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
    
    
    func getCCIHomeTypesList(userType: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.popupView)
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getCCIHomeTypesList() { responseStr in
                        
                        DispatchQueue.main.async {
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            self.pickerCCIHomeTypesArray?.removeAll()
                            
                            if let typeArray = responseStr?.types{
                                
                                for i in 0..<typeArray.count{
                                    
                                    let type = typeArray[i]
                                    
                                    if type.name?.lowercased() == "cncp"{
                                        
                                        if userType == "cwc"{
                                            
                                            if type.homeType?.count ?? 0 > 0{
                                                
                                                for j in 0..<type.homeType!.count{
                                                    
                                                    self.pickerCCIHomeTypesArray?.append(HomeTypeModel.init(admissionTypeId: type.id, homeTypeName: type.homeType![j].name ?? "", homeTypeId: type.homeType![j].id ?? 0))
                                                }
                                            }
                                        }
                                        else if userType == "dcpu"{
                                            
                                            if type.homeType?.count ?? 0 > 0{
                                                
                                                for j in 0..<type.homeType!.count{
                                                    
                                                    self.pickerCCIHomeTypesArray?.append(HomeTypeModel.init(admissionTypeId: type.id, homeTypeName: type.homeType![j].name ?? "", homeTypeId: type.homeType![j].id ?? 0))
                                                }
                                                
                                            }
                                        }
                                    }
                                    
                                    if type.name?.lowercased() == "cicl"{
                                        
                                        if userType == "jjb"{
                                            
                                            if type.homeType?.count ?? 0 > 0{
                                                
                                                for j in 0..<type.homeType!.count{
                                                    
                                                    self.pickerCCIHomeTypesArray?.append(HomeTypeModel.init(admissionTypeId: type.id, homeTypeName: type.homeType![j].name ?? "", homeTypeId: type.homeType![j].id ?? 0))
                                                }
                                                
                                            }
                                        }
                                        else if userType == "dcpu"{
                                            
                                            if type.homeType?.count ?? 0 > 0{
                                                
                                                for j in 0..<type.homeType!.count{
                                                    
                                                    self.pickerCCIHomeTypesArray?.append(HomeTypeModel.init(admissionTypeId: type.id, homeTypeName: type.homeType![j].name ?? "", homeTypeId: type.homeType![j].id ?? 0))
                                                }
                                                
                                            }
                                        }
                                        
                                        
                                    }
                                }
                                
                                
                                self.optionPickerView.delegate = self
                                self.optionPickerView.dataSource = self
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                            }
                            
                            else{
                                
                                ErrorToast.show(message: "No data found!", controller: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
}

