//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class DcpoChatDetailsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblChatMessages: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tfNewTextMessage: UITextField!
    @IBOutlet weak var btnSendMessage: UIButton!
    
    // MARK: - Properties
    var selectedCCIInstitution : Institution?
    
    var chatListArray : [ConversationModel]? = []
    
    var timer: Timer?
    var counter = 30
    
    var lastMessageID = 0
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = .white
        
        // set title text
        lblTitle.text = selectedCCIInstitution?.orgName
        
        // set delegate
        tfNewTextMessage.delegate = self
        tfNewTextMessage.keyboardType = .asciiCapable
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()
        
        // get cci institution list api call
        self.fetchAllChatMessageDetails()
        
        // disable send button
        self.disableSendButton(button: btnSendMessage)
        
        //refresh chat
        self.startTimer()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        invalidateTimer()
    }
    
    func registerNib(){
        
        tblChatMessages.allowsSelection = false
        let customNib = UINib(nibName: "ChatsTableViewCell", bundle: Bundle.main)
        tblChatMessages.register(customNib, forCellReuseIdentifier: "ChatsTableViewCell")
        
        let recieverNib = UINib(nibName: "RecieverTableViewCell", bundle: Bundle.main)
        tblChatMessages.register(recieverNib, forCellReuseIdentifier: "RecieverTableViewCell")
        
        tblChatMessages.separatorStyle = .none
        
    }
    
    
    // MARK: - IBActions
    
    @IBAction func sendMessage(_ sender: UIButton) {
        
        var messageText = tfNewTextMessage.text
        
        // message text
        messageText = messageText?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        // Post message
        self.sendMessage(messageText: messageText ?? "")
    }
    
    
    func enableSendButton(button: UIButton){
        
        button.isEnabled = true
        button.isUserInteractionEnabled = true
        button.setImageTintColor(UIColor.greenTheme)
    }
    
    func disableSendButton(button: UIButton){
        
        button.isEnabled = false
        button.isUserInteractionEnabled = false
        button.setImageTintColor(UIColor.darkGrayShadowTheme)
    }
    
}


extension DcpoChatDetailsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatListArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let conversation = chatListArray?[indexPath.row]
        
        if AppManager.shared.getUserId() == String(conversation?.addedBy ?? 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecieverTableViewCell", for: indexPath) as! RecieverTableViewCell
            cell.selectionStyle = .none
            cell.updateUI(model: conversation)
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatsTableViewCell", for: indexPath) as! ChatsTableViewCell
            cell.selectionStyle = .none
            cell.updateUI(model: conversation)
            
            return cell
        }
    }
    
}

extension DcpoChatDetailsViewController{
    
    func fetchAllChatMessageDetails(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getChatDetailsForInstitutionId(institution: self.selectedCCIInstitution!, recordsPerPage: 20, currentPage: 1) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            self.chatListArray?.removeAll()
                            
                            if responseStr == nil {
                                
                                return
                            }
                            
                            
                            if let count = responseStr?.conversations?.count {
                                
                                if(count > 0){
                                    
                                    self.chatListArray = responseStr?.conversations?.sorted(by: { $0.addedAtText ?? "" < $1.addedAtText ?? "" })
                                }
                                
                                // get last message id
                                self.lastMessageID = self.chatListArray?.last?.id ?? 0
                                
                                self.tblChatMessages.reloadData()
                                
                                if self.chatListArray?.count ?? 0 > 0{
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute: {
                                        
                                        self.scrollToBottom()
                                    })
                                }
                            }
                            else{
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load chat.", message: "", parentView: self)
                            }
                            
                        }
                    }
                    
                }
            }
            
        }
    }
    
    
    func refreshChatHistory(lastMessageId: Int){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.updateChatList(institution: self.selectedCCIInstitution!, lastMessageId: lastMessageId) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                
                                return
                            }
                            
                            
                            if let count = responseStr?.conversations.count {
                                
                                if(count > 0){
                                    
                                    for i in 0..<(responseStr?.conversations.count)!{
                                        
                                        let converstaion = responseStr?.conversations[i]
                                        
                                        self.chatListArray?.append(converstaion!)
                                    }
                                    
                                }
                                
                                self.chatListArray = self.chatListArray?.sorted(by: { $0.addedAtText ?? "" < $1.addedAtText ?? "" })
                                
                                // get last message id
                                self.lastMessageID = self.chatListArray?.last?.id ?? 0
                                
                                // reload table
                                self.tblChatMessages.reloadData()
                                
                                // start timer
                                self.startTimer()
                                
                                if self.chatListArray?.count ?? 0 > 0{
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute: {
                                        
                                        self.scrollToBottom()
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.chatListArray!.count - 1, section: 0)
            self.tblChatMessages.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func sendMessage(messageText: String) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.sendMessage(messageText:messageText, institution:self.selectedCCIInstitution!) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                
                                self.tfNewTextMessage.text = ""
                                
                                // disable button
                                self.disableSendButton(button: self.btnSendMessage)
                                
                                //reload chat list
                                self.fetchAllChatMessageDetails()
                                
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Message not sent!", message: message ?? "", parentView: self)
                                
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
}


extension DcpoChatDetailsViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textField.resignFirstResponder()
        
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if !text.isEmpty{
            
            // enable button
            self.enableSendButton(button: btnSendMessage)
        } else {
            
            // disable button
            self.disableSendButton(button: btnSendMessage)
        }
        
        return true
    }
    
}

extension DcpoChatDetailsViewController {
    fileprivate func startTimer() {
        counter = 30
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
        timer?.fire()
    }
    
    fileprivate func invalidateTimer() {
        if let timer = timer, timer.isValid {
            timer.invalidate()
            
        }
        
        self.timer = nil
        
        // refresh chat list
        self.refreshChatHistory(lastMessageId: self.lastMessageID)
    }
    
    @objc fileprivate func updateCounter() {
        if counter > 0 {
            counter -= 1
        } else {
            
            //time over
            invalidateTimer()
        }
    }
}

