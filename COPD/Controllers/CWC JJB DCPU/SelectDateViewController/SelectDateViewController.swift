//
//  SelectDOBViewController.swift
//  COPD
//
//  Created by Anand Prakash on 14/02/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import UIKit

protocol SelectDateDelegate {
    func selectVisit(date: String, isPicker: String)
}

class SelectDateViewController: UIViewController {
    
    @IBOutlet weak var pickerView: UIDatePicker!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var popupView: UIView!
    
    var delegate: SelectDateDelegate?
    
    var passingVisitDate: String? = ""
    var passingTitle: String? = ""
    var isPicker: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if #available(iOS 13.4, *) {
            pickerView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 250.0)
            pickerView.preferredDatePickerStyle = .wheels
        }
        
        if isPicker == "time"{ //time picker
                       
            //Time picker
            pickerView.datePickerMode = .time
            
        }
        else{ //date picker
            
            //Date picker
            pickerView.datePickerMode = .date
            
            if ((passingVisitDate? .isEmpty)!){
                pickerView.date = Date()
            }
            else{
                
                if ((passingVisitDate?.contains("Date")) != nil){
                    pickerView.date = Date()
                }else{
                    pickerView.date = getDateFromString(dobText: passingVisitDate!)!
                }
              
            }
            
            if isPicker == "visit_date"{ //Date of Visit
                                   
            }
            else if isPicker == "registration_date"{ //Date of Issue or Date of Registration
                                                               
                pickerView.maximumDate = Date()
                
                pickerView.minimumDate = Calendar.current.date(byAdding: .year, value: -5, to: Date())
            }
            else if isPicker == "dob_date"{ //Date of Issue or Date of Registration
                                                               
                pickerView.maximumDate = Date()
                
            }
            
        }
       
                
        lblTitle.text = passingTitle
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideView)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
        
        popupView.roundCorners(corners: [.topLeft, .topRight], radius: 20.0)
    }
    
    func getDateFromString(dobText: String) -> Date? {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        
        return dateFormatter.date(from: dobText)
    }
    
    
    @objc private func didTapOutsideView(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: false, completion: {
              
            if self.isPicker == "time"{
                let selectedTime = self.convertTimeFormater("\(self.pickerView.date)")
                self.delegate?.selectVisit(date: selectedTime, isPicker: self.isPicker!)
            }
            else{
                let selectedDate = self.convertDateFormater("\(self.pickerView.date)")
                self.delegate?.selectVisit(date: selectedDate, isPicker: self.isPicker!)
            }
        })
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        if date.contains("-"){
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        }else{
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss z"
        }
        
        let date = dateFormatter.date(from: date)
        
        if date == nil{
            return "--"
        }
        
        dateFormatter.dateFormat = "dd/MM/yyyy"// "dd-MM-yyyy"
        
        return  dateFormatter.string(from: date!)

    }
    
    func convertTimeFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        if date.contains("-"){
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        }else{
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss z"
        }
        
        let date = dateFormatter.date(from: date)
        
        if date == nil{
            return "--"
        }
        
        dateFormatter.dateFormat = "hh:mm a"
        
        return  dateFormatter.string(from: date!)

    }
}
