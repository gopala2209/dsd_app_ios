//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class NewInspectionVCForDCPU: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblNewInspection: UITableView!
    @IBOutlet weak var btnTypeOfHomes: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var btnStartInspection: UIButton!
    
    // MARK: - Properties
    let defaultForums = ["Name of Institution", "Address of Institution", "Date of Registration", "Date of Visit"]
    let defaultForumsImages = [#imageLiteral(resourceName: "cci_placeholder"),#imageLiteral(resourceName: "location_pin"),#imageLiteral(resourceName: "select_quarter"),#imageLiteral(resourceName: "select_quarter")]
    
    var selectedCCIInstituteTitle = ""
    var selectedDateOfRegistration = ""
    var selectedDateOfVisit = ""
    var addressInfo = ""
    
    var selectedHomeTypeId = 0
    var selectedAdmissionTypeId = 0
    
    var selectedInstitutionUId = ""
    
    var selectedYear = Date().currentYear
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        //lblTitle.text = "\(AppManager.shared.getUsertype()) Inspection Form"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()
        
        // date of visit should be current date by default
        selectedDateOfVisit = Date().currentDate
        
        //set btnYear title
        btnYear.setTitle(selectedYear, for: .normal)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.tblNewInspection.endEditing(true)
        
        //Set shadow
        tblNewInspection.layer.shadowColor = UIColor.grayShadowTheme.cgColor
        tblNewInspection.layer.shadowOpacity = 0.4
        tblNewInspection.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        tblNewInspection.layer.shadowRadius = 3.0
        tblNewInspection.layer.shouldRasterize = true
        tblNewInspection.layer.rasterizationScale = true ? UIScreen.main.scale : 1
        
        tblNewInspection.keyboardDismissMode = .interactive
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    override func viewDidLayoutSubviews(){
        
    }
    
    
    func registerNib(){
        
        let customNib = UINib(nibName: "NewInspectionTableViewCell", bundle: Bundle.main)
        tblNewInspection.register(customNib, forCellReuseIdentifier: "NewInspectionTableViewCell")
        tblNewInspection.separatorStyle = .none
        
    }
    
    
    // MARK: - IBActions
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
        
    }
    
    @IBAction func selectTypeOfHomesAction(_ sender: UIButton) {
        
        // open menu
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT HOME TYPE"
        
        if AppManager.shared.getUsertype().lowercased() == "jjb"{
            nextVC.isSelectionType = "jjb_homes"
        }
        else{
            nextVC.isSelectionType = "default_Homes"
        }
        
        self.present(nextVC, animated: false, completion: nil)
        
    }
    
    @IBAction func selectYearAction(_ sender: UIButton) {
        
        // open menu
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT YEAR"
        
        nextVC.isSelectionType = "yearselection"
        
        self.present(nextVC, animated: false, completion: nil)
        
    }
    
    @IBAction func startInspectionAction(_ sender: UIBarButtonItem) {
        
        // start inspection
        if (selectedCCIInstituteTitle == "" || selectedDateOfRegistration == ""){
            
            ErrorToast.show(message: "Please fill out all fields.", controller: self)
            
            return
        }
        
        
        // post form data to server
        self.postStartInspectionInformation(inspectionuid: self.selectedInstitutionUId, dateOfVisit: selectedDateOfVisit)
        
    }
    
}


extension NewInspectionVCForDCPU: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return defaultForums.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewInspectionTableViewCell", for: indexPath) as! NewInspectionTableViewCell
        
        cell.selectionStyle = .none
        
        cell.tfForumTitle.tag = indexPath.row
        cell.tfForumTitle.placeholder = self.defaultForums[indexPath.row]
        cell.tfForumTitle.keyboardType = .asciiCapable
        
        cell.imageOption.image = self.defaultForumsImages[indexPath.row]
        
        cell.lblForumTitleSubTitle.isHidden = true
        
        cell.tfForumTitle.delegate = self
        
        if indexPath.row != defaultForums.count - 1 {
            cell.tfForumTitle.text = ""
        }
        
        cell.lblForumTitleSubTitle.text = ""
        
        if indexPath.row == 0{
            
            cell.tfForumTitle.isUserInteractionEnabled = true
            cell.tfForumTitle.text = selectedCCIInstituteTitle
            cell.lblForumTitleSubTitle.isHidden = true
            
            cell.imageOption.image = #imageLiteral(resourceName: "cci_placeholder").imageWithColor(color: .darkGray)
            
        }else if indexPath.row == 1{
            
            cell.tfForumTitle.isUserInteractionEnabled = false
            cell.tfForumTitle.text = addressInfo
            cell.lblForumTitleSubTitle.isHidden = true
            
        }else if indexPath.row == 2{
            cell.tfForumTitle.isUserInteractionEnabled = true
            cell.tfForumTitle.text = selectedDateOfRegistration
            cell.lblForumTitleSubTitle.isHidden = true
            
        }else if indexPath.row == 3{
            cell.tfForumTitle.isUserInteractionEnabled = true
            cell.tfForumTitle.text = selectedDateOfVisit
            cell.lblForumTitleSubTitle.isHidden = true
            
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 0{
            
            self.onSelectCCI()
        }
    }
    
    
    @objc func onSelectForum(textField: UITextField){
        
        textField.resignFirstResponder()
        self.tblNewInspection.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            
            switch textField.tag {
                
            case 0: self.onSelectCCI()
            case 2: self.onSelectDateOfRegistration()
            case 3: self.onSelectDateofVisit()
                
            default:
                break
            }
        }
    }
    
    
    func onSelectCCI(){
        
        if selectedHomeTypeId == 0{
            
            ServiceModel().showAlert(title: "Alert", message: "Please select Home type", parentView: self)
            
            return
        }
        
        
        self.tblNewInspection.endEditing(true)
        
        // navigate to Select CCI inspection screen
        let selectCciInstitutionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "selectcciinstitutionvc") as! SelectCciInstitutionVC
        selectCciInstitutionVC.delegate = self
        selectCciInstitutionVC.passingHomeTypeId = self.selectedHomeTypeId
        selectCciInstitutionVC.passingAdmissionTypeId = self.selectedAdmissionTypeId
        selectCciInstitutionVC.passingYear = selectedYear
        
        self.tblNewInspection.endEditing(true)
        
        self.navigationController?.pushViewController(selectCciInstitutionVC, animated: true)
        
    }
    
    
    func onSelectDateofVisit(){
        
        /*let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
         
         nextVC.modalPresentationStyle = .overCurrentContext
         nextVC.delegate = self
         nextVC.passingTitle = "SELECT VISIT DATE"
         nextVC.passingVisitDate = selectedDateOfVisit
         nextVC.isPicker = "visit_date"
         self.present(nextVC, animated: false, completion: nil)*/
        
    }
    
    func onSelectDateOfRegistration(){
        
        /*let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
         
         nextVC.modalPresentationStyle = .overCurrentContext
         nextVC.delegate = self
         nextVC.passingTitle = "SELECT REGISTRATION DATE"
         nextVC.passingVisitDate = selectedDateOfRegistration
         nextVC.isPicker = "registration_date"
         self.present(nextVC, animated: false, completion: nil)*/
    }
    
}


extension NewInspectionVCForDCPU: SelectDateDelegate{
    func selectVisit(date: String, isPicker: String) {
        
        if isPicker == "registration_date"{
            selectedDateOfRegistration = date
        }
        else if isPicker == "visit_date"{
            selectedDateOfVisit = date
        }
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
        }
    }
}

extension NewInspectionVCForDCPU: OptionSelectedDelegate{
    
    func selectMenu(option: String, isSelectType: String, selectedHomeType: HomeTypeModel){
        
        if isSelectType == "yearselection"{
            btnYear.setTitle(option, for: .normal)
            selectedYear = option
        }
        else{
            if option.isEmpty{
                
                btnTypeOfHomes.setTitle(selectedHomeType.homeTypeName, for: .normal)
                
                selectedHomeTypeId = selectedHomeType.homeTypeId ?? 0
                selectedAdmissionTypeId = selectedHomeType.admissionTypeId ?? 0
            }
            else{
                btnTypeOfHomes.setTitle(option, for: .normal)
            }
        }
    }
}

extension NewInspectionVCForDCPU: SelectCCIDelegate {
    func selectCCI(institution: Institution){
        
        selectedInstitutionUId = institution.uid ?? ""
        selectedCCIInstituteTitle = institution.orgName ?? "Unknown"
        
        var subTitleText = ""
        
        if institution.talukName?.isEmpty == false && institution.talukName != ""{
            
            subTitleText = subTitleText + "\(institution.talukName ?? "")" + ", "
            
        }
        if institution.districtName?.isEmpty == false && institution.districtName != ""{
            
            subTitleText = subTitleText + "\(institution.districtName ?? "")" + ", "
            
        }
        if institution.stateName?.isEmpty == false && institution.stateName != ""{
            
            subTitleText = subTitleText + "\(institution.stateName ?? "")"
        }
        
        addressInfo = subTitleText
        
        selectedDateOfRegistration = institution.registrationDate ?? ""
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
        }
    }
    
}


extension NewInspectionVCForDCPU: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        if textField.tag == 5 || textField.tag == 6{
            
            return range.location < 256
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        print("textField.tag == \(textField.tag)")
        if textField.tag == 0 || textField.tag == 2 || textField.tag == 3 || textField.tag == 4{
            onSelectForum(textField: textField)
            return false
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}


extension NewInspectionVCForDCPU{
    func postStartInspectionInformation(inspectionuid: String, dateOfVisit: String) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Starting Inspection ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.startNewInspectionDCPU(institutionUId: inspectionuid, dateOfVisit: dateOfVisit) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                //Toast.show(message: responseStr?.message ?? "Your submission has been sent successfully!", controller: self)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    
                                    // navigate to inspection form
                                    let inspectionFormTitleVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "inspectionformtitlevc") as! InspectionFormTitleVC
                                    
                                    inspectionFormTitleVC.uid = self.selectedInstitutionUId
                                    inspectionFormTitleVC.passingInspectionId = responseStr?.id ?? ""
                                    inspectionFormTitleVC.isCompletedInspection = false
                                    
                                    self.navigationController?.pushViewController(inspectionFormTitleVC, animated: true)
                                }
                                
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Failed to submit!", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
}
