//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class HomeVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblHome: UITableView!
    
    // MARK: - Properties
    let default_Images = [#imageLiteral(resourceName: "inspection_history"), #imageLiteral(resourceName: "current_inspection_nw"), #imageLiteral(resourceName: "current_inspection")]
    let default_Titles = ["New Inspection", "Pending Inspection", "Completed Inspection"]
    
    let default_Dcpu_Images = [#imageLiteral(resourceName: "inspection_history"), #imageLiteral(resourceName: "current_inspection_nw"), #imageLiteral(resourceName: "current_inspection"), #imageLiteral(resourceName: "chat_dcpo")]
    let default_Dcpu_Titles = ["New Inspection", "Pending Inspection", "Completed Inspection", "DCPU Chat"]
    
    let paddingRight = 20
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = .white
        
        lblTitle.text = "\(AppManager.shared.getUsertype()) - Home"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()
        
        let isFirstLaunch = UserDefaults.standard.bool(forKey: "isFirstLaunch")

            if isFirstLaunch == true{
                //It's the initial launch of application.
                Toast.show(message: "You have successfully logged in.", controller: self)
                
                UserDefaults.standard.set(false, forKey: "isFirstLaunch")
                UserDefaults.standard.synchronize()
            }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                    
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: (self.navigationController?.view)!, bgColor: UIColor.blueTheme)
//      self.navigationController?.view.backgroundColor = UIColor.orangeTheme
//      self.navigationController?.navigationBar.barTintColor = UIColor.orangeTheme
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
                
    }
        
    
    func registerNib(){
        
        let customNib = UINib(nibName: "HomeTableViewCell", bundle: Bundle.main)
        tblHome.register(customNib, forCellReuseIdentifier: "HomeTableViewCell")
        tblHome.separatorStyle = .none
        
    }
    
    
    // MARK: - IBActions
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
        
    }
    
}


extension HomeVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if AppManager.shared.getUsertype().lowercased() == "dcpu"{
        return default_Dcpu_Titles.count
        }
        else {
            return default_Titles.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        // DCPU
        if AppManager.shared.getUsertype().lowercased() == "dcpu"{
            
            cell.titleOption.text = self.default_Dcpu_Titles[indexPath.row]
            cell.imageOption.image = self.default_Dcpu_Images[indexPath.row]
            
        }
        // JJB & CWC
        else{
            
            cell.titleOption.text = self.default_Titles[indexPath.row]
            cell.imageOption.image = self.default_Images[indexPath.row]
        }
        
        switch indexPath.row {
        case 0:
            self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.8117647059, green: 0.8509803922, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.8862745098, green: 0.9215686275, blue: 0.9411764706, alpha: 1)], tableCellView: cell.titleBGView, width: tblHome.bounds.width + CGFloat(paddingRight))
            
            cell.labelIncompleted.isHidden = true
            break
            
        case 1:
            self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.9960784314, green: 0.6784313725, blue: 0.6509803922, alpha: 1), #colorLiteral(red: 0.9607843137, green: 0.937254902, blue: 0.937254902, alpha: 1)], tableCellView: cell.titleBGView, width: tblHome.bounds.width + CGFloat(paddingRight))
            
            cell.labelIncompleted.isHidden = true
            break
            
        case 2:
            self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.7529411765, green: 0.7921568627, blue: 0.2, alpha: 0.9746314847), #colorLiteral(red: 0.5098039216, green: 0.6941176471, blue: 1, alpha: 0.2327985699)], tableCellView: cell.titleBGView, width: tblHome.bounds.width + CGFloat(paddingRight))

            cell.labelIncompleted.isHidden = true
            break
            
        case 3:
            self.applyGradientColorToTableCell(colours: [#colorLiteral(red: 0.8117647059, green: 0.8509803922, blue: 0.8745098039, alpha: 1), #colorLiteral(red: 0.8862745098, green: 0.9215686275, blue: 0.9411764706, alpha: 1)], tableCellView: cell.titleBGView, width: tblHome.bounds.width + CGFloat(paddingRight))
            
            cell.labelIncompleted.isHidden = true
            break
        default:
            break
        }
                
        cell.selectionStyle = .none
        
        cell.imageArrow.highlightedImage = UIImage(named: "right_arrow")?.imageWithColor(color: UIColor.whiteTheme)

        cell.titleOption.highlightedTextColor = UIColor.whiteTheme
        cell.labelIncompleted.highlightedTextColor = UIColor.whiteTheme

        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 0{
            if AppManager.shared.getUsertype().lowercased() == "dcpu"{
            
            // navigate to DCPU new inspection screen
            let newInspectionVCForDCPU = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "newinspectionvcfordcpu") as! NewInspectionVCForDCPU
            self.navigationController?.pushViewController(newInspectionVCForDCPU, animated: true)
        }
        else{
            
            // navigate to default new inspection screen
            let newInspectionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "newinspectionvc") as! NewInspectionVC
            self.navigationController?.pushViewController(newInspectionVC, animated: true)
        }
        }
        else if indexPath.row == 1{
            
            // navigate to incompleted inspection
            let completedIncompletedInspectionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "completedincompletedinspectionvc") as! CompletedIncompletedInspectionVC
            
            completedIncompletedInspectionVC.isCompletedInspection = false
            self.navigationController?.pushViewController(completedIncompletedInspectionVC, animated: true)
        }
        
        else if indexPath.row == 2{
            
            // navigate to incompleted inspection
            let completedIncompletedInspectionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "completedincompletedinspectionvc") as! CompletedIncompletedInspectionVC
            
            completedIncompletedInspectionVC.isCompletedInspection = true
            self.navigationController?.pushViewController(completedIncompletedInspectionVC, animated: true)
        }
        
        else if indexPath.row == 3{
          
            if AppManager.shared.getUsertype().lowercased() == "dcpu"{
                
                // navigate to dcpu chat
                let dcpoChatViewController = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "dcpochatviewcontroller") as! DcpoChatViewController
                self.navigationController?.pushViewController(dcpoChatViewController, animated: true)
                
            }
            
        }
        
    }
    
    
    func applyGradientColorToTableCell(colours: [UIColor], tableCellView: UIView, width: CGFloat) {
        
        // Add gradient to Puff Summary view
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0,y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0,y: 0.5)
        gradient.frame = CGRect(x: 0, y: 0, width: width - 40, height: 110)
        gradient.cornerRadius = 10
       
        tableCellView.layer.insertSublayer(gradient, at: 0)
        
    }
    
}
