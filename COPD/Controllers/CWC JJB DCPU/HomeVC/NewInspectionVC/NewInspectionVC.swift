//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class NewInspectionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblNewInspection: UITableView!
    @IBOutlet weak var btnTypeOfHomes: UIButton!
    @IBOutlet weak var btnYear: UIButton!
    
    // MARK: - Properties
    let defaultForums = ["Select CCI", "Date of Visit", "Employee"]
    let defaultForumsImages = [#imageLiteral(resourceName: "cci_placeholder"), #imageLiteral(resourceName: "select_quarter"), #imageLiteral(resourceName: "user_placeholder")]
    
    //let defaultForums = ["Select CCI", "Select Calendar Year", "Select Quarter", "Date of Visit", "Time of Visit", "Name of the Official"]
    //let defaultForumsImages = [#imageLiteral(resourceName: "location_pin"), #imageLiteral(resourceName: "select_calendar"), #imageLiteral(resourceName: "select_quarter"), #imageLiteral(resourceName: "select_quarter"), #imageLiteral(resourceName: "clock_icon"), #imageLiteral(resourceName: "child_icon")]
    
    var selectedCCIInstituteTitle = ""
    var selectedCCIInstituteSubTitle = ""
    var selectedEmployeeName = ""
    var selectedEmployeeDesignation = ""
    var selectedEmployeeId = 0
    
    var selectedTimeOfVisit = ""
    var selectedDateOfVisit = ""
    var selectedCalendarYear = ""
    var selectedQuarter = ""
    var nameOfOfficial = ""
    var selectedYear = Date().currentYear
    
    var selectedHomeTypeId = 0
    var selectedAdmissionTypeId = 0
    var selectedInstitutionUId = ""
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        //lblTitle.text = "\(AppManager.shared.getUsertype()) Inspection Form"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()
        
        // date of visit should be current date by default
        selectedDateOfVisit = Date().currentDate
        
        //set btnYear title
        btnYear.setTitle(selectedYear, for: .normal)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
        self.tblNewInspection.endEditing(true)
        
        //Set shadow
        tblNewInspection.layer.shadowColor = UIColor.grayShadowTheme.cgColor
        tblNewInspection.layer.shadowOpacity = 0.4
        tblNewInspection.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        tblNewInspection.layer.shadowRadius = 3.0
        tblNewInspection.layer.shouldRasterize = true
        tblNewInspection.layer.rasterizationScale = true ? UIScreen.main.scale : 1
        
        tblNewInspection.keyboardDismissMode = .interactive
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)


    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        //self.tableHeightConst?.constant = self.tblNewInspection.contentSize.height
    }
    
    
    func registerNib(){
        
        let customNib = UINib(nibName: "NewInspectionTableViewCell", bundle: Bundle.main)
        tblNewInspection.register(customNib, forCellReuseIdentifier: "NewInspectionTableViewCell")
        tblNewInspection.separatorStyle = .none
    }
    

    // MARK: - IBActions
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        
        // open menu
        openMenu()
        
    }
    
    @IBAction func selectTypeOfHomesAction(_ sender: UIButton) {
        
        // open menu
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT HOME TYPE"
        
        if AppManager.shared.getUsertype().lowercased() == "jjb"{
            nextVC.isSelectionType = "jjb_homes"
        }
        else{
            nextVC.isSelectionType = "default_Homes"
        }
        
        self.present(nextVC, animated: false, completion: nil)
        
    }
    
    @IBAction func selectYearAction(_ sender: UIButton) {
        
        // open menu
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT YEAR"
        
        nextVC.isSelectionType = "yearselection"
        
        self.present(nextVC, animated: false, completion: nil)
        
    }
    
    @IBAction func startInspectionAction(_ sender: UIBarButtonItem) {
        
        // start inspection
        //if (selectedCCIInstituteTitle == "" || selectedTimeOfVisit == "" || selectedDateOfVisit == "" || selectedCalendarYear == "" || selectedQuarter == "" || nameOfOfficial == ""){

        if (selectedCCIInstituteTitle == ""){
            
            ErrorToast.show(message: "Please select the CCI", controller: self)
            
            return
        }
        
        if (selectedEmployeeId == 0){
            
            ErrorToast.show(message: "Please select the employee", controller: self)
            
            return
        }

        // post form data to server
        self.postStartInspectionInformation(inspectionuid: self.selectedInstitutionUId, dateOfVisit: selectedDateOfVisit, employeeId: selectedEmployeeId)
        
    }
    
}


extension NewInspectionVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return defaultForums.count
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewInspectionTableViewCell", for: indexPath) as! NewInspectionTableViewCell
        
        cell.selectionStyle = .none
        
        cell.tfForumTitle.tag = indexPath.row
        cell.tfForumTitle.placeholder = self.defaultForums[indexPath.row]
        cell.tfForumTitle.keyboardType = .asciiCapable
        cell.imageOption.image = self.defaultForumsImages[indexPath.row]
 
        cell.tfForumTitle.delegate = self
        cell.tfForumTitle.text = nameOfOfficial
        
        cell.lblForumTitleSubTitle.isHidden = true
        
        if indexPath.row != defaultForums.count - 1 {
            cell.tfForumTitle.text = ""
        }
        
        cell.lblForumTitleSubTitle.text = ""
                
        switch indexPath.row {
        
        case 0:
            cell.tfForumTitle.text = selectedCCIInstituteTitle
            
            cell.imageOption.image = #imageLiteral(resourceName: "cci_placeholder").imageWithColor(color: .darkGray)
            
            if selectedCCIInstituteSubTitle == ""{
                cell.lblForumTitleSubTitle.isHidden = true
            }else{
                cell.lblForumTitleSubTitle.isHidden = false
                cell.lblForumTitleSubTitle.text = selectedCCIInstituteSubTitle
            }
        
        case 1: cell.tfForumTitle.text = selectedDateOfVisit
            
        case 2:
            
            cell.tfForumTitle.text = selectedEmployeeName
                        
            cell.imageOption.image = #imageLiteral(resourceName: "user_placeholder").imageWithColor(color: .darkGray)
            
            if selectedEmployeeDesignation == ""{
                cell.lblForumTitleSubTitle.isHidden = true
            }else{
                cell.lblForumTitleSubTitle.isHidden = false
                cell.lblForumTitleSubTitle.text = selectedEmployeeDesignation
            }
            
        /*case 1: cell.tfForumTitle.text = selectedCalendarYear
        case 2: cell.tfForumTitle.text = selectedQuarter
        case 3: cell.tfForumTitle.text = selectedDateOfVisit
        case 4: cell.tfForumTitle.text = selectedTimeOfVisit*/
        
        default:
            cell.lblForumTitleSubTitle.isHidden = true
            break
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if indexPath.row == 0{
            
            self.onSelectCCI()
        }
        else if indexPath.row == 2{
            
            self.onSelectEmployees()
        }
        
    }

    
    @objc func onSelectForum(textField: UITextField){
        
        textField.resignFirstResponder()
        self.tblNewInspection.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            
            switch textField.tag {
            
            /*case 0: self.onSelectCCI()
            case 1: self.onSelectCalendarYear()
            case 2: self.onSelectQuarter()
            case 3: self.onSelectDateofVisit()
            case 4: self.onSelectTimeofVisit()*/
            
            case 0: self.onSelectCCI()
            case 1: self.onSelectDateofVisit()
            case 2: self.onSelectEmployees()
                
            default:
                break
            }
        }
    }
    
    
    func onSelectCCI(){
        
        if selectedHomeTypeId == 0{
            
            ServiceModel().showAlert(title: "Alert", message: "Please select Home type", parentView: self)
            
            return
        }
        
        
        self.tblNewInspection.endEditing(true)
        
        // navigate to Select CCI inspection screen
        let selectCciInstitutionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "selectcciinstitutionvc") as! SelectCciInstitutionVC
        selectCciInstitutionVC.delegate = self
        selectCciInstitutionVC.passingHomeTypeId = self.selectedHomeTypeId
        selectCciInstitutionVC.passingAdmissionTypeId = self.selectedAdmissionTypeId
        selectCciInstitutionVC.passingYear = selectedYear
        
        self.tblNewInspection.endEditing(true)
        
        self.navigationController?.pushViewController(selectCciInstitutionVC, animated: true)
    }
    
    
    func onSelectEmployees(){
        
        self.tblNewInspection.endEditing(true)
        
        // navigate to Select employee screen
        let SelectEmployeeVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "selectemployeevc") as! SelectEmployeeVC
        SelectEmployeeVC.delegate = self
        
        self.tblNewInspection.endEditing(true)
        
        self.navigationController?.pushViewController(SelectEmployeeVC, animated: true)
    }
    
    func onSelectCalendarYear(){
        
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT CALENDAR YEAR"
        nextVC.isSelectionType = "calendar"
        
        self.present(nextVC, animated: false, completion: nil)
    }
    
    func onSelectQuarter(){
        
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectOptionViewController") as! SelectOptionViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT QUARTER"
        nextVC.isSelectionType = "quarter"
        
        self.present(nextVC, animated: false, completion: nil)
    }
    
    
    func onSelectDateofVisit(){
        
       /* let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT VISIT DATE"
        nextVC.passingVisitDate = selectedDateOfVisit
        nextVC.isPicker = "visit_date"
        self.present(nextVC, animated: false, completion: nil)*/
        
    }
    
    func onSelectTimeofVisit(){
        
        let nextVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "SelectDateViewController") as! SelectDateViewController
        
        nextVC.modalPresentationStyle = .overCurrentContext
        nextVC.delegate = self
        nextVC.passingTitle = "SELECT VISIT TIME"
        nextVC.passingVisitDate = selectedTimeOfVisit
        nextVC.isPicker = "time"
        self.present(nextVC, animated: false, completion: nil)
    }
    
}


extension NewInspectionVC: SelectDateDelegate{
    func selectVisit(date: String, isPicker: String) {
        
        if isPicker == "time"{
            selectedTimeOfVisit = date
        }
        else{
            selectedDateOfVisit = date
        }
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
        }
    }
}

extension NewInspectionVC: OptionSelectedDelegate{
    
    func selectMenu(option: String, isSelectType: String, selectedHomeType: HomeTypeModel){
        
        if isSelectType == "quarter"{
            selectedQuarter = option
        }else if isSelectType == "calendar"{
            selectedCalendarYear = option
        }else if isSelectType == "yearselection"{
            btnYear.setTitle(option, for: .normal)
            selectedYear = option
        }
        else{
            btnTypeOfHomes.setTitle(selectedHomeType.homeTypeName, for: .normal)
            
            selectedHomeTypeId = selectedHomeType.homeTypeId ?? 0
            selectedAdmissionTypeId = selectedHomeType.admissionTypeId ?? 0
        }
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
        }
    }
}

extension NewInspectionVC: SelectCCIDelegate {
    func selectCCI(institution: Institution){
        
        selectedInstitutionUId = institution.uid ?? ""
        selectedCCIInstituteTitle = institution.orgName ?? "Unknown"
        
        var subTitleText = ""
        
        if institution.talukName?.isEmpty == false && institution.talukName != ""{
            
            subTitleText = subTitleText + "\(institution.talukName ?? "")" + ", "
            
        }
        if institution.districtName?.isEmpty == false && institution.districtName != ""{
            
            subTitleText = subTitleText + "\(institution.districtName ?? "")" + ", "
            
        }
        if institution.stateName?.isEmpty == false && institution.stateName != ""{
            
            subTitleText = subTitleText + "\(institution.stateName ?? "")"
        }
        
        selectedCCIInstituteSubTitle = subTitleText
                
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
            
        }
            
    }
    
}

extension NewInspectionVC: SelectEmployeesDelegate {
    func selectEmployee(employeeName: String, employeeDesignation: String, employeeId : Int){
        
        selectedEmployeeName = employeeName
        selectedEmployeeDesignation = employeeDesignation
        selectedEmployeeId = employeeId
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
            
        }
        
    }
}


extension NewInspectionVC: UITextFieldDelegate {
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        return range.location < 26
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("textField.tag == \(textField.tag)")
        //if case 0...4 = textField.tag{
        if case 0...2 = textField.tag{
            onSelectForum(textField: textField)
            return false
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        nameOfOfficial = textField.text ?? ""
        nameOfOfficial = nameOfOfficial.trimmingCharacters(in: .whitespaces)
        
        DispatchQueue.main.async {
            
            //reload table
            self.tblNewInspection.reloadData()
        }
        
        return true
    }
}


extension NewInspectionVC{
    func postStartInspectionInformation(inspectionuid: String, dateOfVisit: String, employeeId: Int) {
    
    let servicemodel = ServiceModel()
    
    if (NetworkConnection.isConnectedToNetwork()){
        
        do {
            
            HUDIndicatorView.shared.showHUD(view: self.view,
                                            title: "Starting Inspection ..")
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                servicemodel.startNewInspection(institutionUId: inspectionuid, dateOfVisit: dateOfVisit, employeeId: employeeId) { responseStr in
                    
                    DispatchQueue.main.async {
                        
                        if responseStr == nil {
                            HUDIndicatorView.shared.dismissHUD()
                            servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)

                            return
                        }
                                                    
                        let status = responseStr?.status
                        
                        if(status == 1){
                                                                                                
                            HUDIndicatorView.shared.dismissHUD()
                            
                           // Toast.show(message: responseStr?.message ?? "Your submission has been sent successfully!", controller: self)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                
                                // navigate to inspection form
                                let inspectionFormTitleVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "inspectionformtitlevc") as! InspectionFormTitleVC
                            
                                inspectionFormTitleVC.uid = self.selectedInstitutionUId
                                inspectionFormTitleVC.passingInspectionId = responseStr?.id ?? ""
                                inspectionFormTitleVC.isCompletedInspection = false

                                self.navigationController?.pushViewController(inspectionFormTitleVC, animated: true)
                            }
                            
                        }
                        else{
                            let message = responseStr?.message
                            servicemodel.showAlert(title: "Failed to submit!", message: message ?? "", parentView: self)
                            
                            HUDIndicatorView.shared.dismissHUD()
                        }
                    }
                }
            }
             
        }catch {
            
            servicemodel.handleError(error: error, parentView: self)
        }
    }else{
        servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
    }
}

}

