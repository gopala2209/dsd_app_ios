//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import CalendarDateRangePickerViewController

class CompletedIncompletedInspectionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblInspection: UITableView!
    @IBOutlet weak var tfFilterCCI: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnGetAll: UIButton!
    
    @IBOutlet weak var lblNoInspectionFound: UILabel!
       
    // MARK: - Properties
    var inspectionArray : [Inspection]? = []{
        
        didSet{
            self.lblNoInspectionFound.isHidden = !(self.inspectionArray?.count == 0)
        }
    }
    
    var isCompletedInspection: Bool? = true
    
    var dropdownOptionsArray = ["GET ALL", "TODAY", "YESTERDAY", "THIS WEEK", "CUSTOM"]
    var dropDown = DropDown()
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
                
        navigationController?.navigationBar.barTintColor = .white
        
        tfFilterCCI.delegate = self
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
                
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        tblInspection.delegate = self
        
        // register custom table cell
        registerNib()
        
        // make API call
        if isCompletedInspection == true{
            
            lblTitle.text = "Inspection History"
            self.getAllInspectionFormList(institutionUId: "", status: "2", startDate: "", endDate: "")
        }else{
            lblTitle.text = "Pending Inspection"
            self.getAllInspectionFormList(institutionUId: "", status: "1", startDate: "", endDate: "")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    func registerNib(){
        
        let customNib = UINib(nibName: "CurrentInspectionTableViewCell", bundle: Bundle.main)
        tblInspection.register(customNib, forCellReuseIdentifier: "CurrentInspectionTableViewCell")
        tblInspection.separatorStyle = .none
        
    }
    
    // MARK: - IBActions
    
    @IBAction func clearFilterButtonAction(_ sender: UIButton) {
        
        tfFilterCCI.text = ""
        
        // make API call
        if isCompletedInspection == true{
            
            self.getAllInspectionFormList(institutionUId: "", status: "2", startDate: "", endDate: "")
        }else{
            self.getAllInspectionFormList(institutionUId: "", status: "1", startDate: "", endDate: "")
        }
    }
    
    
    @IBAction func getAllAction(_ sender: UIButton) {
        
        // display popup
        self.displayOptionSelectionPopup()
    }
    
    func displayOptionSelectionPopup(){
        DispatchQueue.main.async {
                        
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropdownOptionsArray
            self.dropDown.anchorView = self.btnGetAll
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .center
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.btnGetAll.setTitle(item, for: .normal)
                
                var status = ""
                if self.isCompletedInspection == true{
                    status = "2"
                }else{
                    status = "1"
                }
                
                if item == "GET ALL"{
                    
                    // make api call
                    self.getAllInspectionFormList(institutionUId: "", status: status, startDate: "", endDate: "") //05/10/2022
                }
                else if item == "TODAY"{
                    
                    // make api call
                    self.getAllInspectionFormList(institutionUId: "", status: status, startDate: Date().currentDate, endDate: Date().currentDate)
                }
                else if item == "YESTERDAY"{
                    // make api call
                    self.getAllInspectionFormList(institutionUId: "", status: status, startDate: Date().yesterdayDate, endDate: Date().yesterdayDate)
                }
                else if item == "THIS WEEK"{
                    // make api call
                    self.getAllInspectionFormList(institutionUId: "", status: status, startDate: Date().startOfWeek!, endDate: Date().endOfWeek!)
                    
                }
                else if item == "CUSTOM"{
                    
                    //open calendar to choose start date and end date
                    
                    let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
                    dateRangePickerViewController.delegate = self
//                    dateRangePickerViewController.minimumDate = Date()
//                    dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
                    dateRangePickerViewController.selectedStartDate = Date()
                    dateRangePickerViewController.modalPresentationStyle = .overCurrentContext
                    
                    let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
                    self.navigationController?.present(navigationController, animated: true, completion: nil)
                }
                    
            }
            
            self.dropDown.show()
            
        }
    }
    
    
    func convertDateFormat(_ dateString: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let newDate = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return  dateFormatter.string(from: newDate!)

    }
}


extension CompletedIncompletedInspectionVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return inspectionArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentInspectionTableViewCell", for: indexPath) as! CurrentInspectionTableViewCell
          
        let inspection = self.inspectionArray?[indexPath.row]

        cell.titleOption.text = inspection?.cci?.orgName ?? "Unknown"
        
        var institutionAddress = ""
        
        if inspection?.cci?.addressLine1?.isEmpty == false && inspection?.cci?.addressLine1 != ""{
            
            institutionAddress = institutionAddress + "\(inspection?.cci?.addressLine1 ?? "")" + ", "
        }
        if inspection?.cci?.addressLine2?.isEmpty == false && inspection?.cci?.addressLine2 != ""{
            
            institutionAddress = institutionAddress + "\(inspection?.cci?.addressLine2 ?? "")" + ", "
        }
        if inspection?.cci?.talukName?.isEmpty == false && inspection?.cci?.talukName != ""{
            
            institutionAddress = institutionAddress + "\(inspection?.cci?.talukName ?? "")" + ", "
        }
        if inspection?.cci?.contactLandmark?.isEmpty == false && inspection?.cci?.contactLandmark != ""{
            
            institutionAddress = institutionAddress + "Landmark : \(inspection?.cci?.contactLandmark ?? "")" + ", "
        }
        
        if inspection?.cci?.contactZipcode?.isEmpty == false && inspection?.cci?.contactZipcode != ""{
            
            institutionAddress = institutionAddress + "Pincode - \(inspection?.cci?.contactZipcode ?? "")"
        }
        
        cell.subTitleOption.text = institutionAddress
        
        cell.lblDateOfVisit.text = "Date of Visit : \(inspection?.dateOfVisit ?? "--")"
        
        if AppManager.shared.getUsertype().lowercased() == "dcpu"{
        
            cell.lblEmpolyeeName.isHidden = true
            cell.lblEmpolyeeDesignation.isHidden = true
            cell.lblEmpolyeeNameHeightContraints.constant = 0
            cell.lblEmpolyeeNameTopContraints.constant = 0
            cell.lblEmpolyeeDesignationHeightContraints.constant = 0
            cell.lblEmpolyeeDesignationBottomContraints.constant = 0
        }
        else{
            cell.lblEmpolyeeName.isHidden = false
            cell.lblEmpolyeeDesignation.isHidden = false
            
            if AppManager.shared.getUsertype().lowercased() == "cwc"{
                cell.lblEmpolyeeName.text = "Emp Name : \(inspection?.cwcEmployee?.name ?? "--")"
                cell.lblEmpolyeeDesignation.text = "Designation : \(inspection?.cwcEmployee?.designationName ?? "--")"
                
            }else if AppManager.shared.getUsertype().lowercased() == "jjb"{
                cell.lblEmpolyeeName.text = "Emp Name : \(inspection?.jjbEmployee?.name ?? "--")"
                cell.lblEmpolyeeDesignation.text = "Designation : \(inspection?.jjbEmployee?.designationName ?? "--")"
                
            }
                
            cell.lblEmpolyeeNameHeightContraints.constant = 21
            cell.lblEmpolyeeNameTopContraints.constant = 15
            cell.lblEmpolyeeDesignationHeightContraints.constant = 21
            cell.lblEmpolyeeDesignationBottomContraints.constant = 15
        }
                        
        if isCompletedInspection == true{
            
            cell.lblInspectionStatus.text = "Completed"
            cell.lblInspectionStatus.textColor = UIColor.greenTheme
            
            cell.imageTick.isHidden = false
        }
        else{
            cell.lblInspectionStatus.text = "In-Progress"
            cell.lblInspectionStatus.textColor = UIColor.redErrorTheme
            
            cell.imageTick.isHidden = true
        }
        
        cell.selectionStyle = .none
        
        print("Inspection Timestamp: \(inspection?.requestedAtEpoch ?? 0) And converted: \(Double(inspection?.requestedAtEpoch ?? 0).getDateStringFromUTC())")
        
        return cell
        
    }
    
    
    func getDate(unixdate: Int) -> String {
        if unixdate == 0 {return ""}
        let date = NSDate(timeIntervalSince1970: TimeInterval(unixdate))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        dayTimePeriodFormatter.timeZone =  .current
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return "Updated: \(dateString)"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let inspection = self.inspectionArray?[indexPath.row]
        
        // navigate to inspection form
        let inspectionFormTitleVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "inspectionformtitlevc") as! InspectionFormTitleVC
        inspectionFormTitleVC.uid = inspection?.cci?.uid ?? ""
        inspectionFormTitleVC.passingInspectionId = inspection?.alias ?? ""
        inspectionFormTitleVC.isCompletedInspection = isCompletedInspection!
        
        self.navigationController?.pushViewController(inspectionFormTitleVC, animated: true)
        
    }
    
}


extension CompletedIncompletedInspectionVC{
    func getAllInspectionFormList(institutionUId:String, status: String, startDate: String, endDate: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view, title: "Fetching Inspection ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getAllInspection(institutionUId: institutionUId, status: status, startDate: startDate, endDate: endDate) { responseStr in
                        
                        DispatchQueue.main.async {
                            if responseStr == nil {
                                
                                self.inspectionArray?.removeAll()
                                self.tblInspection.reloadData()
                                
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                                                        
                            self.inspectionArray?.removeAll()
                            
                            if let count = responseStr?.inspections?.count {
                                
                                if(count > 0){
                                    
                                    
                                    if self.isCompletedInspection == false{// sorting array based on epoch timestamp
                                    //pending
                                    self.inspectionArray = responseStr?.inspections?.sorted(by: { Double($0.requestedAtEpoch ?? 0).getDateStringFromUTC() > Double($1.requestedAtEpoch ?? 0).getDateStringFromUTC() })
                                    }
                                    else{
                                    //completed
                                    self.inspectionArray = responseStr?.inspections?.sorted(by: { Double($0.completedAtEpoch ?? 0).getDateStringFromUTC() > Double($1.completedAtEpoch ?? 0).getDateStringFromUTC() })
                                    }
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                            }
                            else{
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load inspection form data.", message: "", parentView: self)
                            }
                            
                            // reload table
                            self.tblInspection.reloadData()
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                        }
                    }
                }
                
            }
        }
    }
}


extension CompletedIncompletedInspectionVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if textField == tfFilterCCI{
            onSelectForum(textField: textField)
            return false
        }
        return true
    }
    
    @objc func onSelectForum(textField: UITextField){
        
        textField.resignFirstResponder()
        self.tblInspection.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            
            self.onSelectCCI()
        }
    }
    
    
    func onSelectCCI(){

        self.tblInspection.endEditing(true)
        
        // navigate to Select CCI inspection screen
        let selectCciInstitutionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "selectcciinstitutionvc") as! SelectCciInstitutionVC
        selectCciInstitutionVC.delegate = self
        selectCciInstitutionVC.passingHomeTypeId = 0
        
        if AppManager.shared.getUsertype().lowercased() == "cwc"{
        selectCciInstitutionVC.passingAdmissionTypeId = 1
        }
        else if AppManager.shared.getUsertype().lowercased() == "jjb"{
            selectCciInstitutionVC.passingAdmissionTypeId = 2
            }
        else{
            selectCciInstitutionVC.passingAdmissionTypeId = 0
        }
        
        self.tblInspection.endEditing(true)
        
        self.navigationController?.pushViewController(selectCciInstitutionVC, animated: true)
    }
        
}


extension CompletedIncompletedInspectionVC: SelectCCIDelegate {
    func selectCCI(institution: Institution){
        
        tfFilterCCI.text = institution.orgName ?? "Unknown"
                
        // make API call
        if isCompletedInspection == true{
            
            self.getAllInspectionFormList(institutionUId: institution.uid ?? "", status: "2", startDate: "", endDate: "")
        }else{
            self.getAllInspectionFormList(institutionUId: institution.uid ?? "", status: "1", startDate: "", endDate: "")
        }
            
    }
    
}


extension CompletedIncompletedInspectionVC: CalendarDateRangePickerViewControllerDelegate{
    func didTapCancel() {
        
        // no action
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        
        self.dismiss(animated: true, completion: {
          
            let startDateNew = self.convertDateFormater("\(startDate!)") //05/10/2022
            let endDateNew = self.convertDateFormater("\(endDate!)") //05/10/2022
            
            // make API call
            if self.isCompletedInspection == true{
                
                self.getAllInspectionFormList(institutionUId: "", status: "2", startDate: startDateNew, endDate: endDateNew)
            }else{
                self.getAllInspectionFormList(institutionUId: "", status: "1", startDate: startDateNew, endDate: endDateNew)
            }
            
        })
        
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        if date.contains("-"){
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        }else{
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss z"
        }
        
        let date = dateFormatter.date(from: date)
        
        if date == nil{
            return "--"
        }
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return  dateFormatter.string(from: date!)

    }
    
}


extension Double {
    func getDateStringFromUTC() -> String {
                
        let date = Date(timeIntervalSince1970: self)

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium

        return dateFormatter.string(from: date)
    }
}

