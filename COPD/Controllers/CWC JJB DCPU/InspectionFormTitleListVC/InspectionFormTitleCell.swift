//
//  ListTableViewCell.swift
//  QuestionListApp
//

import UIKit

class InspectionFormTitleCell: UITableViewCell {
    
    // MARK: - Outlet
    @IBOutlet weak var listTextLabel: UILabel!
    @IBOutlet weak var givenAnswerLabel: UILabel!
    @IBOutlet weak var listProgressView: UIProgressView!
    @IBOutlet weak var listBackgroundView: UIView!
    @IBOutlet weak var imgView: UIImageView!
        
    ///configuring cell
    func configureCell(formTitle: String, iconTitle: String, percentageProgress: Float) {
        

        listTextLabel.text =  formTitle
        listProgressView.progress = percentageProgress
        
        // set image for each form
        
        if iconTitle == "" || iconTitle.count == 0{
           
            imgView.image = UIImage.init(named: "inspection_placeholder.png")
        }else{
            imgView.image = UIImage.init(named: "\(iconTitle).png")
        }
        
        if imgView.image == nil{
            
            imgView.image = UIImage.init(named: "inspection_placeholder.png")
        }
        
                
    }
    
}

