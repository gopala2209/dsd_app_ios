//
//  ListViewController.swift
//  QuestionListApp
//

import UIKit

class InspectionFormTitleVC: UIViewController {
        
    // MARK: - Outlets
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnCompleteInspection: UIButton!
    @IBOutlet weak var btnCompleteInspectionViewHeightContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var inspectionFormListArray: [InspectionForm]? = []
    var passingInspectionId = ""
    var uid = ""
    var isCompletedInspection = false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        lblTitle.text = "Inspection Form"
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        // register custom table cell
        registerNib()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // hide/show Completed button
        if isCompletedInspection == true{
            
            // set back button
            self.setCustomBackBarButtonOnNavigationBar()
            
            btnCompleteInspection.isHidden = true
            btnCompleteInspectionViewHeightContraints.constant = 0
        }else{
            
            // set back button to Home
            self.setCustomBackButtonToNavigateToHomeScreen()
            
            btnCompleteInspection.isHidden = false
            btnCompleteInspectionViewHeightContraints.constant = 64
        }
        
        // fetch inspection form for inspection id
        self.getInspectionFormQuestionList(inspectionId: self.passingInspectionId)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    func registerNib(){
        
        let customNib = UINib(nibName: "InspectionFormTitleCell", bundle: Bundle.main)
        listTableView.register(customNib, forCellReuseIdentifier: "InspectionFormTitleCell")
        listTableView.separatorStyle = .none
    }
    
    @IBAction func completeInspectionClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Complete Inspection",
                                      message: "Are you sure you want to complete this inspection?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "YES", style: .default, handler: { (action) in
            
            // call api to complete inspection
            self.completeInspectionFormAPIInvoke()
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "CANCEL",
                                         style: .cancel,
                                         handler: { (action) in
                                            
                                            alert.dismiss(animated: true, completion: nil)
                                         })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
}

// MARK: - Table Delegate and Data Source
extension InspectionFormTitleVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inspectionFormListArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InspectionFormTitleCell", for: indexPath) as! InspectionFormTitleCell
        
        let inspectionForm = self.inspectionFormListArray?[indexPath.row]
        
        var questionProgress = 0
        
        let inspectionField = inspectionForm?.fields
        
        for i in 0..<(inspectionField?.count ?? 0){
                    
            if inspectionField![i].remarks != "" || inspectionField![i].response != ""{
                
                questionProgress = questionProgress + 1
            }
        }
        
        if inspectionField?.count ?? 0 != 0{
            cell.givenAnswerLabel.text = "\(questionProgress)/\(inspectionField!.count)"
            cell.givenAnswerLabel.addCharacterSpacing()
        }else{
            cell.givenAnswerLabel.text = "0/0"
        }
        
        if inspectionField?.count ?? 0 > 0{
            let percentage = (Float(questionProgress)/Float(inspectionField?.count ?? 0))
            
            // configuring cell
            cell.configureCell(formTitle: inspectionForm?.title ?? "Unknown", iconTitle: inspectionForm?.mobileIcon ?? "inspection_placeholder", percentageProgress: Float(percentage))
        }
        else{

            // configuring cell
            cell.configureCell(formTitle: inspectionForm?.title ?? "Unknown", iconTitle: inspectionForm?.mobileIcon ?? "inspection_placeholder", percentageProgress: 0)
        }
        
        
        cell.selectionStyle = .none
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
                        
        // navigate to inspection form question details screen
        let inspectionFormDetailsVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "inspectionformdetailsquestionvc") as! InspectionFormDetailsQuestionVC
        
        inspectionFormDetailsVC.passingInspectionFormListArray = self.inspectionFormListArray
        inspectionFormDetailsVC.passingInspectionId = self.passingInspectionId
        inspectionFormDetailsVC.passingIndex = indexPath.row
        inspectionFormDetailsVC.isCompletedInspection = isCompletedInspection
        inspectionFormDetailsVC.uid = self.uid
        if indexPath.row == self.inspectionFormListArray!.count - 1{
            inspectionFormDetailsVC.isLastInspectionForm = true
         }
        
        self.navigationController?.pushViewController(inspectionFormDetailsVC, animated: true)
    }
}


extension InspectionFormTitleVC{
    func getInspectionFormQuestionList(inspectionId:String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view, title: "Fetching Inspection Form ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getInspectionFormQuestionList(inspectionId: inspectionId) { responseStr in
                        
                        DispatchQueue.main.async {
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            self.inspectionFormListArray?.removeAll()
                            
                            if let count = responseStr?.forms?.count {
                                
                                if(count > 0){
                                    
                                    self.inspectionFormListArray = responseStr?.forms
                                    
                                    self.listTableView.reloadData()
                                    
                                    HUDIndicatorView.shared.dismissHUD()
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                    HUDIndicatorView.shared.dismissHUD()
                                }
                            }
                            else{
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load inspection form data.", message: "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
}


extension InspectionFormTitleVC{
    
    func completeInspectionFormAPIInvoke() {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Completing Inspection ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.CompleteInspectionForm(inspectionId: self.passingInspectionId) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                Toast.show(message: responseStr?.message ?? "Your submission has been sent successfully!", controller: self)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    
                                    // navigate to success inspection screen
                                    let successInspectionVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "successinspectionvc") as! SuccessInspectionVC
                                    
                                    self.navigationController?.pushViewController(successInspectionVC, animated: true)
                                }
                                
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Failed to complete!", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
}
