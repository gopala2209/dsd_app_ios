//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class DcpoChatViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblCCIChatInstitutions: UITableView!
    
    @IBOutlet weak var lblNoChatFound: UILabel!
        
    // MARK: - Properties
    var chatListArray : [Institution]? = []{
        
        didSet{
            self.lblNoChatFound.isHidden = !(self.chatListArray?.count == 0)
        }
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = .white
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
                
        // register custom table cell
        registerNib()
        
        // get cci institution list api call
        self.fetchAllCCIInstitution()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func registerNib(){
        
        let customNib = UINib(nibName: "ChatTableViewCell", bundle: Bundle.main)
        tblCCIChatInstitutions.register(customNib, forCellReuseIdentifier: "ChatTableViewCell")
        tblCCIChatInstitutions.separatorStyle = .none
        
    }
    
}


extension DcpoChatViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return chatListArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        
        //cell.imageInstitution.image = #imageLiteral(resourceName: "cci_placeholder").imageWithColor(color: #colorLiteral(red: 0.3176470588, green: 0.6784313725, blue: 0.8666666667, alpha: 1))
        
        var newAddressText = ""
        
        if indexPath.row < chatListArray?.count ?? 0{
            
            let institition = chatListArray?[indexPath.row]
            
            cell.titleOption.text = institition?.orgName
            cell.titleOption.textColor = UIColor.cciTitleColorTheme
            
            cell.subTitleOption.text = institition?.statusText
            if cell.subTitleOption.text == "Approved"{
                cell.subTitleOption.textColor = UIColor.cciStatusColorTheme
            }else{
                cell.subTitleOption.textColor = UIColor.orangeTheme
            }
                        
            if institition?.talukName?.isEmpty == false && institition?.talukName != ""{
                
                newAddressText = newAddressText + "\(institition?.talukName ?? "")" + ", "
                
            }
            if institition?.districtName?.isEmpty == false && institition?.districtName != ""{
                
                newAddressText = newAddressText + "\(institition?.districtName ?? "")" + ", "
                
            }
            if institition?.stateName?.isEmpty == false && institition?.stateName != ""{
                
                newAddressText = newAddressText + "\(institition?.stateName ?? "")"
            }
        }
        
        cell.superSubTitleOption.text = newAddressText
                
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let institution = chatListArray?[indexPath.row]
        
        if institution?.statusText == "Approved"{
            
            // navigate to chat details screen
            let dcpoChatDetailsViewController = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "dcpochatdetailsviewcontroller") as! DcpoChatDetailsViewController
            dcpoChatDetailsViewController.selectedCCIInstitution = institution
            
            self.navigationController?.pushViewController(dcpoChatDetailsViewController, animated: true)
        }else{
            ServiceModel().showAlert(title: "Alert", message: "Approved & Waiting for Final Approval Institutions Only Allowed to Chat", parentView: self)
        }
        
        
        
    }
    
}

extension DcpoChatViewController{
    
    func fetchAllCCIInstitution(){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading Chat ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getCCIListFilterBy(searchText: "", homeTypeId: 0, startDate: "", endDate: "", admissionHomeTypeId: 0) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            self.chatListArray?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.institutions?.count {
                                
                                if(count > 0){
                                    
                                    self.chatListArray = responseStr?.institutions
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tblCCIChatInstitutions.reloadData()

                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load chat list.", message: "", parentView: self)
                            }

                        }
                    }
                    
                }
            }
            
        }
    }
}
