//
//  RadioQuestionTypeCell.swift
//  COPD
//
//  Created by Anand Praksh on 01/06/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

protocol QuestionTypeCellDelegate {
    func optionSelected(option:String, fieldId: Int, isMandatory: Int, questionField: InspectionField)
    func remarksChanged(remark:String, fieldId: Int, isMandatory: Int, questionField: InspectionField)
}

class RadioQuestionTypeCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var lblQuestionTitle: UILabel!
    
    @IBOutlet weak var lblOption1: UILabel!
    @IBOutlet weak var lblOption2: UILabel!
    
    @IBOutlet weak var selectionButton1: UIButton!
    @IBOutlet weak var selectionButton2: UIButton!
    
    @IBOutlet weak var imgSelectionButton1: UIImageView!
    @IBOutlet weak var imgSelectionButton2: UIImageView!
    
    @IBOutlet weak var imageMandatory: UIImageView!
    
    @IBOutlet weak var textfieldRemarks: UITextField!
    @IBOutlet weak var tfRemarksHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var tfRemarksTopContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate:QuestionTypeCellDelegate?
    var isMandatory: Int?
    var fieldId:Int?
    var maxLength:Int?
    var questionField: InspectionField?
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // initialization code
        textfieldRemarks.delegate = self
        textfieldRemarks.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        textfieldRemarks.keyboardType = .asciiCapable
        
        // set tint color
        imageMandatory.image = #imageLiteral(resourceName: "mandatory").withTintColor(#colorLiteral(red: 0.6862745098, green: 0.1921568627, blue: 0.1725490196, alpha: 1))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - IBActions
    
    @IBAction func optionSelectedAction(_ sender: UIButton) {
        
        if sender.tag == 1{
            
            // passing remarks text
            self.delegate?.optionSelected(option: lblOption1.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        else{
            
            // passing remarks text
            self.delegate?.optionSelected(option: lblOption2.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        
    }
    
}

extension RadioQuestionTypeCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        // passing remarks text
        self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        // passing remarks text
        self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        return range.location < 256
    }
    
}
