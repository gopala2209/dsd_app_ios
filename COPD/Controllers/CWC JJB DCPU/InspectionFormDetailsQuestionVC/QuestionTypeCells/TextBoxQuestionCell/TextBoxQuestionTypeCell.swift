//
//  TextBoxQuestionTypeCell.swift
//  COPD
//
//  Created by Anand Praksh on 01/06/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class TextBoxQuestionTypeCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var textfieldAnswer: UITextField!
    @IBOutlet weak var textfieldAnswerBottomLine: UIView!
    
    @IBOutlet weak var imageMandatory: UIImageView!
    
    @IBOutlet weak var textfieldRemarks: UITextField!
    @IBOutlet weak var tfRemarksHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var tfRemarksTopContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate:QuestionTypeCellDelegate?
    var isMandatory: Int?
    var fieldId:Int?
    var maxLength:Int?
    var questionField: InspectionField?
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // initialization code
        textfieldAnswer.delegate = self
        
        textfieldAnswer.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        textfieldAnswer.keyboardType = .asciiCapable
        
        textfieldRemarks.delegate = self
        textfieldRemarks.keyboardType = .asciiCapable
        
        textfieldRemarks.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        
        // set tint color
        imageMandatory.image = #imageLiteral(resourceName: "mandatory").withTintColor(#colorLiteral(red: 0.6862745098, green: 0.1921568627, blue: 0.1725490196, alpha: 1))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension TextBoxQuestionTypeCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField == textfieldAnswer{
            
            // passing answer text
            self.delegate?.optionSelected(option: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }else{
            
            // passing remarks text
            self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == textfieldAnswer{
            
            // passing answer text
            self.delegate?.optionSelected(option: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }else{
            
            // passing remarks text
            self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
            if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                return false;
            }
        }
        
        if textField == textfieldAnswer{
            
            if questionField?.type == 1{
                
                return range.location < maxLength ?? 50
            }
            else{
                return range.location < maxLength ?? 50
            }
            
        }
        else{
            
            return range.location < 256
        }
    }
    
}
