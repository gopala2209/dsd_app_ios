//
//  DatePickerQuestionTypeCell.swift
//  COPD
//
//  Created by Anand Praksh on 01/06/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class DatePickerQuestionTypeCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var textfieldAnswer: UITextField!
    @IBOutlet weak var textfieldAnswerBottomLine: UIView!
    @IBOutlet weak var imageMandatory: UIImageView!
    
    @IBOutlet weak var textfieldRemarks: UITextField!
    @IBOutlet weak var tfRemarksHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var tfRemarksTopContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate:QuestionTypeCellDelegate?
    var isMandatory: Int?
    var fieldId:Int?
    var maxLength:Int?
    var dateValidation: Int?
    var questionField: InspectionField?
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // initialization code
        textfieldAnswer.delegate = self
        textfieldAnswer.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidBegin)
        textfieldAnswer.keyboardType = .asciiCapable
        
        textfieldRemarks.delegate = self
        textfieldRemarks.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        textfieldRemarks.keyboardType = .asciiCapable
        
        // set tint color
        imageMandatory.image = #imageLiteral(resourceName: "mandatory").withTintColor(#colorLiteral(red: 0.6862745098, green: 0.1921568627, blue: 0.1725490196, alpha: 1))
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func tapDone() {
        if let datePicker = textfieldAnswer.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            
            dateformatter.dateFormat = "dd/MM/yyyy"
            
            self.textfieldAnswer.text = dateformatter.string(from: datePicker.date)
            
            // passing answer text
            self.delegate?.optionSelected(option: self.textfieldAnswer.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        
        textfieldAnswer.resignFirstResponder()
    }
    
}


extension DatePickerQuestionTypeCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField == textfieldRemarks{
            
            // passing remarks text
            self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textfieldAnswer{
       
            textField.setInputViewDatePicker(target: self, selector: #selector(tapDone), inputDateValidation: self.dateValidation ?? 0)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == textfieldRemarks{
            
            // passing remarks text
            self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        if textField == textfieldRemarks{
            return range.location < 256
        }
        else{
            return true
        }
    }
    
}


extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector, inputDateValidation: Int) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        
        if inputDateValidation == 1{
                datePicker.maximumDate = Date() //choose the past date only
        }
        else if inputDateValidation == 2{

            datePicker.minimumDate = Date() // choose the future date only
        }
        else{
            // no date validation check (any date)
        }
        
        // iOS 14 and above
        if #available(iOS 13.4, *) {// Added condition for iOS 14
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 200.0)
            datePicker.date = Date()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}
