//
//  TextAreaQuestionTypeCell.swift
//  COPD
//
//  Created by Anand Praksh on 01/06/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class TextAreaQuestionTypeCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var textviewAnswer: UITextView!
    @IBOutlet weak var imageMandatory: UIImageView!
    
    @IBOutlet weak var textfieldRemarks: UITextField!
    @IBOutlet weak var tfRemarksHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var tfRemarksTopContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate:QuestionTypeCellDelegate?
    var isMandatory: Int?
    var fieldId:Int?
    var maxLength:Int?
    var questionField: InspectionField?
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // initialization code
        textviewAnswer.delegate = self
        textviewAnswer.tintColor = .label
        
        textviewAnswer.text = "Enter"
        textviewAnswer.textColor = UIColor.lightGray
        textviewAnswer.keyboardType = .asciiCapable
        
        textfieldRemarks.delegate = self
        textfieldRemarks.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        textfieldRemarks.keyboardType = .asciiCapable
        
        // set tint color
        imageMandatory.image = #imageLiteral(resourceName: "mandatory").withTintColor(#colorLiteral(red: 0.6862745098, green: 0.1921568627, blue: 0.1725490196, alpha: 1))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension TextAreaQuestionTypeCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        // passing remarks text
        self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        // passing remarks text
        self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
            if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                return false;
            }
        }
        
        return range.location < 256
    }
    
}


extension TextAreaQuestionTypeCell: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter" {
            textView.text = ""
            textView.textColor = .label
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.isFirstResponder {
            if textView.textInputMode?.primaryLanguage == nil || textView.textInputMode?.primaryLanguage == "emoji" {
                return false;
            }
        }
        
        if(textView.text.count > maxLength ?? 256 && range.length == 0){
            
            return false
        }
        
        return true
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Enter"
            textView.textColor = UIColor.lightGray
            
        }else{
            // passing text area text
            self.delegate?.optionSelected(option: textView.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
    }
}
