//
//  SpinnerQuestionTypeCell.swift
//  COPD
//
//  Created by Anand Praksh on 01/06/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit
import DropDown

class SpinnerQuestionTypeCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var textfieldAnswer: UITextField!
    @IBOutlet weak var imgDownArrow: UIImageView!
    @IBOutlet weak var imageMandatory: UIImageView!
    
    @IBOutlet weak var viewTextfieldAnswer: UIView!
    
    @IBOutlet weak var textfieldRemarks: UITextField!
    @IBOutlet weak var tfRemarksHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var tfRemarksTopContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var delegate:QuestionTypeCellDelegate?
    var dropdownOptionsArray = [String]()
    var dropDown = DropDown()
    
    var isMandatory: Int?
    var fieldId:Int?
    var maxLength:Int?
    var questionField: InspectionField?
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // initialization code
        
        self.createTouchForView()
        
        textfieldAnswer.delegate = self
        textfieldAnswer.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidBegin)
        textfieldAnswer.keyboardType = .asciiCapable
        
        textfieldRemarks.delegate = self
        textfieldRemarks.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
        textfieldRemarks.keyboardType = .asciiCapable
        
        // change image tint color
        imgDownArrow.image = imgDownArrow.image?.imageWithColor(color: textfieldRemarks.textColor!)
        
        
        // set tint color
        imageMandatory.image = #imageLiteral(resourceName: "mandatory").withTintColor(#colorLiteral(red: 0.6862745098, green: 0.1921568627, blue: 0.1725490196, alpha: 1))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func createTouchForView(){
        let yearTap = UITapGestureRecognizer(target: self, action: #selector(self.displaySelectYearTap(_:)))
        textfieldAnswer.addGestureRecognizer(yearTap)
    }
    
    @objc func displaySelectYearTap(_ sender: UITapGestureRecognizer? = nil) {
        displayOptionSelectionPopup()
    }

    
    func displayOptionSelectionPopup(){
        DispatchQueue.main.async {
            
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropdownOptionsArray
            self.dropDown.anchorView = self.textfieldAnswer
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .left
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.textfieldAnswer.text = item
                
                // passing answer text
                self.delegate?.optionSelected(option: item, fieldId: self.fieldId ?? 0, isMandatory: self.isMandatory ?? 0, questionField: self.questionField!)
            }
            
            self.dropDown.show()
            
        }
    }
    
}

extension SpinnerQuestionTypeCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        if textField == textfieldRemarks{
            
            // passing remarks text
            self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == textfieldRemarks{
            
            // passing remarks text
            self.delegate?.remarksChanged(remark: textField.text ?? "", fieldId: fieldId ?? 0, isMandatory: isMandatory ?? 0, questionField: questionField!)
        }
        else{
            
            // dismiss keyboard
            textField.resignFirstResponder()
        }
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isFirstResponder {
              if textField.textInputMode?.primaryLanguage == nil || textField.textInputMode?.primaryLanguage == "emoji" {
                   return false;
               }
           }
        
        if textField == textfieldRemarks{
            return range.location < 256
        }
        else{
            return true
        }
    }
    
}
