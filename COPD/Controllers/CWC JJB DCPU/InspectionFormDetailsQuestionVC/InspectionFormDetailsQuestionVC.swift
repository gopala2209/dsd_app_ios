//
//  ListViewController.swift
//  QuestionListApp
//

import UIKit

struct FormAnswerModel: Decodable {
    
    /*var field_id: Int! = 0
    var is_mandatory: Int! = 0
    var response: String! = ""
    var remarks: String! = ""
    var dateValidation: String! = ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = ["field_id":self.field_id as Any, "is_mandatory":self.is_mandatory as Any,"response":self.response as Any, "remarks":self.remarks as Any]
        return dic
    }*/
    
    var field_id: Int! = 0
    var is_mandatory: Int! = 0
    var response: String! = ""
    var remarks: String! = ""
    var dateValidation: String! = ""
    var id: String! = ""
    var is_remarks_required: String! = ""
    var max_length: String! = ""
    var type: String! = ""
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = ["field_id":self.field_id as Any, "is_mandatory":self.is_mandatory as Any,"response":self.response as Any, "remarks":self.remarks as Any, "dateValidation":self.dateValidation as Any, "id":self.id as Any,"is_remarks_required":self.is_remarks_required as Any, "max_length":self.max_length as Any,"type":self.type as Any]
        return dic
    }
}


class InspectionFormDetailsQuestionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblQuestionList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnSaveInspection: UIButton!
    @IBOutlet weak var btnCompleteInspectionViewHeightContraints: NSLayoutConstraint!
    
    // MARK: - Properties
    var inspectionQuestionListArray: [InspectionField]? = []
    
    var inspectionFormAnswerArray: [FormAnswerModel]? = []
    
    var passingInspectionFormListArray: [InspectionForm]? = []
    var passingIndex: Int?
    
    var selectedInspectionForm : InspectionForm?
    var passingInspectionId = ""
    var uid = ""
    var isLastInspectionForm = false
    
    var isCompletedInspection = false
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialUI()
        
        // set back button with alert
        self.setCustomBackBarButtonWithAlertOnNavigationBar()
        
        // show the navigation bar
        navigationController?.navigationBar.barTintColor = .white
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // hide/show Completed button
        if isCompletedInspection == true{
            btnSaveInspection.isHidden = true
            btnCompleteInspectionViewHeightContraints.constant = 0
        }else{
            btnSaveInspection.isHidden = false
            btnCompleteInspectionViewHeightContraints.constant = 50
            
            if isLastInspectionForm == true{
                
                btnSaveInspection.setTitle("SUBMIT", for: .normal)
            }else{
                btnSaveInspection.setTitle("SAVE & NEXT", for: .normal)
            }
        }
        
        
        // set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    func setCustomBackBarButtonWithAlertOnNavigationBar()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "back_icon")?.imageWithColor(color: UIColor.blueTheme), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(backButtonPressedWithAlert), for: UIControl.Event.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backButtonPressedWithAlert()
    {
        if isCompletedInspection == true{
            
            // dismiss
            self.navigationController?.popViewController(animated: true)
        }
        else{
            
            let postData = self.getAnswerModelData()
            
            if postData.count == 0{
                
                // dismiss
                self.navigationController?.popViewController(animated: true)
            }else{
                //Device not saved– data can be lost, would you like to continue?
                let alert = UIAlertController(title: "Alert",
                                              message: "Are you sure. Do you want to go back?",
                                              preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    
                    let postData = self.getAnswerModelData()
                    
                    // call api to save inspection
                    self.saveIndividualInspectionForm(formId: self.selectedInspectionForm?.formID ?? 0, formAnswerModel: postData, isBackButtonPressed: true)
                    
                    alert.dismiss(animated: true, completion: nil)
                })
                
                let cancelAction = UIAlertAction(title: "No",
                                                 style: .cancel,
                                                 handler: { (action) in
                                                                                                        
                                                    alert.dismiss(animated: true, completion: nil)
                                                 })
                
                alert.addAction(cancelAction)
                alert.addAction(okAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    // method to register cell
    func registerCell(tableView: UITableView) {
        
        tableView.register(TextBoxQuestionTypeCell.nib, forCellReuseIdentifier: TextBoxQuestionTypeCell.reuseIdentifier)
        tableView.register(RadioQuestionTypeCell.nib, forCellReuseIdentifier: RadioQuestionTypeCell.reuseIdentifier)
        tableView.register(DatePickerQuestionTypeCell.nib, forCellReuseIdentifier: DatePickerQuestionTypeCell.reuseIdentifier)
        tableView.register(TextAreaQuestionTypeCell.nib, forCellReuseIdentifier: TextAreaQuestionTypeCell.reuseIdentifier)
        tableView.register(SpinnerQuestionTypeCell.nib, forCellReuseIdentifier: SpinnerQuestionTypeCell.reuseIdentifier)
        
        // for completed
        tableView.register(CompletedInspectionQuestionTypeCell.nib, forCellReuseIdentifier: CompletedInspectionQuestionTypeCell.reuseIdentifier)
    }
    
    func getAnswerModelData() -> [[String: Any]]{
        var result: [[String: Any]] = []
        if inspectionFormAnswerArray?.count ?? 0 > 0{
            for i in inspectionFormAnswerArray!{
                var data: FormAnswerModel! = FormAnswerModel()
                data.dateValidation = i.dateValidation
                data.field_id = i.field_id
                data.id = i.id
                data.is_mandatory = i.is_mandatory
                data.is_remarks_required = i.is_remarks_required
                data.max_length = i.max_length
                data.remarks = i.remarks
                data.response = i.response
                data.type = i.type
                
                let addedData = data.map { $0.convertToDictionary() }!
                result.append(addedData)
            }
            return result
        }else{
            return []
        }
    }
    
    @IBAction func generalInfo(_ sender: Any) {
        let generalInfo = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "CCIGeneralInfoViewController") as! CCIGeneralInfoViewController
        generalInfo.id = self.uid
        self.navigationController?.pushViewController(generalInfo, animated: true)
    }
    
    @IBAction func saveInspectionClicked(_ sender: Any) {
        
        let postData = self.getAnswerModelData()
        
        if postData.count == 0{
            
            if inspectionQuestionListArray?.count ?? 0 == 0{
                
                return
            }
            else{
            
                ServiceModel().showAlert(title: "You have not yet filled this form, please fill the form before submit.", message:"" , parentView: self)
                return
            }
        }
        
        var prefilledCount = 0
        var missedMandatoryCount = 0
        for i in 0..<(inspectionQuestionListArray?.count ?? 0){
            let form = inspectionQuestionListArray?[i]

            if form?.isMandatory == 1{
                missedMandatoryCount = missedMandatoryCount + 1
            }
        }
        
        for i in 0..<(inspectionQuestionListArray?.count ?? 0){
            
            let form = inspectionQuestionListArray?[i]
            
            
            if form?.response != "" || form?.remarks != ""{
                
                var exist = false
                
                for j in 0..<(inspectionFormAnswerArray?.count ?? 0){
                    
                    let answerArray = inspectionFormAnswerArray?[j]
                    
                    if answerArray?.field_id == form?.fieldID{
                        
                        exist = true

                    }
                }
                
                if !exist{
                    prefilledCount = prefilledCount + 1
                    
                    if form?.isMandatory == 1{
                        missedMandatoryCount = missedMandatoryCount - 1
                    }
                }
            }
            
        }
                
        
        for j in 0..<(inspectionFormAnswerArray?.count ?? 0){

            let answerArray = inspectionFormAnswerArray?[j]

            if answerArray?.is_mandatory == 1{
                missedMandatoryCount = missedMandatoryCount - 1
            }

        }
        
        print("Prefilled count = \(prefilledCount), missed mandatory count = \(missedMandatoryCount)")

        if missedMandatoryCount > 0{
                        
            // error alert to fill mandatory questions
            if missedMandatoryCount == 1{
                
                ServiceModel().showAlert(title: "Alert", message: "You have missed \(missedMandatoryCount) mandatory question. All mandatory questions must be filled before submission.", parentView: self)
            }else{
                
                ServiceModel().showAlert(title: "Alert", message: "You have missed \(missedMandatoryCount) mandatory questions. All mandatory questions must be filled before submission.", parentView: self)
            }
            

            return
        }
        
        let totalCount = (inspectionFormAnswerArray?.count ?? 0) + prefilledCount
        
        if (totalCount - (inspectionQuestionListArray?.count ?? 0)) < 0{
            
            let missedCount = ((inspectionQuestionListArray?.count ?? 0) - ((inspectionFormAnswerArray?.count ?? 0) + prefilledCount))
            
            // error alert
            self.showErrorAlertForIncompleteFormSubmission(missedQuestionCount: missedCount)
            
            return
        }
        
        // call api to save inspection
        self.saveIndividualInspectionForm(formId: selectedInspectionForm?.formID ?? 0, formAnswerModel: postData, isBackButtonPressed: false)
        
    }
    
    func showErrorAlertForIncompleteFormSubmission(missedQuestionCount: Int){
        
        var questionText = ""
        
        if missedQuestionCount == 1{
            questionText = "question"
        }else{
            questionText = "questions"
        }
        
        // error alert
        let alert = UIAlertController(title: "Alert",
                                      message: "You have missed \(missedQuestionCount) \(questionText). Do you want to submit this form?",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "YES", style: .default, handler: { (action) in
            
            let postData = self.getAnswerModelData()
            
            // call api to save inspection
            self.saveIndividualInspectionForm(formId: self.selectedInspectionForm?.formID ?? 0, formAnswerModel: postData, isBackButtonPressed: false)
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "NO",
                                         style: .cancel,
                                         handler: { (action) in
                                            
                                            // no action
                                            
                                            alert.dismiss(animated: true, completion: nil)
                                         })
        
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}


// MARK: - Private Methods
extension InspectionFormDetailsQuestionVC {
    
    ///configuring initial view
    private func setInitialUI() {
        inspectionFormAnswerArray?.removeAll()
        // registering cell
        self.registerCell(tableView: tblQuestionList)
        
        selectedInspectionForm = passingInspectionFormListArray?[passingIndex!]
        
        lblTitle.text = selectedInspectionForm?.title ?? "Inspection Form"
        
        inspectionQuestionListArray = selectedInspectionForm?.fields ?? []
        
        for i in 0..<(selectedInspectionForm!.fields?.count ?? 0){
            if selectedInspectionForm!.fields?[i].response != "" || selectedInspectionForm!.fields?[i].remarks != ""{
                //let formAnswerModel = FormAnswerModel.init(field_id: selectedInspectionForm!.fields?[i].fieldID, response: selectedInspectionForm!.fields?[i].response, remarks: selectedInspectionForm!.fields?[i].remarks)
                
                let formAnswerModel = FormAnswerModel.init(field_id: selectedInspectionForm!.fields?[i].fieldID, is_mandatory: Int(selectedInspectionForm!.fields?[i].isMandatory ?? 0), response: selectedInspectionForm!.fields?[i].response, remarks: selectedInspectionForm!.fields?[i].remarks, dateValidation: "\(selectedInspectionForm!.fields?[i].dateValidation ?? 0)", id: "\(selectedInspectionForm!.fields?[i].id ?? 0)", is_remarks_required: "\(selectedInspectionForm!.fields?[i].isRemarksRequired ?? false)", max_length: "\(selectedInspectionForm!.fields?[i].maxLength ?? 0)", type: "\(selectedInspectionForm!.fields?[i].type ?? 0)")
                
                inspectionFormAnswerArray?.append(formAnswerModel)

            }
            
        }

        
        
        // reload table view
        tblQuestionList.reloadData()
    }
}

// MARK: - Table Delegate and Data Source
extension InspectionFormDetailsQuestionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inspectionQuestionListArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // getting question and its type to display cell accordingly
        let inspectionQuestion = self.inspectionQuestionListArray?[indexPath.row]
        
        if isCompletedInspection == false{
            
            // configuring multiple custom cells
            if inspectionQuestion?.type == 1 || inspectionQuestion?.type == 6{// text box
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextBoxQuestionTypeCell.reuseIdentifier) as! TextBoxQuestionTypeCell
                
                cell.lblQuestionTitle.text = "\(indexPath.row + 1)" + ".  " + "\(inspectionQuestion?.name ?? "Unknown")"
                
                //text field answer
                cell.textfieldAnswerBottomLine.isHidden = false
                
                cell.textfieldAnswer.setLeftPaddingPoints(10) //set left padding
                cell.textfieldAnswer.setRightPaddingPoints(10) //set right padding
                
                cell.textfieldAnswer.clearButtonMode = .always
                
                //remarks
                cell.textfieldRemarks.setLeftPaddingPoints(10)//set left padding
                cell.textfieldRemarks.setRightPaddingPoints(10) //set right padding
                
                // for selection
                var alreadyExist = -1
                
                for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                    
                    let form = inspectionFormAnswerArray?[i]
                    
                    if form?.field_id == inspectionQuestion?.fieldID{
                        
                        alreadyExist = i
                    }
                }
                
                if alreadyExist == -1{
                    
                    cell.textfieldAnswer.text = inspectionQuestion?.response ?? ""
                    
                    if inspectionQuestion?.isRemarksRequired == true{
                        
                        cell.textfieldRemarks.text = inspectionQuestion?.remarks
                        cell.textfieldRemarks.isHidden = false
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        cell.textfieldRemarks.text = ""
                        cell.textfieldRemarks.isHidden = true
                        cell.tfRemarksHeightContraints.constant = 0
                        cell.tfRemarksTopContraints.constant = 0
                    }
                    
                }
                else{
                    
                    let selectedForm = inspectionFormAnswerArray?[alreadyExist]
                    
                    cell.textfieldAnswer.text = selectedForm?.response ?? ""
                    
                    if selectedForm?.remarks != ""{
                        
                        cell.textfieldRemarks.isHidden = false
                        cell.textfieldRemarks.text = selectedForm?.remarks ?? ""
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        if inspectionQuestion?.isRemarksRequired == true{
                            
                            cell.textfieldRemarks.text = inspectionQuestion?.remarks
                            cell.textfieldRemarks.isHidden = false
                            
                            cell.tfRemarksHeightContraints.constant = 44
                            cell.tfRemarksTopContraints.constant = 20
                            
                        }else{
                            
                            cell.textfieldRemarks.text = ""
                            cell.textfieldRemarks.isHidden = true
                            cell.tfRemarksHeightContraints.constant = 0
                            cell.tfRemarksTopContraints.constant = 0
                        }
                        
                    }
                }
                
                // show/hide mandatory sign
                cell.imageMandatory.isHidden = !(inspectionQuestion?.isMandatory == 1)
                
                if inspectionQuestion?.type == 1{
                    
                    cell.textfieldAnswer.keyboardType = .namePhonePad
                }else{
                    cell.textfieldAnswer.keyboardType = .numberPad
                }
                
                cell.selectionStyle = .none
                
                cell.fieldId = inspectionQuestion?.fieldID
                cell.isMandatory = inspectionQuestion?.isMandatory
                cell.maxLength = inspectionQuestion?.maxLength
                cell.questionField = inspectionQuestion
                
                cell.delegate = self
                
                return cell
            }
            else if inspectionQuestion?.type == 2{// radio
                
                let cell = tableView.dequeueReusableCell(withIdentifier: RadioQuestionTypeCell.reuseIdentifier) as! RadioQuestionTypeCell
                
                cell.lblQuestionTitle.text = "\(indexPath.row + 1)" + ".  " + "\(inspectionQuestion?.name ?? "Unknown")"
                
                //remarks
                cell.textfieldRemarks.setLeftPaddingPoints(10)//set left padding
                cell.textfieldRemarks.setRightPaddingPoints(10) //set right padding
                
                // radio option title
                if inspectionQuestion?.options?.count == 2{
                    cell.lblOption1.text = inspectionQuestion?.options?[0]
                    cell.lblOption2.text = inspectionQuestion?.options?[1]
                    
                    // for selection
                    var alreadyExist = -1
                    
                    for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                        
                        let form = inspectionFormAnswerArray?[i]
                        
                        if form?.field_id == inspectionQuestion?.fieldID{
                            
                            alreadyExist = i
                        }
                    }
                    
                    if alreadyExist == -1{
                        
                        if inspectionQuestion?.response != ""{
                            
                            if inspectionQuestion?.response == cell.lblOption1.text{

                                cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_checked")
                                cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                            }else{
                                cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                                cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_checked")
                            }
                        }
                        else{
                            
                            cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                            cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                        }
                        
                        if inspectionQuestion?.isRemarksRequired == true{
                            
                            cell.textfieldRemarks.text = inspectionQuestion?.remarks
                            cell.textfieldRemarks.isHidden = false
                            
                            cell.tfRemarksHeightContraints.constant = 44
                            cell.tfRemarksTopContraints.constant = 20
                            
                        }else{
                            
                            cell.textfieldRemarks.text = ""
                            cell.textfieldRemarks.isHidden = true
                            cell.tfRemarksHeightContraints.constant = 0
                            cell.tfRemarksTopContraints.constant = 0
                        }
                        
                    }
                    else{
                        
                        let selectedForm = inspectionFormAnswerArray?[alreadyExist]
                        
                        if selectedForm?.response != ""{
                            if selectedForm?.response == cell.lblOption1.text{

                                cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_checked")
                                cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                            }else{

                                cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                                cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_checked")
                            }
                        }else{
                            
                            if inspectionQuestion?.response != ""{
                                
                                if inspectionQuestion?.response == cell.lblOption1.text{

                                    cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_checked")
                                    cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                                }else{

                                    cell.imgSelectionButton1.image = #imageLiteral(resourceName: "radio_button_unchecked").withTintColor(.tertiarySystemFill)
                                    cell.imgSelectionButton2.image = #imageLiteral(resourceName: "radio_button_checked")
                                }
                                
                            }
                            
                        }
                        
                        if selectedForm?.remarks != ""{
                            
                            cell.textfieldRemarks.isHidden = false
                            cell.textfieldRemarks.text = selectedForm?.remarks ?? ""
                            
                            cell.tfRemarksHeightContraints.constant = 44
                            cell.tfRemarksTopContraints.constant = 20
                            
                        }else{
                            
                            if inspectionQuestion?.isRemarksRequired == true{
                                
                                cell.textfieldRemarks.text = inspectionQuestion?.remarks
                                cell.textfieldRemarks.isHidden = false
                                
                                cell.tfRemarksHeightContraints.constant = 44
                                cell.tfRemarksTopContraints.constant = 20
                                
                            }else{
                                
                                cell.textfieldRemarks.text = ""
                                cell.textfieldRemarks.isHidden = true
                                cell.tfRemarksHeightContraints.constant = 0
                                cell.tfRemarksTopContraints.constant = 0
                            }
                        }
                    }
                }
                
                // show/hide mandatory sign
                cell.imageMandatory.isHidden = !(inspectionQuestion?.isMandatory == 1)
                
                cell.selectionStyle = .none
                
                cell.fieldId = inspectionQuestion?.fieldID
                cell.isMandatory = inspectionQuestion?.isMandatory
                cell.maxLength = inspectionQuestion?.maxLength
                cell.questionField = inspectionQuestion
                
                cell.delegate = self
                
                return cell
            }
            else if inspectionQuestion?.type == 3{// date selection
                
                let cell = tableView.dequeueReusableCell(withIdentifier: DatePickerQuestionTypeCell.reuseIdentifier) as! DatePickerQuestionTypeCell
                
                cell.lblQuestionTitle.text = "\(indexPath.row + 1)" + ".  " + "\(inspectionQuestion?.name ?? "Unknown")"
                
                //text field answer
                cell.textfieldAnswerBottomLine.isHidden = false
                
                cell.textfieldAnswer.setLeftPaddingPoints(10)//set padding
                
                //remarks
                cell.textfieldRemarks.setLeftPaddingPoints(10)//set left padding
                cell.textfieldRemarks.setRightPaddingPoints(10) //set right padding
                
                // for selection
                var alreadyExist = -1
                
                for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                    
                    let form = inspectionFormAnswerArray?[i]
                    
                    if form?.field_id == inspectionQuestion?.fieldID{
                        
                        alreadyExist = i
                    }
                }
                
                if alreadyExist == -1{
                    
                    cell.textfieldAnswer.text = inspectionQuestion?.response ?? ""
                    
                    if inspectionQuestion?.isRemarksRequired == true{
                        
                        cell.textfieldRemarks.text = inspectionQuestion?.remarks
                        cell.textfieldRemarks.isHidden = false
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        cell.textfieldRemarks.text = ""
                        cell.textfieldRemarks.isHidden = true
                        cell.tfRemarksHeightContraints.constant = 0
                        cell.tfRemarksTopContraints.constant = 0
                    }
                    
                }
                else{
                    
                    let selectedForm = inspectionFormAnswerArray?[alreadyExist]
                    
                    cell.textfieldAnswer.text = selectedForm?.response ?? ""
                    
                    if selectedForm?.remarks != ""{
                        
                        cell.textfieldRemarks.isHidden = false
                        cell.textfieldRemarks.text = selectedForm?.remarks ?? ""
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        if inspectionQuestion?.isRemarksRequired == true{
                            
                            cell.textfieldRemarks.text = inspectionQuestion?.remarks
                            cell.textfieldRemarks.isHidden = false
                            
                            cell.tfRemarksHeightContraints.constant = 44
                            cell.tfRemarksTopContraints.constant = 20
                            
                        }else{
                            
                            cell.textfieldRemarks.text = ""
                            cell.textfieldRemarks.isHidden = true
                            cell.tfRemarksHeightContraints.constant = 0
                            cell.tfRemarksTopContraints.constant = 0
                        }
                        
                    }
                }
                
                // show/hide mandatory sign
                cell.imageMandatory.isHidden = !(inspectionQuestion?.isMandatory == 1)
                
                cell.selectionStyle = .none
                
                cell.fieldId = inspectionQuestion?.fieldID
                cell.isMandatory = inspectionQuestion?.isMandatory
                cell.maxLength = inspectionQuestion?.maxLength
                cell.dateValidation = inspectionQuestion?.dateValidation
                cell.questionField = inspectionQuestion
                
                cell.delegate = self
                
                return cell
            }
            else if inspectionQuestion?.type == 4{// text area
                
                let cell = tableView.dequeueReusableCell(withIdentifier: TextAreaQuestionTypeCell.reuseIdentifier) as! TextAreaQuestionTypeCell
                
                cell.lblQuestionTitle.text = "\(indexPath.row + 1)" + ".  " + "\(inspectionQuestion?.name ?? "Unknown")"
                
                //textview padding
                cell.textviewAnswer.layer.borderWidth = 1
                cell.textviewAnswer.layer.borderColor = UIColor.lightGray.cgColor
                cell.textviewAnswer.layer.cornerRadius = 5
                
                cell.textviewAnswer.textContainerInset = .init(top: 7, left: 10, bottom: 0, right: 10)
                cell.textviewAnswer.textContainer.lineFragmentPadding = 0
                
                //remarks
                cell.textfieldRemarks.setLeftPaddingPoints(10)//set left padding
                cell.textfieldRemarks.setRightPaddingPoints(10) //set right padding
                
                // for selection
                var alreadyExist = -1
                
                for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                    
                    let form = inspectionFormAnswerArray?[i]
                    
                    if form?.field_id == inspectionQuestion?.fieldID{
                        
                        alreadyExist = i
                    }
                }
                
                if alreadyExist == -1{
                    
                    cell.textviewAnswer.text = inspectionQuestion?.response ?? ""
                    
                    if cell.textviewAnswer.text == "" {
                        
                        cell.textviewAnswer.text = "Enter"
                        cell.textviewAnswer.textColor = .lightGray
                        
                    }else{
                        cell.textviewAnswer.textColor = .label
                    }
                    
                    if inspectionQuestion?.isRemarksRequired == true{
                        
                        cell.textfieldRemarks.text = inspectionQuestion?.remarks
                        cell.textfieldRemarks.isHidden = false
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        cell.textfieldRemarks.text = ""
                        cell.textfieldRemarks.isHidden = true
                        cell.tfRemarksHeightContraints.constant = 0
                        cell.tfRemarksTopContraints.constant = 0
                    }
                    
                }
                else{
                    
                    let selectedForm = inspectionFormAnswerArray?[alreadyExist]
                    
                    cell.textviewAnswer.text = selectedForm?.response ?? ""
                    
                    if cell.textviewAnswer.text == "" {
                        
                        cell.textviewAnswer.text = "Enter"
                        cell.textviewAnswer.textColor = .lightGray
                        
                    }else{
                        cell.textviewAnswer.textColor = .label
                    }
                    
                    if selectedForm?.remarks != ""{
                        
                        cell.textfieldRemarks.isHidden = false
                        cell.textfieldRemarks.text = selectedForm?.remarks ?? ""
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        if inspectionQuestion?.isRemarksRequired == true{
                            
                            cell.textfieldRemarks.text = inspectionQuestion?.remarks
                            cell.textfieldRemarks.isHidden = false
                            
                            cell.tfRemarksHeightContraints.constant = 44
                            cell.tfRemarksTopContraints.constant = 20
                            
                        }else{
                            
                            cell.textfieldRemarks.text = ""
                            cell.textfieldRemarks.isHidden = true
                            cell.tfRemarksHeightContraints.constant = 0
                            cell.tfRemarksTopContraints.constant = 0
                        }
                        
                    }
                }
                
                // show/hide mandatory sign
                cell.imageMandatory.isHidden = !(inspectionQuestion?.isMandatory == 1)
                
                cell.selectionStyle = .none
                
                cell.fieldId = inspectionQuestion?.fieldID
                cell.isMandatory = inspectionQuestion?.isMandatory
                cell.maxLength = inspectionQuestion?.maxLength
                cell.questionField = inspectionQuestion
                
                cell.delegate = self
                
                
                return cell
            }
            else{// spinner dropdown
                
                let cell = tableView.dequeueReusableCell(withIdentifier: SpinnerQuestionTypeCell.reuseIdentifier) as! SpinnerQuestionTypeCell
                
                cell.lblQuestionTitle.text = "\(indexPath.row + 1)" + ".  " + "\(inspectionQuestion?.name ?? "Unknown")"
                
                //text field answer
                cell.textfieldAnswer.setLeftPaddingPoints(10)//set padding
                
                //remarks
                cell.textfieldRemarks.setLeftPaddingPoints(10)//set left padding
                cell.textfieldRemarks.setRightPaddingPoints(10) //set right padding
                
                // for selection
                var alreadyExist = -1
                
                for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                    
                    let form = inspectionFormAnswerArray?[i]
                    
                    if form?.field_id == inspectionQuestion?.fieldID{
                        
                        alreadyExist = i
                    }
                }
                
                if alreadyExist == -1{
                    
                    cell.textfieldAnswer.text = inspectionQuestion?.response ?? ""
                    
                    if inspectionQuestion?.isRemarksRequired == true{
                        
                        cell.textfieldRemarks.text = inspectionQuestion?.remarks
                        cell.textfieldRemarks.isHidden = false
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        cell.textfieldRemarks.text = ""
                        cell.textfieldRemarks.isHidden = true
                        cell.tfRemarksHeightContraints.constant = 0
                        cell.tfRemarksTopContraints.constant = 0
                    }
                    
                }
                else{
                    
                    let selectedForm = inspectionFormAnswerArray?[alreadyExist]
                    
                    cell.textfieldAnswer.text = selectedForm?.response ?? ""
                    
                    if selectedForm?.remarks != ""{
                        
                        cell.textfieldRemarks.isHidden = false
                        cell.textfieldRemarks.text = selectedForm?.remarks ?? ""
                        
                        cell.tfRemarksHeightContraints.constant = 44
                        cell.tfRemarksTopContraints.constant = 20
                        
                    }else{
                        
                        if inspectionQuestion?.isRemarksRequired == true{
                            
                            cell.textfieldRemarks.text = inspectionQuestion?.remarks
                            cell.textfieldRemarks.isHidden = false
                            
                            cell.tfRemarksHeightContraints.constant = 44
                            cell.tfRemarksTopContraints.constant = 20
                            
                        }else{
                            
                            cell.textfieldRemarks.text = ""
                            cell.textfieldRemarks.isHidden = true
                            cell.tfRemarksHeightContraints.constant = 0
                            cell.tfRemarksTopContraints.constant = 0
                        }
                    }
                }
                
                // show/hide mandatory sign
                cell.imageMandatory.isHidden = !(inspectionQuestion?.isMandatory == 1)
                
                cell.selectionStyle = .none
                cell.fieldId = inspectionQuestion?.fieldID
                cell.isMandatory = inspectionQuestion?.isMandatory
                cell.maxLength = inspectionQuestion?.maxLength
                cell.questionField = inspectionQuestion
                
                cell.dropdownOptionsArray = inspectionQuestion?.options ?? []
                
                cell.delegate = self
                
                cell.imgDownArrow.isHidden = false
                cell.viewTextfieldAnswer.layer.borderWidth = 1
                cell.viewTextfieldAnswer.layer.borderColor = UIColor.lightGray.cgColor
                cell.viewTextfieldAnswer.layer.cornerRadius = 5
                
                cell.viewTextfieldAnswer.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
                
                return cell
            }
        }
        else{
            
            // completed inspection details
            let cell = tableView.dequeueReusableCell(withIdentifier: CompletedInspectionQuestionTypeCell.reuseIdentifier) as! CompletedInspectionQuestionTypeCell
            
            cell.lblQuestionTitle.text = "\(indexPath.row + 1)" + ".  " + "\(inspectionQuestion?.name ?? "Unknown")"
            
            cell.lblAnswer.text = inspectionQuestion?.response ?? "No data Entry"
            
            if cell.lblAnswer.text == ""{
                cell.lblAnswer.text = "No data Entry"
            }
            
            if inspectionQuestion?.isRemarksRequired == true{
                                
                if inspectionQuestion?.remarks == ""{
                    cell.lblRemarks.text = "No data Entry"
                    cell.lblRemarks.isHidden = true
                    cell.lblRemarksTopContraints.constant = 0
                    cell.lblRemarksBottomContraints.constant = 0
                }else{
                    
                    cell.lblRemarks.text = inspectionQuestion?.remarks ?? "No data Entry"
                    cell.lblRemarks.isHidden = false
                    
                    cell.lblRemarksTopContraints.constant = 20
                    cell.lblRemarksBottomContraints.constant = 15
                }
            }else{
                cell.lblRemarks.text = "No data Entry"
                cell.lblRemarks.isHidden = true
                cell.lblRemarksTopContraints.constant = 0
                cell.lblRemarksBottomContraints.constant = 0
            }
            
            cell.isUserInteractionEnabled = false
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
}


extension InspectionFormDetailsQuestionVC{
    
    func saveIndividualInspectionForm(formId: Int, formAnswerModel: [[String: Any]], isBackButtonPressed: Bool) {
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Saving Inspection ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.submitIndividualInspectionForm(inspectionId: self.passingInspectionId, formId: formId, formAnswerModel: formAnswerModel) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            let status = responseStr?.status
                            
                            if(status == 1){
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                if isBackButtonPressed == false{
                                    
                                    if self.btnSaveInspection.title(for: .normal) == "SAVE & NEXT"{
                                        
                                        // remove answer array
                                        self.inspectionFormAnswerArray?.removeAll()
                                        
                                        // increase index
                                        self.passingIndex = self.passingIndex! + 1
                                        
                                        self.selectedInspectionForm = self.passingInspectionFormListArray?[self.passingIndex!]
                                        
                                        self.lblTitle.text = self.selectedInspectionForm?.title ?? "Inspection Form"
                                        
                                        self.inspectionQuestionListArray = self.selectedInspectionForm?.fields ?? []
                                        
                                        // reload table view
                                        self.tblQuestionList.reloadData()
                                        
                                        self.scrollTableToFirstRow()
                                        
                                        //customize button title
                                        if self.passingIndex == self.passingInspectionFormListArray!.count - 1{
                                            
                                            self.btnSaveInspection.setTitle("SUBMIT", for: .normal)
                                        }
                                        
                                        return
                                        
                                    }
                                    else{ //last form
                                        
                                        Toast.show(message: responseStr?.message ?? "Your submission has been saved successfully!", controller: self)
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                            
                                            // dismiss
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                    
                                }
                                else{
                                    
                                    Toast.show(message: "Your changes has been saved successfully!", controller: self)
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                        
                                        // dismiss
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                }
                                
                            }
                            else{
                                let message = responseStr?.message
                                servicemodel.showAlert(title: "Failed to save!", message: message ?? "", parentView: self)
                                
                                HUDIndicatorView.shared.dismissHUD()
                            }
                        }
                    }
                }
                
            }catch {
                
                servicemodel.handleError(error: error, parentView: self)
            }
        }else{
            servicemodel.showAlert(title: "Alert", message:"Please check your network connection!", parentView: self)
        }
    }
    
    
    func scrollTableToFirstRow() {
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.tblQuestionList.scrollToRow(at: indexPath, at: .top, animated: false)
    }
    
    
}

extension InspectionFormDetailsQuestionVC: QuestionTypeCellDelegate{
    
    func optionSelected(option:String, fieldId: Int, isMandatory: Int, questionField: InspectionField){
        
        if inspectionFormAnswerArray?.count ?? 0 == 0{
            
            let formAnswerModel = FormAnswerModel.init(field_id: fieldId, is_mandatory:isMandatory, response: option, remarks: "", dateValidation: "\(questionField.dateValidation ?? 0)", id: "\(questionField.id ?? 0)", is_remarks_required: "\(questionField.isRemarksRequired ?? false)", max_length: "\(questionField.maxLength ?? 0)", type: "\(questionField.type ?? 0)")
            
            if option.trimmingCharacters(in: .whitespaces) != ""{
                
                inspectionFormAnswerArray?.append(formAnswerModel)
            }
            
        }
        else{
            
            var alreadyExist = -1
            
            for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                
                let selectedForm = inspectionFormAnswerArray?[i]
                
                if selectedForm?.field_id == fieldId{
                    
                    alreadyExist = i
                }
            }
            
            if alreadyExist == -1{
                
                // append new entry
                let formAnswerModel = FormAnswerModel.init(field_id: fieldId, is_mandatory:isMandatory, response: option, remarks: "", dateValidation: "\(questionField.dateValidation ?? 0)", id: "\(questionField.id ?? 0)", is_remarks_required: "\(questionField.isRemarksRequired ?? false)", max_length: "\(questionField.maxLength ?? 0)", type: "\(questionField.type ?? 0)")
                
                if option.trimmingCharacters(in: .whitespaces) != ""{
                    
                    inspectionFormAnswerArray?.append(formAnswerModel)
                }
                
            }
            else{
                // update existing entry
                let selectedForm = inspectionFormAnswerArray?[alreadyExist]
                
                let updatedformAnswerModel = FormAnswerModel.init(field_id: selectedForm?.field_id, is_mandatory:selectedForm?.is_mandatory, response: option.trimmingCharacters(in: .whitespaces), remarks: selectedForm?.remarks, dateValidation: "\(questionField.dateValidation ?? 0)", id: "\(questionField.id ?? 0)", is_remarks_required: "\(questionField.isRemarksRequired ?? false)", max_length: "\(questionField.maxLength ?? 0)", type: "\(questionField.type ?? 0)")
                
                inspectionFormAnswerArray?.remove(at: alreadyExist)
                
                inspectionFormAnswerArray?.append(updatedformAnswerModel)
                
            }
            
        }
        
        // reload table view
        self.tblQuestionList.reloadData()
    }
    
    
    func remarksChanged(remark:String, fieldId: Int, isMandatory: Int, questionField: InspectionField){
        
        if inspectionFormAnswerArray?.count ?? 0 == 0{
            
            let formAnswerModel = FormAnswerModel.init(field_id: fieldId, is_mandatory:isMandatory, response: "", remarks: remark, dateValidation: "\(questionField.dateValidation ?? 0)", id: "\(questionField.id ?? 0)", is_remarks_required: "\(questionField.isRemarksRequired ?? false)", max_length: "\(questionField.maxLength ?? 0)", type: "\(questionField.type ?? 0)")
            
            if remark.trimmingCharacters(in: .whitespaces) != ""{
                
                inspectionFormAnswerArray?.append(formAnswerModel)
            }
            
        }
        else{
            
            var alreadyExist = -1
            
            for i in 0..<(inspectionFormAnswerArray?.count ?? 0){
                
                let selectedForm = inspectionFormAnswerArray?[i]
                
                if selectedForm?.field_id == fieldId{
                    
                    alreadyExist = i
                }
            }
            
            if alreadyExist == -1{
                
                // append new entry
                let formAnswerModel = FormAnswerModel.init(field_id: fieldId, is_mandatory:isMandatory, response: "", remarks: remark, dateValidation: "\(questionField.dateValidation ?? 0)", id: "\(questionField.id ?? 0)", is_remarks_required: "\(questionField.isRemarksRequired ?? false)", max_length: "\(questionField.maxLength ?? 0)", type: "\(questionField.type ?? 0)")
                
                if remark.trimmingCharacters(in: .whitespaces) != ""{
                    
                    inspectionFormAnswerArray?.append(formAnswerModel)
                }
                
            }
            else{
                // update existing entry
                let selectedForm = inspectionFormAnswerArray?[alreadyExist]

                let updatedformAnswerModel = FormAnswerModel.init(field_id: selectedForm?.field_id, is_mandatory:selectedForm?.is_mandatory, response: selectedForm?.response, remarks: remark.trimmingCharacters(in: .whitespaces), dateValidation: "\(questionField.dateValidation ?? 0)", id: "\(questionField.id ?? 0)", is_remarks_required: "\(questionField.isRemarksRequired ?? false)", max_length: "\(questionField.maxLength ?? 0)", type: "\(questionField.type ?? 0)")
                
                inspectionFormAnswerArray?.remove(at: alreadyExist)
                
                inspectionFormAnswerArray?.append(updatedformAnswerModel)
            }
        }
        
        // reload table view
        self.tblQuestionList.reloadData()
        
    }
}

