//
//  CompletedInspectionQuestionTypeCell.swift
//  COPD
//
//  Created by Anand Praksh on 01/06/21.
//  Copyright © 2021 Anand Praksh. All rights reserved.
//

import UIKit

class CompletedInspectionQuestionTypeCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    
    @IBOutlet weak var lblRemarks: UILabel!
    @IBOutlet weak var lblRemarksTopContraints: NSLayoutConstraint!
    @IBOutlet weak var lblRemarksBottomContraints: NSLayoutConstraint!
        
    // MARK: - Properties
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // initialization code        

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
