//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import CalendarDateRangePickerViewController

protocol SelectCCIDelegate {
    func selectCCI(institution: Institution)
}

class SelectCciInstitutionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblCCIInstitutions: UITableView!
    @IBOutlet weak var tfSearchInstitutions: UITextField!
    
    @IBOutlet weak var lblNoInstitutionFound: UILabel!
    
    @IBOutlet weak var btnGetAll: UIButton!
    
    // MARK: - Properties
    var delegate: SelectCCIDelegate?
    var passingHomeTypeId = 0
    var passingAdmissionTypeId = 0
    var passingYear = ""
    
    var dropdownOptionsArray = ["GET ALL", "TODAY", "YESTERDAY", "THIS WEEK", "CUSTOM"]
    var dropDown = DropDown()
    
    var institutionsArray : [Institution]? = []{
        
        didSet{
            self.lblNoInstitutionFound.isHidden = !(self.institutionsArray?.count == 0)
        }
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = .white
        
        // set back button
        self.setCustomBackBarButtonOnNavigationBar()
        
        // Set title image on navigation bar
        self.addLogoToNavigationBar()
        
        tfSearchInstitutions.delegate = self
        tfSearchInstitutions.clearButtonMode = .whileEditing
        
        // register custom table cell
        registerNib()
        
        if passingYear != ""{
            
            self.btnGetAll.setTitle(passingYear, for: .normal)
            
            // get cci institution list api call
            self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: "01/01/\(passingYear)", endDate: "31/12/\(passingYear)")
        }
        else{
            self.btnGetAll.setTitle("GET ALL", for: .normal)
            
            // get cci institution list api call
            self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: "", endDate: "")
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
        
        // show the navigation bar
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func registerNib(){
        
        let customNib = UINib(nibName: "SelectCCITableViewCell", bundle: Bundle.main)
        tblCCIInstitutions.register(customNib, forCellReuseIdentifier: "SelectCCITableViewCell")
        tblCCIInstitutions.separatorStyle = .none
        
    }
    
    
    @IBAction func getAllAction(_ sender: UIButton) {
        
        // display popup
        self.displayOptionSelectionPopup()
    }
    
    func displayOptionSelectionPopup(){
        DispatchQueue.main.async {
                        
            self.dropDown.direction = .bottom
            
            self.dropDown.dataSource = self.dropdownOptionsArray
            self.dropDown.anchorView = self.btnGetAll
            
            self.dropDown.textFont = UIFont.init(name: AppFonts.PoppinsRegular, size: 15.0)!
            self.dropDown.bottomOffset =  CGPoint(x: 0, y: (self.dropDown.anchorView?.plainView.bounds.height)!)
            
            self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                // Setup your custom label of cell components
                cell.optionLabel.textAlignment = .center
            }
            
            self.dropDown.selectionAction = { (index: Int, item: String) in
                
                self.btnGetAll.setTitle(item, for: .normal)
                
                if item == "GET ALL"{
                    
                    // make api call
                    self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: "", endDate: "")

                }
                else if item == "TODAY"{
                    
                    // make api call
                    self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: Date().currentDate, endDate: Date().currentDate)
                }
                else if item == "YESTERDAY"{
                    // make api call
                    self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: Date().yesterdayDate, endDate: Date().yesterdayDate)
                }
                else if item == "THIS WEEK"{
                    // make api call
                    self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: Date().startOfWeek!, endDate: Date().endOfWeek!)
                    
                }
                else if item == "CUSTOM"{
                    
                    //open calendar to choose start date and end date
                    
                    let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
                    dateRangePickerViewController.delegate = self

                    dateRangePickerViewController.selectedStartDate = Date()
                    dateRangePickerViewController.modalPresentationStyle = .overCurrentContext
                    
                    let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
                    self.navigationController?.present(navigationController, animated: true, completion: nil)
                }
                    
            }
            
            self.dropDown.show()
            
        }
    }
    
}


extension SelectCciInstitutionVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return institutionsArray?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCCITableViewCell", for: indexPath) as! SelectCCITableViewCell
        
        cell.imageInstitution.image = #imageLiteral(resourceName: "cci_placeholder").imageWithColor(color: #colorLiteral(red: 0.3176470588, green: 0.6784313725, blue: 0.8666666667, alpha: 1))
        
        var newAddressText = ""
        
        if indexPath.row < institutionsArray?.count ?? 0{
            
            let institition = institutionsArray?[indexPath.row]
            
            cell.titleOption.text = institition?.orgName
            cell.titleOption.textColor = UIColor.cciTitleColorTheme
            
            cell.subTitleOption.text = institition?.statusText
            if cell.subTitleOption.text == "Approved"{
                cell.subTitleOption.textColor = UIColor.cciStatusColorTheme
            }else{
                cell.subTitleOption.textColor = UIColor.orangeTheme
            }
            
            if institition?.talukName?.isEmpty == false && institition?.talukName != ""{
                
                newAddressText = newAddressText + "\(institition?.talukName ?? "")" + ", "
                
            }
            if institition?.districtName?.isEmpty == false && institition?.districtName != ""{
                
                newAddressText = newAddressText + "\(institition?.districtName ?? "")" + ", "
                
            }
            if institition?.stateName?.isEmpty == false && institition?.stateName != ""{
                
                newAddressText = newAddressText + "\(institition?.stateName ?? "")"
            }
        }
        
        cell.superSubTitleOption.text = newAddressText
                
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let institution = institutionsArray?[indexPath.row]
        
        if institution?.statusText == "Approved"{
            
            // passing value using delegate
            delegate?.selectCCI(institution: institution!)
            
            self.navigationController?.popViewController(animated: true)
        }else{
            ServiceModel().showAlert(title: "Alert", message: "Waiting for Final Approval & Approved Institutions Only Allowed to Start Inspection", parentView: self)
        }
        
    }
    
}


extension SelectCciInstitutionVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        var searchText = textField.text
        // search text
        searchText = searchText?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        // search from api
        self.fetchCCIInstitutionDataFilterBy(searchText: searchText ?? "", homeTypeId: self.passingHomeTypeId, startDate: "", endDate: "") // search
        
        return true
    }
    
}



extension SelectCciInstitutionVC{
    
    func fetchCCIInstitutionDataFilterBy(searchText: String, homeTypeId: Int, startDate: String, endDate: String){
        
        let servicemodel = ServiceModel()
        
        if (NetworkConnection.isConnectedToNetwork()){
            
            do {
                
                HUDIndicatorView.shared.showHUD(view: self.view,
                                                title: "Loading CCI ..")
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    servicemodel.getCCIListFilterBy(searchText: searchText, homeTypeId: homeTypeId, startDate: startDate, endDate: endDate, admissionHomeTypeId: self.passingAdmissionTypeId) { responseStr in
                        
                        DispatchQueue.main.async {
                            
                            self.institutionsArray?.removeAll()
                            
                            if responseStr == nil {
                                HUDIndicatorView.shared.dismissHUD()
                                //servicemodel.showAlert(title: "Server Error.", message: "Please try again!", parentView: self)
                                
                                return
                            }
                            
                            HUDIndicatorView.shared.dismissHUD()
                            
                            if let count = responseStr?.institutions?.count {
                                
                                if(count > 0){
                                    
                                    self.institutionsArray = responseStr?.institutions
                                    
                                }
                                else{
                                    ErrorToast.show(message: "No data found!", controller: self)
                                }
                                
                                self.tblCCIInstitutions.reloadData()

                            }
                            else{
                                
                                HUDIndicatorView.shared.dismissHUD()
                                
                                // handle Error or show error message to the user
                                servicemodel.showAlert(title: "Failed to load CCI list.", message: "", parentView: self)
                            }

                        }
                    }
                    
                }
            }
            
        }
    }
}

extension SelectCciInstitutionVC: CalendarDateRangePickerViewControllerDelegate{
    func didTapCancel() {
        
        // no action
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        
        self.dismiss(animated: true, completion: {
          
            let startDateNew = self.convertDateFormater("\(startDate!)") //05/10/2022
            let endDateNew = self.convertDateFormater("\(endDate!)") //05/10/2022
            
            // make API call
            
            self.fetchCCIInstitutionDataFilterBy(searchText: "", homeTypeId: self.passingHomeTypeId, startDate: startDateNew, endDate: endDateNew)
            
        })
        
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        
        if date.contains("-"){
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        }else{
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss z"
        }
        
        let date = dateFormatter.date(from: date)
        
        if date == nil{
            return "--"
        }
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return  dateFormatter.string(from: date!)

    }
    
}
