//
//  LoginModel.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation

// MARK: - CCIListModel
struct CCIListModel: Codable {
    let institutions: [Institution]?
}

// MARK: - Institution
struct Institution: Codable {
    let govHomeType, ngoHomeType: Int?
    let cncpHomeTypeText, ngoHomeTypeText, institutionName, registrationPlace, statusText: String?
    let totalChild: Int?
    let uid, districtName, stateName: String?
    let ciclHomeType: Int?
    let contactLastName: String?
    let id: Int?
    let orgName, fax, contactFirstName, email: String?
    let childAdmissionType, campusSqft: Int?
    let natureOfHomeText: String?
    let cncpHomeType: Int?
    let talukName: String?
    let natureOfHome, registrationAct: Int?
    let mobile: String?
    let totalStaff: Int?
    let registrationDate, childAdmissionTypeText, govHomeTypeText, ciclHomeTypeText: String?
    let status: Int?

    enum CodingKeys: String, CodingKey {
        case govHomeType = "gov_home_type"
        case ngoHomeType = "ngo_home_type"
        case cncpHomeTypeText = "cncp_home_type_text"
        case ngoHomeTypeText = "ngo_home_type_text"
        case institutionName = "institution_name"
        case registrationPlace = "registration_place"
        case totalChild = "total_child"
        case statusText = "status_text"
        case uid
        case districtName = "district_name"
        case stateName = "state_name"
        case ciclHomeType = "cicl_home_type"
        case contactLastName = "contact_last_name"
        case id
        case orgName = "org_name"
        case fax
        case contactFirstName = "contact_first_name"
        case email
        case childAdmissionType = "child_admission_type"
        case campusSqft = "campus_sqft"
        case natureOfHomeText = "nature_of_home_text"
        case cncpHomeType = "cncp_home_type"
        case talukName = "taluk_name"
        case natureOfHome = "nature_of_home"
        case registrationAct = "registration_act"
        case mobile
        case totalStaff = "total_staff"
        case registrationDate = "registration_date"
        case childAdmissionTypeText = "child_admission_type_text"
        case govHomeTypeText = "gov_home_type_text"
        case ciclHomeTypeText = "cicl_home_type_text"
        case status
    }
}


// MARK: - CalendarModel

struct CalendarModel: Codable {
    let calenders: [String]?
}



// MARK: - QuarterModel

struct QuarterModel: Codable {
    let quarters: [String]?
}


//// MARK: - CCIHomeTypeModel
//struct CCIHomeTypeModel: Codable {
//    let cciHomes: [CciHome]?
//}
//
//// MARK: - CciHome
//struct CciHome: Codable {
//    let name: String?
//    let id: Int?
//}

// MARK: - CCIHomeTypeModel
struct CCIHomeTypeModel: Codable {
    let types: [CciHome]
}

// MARK: - CciHome
struct CciHome: Codable {
    let name: String?
    let id: Int?
    let homeType: [HomeType]?

    enum CodingKeys: String, CodingKey {
        case name, id
        case homeType = "home_type"
    }
}

// MARK: - HomeType
struct HomeType: Codable {
    let name: String?
    let id: Int?
}


// MARK: - HomeType
struct HomeTypeModel: Codable {
    let admissionTypeId: Int?
    let homeTypeName: String
    let homeTypeId: Int?
}


// MARK: - InspectionFormModel
struct InspectionFormModel: Codable {
    let forms: [InspectionForm]?
}

// MARK: - InspectionForm
struct InspectionForm: Codable {
    let formID: Int?
    let name: String?
    let mobileIcon: String?
    let id: Int?
    let title: String?
    let fields: [InspectionField]?

    enum CodingKeys: String, CodingKey {
        case formID = "form_id"
        case name, id, title, fields
        case mobileIcon = "mobile_icon"
    }
}

// MARK: - InspectionField
struct InspectionField: Codable {
    let fieldID, dateValidation: Int?
    let isRemarksRequired: Bool?
    let response: String?
    let name, fieldDescription: String?
    let isMandatory, id, type: Int?
    let remarks: String?
    let maxLength, status: Int?
    let options: [String]?

    enum CodingKeys: String, CodingKey {
        case fieldID = "field_id"
        case dateValidation = "date_validation" //New
        case isRemarksRequired = "is_remarks_required"
        case response, name
        case fieldDescription = "description"
        case isMandatory = "is_mandatory" //New
        case maxLength = "max_length" //New
        case id, type, remarks, status, options
    }
}


// MARK: - InspectionModelResponse
struct InspectionModelResponse: Codable {
    let id, message: String?
    let status: Int?
}

struct PoAnswerModelData: Codable {
    let isUpdate: Bool?
    let id: Int?
    let message: String?
    let status: Int?
    let error: String?

    enum CodingKeys: String, CodingKey {
        case isUpdate = "is_update"
        case id, message, status, error
    }
}


//// MARK: - PoAnswerModelData
//struct PoAnswerModelData: Codable {
//    let id: Int?
//    let message: String?
//    let status: Int?
//}



// MARK: - InspectionStatus
struct InspectionStatus: Codable {
    let inspections: [Inspection]?
}

// MARK: - Inspection
struct Inspection: Codable {
    let cwcEmployee: CwcInspectionEmployee?
    let jjbEmployee: JjbInspectionEmployee?
    let alias: String?
    let statusText, dateOfVisit: String?
    let cci: HistoryCCI?
    let requestedAt: String?
    let requestedAtEpoch: Int?
    let dateOfVisitEpoch: Int?
    let completedAtEpoch: Int?

    enum CodingKeys: String, CodingKey {
        case cwcEmployee
        case jjbEmployee
        case alias
        case statusText = "status_text"
        case dateOfVisit = "date_of_visit"
        case cci
        case requestedAt = "requested_at"
        case completedAtEpoch = "completed_at_epoch"
        case requestedAtEpoch = "requested_at_epoch"
        case dateOfVisitEpoch = "date_of_visit_epoch"
    }
}


// MARK: - CwcEmployee
struct CwcInspectionEmployee: Codable {
    let phone: String?
    let name: String?
    let id: Int?
    let designationName: String?

    enum CodingKeys: String, CodingKey {
        case phone
        case name
        case id
        case designationName = "designation_name"
    }
}


// MARK: - JjbEmployee
struct JjbInspectionEmployee: Codable {
    let phone: String?
    let name: String?
    let id: Int?
    let designationName: String?

    enum CodingKeys: String, CodingKey {
        case phone
        case name
        case id
        case designationName = "designation_name"
    }
}


// MARK: - HistoryCCI
struct HistoryCCI: Codable {
    let institutionName: String?
    let orgName: String?
    let contactAddressLine1: String?
    let contactAddressLine2: String?
    let contactCity: String?
    let contactLandmark: String?
    let contactZipcode: String?
    let uid: String?
    let city: String?
    let addressLine2, addressLine1: String?
    let talukName: String?

    enum CodingKeys: String, CodingKey {
        case uid
        case orgName = "org_name"
        case institutionName = "institution_name"
        case contactAddressLine1 = "contact_address_line1"
        case contactAddressLine2 = "contact_address_line2"
        case contactCity = "contact_city"
        case contactLandmark = "contact_landmark"
        case contactZipcode = "contact_zipcode"
        case city
        case addressLine2 = "address_line2"
        case addressLine1 = "address_line1"
        case talukName = "taluk_name"
    }
}


// MARK: - CWCEmployeesModel
struct CWCEmployeesModel: Codable {
    let cwcEmployees: [CwcEmployee]?

    enum CodingKeys: String, CodingKey {
        case cwcEmployees = "cwc_employees"
    }
}

// MARK: - CwcEmployee
struct CwcEmployee: Codable {
    let phone, dob, serviceFromDate, maritalType: String?
    let name, dateOfJoining: String?
    let id: Int?
    let designation, trainingDate, serviceToDate, email: String?

    enum CodingKeys: String, CodingKey {
        case phone, dob
        case serviceFromDate = "service_from_date"
        case maritalType = "marital_type"
        case name
        case dateOfJoining = "date_of_joining"
        case id, designation
        case trainingDate = "training_date"
        case serviceToDate = "service_to_date"
        case email
    }
}


// MARK: - JJBEmployeesModel
struct JJBEmployeesModel: Codable {
    let employees: [Employee]?
}

// MARK: - Employee
struct Employee: Codable {
    let districtName, phone, dob, serviceFromDate: String?
    let maritalType, name, dateOfJoining: String?
    let id: Int?
    let designation, trainingDate, serviceToDate, email: String?

    enum CodingKeys: String, CodingKey {
        case districtName = "district_name"
        case phone, dob
        case serviceFromDate = "service_from_date"
        case maritalType = "marital_type"
        case name
        case dateOfJoining = "date_of_joining"
        case id, designation
        case trainingDate = "training_date"
        case serviceToDate = "service_to_date"
        case email
    }
}

