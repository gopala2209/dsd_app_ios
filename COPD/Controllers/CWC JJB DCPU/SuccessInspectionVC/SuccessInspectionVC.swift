//
//  RegisterVC.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit

class SuccessInspectionVC: UIViewController {
        
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //hide the navigation bar
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //set the status bar bg color
        ServiceModel().setStausBarColor(parentView: self.view, bgColor: UIColor.blueTheme)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    // MARK: - IBActions
    @IBAction func closeButtonAction(_ sender: UIButton) {
        
        // navigate to home screen
        self.navigationController?.popToViewController(ofClass: HomeVC.self)
    }
    
}


