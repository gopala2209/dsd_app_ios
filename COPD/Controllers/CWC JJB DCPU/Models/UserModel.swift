//
//  ProfileModel.swift
//  COPD
//
//  Created by Anand Prakash on 1/3/21.
//

import UIKit

struct ProfileModel: Decodable {
    
    let name: String?
    let usertype: String?
    let userimage: String?
}

