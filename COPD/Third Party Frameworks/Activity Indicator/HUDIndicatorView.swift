//
//  HUDIndicatorView.swift
//  JukeBaux
//
//  Created by Anonymous on 15/10/17.
//  Copyright © 2017 Anonymous. All rights reserved.
//

import Foundation
import JGProgressHUD
import UIKit

class HUDIndicatorView {
    
    // MARK: - Properties
    static let shared =  HUDIndicatorView()
    
    // MARK: - Properties
    private var hudInstance: JGProgressHUD?
    
    // MARK: - Public method
    func showHUD(view: UIView, title: String? = nil) {
        
        if let hudInstance = self.hudInstance {
            hudInstance.dismiss(animated: true)
        }
        
        hudInstance = JGProgressHUD(style: .dark)
        hudInstance?.contentView.backgroundColor = UIColor.borderBlueTheme
        
        if let title = title {
            hudInstance?.textLabel.text = title
        }

        DispatchQueue.main.async {
            self.hudInstance?.show(in: view, animated: false)
        }
    }
    
    func dismissHUD() {
        DispatchQueue.main.async {
            self.hudInstance?.dismiss(animated: false)
            self.hudInstance = nil
        }
    }
}
