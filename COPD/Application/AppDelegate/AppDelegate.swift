//
//  AppDelegate.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
           
        // Move textfield when keyboard appears
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        //to prevent screen lock
        application.isIdleTimerDisabled = true
        
        // By-pass login
        AppManager.shared.isFirstTimeLogin { (isFirstTimeLogin) in
            
            let loginVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "loginvc") as? LoginVC
            
            // PO
            if AppManager.shared.getUsertype().lowercased() == "po"{
                
                let homeVC_PO = Constant.POStoryBoard.instantiateViewController(withIdentifier: "homevcpo") as! HomeVC_PO
                
                var navVCrootVC: UIViewController = loginVC!
                            
                var navigation = Constant.UserStoryBoard.instantiateViewController(identifier: "UINavigationControllerForLogin") as! UINavigationController
                
                if isFirstTimeLogin{
                    
                    navVCrootVC = loginVC!

                }
                else{
                    
                    UserDefaults.standard.set(false, forKey: "isFirstLaunch")
                    UserDefaults.standard.synchronize()
                                        
                    navVCrootVC = homeVC_PO
                    
                    navigation = Constant.HomeStoryBoard.instantiateViewController(identifier: "UINavigationController") as! UINavigationController
                }
                
                //Disable left swipe from any screen
                navigation.interactivePopGestureRecognizer?.isEnabled = false
                
                navigation.setViewControllers([navVCrootVC], animated: false)
                
                self.window?.rootViewController = navigation
            }
            else{// CWC, JJB & DCPU
                
                let homeVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "homevc") as! HomeVC
                
                var navVCrootVC: UIViewController = loginVC!
                            
                var navigation = Constant.UserStoryBoard.instantiateViewController(identifier: "UINavigationControllerForLogin") as! UINavigationController
                
                if isFirstTimeLogin{
                    
                    navVCrootVC = loginVC!
                }
                else{
                    
                    UserDefaults.standard.set(false, forKey: "isFirstLaunch")
                    UserDefaults.standard.synchronize()
                    
                    navVCrootVC = homeVC
                    
                    navigation = Constant.HomeStoryBoard.instantiateViewController(identifier: "UINavigationController") as! UINavigationController
                }
                
                //Disable left swipe from any screen
                navigation.interactivePopGestureRecognizer?.isEnabled = false
                
                navigation.setViewControllers([navVCrootVC], animated: false)
                
                self.window?.rootViewController = navigation
            }
            
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }

    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

