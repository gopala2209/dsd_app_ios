//
//  SceneDelegate.swift
//  COPD
//
//  Created by Anand Prakash on 11/02/21.
//  Copyright © 2021 Anand Prakash. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
                
        /// 2. Create a new UIWindow using the windowScene constructor which takes in a window scene.
        window = UIWindow(windowScene: windowScene)
        
        // Move textfield when keyboard appears
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        AppManager.shared.isFirstTimeLogin { (isFirstTimeLogin) in
            
            let loginVC = Constant.UserStoryBoard.instantiateViewController(withIdentifier: "loginvc") as? LoginVC
            
            // PO
            if AppManager.shared.getUsertype().lowercased() == "po"{
                
                let homeVC_PO = Constant.POStoryBoard.instantiateViewController(withIdentifier: "homevcpo") as! HomeVC_PO
                
                var navVCrootVC: UIViewController = loginVC!
                            
                var navigation = Constant.UserStoryBoard.instantiateViewController(identifier: "UINavigationControllerForLogin") as! UINavigationController
                
                if isFirstTimeLogin{
                    
                    navVCrootVC = loginVC!
                }
                else{
                    navVCrootVC = homeVC_PO
                    
                    navigation = Constant.HomeStoryBoard.instantiateViewController(identifier: "UINavigationController") as! UINavigationController
                }
                
                //Disable left swipe from any screen
                navigation.interactivePopGestureRecognizer?.isEnabled = false
                
                navigation.setViewControllers([navVCrootVC], animated: false)
                
                self.window?.rootViewController = navigation
            }
            else{// CWC, JJB & DCPU
                
                let homeVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "homevc") as! HomeVC
                
                var navVCrootVC: UIViewController = loginVC!
                            
                var navigation = Constant.UserStoryBoard.instantiateViewController(identifier: "UINavigationControllerForLogin") as! UINavigationController
                
                if isFirstTimeLogin{
                    
                    navVCrootVC = loginVC!
                }
                else{
                    navVCrootVC = homeVC
                    
                    navigation = Constant.HomeStoryBoard.instantiateViewController(identifier: "UINavigationController") as! UINavigationController
                }
                
                //Disable left swipe from any screen
                navigation.interactivePopGestureRecognizer?.isEnabled = false
                
                navigation.setViewControllers([navVCrootVC], animated: false)
                
                self.window?.rootViewController = navigation
            }
            
            self.window?.makeKeyAndVisible()
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

