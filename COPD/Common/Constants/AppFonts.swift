//
//  AppFonts.swift
//  COPD
//
//  Created by Anand Prakash on 22/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation

/*---------------------------------------------------
 MARK:- App Custom Fonts
 ---------------------------------------------------*/

enum AppFonts {
    static let PoppinsBold = "Poppins-Bold"
    static let PoppinsLight = "Poppins-Light"
    static let PoppinsMedium = "Poppins-Medium"
    static let PoppinsRegular = "Poppins-Regular"
    static let PoppinsThin = "Poppins-Thin"
    static let PoppinsSemiBold = "Poppins-SemiBold"
    static let PoppinsBlack = "Poppins-Black"
    static let PoppinsBlackItalic = "Poppins-BlackItalic"
    static let PoppinsExtraBold = "Poppins-ExtraBold"
    static let PoppinsExtraLight = "Poppins-ExtraLight"
    static let PoppinsItalic = "Poppins-Italic"
    static let PoppinsLightItalic = "Poppins-LightItalic"
}
