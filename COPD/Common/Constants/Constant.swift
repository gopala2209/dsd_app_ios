//
//  HelperClass.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

/*---------------------------------------------------
 MARK:- App Constants
 ---------------------------------------------------*/

class Constant {
    static let APP_NAME = Bundle.main.infoDictionary?["CFBundleName"] as? String
    static let APP_TYPE = "ios"

    static let APP_CONNECTION_TYPE = NetworkReachabilityManager.getNetworkType().trackingId
    
    static let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    static let StoryBoard : UIStoryboard = UIStoryboard (name: "Main", bundle: Bundle.main)
    static let UserStoryBoard : UIStoryboard = UIStoryboard (name: "User", bundle: Bundle.main)
    static let HomeStoryBoard : UIStoryboard = UIStoryboard (name: "Home", bundle: Bundle.main)
    static let POStoryBoard : UIStoryboard = UIStoryboard (name: "Po", bundle: Bundle.main)
    
    // Web services
    static let Content_Type = "application/json"
    
    static let APP_BASE_URL = "https://devcpims.devops-in22labs.com/api/v1/"//LIVE
    static let X_API_KEY = "AUVJw4SxEV7y9xYzXuEp_2rFnQpfOgYyXdjdzNbs1UU"//LIVE
    
    static let APP_SHAREUS_URL = "http://play.google.com/store/apps/details?id=com.in22labs.socialdefense"
}
