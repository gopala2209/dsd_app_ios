//
//  AppError.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation

/*---------------------------------------------------
 MARK:- App General Errors
 ---------------------------------------------------*/

enum AppError: Error, LocalizedError {
    
    // MARK: - General Errors
    case loginFailed
    case wrapMessage(message: String)
    case wrap(error: Error)
    
    var errorDescription: String? {
        switch self {
        case .loginFailed:              return "Login failed.. Please try again."
        case let .wrapMessage(message): return message
        case let .wrap(error):          return error.localizedDescription
            
        default:                        return "Unknown Error"
        }
    }
    
    
    // MARK: - Login Screen Errors
    enum LoginScreenError: Error, LocalizedError {
        
        case usertypeFieldEmpty
        case usernameFieldEmpty
        case emailFieldEmpty
        case invalidEmail
        case passwordFieldEmpty
        case questionFieldEmpty
        case invalidPhoneNumber
        
        case otpFieldEmpty
        case newPasswordFieldEmpty
                
        var errorDescription: String? {
            switch self {
            case .usertypeFieldEmpty:        return "Please select your user type"
            case .usernameFieldEmpty:        return "Please enter the username"
            case .emailFieldEmpty:           return "Please enter the email"
            case .passwordFieldEmpty:        return "Please enter the password"
            case .questionFieldEmpty:        return "Please give your answer"
            case .invalidEmail:              return "Invalid Email"
            case .invalidPhoneNumber:        return "Invalid Phone number"
                
            case .otpFieldEmpty:             return "Please enter the OTP"
            case .newPasswordFieldEmpty:        return "Please enter the new password"

            }
        }
    }

}


