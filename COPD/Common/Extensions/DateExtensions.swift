//
//  DateExtensions.swift
//  MyAdhero
//
//  Created by Anand Prakash on 06/08/20.
//  Copyright © 2020 Anand Prakash. All rights reserved.
//

import Foundation

extension Date {

    var currentDate: String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    var currentYear: String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    var yesterdayDate: String {
        let date = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return dateFormatter.string(from: date!)
    }
    
    var currentDateWithDifferentFormat: String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    
    var getCurrentDateTime: String {
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm aa"

        return dateFormatter.string(from: date)
    }
    
    var getCurrentDateTimeWithDifferentFormat: String {
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss"

        return dateFormatter.string(from: date)
    }
    
    var sevenDaysBeforeFromCurrentDate: String {
        let date = Calendar.current.date(byAdding: .day, value: -7, to: self)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    var before30DayFromCurrentDate: String {
        let date = Calendar.current.date(byAdding: .day, value: -30, to: self)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    var after30DayFromCurrentDate: String {
        let date = Calendar.current.date(byAdding: .day, value: 30, to: self)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
  
    func generatePreviousSevenDays() -> [String] {
        var sevenDates = [String]()
        sevenDates.append(currentDate)
        for i in 1...6{
            let date = Calendar.current.date(byAdding: .day, value: -i, to: self)!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            sevenDates.append(dateFormatter.string(from: date))
        }
        return sevenDates
    }
    
    
    func generatePreviousThirtyDays() -> [String] {
        var thirtyDates = [String]()
        for i in 1...29{
            let date = Calendar.current.date(byAdding: .day, value: -i, to: self)!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            thirtyDates.append(dateFormatter.string(from: date))
        }
        return thirtyDates
    }
    
   static func formateDate(date: Date)-> String {
        let formattor = DateFormatter()
        formattor.dateFormat = "YYYY-MM-dd"
        let stringDate = formattor.string(from: date)
        return stringDate
    }
  
    static func displayDateFormate(_ dateString: String?)-> String? {
        guard let dateString = dateString else { return nil }
        let dateFormatter = DateFormatter()
        let dateFormates = ["dd/MM/yyyy","dd-MM-yyyy","YYYY-MM-dd","YYYY/MM/dd"]
        
        if dateString.count > 5 {
            let thirsCharacter = Array(dateString)[2]
            switch thirsCharacter {
            case "/":
                dateFormatter.dateFormat = dateFormates[0]
            case "-":
                dateFormatter.dateFormat = dateFormates[1]
            default:
                break
            }
            let forthCharacter = Array(dateString)[4]
            switch forthCharacter {
            case "-":
                dateFormatter.dateFormat = dateFormates[2]
            case "/":
                dateFormatter.dateFormat = dateFormates[3]
            default:
                break
            }
        }
        
        guard let dateFromString = dateFormatter.date(from: dateString) else { return nil}
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: dateFromString)
    }
    
    
    static func displayDateFormatForDayTab(_ dateString: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "yyyy-mm-dd" //dd MMMM yyyy
        return dateFormatter.string(from: date!)
        
    }
    
    
  static func convert24HoursToTimeInterval (_ stime: String?)-> Double {
        guard  var time = stime else { return 0 }
        time = time.replacingOccurrences(of: ":", with: ".")
        let timeAsDouble = (time as NSString).doubleValue
        return timeAsDouble * 3600
    }

    
    
   static func convertGiveDateToTimeIntervalSince1970(_ dateString: String?)-> Double? {
        guard let dateString = dateString else { return nil }
        let dateFormatter = DateFormatter()
        let dateFormates = ["dd/MM/yyyy","dd-MM-yyyy","YYYY-MM-dd","YYYY/MM/dd"]
        
        if dateString.count > 5 {
            let thirdCharacter = Array(dateString)[2]
            switch thirdCharacter {
            case "/":
                dateFormatter.dateFormat = dateFormates[0]
            case "-":
                dateFormatter.dateFormat = dateFormates[1]
            default:
                break
            }
            let forthCharacter = Array(dateString)[4]
            switch forthCharacter {
            case "-":
                dateFormatter.dateFormat = dateFormates[2]
            case "/":
                dateFormatter.dateFormat = dateFormates[3]
            default:
                break
            }
        }
        
        guard let date = dateFormatter.date(from: dateString) else { return nil}
        return date.timeIntervalSince1970
        
    }
    
    //First Date Of The Month
    static func getFirstDateOfTheMonth(index: Int) -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: Date()) as NSDateComponents
        
        if index == 0{
            components.month -= 1
        }
        else if index == 2{
            components.month += 1
        }
        
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    
    //Last Date Of The Month
    static func getLastDateOfTheMonth(index: Int) -> Date? {
        
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: Date()) as NSDateComponents
        
        if index == 1{
            
            components.month += 1
        }
        else if index == 2{
            
            components.month += 2
        }
        
        components.day = 1
        components.day -= 1
        
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    static func startDateOfWeek(index: Int) -> Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date())) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    static func endDateOfWeek(index: Int) -> Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date())) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
}
extension Date {
    static func getYears(forLastNYears nYears: Int) -> [String] {
        let cal = NSCalendar.current
        // start with today
        var date = cal.startOfDay(for: Date())
        
        var arrDates = [String]()
        
        arrDates.append(Date().currentYear) //add current year first
        
        for _ in 1 ... nYears {
                        
            // move back in time by one day:
            date = cal.date(byAdding: Calendar.Component.year, value: -1, to: date)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            let dateString = dateFormatter.string(from: date)
            arrDates.append(dateString)
        }
        
        return arrDates
    }
    
    static func getDates(forNextNDays nDays: Int) -> [String] {
        let cal = NSCalendar.current
        // start with today
        var date = cal.startOfDay(for: Date())
        
        var arrDates = [String]()
        
        for _ in 1 ... nDays {
            // move back in time by one day:
            date = cal.date(byAdding: Calendar.Component.day, value: 1, to: date)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
            arrDates.append(dateString)
        }
        print(arrDates)
        return arrDates
    }
    
    func getNextMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: 1, to: self)
    }
    
    func getPreviousMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -1, to: self)
    }
}


extension Date {
    
    var startOfWeek: String? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: gregorian.date(byAdding: .day, value: 1, to: sunday)!) // string purpose I add here
        
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy"
        
        // again convert your date to string
        let stringDate = formatter.string(from: yourDate!)
        
        return stringDate
    }
    
    var endOfWeek: String? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: gregorian.date(byAdding: .day, value: 7, to: sunday)!) // string purpose I add here
        
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy"
        
        // again convert your date to string
        let stringDate = formatter.string(from: yourDate!)
        
        return stringDate
    }
    
    mutating func changeDays(by days: Int) {
        self = Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
}
