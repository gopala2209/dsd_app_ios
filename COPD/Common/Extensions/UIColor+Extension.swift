//
//  UIColor+Extension.swift
//  COPD
//
//  Created by Anand Prakash on 1/2/21.
//

import UIKit

extension UIColor {
    
    static var orangeTheme: UIColor {
        return UIColor(red: 242.0 / 255.0, green: 137.0 / 255.0, blue: 60.0 / 255.0, alpha: 1.0)
    }
    
    static var blueTheme: UIColor {
        return UIColor(red: 2.0 / 255.0, green: 23.0 / 255.0, blue: 78.0 / 255.0, alpha: 1.0)
    }
    
    static var borderBlueTheme: UIColor {
        return UIColor(red: 45.0 / 255.0, green: 95.0 / 255.0, blue: 115.0 / 255.0, alpha: 1.0)
    }
    
    static var grayShadowTheme: UIColor {
        return UIColor(red: 128.0 / 255.0, green: 128.0 / 255.0, blue: 128.0 / 255.0, alpha: 1.0)
    }
    
    static var darkGrayShadowTheme: UIColor {
        return UIColor(red: 40.0 / 255.0, green: 40.0 / 255.0, blue: 40.0 / 255.0, alpha: 1.0)
    }
    
    static var redErrorTheme: UIColor {
        return UIColor(red: 207.0 / 255.0, green: 48.0 / 255.0, blue: 27.0 / 255.0, alpha: 1.0)
    }
    
    static var whiteTheme: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    
    static var greenTheme: UIColor {
        return UIColor(red: 74.0 / 255.0, green: 142.0 / 255.0, blue: 67.0 / 255.0, alpha: 1.0)
    }
    
    static var newInspectionTheme: UIColor {
        return UIColor(red: 253.0 / 255.0, green: 237.0 / 255.0, blue: 211.0 / 255.0, alpha: 1.0)
    }
    
    static var currentInspectionTheme: UIColor {
        return UIColor(red: 219.0 / 255.0, green: 236.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
    }
    
    static var completedInspectionAndInspectionHistoryTheme: UIColor {
        return UIColor(red: 232.0 / 255.0, green: 235.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0)
    }
    
    static var cciStatusColorTheme: UIColor {
        return UIColor(red: 18.0 / 255.0, green: 61.0 / 255.0, blue: 88.0 / 255.0, alpha: 1.0)
    }
    
    
    static var cciTitleColorTheme: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 153.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
    }
    
}
