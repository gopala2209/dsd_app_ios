//
//  UIImageViewExtension.swift
//  COPD
//
//  Created by Anand Prakash on 17/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import Foundation
import AlamofireImage
import UIKit

extension UIImage {
    
    func imageWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()

        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

extension UIImageView {
    
    func addRoundedCorner() {
        self.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    public func imageFromURL(urlString: String, ignoreCache: Bool) {
        
        
        if ignoreCache {
            self.af_setImageIgnoreCache(string: urlString)
        } else {
            
            if let url = URL(string: urlString) {
                self.af_setImage(withURL: url,
                                 placeholderImage: #imageLiteral(resourceName: "RoaryselectedIcon"),
                                 imageTransition: .crossDissolve(0.5))
            } else {
                self.image = #imageLiteral(resourceName: "RoaryselectedIcon")
            }
        }
    }
    
    func af_setImageIgnoreCache(string: String?) {
        guard let url = string, let nsurl = URL(string: url) else { return }
        let urlRequest = URLRequest(url: nsurl, cachePolicy: .reloadIgnoringLocalCacheData)
        
        let imageDownloader = ImageDownloader.default
        if let imageCache = imageDownloader.imageCache as? AutoPurgingImageCache, let urlCache = imageDownloader.sessionManager.session.configuration.urlCache {
            _ = imageCache.removeImages(matching: urlRequest)
            urlCache.removeCachedResponse(for: urlRequest)
        }
        
        af_setImage(withURLRequest: urlRequest,
                    placeholderImage: #imageLiteral(resourceName: "placeholder"),
                    imageTransition: .crossDissolve(0.5))
    }
}
