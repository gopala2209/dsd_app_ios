//
//  UIViewController+Extension.swift
//  COPD
//
//  Created by Anand Prakash on 1/4/21.
//

import UIKit
import SideMenu

extension UILabel {
  func addCharacterSpacing(kernValue: Double = 1.15) {
    if let labelText = text, labelText.count > 0 {
      let attributedString = NSMutableAttributedString(string: labelText)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
      attributedText = attributedString
    }
  }
}


extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}


extension UIViewController {
    
    func openMenu() {
        
        let navigation = SideMenuNavigationController(rootViewController: Constant.HomeStoryBoard.instantiateViewController(identifier: "\(MenuViewController.self)"))
        
        navigation.presentationStyle = .menuSlideIn
        navigation.leftSide = true
        navigation.menuWidth = UIScreen.main.bounds.width
        navigation.statusBarEndAlpha = 0
        
        SideMenuManager.default.leftMenuNavigationController = navigation
        SideMenuManager.default.addPanGestureToPresent(toView: view)
        
        present(SideMenuManager.default.leftMenuNavigationController ?? UINavigationController(), animated: true, completion: nil)
    }
    
    
    func setCustomBackBarButtonOnNavigationBar()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "back_icon")?.imageWithColor(color: UIColor.blueTheme), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(backButtonPressed), for: UIControl.Event.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    @objc func backButtonPressed()
    {
        // dismiss
        self.navigationController?.popViewController(animated: true)

    }
    
    
    func setCustomBackButtonToNavigateToHomeScreen()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "back_icon")?.imageWithColor(color: UIColor.blueTheme), for: UIControl.State())
        btnLeftMenu.addTarget(self, action: #selector(backButtonPressedToHome), for: UIControl.Event.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    @objc func backButtonPressedToHome()
    {
        // navigate to home screen
        self.navigateToHomeVC()
    }
    
    func navigateToHomeVC(){
        
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
              let delegate = windowScene.delegate as? SceneDelegate, let window = delegate.window else { return  }
        
        let navigation = Constant.HomeStoryBoard.instantiateViewController(identifier: "\(UINavigationController.self)") as! UINavigationController
        
        // PO
        if AppManager.shared.getUsertype().lowercased() == "po"{
            
            let homeVC_PO = Constant.POStoryBoard.instantiateViewController(withIdentifier: "homevcpo") as! HomeVC_PO
            
            navigation.setViewControllers([homeVC_PO], animated: false)
        }
        else{// CWC, JJB & DCPU
            
            let homeVC = Constant.HomeStoryBoard.instantiateViewController(withIdentifier: "homevc") as! HomeVC
            
            navigation.setViewControllers([homeVC], animated: false)
        }
        
        window.rootViewController = navigation
        
        window.makeKeyAndVisible()
        
    }
    
    
    func setNavigationTitleWithImage(_ title: String, andImage image: UIImage) {
        let titleLbl = UILabel()
        titleLbl.text = title
        titleLbl.textColor = .label
        titleLbl.font = UIFont.init(name: AppFonts.PoppinsBold, size: 16)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        let titleView = UIStackView(arrangedSubviews: [imageView, titleLbl])
        titleView.axis = .horizontal
        titleView.spacing = 10.0
        navigationItem.titleView = titleView
    }
    
    
    func addLogoToNavigationBar() {
            let imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.heightAnchor.constraint(equalToConstant: 42.0).isActive = true
            imageView.contentMode = .scaleAspectFit
            let image = UIImage(named: "navigation_logo.png") //Your logo image here
            imageView.image = image

            // In order to center the title view image no matter what buttons there are, do not set the
            // image view as title view, because it doesn't work. If there is only one button, the image
            // will not be aligned. Instead, a content view is set as title view, then the image view is
            // added as child of the content view. Finally, using constraints the image view is aligned
            // inside its parent.
        
            let contentView = UIView()
            self.navigationItem.titleView = contentView
            self.navigationItem.titleView?.addSubview(imageView)
            imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            //imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
            imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -1).isActive = true

        }
}


extension UserDefaults{
    
    var hasLaunchBefore: Bool {
    get {
      return self.bool(forKey: #function)
    }
    set {
      self.set(newValue, forKey: #function)
    }
}
}


@propertyWrapper
struct UserDefaultWrapper<T> {
    let key: String
    let defaultValue: T

    init(_ key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }

    var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}

struct UserDefaultsStore {
    @UserDefaultWrapper("has_launch_before", defaultValue: false)
    static var hasLaunchBefore: Bool
}
