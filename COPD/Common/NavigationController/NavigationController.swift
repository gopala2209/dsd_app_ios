//
//  NavigationController.swift
//  JukeBaux
//
//  Created by Anonymous on 27/09/17.
//  Copyright © 2017 Anonymous. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.isHidden = true
        interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func setRootViewController(vc: UIViewController) {
        self.viewControllers = [vc]
    }
}
