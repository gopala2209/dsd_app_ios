//
//  ServiceModel.swift
//  COPD
//
//  Created by Anand Prakash on 21/10/19.
//  Copyright © 2019 Anand Prakash. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import Reachability
import SystemConfiguration

class ServiceModel: NSObject {
    
    /*---------------------------------------------------
     MARK:- Users API calls
     ---------------------------------------------------*/
    
    //MARK: - User Login API
    
    func login(username:String, password:String, usertype:String,completion:@escaping (LoginModel?) -> ()) {
        
        let urlEncodedPassword = password.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"user/login?username=\(username)&password=\(urlEncodedPassword ?? "")") {
            
            print("API End Url : https://devcpims.devops-in22labs.com/api/v1/user/login")
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                print("API Response: \(response ?? nil)")
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making login api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Login Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(LoginModel.self, from: d)
                        completion(model)
                    }else{
                        completion(nil)
                        return
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
        
        
        
    }
    
    
    //MARK:-  Verify OTP
    
    func verifyOTPFromServer(otp:String, userId: Int, completion:@escaping (LoginModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"user/verifyotp?user_id=\(userId)&otp=\(otp)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making otp verification api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Verify OTP Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(LoginModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    //MARK:-  Forget Password
    
    func forgetPassword(username:String, completion:@escaping (LoginModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"user/resetpassword?username=\(username)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making forget password api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Forget Password Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(LoginModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    func forgetPasswordConfirmation(password:String, otp: String, userId: Int, completion:@escaping (LoginModel?) -> ()) {
        
        let urlEncodedPassword = password.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"user/resetconfirmation?password=\(urlEncodedPassword ?? "")&otp=\(otp)&user_id=\(userId)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when creating new password api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Create new Password Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(LoginModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    /*---------------------------------------------------
     MARK:- CWC / JJB / DCPU API calls
     ---------------------------------------------------*/
    
    func getPoCaseDetails(alias: String, completion:@escaping (CaseDetailPOModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"case/\(alias)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("CaseDetailPOModelData Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CaseDetailPOModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //REHABILITATION MODULE
    func getICPRehabilitationReportDetail(reportType: String, aliasId:String, completion:@escaping (RehabilitationModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get Rehabilitation icp request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Rehabilitation ICP Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(RehabilitationModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    //PRE RELEASE MODULE
    func getICPPreReleaseReportDetail(reportType: String, aliasId:String, completion:@escaping (PreReleaseModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get pre release icp request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Pre Release ICP Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(PreReleaseModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //POST RELEASE MODULE
    func getICPPostReleaseReportDetail(reportType: String, aliasId:String, completion:@escaping (PostReleaseModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get post release icp request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("POST Release ICP Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(PostReleaseModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    //MARK:-  GET Intake and ICP Report
    
    func getCompleteIntakeReportDetail(reportType: String, aliasId:String, completion:@escaping ([String:AnyObject]?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("IntakeDetailModelPoData Response:\(responseString!)")
                        }
                        let model  = self.convertStringToDictionary(text: responseString!)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
    func getIntakeReportDetail(reportType: String, aliasId:String, completion:@escaping (IntakeDetailModelPoData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("IntakeDetailModelPoData Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(IntakeDetailModelPoData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  GET SIR Report
    
    func getSIRReportDetail(reportType: String, aliasId:String, completion:@escaping (SIRDetailModelPoData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("SIRDetailModelPoData Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(SIRDetailModelPoData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    func getSupervisionPODetail(reportType: String, aliasId:String, completion:@escaping (SupervisionDetainPoModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"report/\(reportType)/\(aliasId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("SupervisionDetainPoModelData Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(SupervisionDetainPoModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    func getCCiGeneralInfo(id: String, completion:@escaping (CCIGeneralInfoModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"cci/\(id)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("General Info Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CCIGeneralInfoModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    func getAllIntakeReportPO(reportType: String, caseID:String, typeId: Int, completion:@escaping (IntakeReportListPoModelData?) -> ()) {
        
        var endPoint = "report/\(reportType)?case_id=\(caseID)&type=\(typeId)"
        
        if reportType == "icp"{
            endPoint = "report/\(reportType)?case_alias=\(caseID)"
        }
        
        if let url = NSURL(string: Constant.APP_BASE_URL + endPoint) {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get Intake Report request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("IntakeReportListPoModelData Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(IntakeReportListPoModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    func getPoListData(searchText:String, startDate: String, endDate: String, year: String, quarter: String, admissionHomeTypeId: String, completion:@escaping (PoCaseListModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"case?type=\(admissionHomeTypeId)&start_date=\(startDate)&end_date=\(endDate)&year=\(year)&quarter=\(quarter)&filter_type=1&crime_number=\(searchText)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("PoCaseListModelData Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(PoCaseListModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    func getCCIListFilterBy(searchText:String, homeTypeId:Int, startDate: String, endDate: String, admissionHomeTypeId: Int, completion:@escaping (CCIListModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"cci?search_text=\(searchText)&home_type=\(homeTypeId)&registration_start_date=\(startDate)&registration_end_date=\(endDate)&admission_type=\(admissionHomeTypeId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Calendar Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CCIListModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    
    
    //MARK:-  GET Calendar List
    
    func getCalendarList(completion:@escaping (CalendarModel?) -> ()) {
        if let url = NSURL(string: Constant.APP_BASE_URL+"calender") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get calendar list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Calendar Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CalendarModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  GET Quarter List
    
    func getQuarterList(completion:@escaping (QuarterModel?) -> ()) {
        if let url = NSURL(string: Constant.APP_BASE_URL+"quarter") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get quarter list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            //print("Quarter Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(QuarterModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    func getCountryRelatedDetail(postUrl: String, completion:@escaping (CountryListModelData?) -> ()) {
        if let url = NSURL(string: Constant.APP_BASE_URL+postUrl) {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get quarter list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            //print("Quarter Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CountryListModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    
    //MARK:-  GET Home Type List
    
    func getCCIHomeTypesList(completion:@escaping (CCIHomeTypeModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"master/cci/home/type")
        {
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get cci home types list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("CCI Home Types Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CCIHomeTypeModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  Start New Inspection
    
    func startNewIntakeReport(reportType: String, childId:String, caseId:String, completion:@escaping (InspectionModelResponse?) -> ()) {
        var postURl = "report/\(reportType)/start?case_id=\(caseId)&child_id=\(childId)"
        if reportType == "icp"{
            let currentDate = Date().string(format: "dd/MM/yyyy")
            postURl = postURl + "&date_of_icp=\(currentDate)"
        }
        if let url = NSURL(string: Constant.APP_BASE_URL + postURl) {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making Start inspection api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Start Inspection API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionModelResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    func startNewInspection(institutionUId:String, dateOfVisit:String, employeeId: Int, completion:@escaping (InspectionModelResponse?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"inspection/cci/\(institutionUId)/start?date_of_visit=\(dateOfVisit)&employee_id=\(employeeId)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making Start inspection api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Start Inspection API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionModelResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    func startNewInspectionDCPU(institutionUId:String, dateOfVisit:String, completion:@escaping (InspectionModelResponse?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"inspection/cci/\(institutionUId)/start?date_of_visit=\(dateOfVisit)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making Start inspection api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Start Inspection API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionModelResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    //MARK:- Upload Api
    func uploadPdfDocument(id: Int, file_string:String,type:Int, completion:@escaping (UploadPdfResponse?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"upload") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            let json = ["id": id, "file_string": file_string, "type": type] as [String : Any]
            let jsonData = try! JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            request.httpBody = jsonData
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when uploading document")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Upload document API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(UploadPdfResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  GET Inspection Form
    
    func getInspectionFormQuestionList(inspectionId:String, completion:@escaping (InspectionFormModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"inspection/\(inspectionId)/form") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get inspection form list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Inspection Form Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionFormModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  GET CWC Employees List
    
    func getCWCEmployeesList(completion:@escaping (CWCEmployeesModel?) -> ()) {
        if let url = NSURL(string: Constant.APP_BASE_URL+"cwc/employees") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get CWC employess list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("CWC Employess Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(CWCEmployeesModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    
    //MARK:-  GET JJB Employees List
    
    func getJJBEmployeesList(completion:@escaping (JJBEmployeesModel?) -> ()) {
        if let url = NSURL(string: Constant.APP_BASE_URL+"jjb/employees") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get JJB employess list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("JJB Employess Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(JJBEmployeesModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  Submit/ Save Individual Inspection
    
    func submitPoQuestionsToServer(httpMethod: String, postUrl:String, formAnswerModel: [String: Any], completion:@escaping (PoAnswerModelData?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL + postUrl) {
            
            print("API End Url : \(url)")
            
            let request = NSMutableURLRequest( url: url as URL)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: formAnswerModel, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            var count = 0
            for (_,_) in formAnswerModel{
                count = count + 1
            }
            if count > 0{
                request.httpBody = jsonData
            }
            request.httpMethod = httpMethod
            
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when submitting PO Report")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Submit PO Report API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(PoAnswerModelData.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    
    func submitIndividualInspectionForm(inspectionId:String, formId:Int, formAnswerModel: [[String: Any]], completion:@escaping (InspectionModelResponse?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"inspection/\(inspectionId)/form/\(formId)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            print(formAnswerModel, url)
            
            let jsonData = try! JSONSerialization.data(withJSONObject: formAnswerModel, options: JSONSerialization.WritingOptions.init(rawValue: 0))
            
            request.httpBody = jsonData
            request.httpMethod = "POST"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making Submit Individual Inspection api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Submit Individual Inspection API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionModelResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  Complete Inspection
    
    func CompleteInspectionForm(inspectionId:String, completion:@escaping (InspectionModelResponse?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"inspection/\(inspectionId)/complete") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making Complete inspection api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Complete Inspection API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionModelResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  GET Inspection Form
    
    func getAllInspection(institutionUId:String, status:String, startDate:String, endDate:String, completion:@escaping (InspectionStatus?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"inspection?status=\(status)&start_date=\(startDate)&end_date=\(endDate)&uid=\(institutionUId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making GetAllInspection form list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("GetAllInspection Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(InspectionStatus.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  GET Chat details
    
    func getChatDetailsForInstitutionId(institution: Institution, recordsPerPage: Int, currentPage: Int, completion:@escaping (ChatListModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"cci/\(institution.id ?? 0)/conversation?records_per_page=\(recordsPerPage)&current_page=\(currentPage)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get Chat list details api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Chat details API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(ChatListModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    //MARK:-  GET to update chat messages
    
    func updateChatList(institution: Institution, lastMessageId: Int, completion:@escaping (UpdateChatListModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"cci/\(institution.id ?? 0)/conversation/poll/\(lastMessageId)") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making update Chat list details api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Update Chat details API Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(UpdateChatListModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    //MARK:-  Post Messages
    
    func sendMessage(messageText:String, institution: Institution, completion:@escaping (SendMessageResponse?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"cci/\(institution.id ?? 0)/conversation?message=\(messageText)") {
            
            let request = NSMutableURLRequest( url: url as URL)
            
            request.httpMethod = "POST"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making send message api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("Send message Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(SendMessageResponse.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    //MARK:-  Set Status Bar Color
    
    func setStausBarColor(parentView:UIView, bgColor: UIColor){
        
        // Set the Status bar bg color for iOS 13
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            let statusbarView = UIView()
            statusbarView.backgroundColor = bgColor
            parentView.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: parentView.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: parentView.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: parentView.centerXAnchor).isActive = true
        } else {// For other iOS
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = bgColor
        }
    }
    
    
    /*---------------------------------------------------
     MARK:- PO API calls
     ---------------------------------------------------*/
    
    
    //MARK:-  GET Report List
    
    func getReportListFilterBy(searchText: String, selectedYear: String, selectedQuarter: String, reportType: String, completion:@escaping (SIRReportModel?) -> ()) {
        
        if let url = NSURL(string: Constant.APP_BASE_URL+"case/sirreport") {
            
            print(url)
            let request = NSMutableURLRequest( url: url as URL)
            request.httpMethod = "GET"
            
            AppManager.shared.getAppAuthenticationToken { (token) in
                
                request.setValue(Constant.X_API_KEY, forHTTPHeaderField: "X-API-KEY")
                request.setValue(token, forHTTPHeaderField: "Access-Token")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                do{
                    if let httpResponse = response as? HTTPURLResponse {
                        guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                            print("Received status code \(httpResponse.statusCode) when making get SIR report list api request")
                            completion(nil)
                            return
                        }
                    }
                    
                    if let d = data {
                        let responseString = String(data: d, encoding: .utf8)
                        if responseString != nil {
                            print("SIR report Response:\(responseString!)")
                        }
                        let model  = try JSONDecoder().decode(SIRReportModel.self, from: d)
                        completion(model)
                    }
                }
                
                catch{
                    print("error=\(error.localizedDescription)")
                    
                    completion(nil)
                    
                }
            }
            task.resume()
        }
    }
    
    
    
    
    
    
    /*---------------------------------------------------
     MARK:- Enable / Disable button methods
     ---------------------------------------------------*/
    
    //MARK:-  Enable & disable buttons
    
    func enableButton(button:UIButton) -> Void {
        
        button.isUserInteractionEnabled = true
        button.alpha = 1.0
    }
    
    func disableButton(button:UIButton) -> Void {
        button.isUserInteractionEnabled = false
        button.alpha = 0.5
    }
    
    
    /*---------------------------------------------------
     MARK:- Display Custom Alert
     ---------------------------------------------------*/
    
    //MARK:-  Custom ALert
    
    func showAlert(title: String, message: String, parentView:UIViewController) -> Void {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let destructiveAction  = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (result:UIAlertAction) -> Void in
        })
        alert.addAction(destructiveAction)
        parentView.present(alert, animated: true, completion: nil)
        
    }
    
    func handleError(error: Error, parentView:UIViewController) {
        
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: .default,
                                     handler: { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(okAction)
        
        parentView.present(alert, animated: true, completion: nil)
    }
}


/*---------------------------------------------------
 MARK:- Network Reachability
 ---------------------------------------------------*/

//MARK:-  checking the network reachability functionality

public class NetworkConnection {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
    }
}

extension String {
    func appendLineToURL(fileURL: URL) throws {
        try (self + "\n").appendToURL(fileURL: fileURL)
    }
    
    func appendToURL(fileURL: URL) throws {
        let data = self.data(using: String.Encoding.utf8)!
        try data.append(fileURL: fileURL)
    }
    
    public var isValidContact: Bool {
            let phoneNumberRegex = "^[6-9]\\d{9}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
            let isValidPhone = phoneTest.evaluate(with: self)
            return isValidPhone
        }
    
}

extension Data {
    func append(fileURL: URL) throws {
        if let fileHandle = FileHandle(forWritingAtPath: fileURL.path) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        }
        else {
            try write(to: fileURL, options: .atomic)
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func addBottomBorder(width: CGFloat){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height + 1, width: width, height: 1)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}


extension UIButton{
    
    func setImageTintColor(_ color: UIColor) {
        let tintedImage = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.setImage(tintedImage, for: .normal)
        self.tintColor = color
    }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}



struct UploadPdfResponse: Codable {
    let extensionType, name, location, message: String?
    let urlLocation: String?
    let uploadName: String?
    let status: Bool?
    
    enum CodingKeys: String, CodingKey {
        case extensionType = "extension"
        case name, location, message
        case urlLocation = "url_location"
        case uploadName = "upload_name"
        case status
    }
}
