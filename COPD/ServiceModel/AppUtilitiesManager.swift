//
//  AppUtilitiesManager.swift
//  COPD
//
//  Created by Anand Prakash on 1/2/21.
//

import Foundation
import SDWebImage

class AppUtilitiesManager {
    
    private init() { }
    
    static let shared = AppUtilitiesManager()
    
    enum Menus: String, CaseIterable {
        case shareus
        case rateus
        case privacypolicy
        case termsconditions
    }
    
    var selectedMenu = Menus.shareus.rawValue
    
    func showSDWebImageLoader(imageView: UIImageView, url: String, placeHolderImage: UIImage?, indicatorType: SDWebImageActivityIndicator) {
        imageView.sd_imageIndicator = indicatorType
        imageView.sd_imageIndicator?.startAnimatingIndicator()
        imageView.sd_setImage(with: URL(string: url), placeholderImage: placeHolderImage, options: .highPriority) { (image, error, type, url) in
            imageView.sd_imageIndicator?.stopAnimatingIndicator()
        }
    }
    
    func addShadowAroundView(_ aView: UIView, _ cornerRadius: CGFloat, _ shadowRadius: CGFloat, _ shadowOpacity: Float, _ shadowColor: UIColor) {
        aView.layer.shadowColor = shadowColor.cgColor
        aView.layer.shadowOffset = .zero
        aView.layer.shadowOpacity = shadowOpacity
        aView.layer.shadowRadius = shadowRadius
        aView.layer.cornerRadius = cornerRadius
    }
    
    func applyCornerRadius(_ aView: UIView, _ cornerRadius: CGFloat, _ borderWidth: CGFloat, _ borderColor: UIColor) {
        aView.clipsToBounds = true
        aView.layer.cornerRadius = cornerRadius
        aView.layer.borderWidth = borderWidth
        aView.layer.borderColor = borderColor.cgColor
    }
    
    func convertToUTC(dateStr: String?, requiredFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let date = formatter.date(from: dateStr ?? "") ?? Date()
        
        formatter.dateFormat = requiredFormat
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter.string(from: date)
    }
    
}
